var FRReport = {
    //容器类
    win: null,//报表页面的容器
    base: null,//报表的基本信息，包括标题，左右侧的设置
    column: null,//报表的列信息，包含列标题、名称、宽度、对齐、统计等
    search: null,//报表的搜索数组
    pdf: "",//报表的pdf页面完整的地址
    xml: "",//报表的xml页面完整的地址

    //常量类
    page: "/station/report/show",//报表页面的地址
    menu: "",//目录名，默认和Name一致
    id: "",//报表页面的id值,默认和Name一致，使用时，前面加上【FuRenReport_】

    //必输项
    grid: null,//grid，用于获取列信息和搜索信息
    name: null,//报表的唯一标示
    classname: null,//报表容器的className
    title: "报表",//报表的默认标题
    url: "reportpdf",//pdf的加载地址【必输项】
    'export': "report",//报表导出的页面地址【必输项】

    create: function () {
        var g = this;
        $.ajax({
            type: 'post',
            url: "/station/report/check",
            data: { name: g.name },
            cache: false,
            dataType: 'json',
            success: function (data) {
                var have = data.have;//jrxml文件是否存在
                var report = FRGetBase(g.name, g.title);//获取初始化数据
                var column = FRGetBaseColumn(g.grid.getColumns(), g.name);//获取初始化数据
                g.base = $.extend(report, data.base);//数据合并
                g.column = extendColumn(column, data.column);//数据合并
                //   jrxml文件不存在的情况
                if (have == 0) {
                    var config = { className: g.classname, report: g.base, column: g.column };
                    var jsondata = JSON.stringify(config);
                    $.ajax({
                        type: 'post',
                        url: "/station/report/saveReport",
                        contentType: "application/json",
                        data: jsondata,
                        dataType: "json",
                        success: function (d) {
                            FROpenWinFrom(g);
                        }
                    });
                } else {
                    //HRXML存在
                    FROpenWinFrom(g);
                }
            }
        });
    },
    
    
    update: function () {
        var g = this;
        $.ajax({
            type: 'post',
            url: "/station/report/check",
            data: { name: g.name },
            cache: false,
            dataType: 'json',
            success: function (data) {
                var have = data.have;//jrxml文件是否存在
                var report = FRGetBase(g.name, g.title);//获取初始化数据
                var column = FRGetBaseColumn(g.grid.getColumns(), g.name);//获取初始化数据
                g.base = $.extend(report, data.base);//数据合并
                g.column = extendColumn(column, data.column);//数据合并
                //   jrxml文件不存在的情况
                if (have == 0) {
                    var config = { className: g.classname, report: g.base, column: g.column };
                    var jsondata = JSON.stringify(config);
                    $.ajax({
                        type: 'post',
                        url: "/station/report/saveReport",
                        contentType: "application/json",
                        data: jsondata,
                        dataType: "json",
                        success: function (d) {
                            FROpenWinFrom(g);
                        }
                    });
                } else {
                    //HRXML存在
                    FROpenWinFrom(g);
                }
            }
        });
    },
    
    /*updateColumn: function () {
        var g = this;
        $.ajax({
            type: 'post',
            url: "/station/report/check",
            data: { name: g.name },
            cache: false,
            dataType: 'json',
            success: function (data) {
                var have = data.have;//jrxml文件是否存在
                var report = FRGetBase(g.name, g.title);//获取初始化数据
                var column = FRGetBaseColumn(g.grid.getColumns(), g.name);//获取初始化数据
                g.base = $.extend(report, data.base);//数据合并
                g.column = extendColumn(column, data.column);//数据合并
                //jrxml文件不存在的情况
                if (have == 0) {
                    var config = { className: g.classname, report: g.base, column: g.column };
                    var jsondata = JSON.stringify(config);
                    $.ajax({
                        type: 'post',
                        url: "/station/report/saveReport",
                        contentType: "application/json",
                        data: jsondata,
                        dataType: "json",
                        success: function (d) {
                            FROpenWinFrom(g);
                        }
                    });
                } else {
                    //HRXML存在
                    FROpenWinFrom(g);
                }
            }
        });
    },*/
    
    show: function () {
        var g = this;
        FROpenWinFrom(g);
    }
}

//主方法，创建此控件
function FuRenReport(data) {
    var ds = jQuery.extend(true, {}, FRReport);//复制
    var d = $.extend({}, ds, data);
    if (!d.menu || d.menu == "") d.menu = d.name;
    if (!d.id || d.id == "") d.id = d.name;
    return d;
}

//初始化报表基本数组
function FRGetBase(name, title) {
    var d = {
        name: name,
        title: title,
        titleSize: 16,
        titleBold: "bold",
        headLeft: "",
        headRight: "",
        headSize: 12,
        headBold: "normal"
    }
    return d;
}

//初始化报表列数组
function FRGetBaseColumn(columns, name) {
    var column = [];
    for (var i in columns) {
        if (columns[i].issystem || columns[i]._hide) {
            continue;
        }
        var row = {
            name: name,
            field: columns[i].columnname,
            title: columns[i].display,
            width: parseInt(columns[i]._width),
            align: "center",
            sum: "false"
        };
        column.push(row);
    }
    return column;
}

//报表数组的拼接
function extendColumn(p, d) {
    for (var i in p) {
        for (var j in d) {
            if (p[i].field == d[j].field) {
                p[i].title = d[j].title;
                p[i].width = d[j].width;
                p[i].align = d[j].align;
                p[i].sum = d[j].sum;
                
                //p[i].sum1 = d[j].isDispaly;
            }
        }
    }
    return p;
}

//打开报表页面
function FROpenWinFrom(g) {
    //拼接div的url
    var sd = FRDataNoNull(FRDataChange(g.grid.options.parms));
    var osd = g.search;
    var same = true;
    if (!osd) {
        same = false;
    } else {
        same = FRCheckSearch(sd, osd);
    }

    if (!same) {
        g.search = sd;
        var url = FRPingUrl(g.search);
        var d = new Date();
        g.pdf = "/station/" + g.menu + "/" + g.url + "?" + url + "&noCache=" + d.getTime();
        g.xml = "/station/" + g.menu + "/" + g["export"] + "?" + url;
    }
    
    doSetDailog(g);
    var divname = "FuRenReport_" + g.id;
    
    
    
    var a = $('<a class="link logout-link" href="/station/report/pdf?src='+ g.pdf+ '" target="_blank" seed="J_ruleLink-linkT2" smartracker="on">接入指引</a>').get(0);
    var e = document.createEvent('MouseEvents');
    e.initEvent( 'click', true, true );
    a.dispatchEvent(e);
    
    
    //<!-- 改修点3 begin  -->
//    window.open("/station/report/pdf?src=" + g.pdf,"","","true");
//    window.open("https://blog.csdn.net/shikenian520/article/details/49678947",divname,"","true");
//    var a = $('<a class="link logout-link" href="https://mrchportalweb.alipay.com/settling/selfhelp/accessGuide.htm" target="_blank" seed="J_ruleLink-linkT2" smartracker="on">接入指引</a>').get(0);
//    var e = document.createEvent('MouseEvents');
//    e.initEvent( 'click', true, true );
//    a.dispatchEvent(e);
    return;
    
    
    
    //打开窗口
    if (!g.win) {
        g.win = CreateWinFrom(g);
    } else {
        var divname = "#FuRenReport_" + g.id;
        $(jqname)[0].contentWindow.pdfload(g.pdf);
        g.win.show();
        
    }
  //<!-- 改修点3 end  -->
}


var dialog = null

function doSetDailog(g) {
    dialog =  {'xml':g.xml,'DataSearch':g.grid.options.parms,'report_base': g.base,'report_column': g.column,'report_pdf': g.pdf,'report_classname':g.classname,'report_name':g.name};
}

//创建报表页面
function CreateWinFrom(g) {
    var divname = "FuRenReport_" + g.id;
    var jqname = "#" + divname;
    var winreport = $.ligerDialog.open({
        height: parseInt(document.body.scrollHeight-120),  //报表预览页面的高度
        width: parseInt(document.body.scrollWidth - 30),  //报表预览页面的宽度
        //id:divname,
        url: "/station/report/show",                 //报表预览页面的访问地址
        title: '导出预览',                         //报表预览页面的标题
        name: divname,  //报表预览页面的id值
        isResize: true,
        slide: false,
        allowClose: true,
        isHidden: false,
        DataSearch: grid.options.parms, //本页面搜索条件
        report_base: g.base,
        report_column: g.column,
        report_pdf: g.pdf,
        report_classname: g.classname,
        report_name:g.name,

        buttons: [
           { text: '页面设置权限', onclick: function (item, dialog) { doLoginReport(); } },
           { text: '页面设置', onclick: function (item, dialog) {
                var setKey='xyz';
               if(setKey=="xyz"){
                   $(jqname)[0].contentWindow.attributeTable();
               }else{
                   $.ligerDialog.error("请登录获取“页面设置”权限！", "提示");
               }
               } },
           { text: '导出', onclick: function (item, dialog) { window.open(g.xml); } },
           { text: '关闭', onclick: function (item, dialog) { dialog.close(); } }
        ]
    });
    return winreport;
}

function doContentWindow(){
}


//数组转换
function FRDataChange(data) {
    var newdata = {};
    for (var d in data) {
        var sub={};
        sub[data[d].name]=data[d].value;
        newdata = $.extend(newdata, sub);
    }
    return newdata;
}

//数组的排空处理，{}
function FRDataNoNull(data) {
    var newdata = {};
    for (var d in data) {
        if (data[d] && data[d] != "")
            newdata[d] = data[d];
    }
    return newdata;
}

//检测搜索条件是否发生改变
function FRCheckSearch(nd, od) {
    if (nd.length == 0 && od == 0)
        return true;
    if (nd.length != od.length)
        return false;
    for (var n in nd) {
        if (!od[n] || nd[n] != od[n])
            return false;
    }
    return true;
}

//拼接pdf的url地址
function FRPingUrl(data) {
    var ps = [];
    for (var d in data) {
        ps.push(d + "=" + data[d]);
    }
    return ps.join("&");
}


/*window['k'] =
    grid = $("#maingrid2").ligerGrid({
        height: '99%',
        columns: [
         { display: '导出表名称', name: 'name', align: 'center', width: 100 },
         { display: '字段名称', name: 'field', align: 'center', width: 120 },
         { display: '字段标题', name: 'title', align: 'center' },
         { display: '宽度', name: 'width', align: 'center' },
         { display: '格式', name: 'align', width: 200 },
         { display: '是否累加', name: 'sum', align: 'center' },
         { display: '字段是否隐藏', name: 'is_dispaly', align: 'center' },
        ], url: 'search', root: 'data', record: 'count', checkbox: true, rownumbers: true
});*/