var FRReport = {
    //容器类
    pdf: null,//报表页面的容器
    xml: null,//报表页面的容器
    base: null,//报表的基本信息，包括标题，左右侧的设置
    column: null,//报表的列信息，包含列标题、名称、宽度、对齐、统计等
};

function doCreateReport(pname,ptitle,pcolumn,pclassname, psearch, psexport,pparam,purl,pmenu,obj) {

    if(pparam['fid']){
        FROpenWinFrom(pparam, psearch,purl,pmenu,psexport,obj)
    }else{
        $.ajax({
            type: 'post',
            url: "/hmdl/report/check",
            data: { name: pname },
            cache: false,
            dataType: 'json',
            success: function (data) {
                var have = data.have;//jrxml文件是否存在
                var report = FRGetBase(pname, ptitle);//获取初始化数据
                var column = FRGetBaseColumn(pcolumn, pname);//获取初始化数据
                var base = $.extend(report, data.base);//数据合并
                var newcolumn = extendColumn(column, data.column);//数据合并
                
                FRReport.base = base;
                FRReport.column = newcolumn;
                //   jrxml文件不存在的情况
                if (have == 0) {
                    var config = { className: pclassname, report: base, column: newcolumn };
                    var jsondata = JSON.stringify(config);
                    $.ajax({
                        type: 'post',
                        url: "/hmdl/report/saveReport",
                        contentType: "application/json",
                        data: jsondata,
                        dataType: "json",
                        success: function (d) {
                            //FROpenWinFrom(g);
                            FROpenWinFrom(pparam, psearch,purl,pmenu,psexport,obj)
                        }
                    });
                } else {
                    //HRXML存在
                    //FROpenWinFrom(g);
                    //FROpenWinFrom(param, search,url,menu,sexport)
                    FROpenWinFrom(pparam, psearch,purl,pmenu,psexport,obj)
                }
            }
        });
    }

}


//主方法，创建此控件
function FuRenReport(data) {
    var ds = jQuery.extend(true, {}, FRReport);//复制
    var d = $.extend({}, ds, data);
    if (!d.menu || d.menu == "") d.menu = d.name;
    if (!d.id || d.id == "") d.id = d.name;
    return d;
}

//初始化报表基本数组
function FRGetBase(name, title) {
    var d = {
        name: name,
        title: title,
        titleSize: 16,
        titleBold: "bold",
        headLeft: "",
        headRight: "",
        headSize: 12,
        headBold: "normal"
    }
    return d;
}

//初始化报表列数组
function FRGetBaseColumn(columns, name) {
    var column = [];
    for (var i in columns) {
        if (columns[i].issystem || columns[i]._hide) {
            continue;
        }
        var row = {
            name: name,
            field: columns[i].columnname,
            title: columns[i].display,
            width: parseInt(columns[i]._width),
            align: "center",
            sum: "false"
        };
        column.push(row);
    }
    return column;
}

//报表数组的拼接
function extendColumn(p, d) {
    for (var i in p) {
        for (var j in d) {
            if (p[i].field == d[j].field) {
                p[i].title = d[j].title;
                p[i].width = d[j].width;
                p[i].align = d[j].align;
                p[i].sum = d[j].sum;
                
                //p[i].sum1 = d[j].isDispaly;
            }
        }
    }
    return p;
}


//打开报表页面
function FROpenWinFrom(param, search,purl,menu,sexport,obj) {
  //拼接div的url
  var sd = FRDataNoNull(FRDataChange(param));
  var osd = search;
  var same = true;
  if (!osd) {
      same = false;
  } else {
      same = FRCheckSearch(sd, osd);
  }

  if (!same) {
      search = sd;
      if(param['fid']){
          var url = "fid="+param['fid']+"&ftime="+param['ftime'];
      }else{
          var url = FRPingUrl(search);
      }
      var d = new Date();
      var pdf = "/hmdl/" + menu + "/" + purl + "?" + url + "&noCache=" + d.getTime();
      var xml = "/hmdl/" + menu + "/" + sexport + "?" + url;
     
      pdf = pdf.replace(/\&/g,"||");//防止自动拦截&符号，先替换后转换回来
      
      FRReport.pdf = "../report/pdf?src=" + pdf;
      FRReport.xml = xml;
      obj.attr("src", FRReport.pdf);
  }
  
  
  
//  doSetDailog(g);
//  var divname = "FuRenReport_" + g.id;
//  
//  
//  
//  var a = $('<a href="/station/report/pdf?src='+ g.pdf+ '" target="_blank" seed="J_ruleLink-linkT2" smartracker="on">接入指引</a>').get(0);
//  var e = document.createEvent('MouseEvents');
//  e.initEvent( 'click', true, true );
//  a.dispatchEvent(e);
//  
//  
//  //<!-- 改修点3 begin  -->
////  window.open("/station/report/pdf?src=" + g.pdf,"","","true");
////  window.open("https://blog.csdn.net/shikenian520/article/details/49678947",divname,"","true");
////  var a = $('<a class="link logout-link" href="https://mrchportalweb.alipay.com/settling/selfhelp/accessGuide.htm" target="_blank" seed="J_ruleLink-linkT2" smartracker="on">接入指引</a>').get(0);
////  var e = document.createEvent('MouseEvents');
////  e.initEvent( 'click', true, true );
////  a.dispatchEvent(e);
//  return;
//  
//  
//  
//  //打开窗口
//  if (!g.win) {
//      g.win = CreateWinFrom(g);
//  } else {
//      var divname = "#FuRenReport_" + g.id;
//      $(jqname)[0].contentWindow.pdfload(g.pdf);
//      g.win.show();
//      
//  }
////<!-- 改修点3 end  -->
}
//
////打开报表页面
//function FROpenWinFrom(g) {
//    //拼接div的url
//    var sd = FRDataNoNull(FRDataChange(g.grid.options.parms));
//    var osd = g.search;
//    var same = true;
//    if (!osd) {
//        same = false;
//    } else {
//        same = FRCheckSearch(sd, osd);
//    }
//
//    if (!same) {
//        g.search = sd;
//        var url = FRPingUrl(g.search);
//        var d = new Date();
//        g.pdf = "/station/" + g.menu + "/" + g.url + "?" + url + "&noCache=" + d.getTime();
//        g.xml = "/station/" + g.menu + "/" + g["export"] + "?" + url;
//    }
//    
//    doSetDailog(g);
//    var divname = "FuRenReport_" + g.id;
//    
//    
//    
//    var a = $('<a href="/station/report/pdf?src='+ g.pdf+ '" target="_blank" seed="J_ruleLink-linkT2" smartracker="on">接入指引</a>').get(0);
//    var e = document.createEvent('MouseEvents');
//    e.initEvent( 'click', true, true );
//    a.dispatchEvent(e);
//    
//    
//    //<!-- 改修点3 begin  -->
////    window.open("/station/report/pdf?src=" + g.pdf,"","","true");
////    window.open("https://blog.csdn.net/shikenian520/article/details/49678947",divname,"","true");
////    var a = $('<a class="link logout-link" href="https://mrchportalweb.alipay.com/settling/selfhelp/accessGuide.htm" target="_blank" seed="J_ruleLink-linkT2" smartracker="on">接入指引</a>').get(0);
////    var e = document.createEvent('MouseEvents');
////    e.initEvent( 'click', true, true );
////    a.dispatchEvent(e);
//    return;
//    
//    
//    
//    //打开窗口
//    if (!g.win) {
//        g.win = CreateWinFrom(g);
//    } else {
//        var divname = "#FuRenReport_" + g.id;
//        $(jqname)[0].contentWindow.pdfload(g.pdf);
//        g.win.show();
//        
//    }
//  //<!-- 改修点3 end  -->
//}


var dialog = null

function doSetDailog(g) {
    dialog =  {'xml':g.xml,'DataSearch':g.grid.options.parms,'report_base': g.base,'report_column': g.column,'report_pdf': g.pdf,'report_classname':g.classname,'report_name':g.name};
}

//创建报表页面
function CreateWinFrom(g) {
    var divname = "FuRenReport_" + g.id;
    var jqname = "#" + divname;
    var winreport = $.ligerDialog.open({
        height: parseInt(document.body.scrollHeight-120),  //报表预览页面的高度
        width: parseInt(document.body.scrollWidth - 30),  //报表预览页面的宽度
        //id:divname,
        url: "/hmdl/report/show",                 //报表预览页面的访问地址
        title: '导出预览',                         //报表预览页面的标题
        name: divname,  //报表预览页面的id值
        isResize: true,
        slide: false,
        allowClose: true,
        isHidden: false,
        DataSearch: grid.options.parms, //本页面搜索条件
        report_base: g.base,
        report_column: g.column,
        report_pdf: g.pdf,
        report_classname: g.classname,
        report_name:g.name,

        buttons: [
//           { text: '页面设置权限', onclick: function (item, dialog) { doLoginReport(); } },
//           { text: '页面设置', onclick: function (item, dialog) {
//                var setKey='xyz';
//               if(setKey=="xyz"){
//                   $(jqname)[0].contentWindow.attributeTable();
//               }else{
//                   $.ligerDialog.error("请登录获取“页面设置”权限！", "提示");
//               }
//               } },
           { text: '导出', onclick: function (item, dialog) { window.open(g.xml); } },
           { text: '关闭', onclick: function (item, dialog) { dialog.close(); } }
        ]
    });
    return winreport;
}

function doContentWindow(){
}


//数组转换
function FRDataChange(data) {
    var newdata = {};
    for (var d in data) {
        var sub={};
        sub[data[d].name]=data[d].value;
        newdata = $.extend(newdata, sub);
    }
    return newdata;
}

//数组的排空处理，{}
function FRDataNoNull(data) {
    var newdata = {};
    for (var d in data) {
        if (data[d] && data[d] != "")
            newdata[d] = data[d];
    }
    return newdata;
}

//检测搜索条件是否发生改变
function FRCheckSearch(nd, od) {
    if (nd.length == 0 && od == 0)
        return true;
    if (nd.length != od.length)
        return false;
    for (var n in nd) {
        if (!od[n] || nd[n] != od[n])
            return false;
    }
    return true;
}

//拼接pdf的url地址
function FRPingUrl(data) {
    var ps = [];
    for (var d in data) {
        ps.push(d + "=" + data[d]);
    }
    return ps.join("&");
}


/*window['k'] =
    grid = $("#maingrid2").ligerGrid({
        height: '99%',
        columns: [
         { display: '导出表名称', name: 'name', align: 'center', width: 100 },
         { display: '字段名称', name: 'field', align: 'center', width: 120 },
         { display: '字段标题', name: 'title', align: 'center' },
         { display: '宽度', name: 'width', align: 'center' },
         { display: '格式', name: 'align', width: 200 },
         { display: '是否累加', name: 'sum', align: 'center' },
         { display: '字段是否隐藏', name: 'is_dispaly', align: 'center' },
        ], url: 'search', root: 'data', record: 'count', checkbox: true, rownumbers: true
});*/