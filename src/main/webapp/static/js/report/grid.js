

$.extend($.ligerDefaults.Grid, {
    rowHeight: 24,
    fixedCellHeight: false,
    frozen: false,
    async: true,
    headerRowHeight: 30,
    allowUnSelectRow: true
});
//去掉  大于小于包括,并改变顺序
$.ligerDefaults.Filter.operators['string'] =
    $.ligerDefaults.Filter.operators['text'] =
    ["like", "equal", "notequal", "startwith", "endwith"];

//扩展一个 数字输入 的编辑器
$.ligerDefaults.Grid.editors['numberbox'] = {
    create: function (container, editParm)
    {
        var column = editParm.column;
        var precision = column.editor.precision;
        var input = $("<input type='text' style='text-align:right' class='l-text' />");
        input.bind('keypress', function (e)
        {
            var keyCode = window.event ? e.keyCode : e.which;
            return keyCode >= 48 && keyCode <= 57 || keyCode == 46 || keyCode == 8;
        });
        input.bind('blur', function ()
        {
            var value = input.val();
            input.val(parseFloat(value).toFixed(precision));
        });
        container.append(input);
        return input;
    },
    getValue: function (input, editParm)
    {
        return parseFloat(input.val());
    },
    setValue: function (input, value, editParm)
    {
        var column = editParm.column;
        var precision = column.editor.precision;
        input.val(value.toFixed(precision));
    },
    resize: function (input, width, height, editParm)
    {
        input.width(width).height(21);
    }
}; 
$.ligerDefaults.Grid.editors['string'] =
$.ligerDefaults.Grid.editors['text'] = {
    create: function (container, editParm)
    {
        var input = $("<input type='text' style='border:1px solid #d3d3d3;'/>");
        container.append(input);
        return input;
    },
    getValue: function (input, editParm)
    {
        return input.val();
    },
    setValue: function (input, value, editParm)
    {
        input.val(value);
    },
    resize: function (input, width, height, editParm)
    {
        input.width(width).height(21);
    }
};
$.ligerDefaults.Grid.editors['select'] =
{
    create: function (container, editParm)
    {
        var column = editParm.column;
        var input = $("<select></select");
        container.append(input);
        var data = column.editor.data;
        if (!data) return input;
        $(data).each(function ()
        {
            input.append('<option value="' + this.value + '">' + (this.text || this.name) + '</option>');
        });
        return input;
    },
    getValue: function (input, editParm)
    {
        return input.val();
    },
    setValue: function (input, value, editParm)
    {
        if (value)
            input.val(value);
    },
    resize: function (input, width, height, editParm)
    {
        input.css({ width: width, height: 22 });
    }
};


var root = "../../";
var alignData = [{ value: 'left', text: '居左' }, { value: 'center', text: '居中' }, { value: 'right', text: '居右' }];
  




function outjson()
{
	var d = bulidData();
}

//获取 表单和表格 结构 所需要的数据
function bulidData()
{ 
    var griddata = [], searchdata= [], formdata= [];   
    for (var i = 0, l = grid.rows.length; i < l; i++)
    {
        var o = grid.rows[i];
        if (o.inlist)
            griddata.push({ display: o.display, name: o.name, width: o.listwidth });
        if (o.insearch)
            searchdata.push(getFieldData(o, true));
        if (o.inform)
            formdata.push(getFieldData(o));
    }
    return { grid: griddata, search: searchdata, form: formdata };

    function getFieldData(o, search)
    {
        if (o.type == "hidden") return { name: o.name, type: o.type };
        var field = {
            display: o.display,
            name: o.name,
            newline: o.newline,
            labelWidth: o.labelwidth || o.labelWidth,
            width: o.width,
            space: o.space,
            type: o.type
        }; 
        if (!search)
        {
            field.validate = getValidate(o);
            field.group = o.group;
            field.groupicon = "../icons/communication.gif";
        }
        else
        {
            field.cssClass = "field";
            field.newline = o.search_newline ? true : false;
        }
        return field;
    }
    function getValidate(o)
    {
        if (o.validate) return o.validate;
        if (!o.allownull) return { required: true };
        return null;
    }
}

//字段类型渲染器
function fieldTypeRender(r, i, value)
{
    for (var i = 0, l = alignData.length; i < l; i++)
    {
        var o = alignData[i];
        if (o.value == value) return o.text || o.name;
    }
    return "居中";
}
//是否类型的模拟复选框的渲染函数
function checkboxRender(rowdata, rowindex, value, column)
{
    var iconHtml = '<div class="chk-icon';
    if (value) iconHtml += " chk-icon-selected";
    iconHtml += '"';
    iconHtml += ' rowid = "' + rowdata['__id'] + '"';
    iconHtml += ' gridid = "' + this.id + '"';
    iconHtml += ' columnname = "' + column.name + '"';
    iconHtml += '></div>';
    return iconHtml;
}  


//是否类型的模拟复选框的点击事件

