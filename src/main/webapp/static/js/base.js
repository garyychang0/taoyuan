$(function() {
    // 设置jQuery Ajax全局的参数
    $.ajaxSetup({
        type : "POST",
        error : function(jqXHR, textStatus, errorThrown) {
            alert("系统故障！");
            top.location.href = "../login";
        }
    });
});

// 扩展函数 监听div size变化
(function($, h, c) {
    var a = $([]), e = $.resize = $.extend($.resize, {}), i, k = "setTimeout", j = "resize", d = j
            + "-special-event", b = "delay", f = "throttleWindow";
    e[b] = 250;
    e[f] = true;
    $.event.special[j] = {
        setup : function() {
            if (!e[f] && this[k]) {
                return false;
            }
            var l = $(this);
            a = a.add(l);
            $.data(this, d, {
                w : l.width(),
                h : l.height()
            });
            if (a.length === 1) {
                g();
            }
        },
        teardown : function() {
            if (!e[f] && this[k]) {
                return false;
            }
            var l = $(this);
            a = a.not(l);
            l.removeData(d);
            if (!a.length) {
                clearTimeout(i);
            }
        },
        add : function(l) {
            if (!e[f] && this[k]) {
                return false;
            }
            var n;
            function m(s, o, p) {
                var q = $(this), r = $.data(this, d);
                r.w = o !== c ? o : q.width();
                r.h = p !== c ? p : q.height();
                n.apply(this, arguments);
            }
            if ($.isFunction(l)) {
                n = l;
                return m;
            } else {
                n = l.handler;
                l.handler = m;
            }
        }
    };
    function g() {
        i = h[k](function() {
            a.each(function() {
                var n = $(this), m = n.width(), l = n.height(), o = $.data(
                        this, d);
                if (m !== o.w || l !== o.h) {
                    n.trigger(j, [ o.w = m, o.h = l ]);
                }
            });
            g();
        }, e[b]);
    }
})(jQuery, this);

function serializeObject(form) {
    var o = {};
    $.each(form.serializeArray(), function(index) {
        if (o[this['name']]) {
            o[this['name']] = o[this['name']] + "," + this['value'];
        } else {
            o[this['name']] = this['value'];
        }
    });
    return o;
}

function loadData(obj, name) {
    if (!name)
        name = "saveForm";
    name = "#" + name;
    var key, value, tagName, type, arr;
    for (x in obj) {
        key = x;
        value = obj[x];

        $(name + " [name='" + key + "']," + name + "[ name='" + key + "[]']")
                .each(function() {
                    tagName = $(this)[0].tagName;
                    type = $(this).attr('type');
                    if (tagName == 'INPUT') {
                        if (type == 'radio') {
                            $(this).attr('checked', $(this).val() == value);
                        } else if (type == 'checkbox') {
                            arr = value.split(',');
                            for (var i = 0; i < arr.length; i++) {
                                if ($(this).val() == arr[i]) {
                                    $(this).attr('checked', true);
                                    break;
                                }
                            }
                        } else {
                            $(this).val(value);
                        }
                    } else if (tagName == 'SELECT' || tagName == 'TEXTAREA') {
                        $(this).val(value);
                    }

                });
    }
}

function getData(obj, url) {
    $.get(url, function(data) {
        data = eval('(' + data + ')');
        obj.setData(data);
    });
}

function getCity(obj) {// 省市联动
    var thisid = obj.id;
    var p_sn = obj.value;
    var opobj = thisid.split("_")[0] + "_city";
    $("#" + opobj).empty();
    $("#" + opobj).append("<option value=''>===请选择===</option>");// 添加的时候可以选择
    $.get("getcity", {
        p_sn : p_sn
    }, function(data) {
        if (data != null && data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                $("#" + opobj).append(
                        "<option value='" + data[i].sn + "'>" + data[i].name
                                + "</option>");
            }
        }
    }, "json");
}
function getArea(obj) {// 省市联动
    var thisid = obj.id;
    var c_sn = obj.value;
    var opobj = thisid.split("_")[0] + "_area";
    $("#" + opobj).empty();
    $("#" + opobj).append("<option value=''>===请选择===</option>");// 添加的时候可以选择
    $.get("getarea", {
        c_sn : c_sn
    }, function(data) {
        if (data != null && data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                $("#" + opobj).append(
                        "<option value='" + data[i].sn + "'>" + data[i].name
                                + "</option>");
            }
        }
    }, "json");
}

function pad(num, n) {
    return (Array(n).join(0) + num).slice(-n);
}
// 查询变电所type 特殊执行方法
function substationSearch(type, selectid) {

    $.ajax({
        type : 'post',
        url : "../stationData/stationDataSearch",
        data : {
            isIp : 1,
            tempdata : Date.parse(new Date()),
            id : selectid,
            substationName : $("#search_substationName").val().trim()
        },
        cache : false,
        dataType : 'json',
        success : function(data) { // 如果没有数据，清空变电所树
            if (data == null || data == false) {
                orgTree.clear();
            } else { // 加载变电所树
                orgTree = $("#orgInfoTree").ligerTree(
                        {
                            nodeWidth : 220,
                            data : data,
                            checkbox : false,
                            idFieldName : 'id',
                            textFieldName : "text",
                            parentIDFieldName : "pid",
                            isExpand : 1,// 展开层数1层，区域
                            slide : true,// 是否显示动画
                            onSelect : function(node, e) {
                                var data = node.data;
                                /* 改变标题 */
                                if (type == 'changeTitle') {
                                    changeTitle(data.id);
                                } else {
                                    $(".l-layout-center .l-layout-header")
                                            .html(
                                                    '&emsp;<a style="color:#428bca">站点名称：['
                                                            + node.data.text
                                                            + ']</a>');
                                }
                                var sn = data.id;
                                if (sn != "") {
                                    // 获取变电站编号
                                    $("#org_sn").val(sn);
                                    // 获取变电站名称
                                    $("#org_name").val(data.text);
                                    grid.setParm("substationSn", sn);
                                    grid.setParm("substationName", data.text);
                                    grid.loadData();
                                    // 跳至第一页
                                    grid.changePage('first');
                                }
                            }
                        });
            }
        }
    });
}

//绑定全屏
$("#btn", parent.document).click(function() {
 window.location.reload();
});