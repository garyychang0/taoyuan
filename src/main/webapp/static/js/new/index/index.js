$(function () {
    //////////////////////////////////////////////////////////////
    var flag=0;
    //隐藏左侧栏
    function fun() {
        if(flag==0){
            hide();
            flag=1;
        }else {
            show();
            flag=0;
        }
    }
    var attr;//属性
    function hide() {
        //记住位置
        $(".index_app_hide").each(function (index, item) {
            if($(item).prop("class")=="layui-nav-item index_app_hide layui-nav-itemed"){
                attr = index;
            }
        });
        $(".layui-header").animate({left:"0px"},"fast","swing");
        $(".layui-side").animate({width:"60px"},"fast","swing");
        $(".layui-side").find("div").each(function (index, item) {
            $(item).animate({width:"60px"},"fast","swing");
        });
        $(".layui-side").find("ul").each(function (index, item) {
            $(item).animate({width:"60px"},"fast","swing");
        });
        $(".layui-body").animate({left:"60px"},"fast","swing");
        $(".layui-footer").animate({left:"60px"},"fast","swing");
        $(".layui-logo").animate({width:"60px"},"fast","swing");
        $(".layui-layout-left").animate({left:"60px"},"fast","swing");
        $(".layui-layout-right").animate({right:"0px"},"fast","swing");
        $("#LAY_app_logo").html("<i class='layui-icon layui-icon-tree' style='font-size: 30px'></i>");
        $(".index_nav_side_name cite").css("display","none");
        $(".index_nav_side_name span").css("display","none");
        $("#LAY_app_flexible").prop("class","layui-icon layui-icon-spread-left");
        $(".index_app_hide").prop("class","layui-nav-item index_app_hide");
        //记住位置
        $(".index_app_hide").each(function (index, item) {
            if(attr == index){
                $(item).prop("class","layui-nav-item index_app_hide layui-bg-red");
            }
        });
    }
    function show() {
        $(".layui-header").animate({left:"0px"},"fast","swing");
        $(".layui-side").animate({width:"200px"},"fast","swing");
        $(".layui-body").animate({left:"200px"},"fast","swing");
        $(".layui-footer").animate({left:"200px"},"fast","swing");
        $(".layui-logo").animate({width:"200px"},"fast","swing");
        $(".layui-layout-left").animate({left:"200px"},"fast","swing");
        $(".layui-layout-right").animate({right:"0px"},"fast","swing");
        $("#LAY_app_logo").html("综合后台办公管理系统");
        $(".index_nav_side_name cite").removeAttr("style");
        $("#LAY_app_flexible").prop("class","layui-icon layui-icon-shrink-right");
        //记住位置
        $(".index_app_hide").each(function (index, item) {
            if(attr == index){
                $(item).prop("class","layui-nav-item index_app_hide layui-nav-itemed");
            }
        });
    }
    function add(name) {

    }

    $("#LAY_app_hide").bind("click",function () {
        fun();
    });

    $(".index_app_hide").click("click",function () {
        show();
        flag=0;
    });

    /*layui.use(['layer'],function(){
        var index_side_tips = 0;
        $(".index_nav_side_name").mouseenter(function(){
            var that = this;
            if(flag==1){
                index_side_tips = layui.layer.tips($(that).children().eq(1).html(),that);
            }
        }).mouseleave(function(){
            layui.layer.close(index_side_tips);
        });
    })*/
})

//////////////////////////////////////////////////////////////


//
layui.use('element', function(){
    var element = layui.element;

});
layui.use('carousel', function(){
    var carousel = layui.carousel;
    //建造实例
    carousel.render({
        elem: '#test1'
        ,width: '100%' //设置容器宽度
        ,height: '350px'
        ,interval: '2000'
        ,arrow: 'always' //始终显示箭头
        //,anim: 'updown' //切换动画方式
    });
});

// 菜单页面
function show(url) {
    console.log(url);
    $.ajax({
        dataType :'html',
        async: false,
        type: 'post',
        cache: false,
        url: url,
        success: function (result) {
            $("#content").html(result);
        }
    });
}

// 登出操作
function loginOut() {
    layui.use('layer', function(){
        var layer = layui.layer;

        layer.confirm('确定退出？',
            {
                btn:['确定','取消'],
                btn1:function () {
                    // 注销后台session登录信息
                    $.ajax({
                        type: 'post',
                        cache: false,
                        url: "loginOut",
                        success: function(result) {
                            // 进行迁移到登录画面
                            location.href = decodeURIComponent("login");
                        }
                    });
                },btn2:function () {
                    layer.close();
                }
            }
        );
    });
}