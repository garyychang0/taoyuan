<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link id="link1" href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all1.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link id="link2" href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all1.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/My97DatePicker/WdatePicker.js"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerDrag.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerLayout.js" type="text/javascript"></script>
    <style>
	.l-grid-body l-grid-body2 l-scroll{background-color:#071F23}
	.l-dialog-tc {
	    background: -webkit-gradient(linear, 0 0, 0 100%, from(#FFFF66), to(#FFFF66));
	    height:45px;
	    border-bottom: 0px;
	    border-radius: 10px 10px 0px 0px;
	    border-collapse: separate;
	    border-spacing: 0;
	    cursor: move;
	}
	.l-dialog-cc {
		border-radius: 0px 0px 10px 10px;
	    border-collapse: separate;
	    border-spacing: 0;
	}
	.l-dialog-buttons{
		background: #FFFFCB;
		margin:0px;
		height:30px;
		border-radius: 0px 0px 10px 10px;
	    border-collapse: separate;
	    border-spacing: 0;
	}
	.l-dialog-content{
		background: #FFFFCB;
		border-top: 0px;
	}
	.l-dialog td{
		font-family:微软雅黑;
		border-top: 0px;
	}
	.l-grid td div
	{
	    font-size: 15px;
	    height: auto !important;
	}
	.l-dialog{
		border-radius: 10px;
	}
	.l-dialog td div{
		
	}
	.l-dialog-table{
		
	}
	.l-dialog-buttons-inner{
		text-align: center;
	}
	.l-dialog-btn-confirm {
	box-shadow: 0 1px 1px rgba(0,0,0,0.3);
	background: #337fa9;
	background: -moz-linear-gradient(top, #4994be, #337fa9);
	background: -webkit-gradient(linear, 0 0, 0 100%, from(#E1A38F), to(#E1A38F));
	background: -o-linear-gradient(top, #4994be, #337fa9);
	background: -ms-linear-gradient(top, #4994be 0%,#337fa9 100%);
	background: linear-gradient(top, #4994be, #337fa9);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#4994be', endColorstr='#337fa9');
	font-weight: bold;
	color: black;
	text-shadow: 0 2px 2px rgba(0,0,0,0.22);
	margin-left: -20px;
	}
	.l-dialog-btn-back {
	box-shadow: 0 1px 1px rgba(0,0,0,0.3);
	background: #337fa9;
	background: -moz-linear-gradient(top, #4994be, #337fa9);
	background: -webkit-gradient(linear, 0 0, 0 100%, from(#BDBDAB), to(#BDBDAB));
	background: -o-linear-gradient(top, #4994be, #337fa9);
	background: -ms-linear-gradient(top, #4994be 0%,#337fa9 100%);
	background: linear-gradient(top, #4994be, #337fa9);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#4994be', endColorstr='#337fa9');
	font-weight: bold;
	color: black;
	text-shadow: 0 2px 2px rgba(0,0,0,0.22);.
	margin-right: 50px;
	}
	 .l-dialog-btn-confirm:hover
	{
	    background: #E1A38F;
	    color: #555;
	}
	 .l-dialog-btn-back:hover
	{
	    background: #BDBDAB;
	    color: #555;
	}
	.l-dialog-tc{
		height: 38px;
	}
    .l-dialog-btn-highlight:hover
    {
        filter: progid:DXImageTransform.Microsoft.Gradient(enabled=false);
        background: #3286b4;
        color: #fff;
    }
    </style>
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">
<div id="layout1">
	<!-- 左侧树 -->
	<div position="left" title="变电所一览" style="height:calc(100% - 40px);overflow: auto; ">
		<!-- 变电所查询条件部分 -->
		<div id="org_search" style="margin-left:8px;margin-top:0px;">
			<ul>
				<li class="label">
					<input type="text" placeholder="输入名称进行搜索" style="text-indent:5px;height:23px;border-radius: .25em;width:60%;margin-left:0;margin-top: 5px" name="search_substationName" id="search_substationName"  />					
					<button type="button" id="org_btnSearch" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x"><i class="fa fa-search"></i></button>
					<button type="button" id="org_expend" onclick="expend()" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x"><i class="fa fa-plus"></i></button>
					<button type="button" id="org_unexpend" onclick="unexpend()" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x;display:none"><i class="fa fa-minus"></i></button>
				</li>
			</ul>
		</div><div id="orgInfoTree"></div><br/><br/>
		<input type="hidden" id="org_sn" name="org_sn" value=""/>
		<input type="hidden" id="org_name" name="org_name" value=""/>
	</div>
	<div position="center"  title="变电所" >
		<!-- 标题部分 -->
		<h3 id="title1" class="hmdl_title_h3">报警查询
		    <span class="hmdl_title_span">
		        &nbsp;/&nbsp;报警一览
		    </span>
		</h3>
		<hr id="hr1" class="hmdl_title_hr"/>
		<!-- 查询条件部分 -->
		<div id="grid_search" style="margin-left:8px;margin-top:20px;">
		    <form name="searchForm" id="searchForm" style="">
		    	<input type="hidden" id="substationSn" name="substationSn" value="${substationSn}"><!-- 隐藏域传值 -->
		        &emsp;时间：<input type="text" class="Wdate" name="callTime" id="callTime" placeholder="输入起始时间" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="text-indent:5px;width:150px;height:25px;border-radius: .25em;"/>
		        ~&nbsp;<input type="text" class="Wdate" name="alarmEndTime" id="alarmEndTime" placeholder="输入结束时间" onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"  style="text-indent:5px;width:150px;height:25px;border-radius: .25em;"/>
		        &emsp;设备名称：<input type="text" id="equName" name="equName" placeholder="输入设备名称"  style="text-indent:5px;width:150px;height:23px;border-radius: .25em;"/>
		        <select id="state" name="state" onchange="checkFast()" style="text-indent:5px;width:80px;height:28px;border-radius: .25em;">
		        	<option selected="selected" value="">选择状态</option>
		        	<option value="1">未确认</option>
		        	<option value="2">已确认</option>
		        	<option value="3">已处理</option>
		        </select>
		        &emsp;报警等级：
		        <select id="alarmLevel" name="alarmLevel" onchange="checkFast()" style="text-indent:5px;width:100px;height:28px;border-radius: .25em;">
		        	<option selected="selected" value="">选择报警等级</option>
		        	<option value="1">1-故障</option>
		        	<option value="2">2-报警</option>
		        	<option value="3">3-警告</option>
		        </select>
		       	&emsp;主题切换：
		        <select id="theme" onchange="changeTheme()" style="text-indent:5px;width:80px;height:28px;border-radius: .25em;">
		        	<option value="1" style="background-color: #013848;color: white;" selected="selected">青绿主题</option>
		        	<option value="2" style="background-color: #3C3C3C;color: white;">深灰主题</option>
		        	<option value="3" style="background-color: white;color: black;">亮白主题</option>
		        </select>
		        <div id="floatDiv" style="border-radius:7px 7px 7px 0px;border:1px solid gray;box-shadow:2px 2px 2px gray;margin:-73px 0px 0px 1018px;display: none;width: 120px;height: 40px;background-color:white;position:absolute;z-index: 500;">提示：主题切换将会重置查询条件！</div>
		        &emsp;<button id="btnSearch" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
		        &emsp;<button id="doReport" type="button" class="btn btn-success  btn-small btn-mini"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;导出&nbsp;&nbsp;</button>
		    </form>
		</div>
		<br id="br1"/>
		<!-- 列表部分 -->
		<div id="grid0" style="display: none;height:100%"></div>
		<div id="grid1" style="display: none"></div>
		<div id="grid2" style="display: none"></div>
		<div id="grid3" style="display: none"></div>
		<!-- 确认处理方式弹出窗口 -->
		<form id="confirmForm" name="confirmForm" style="border-radius: 8px;border-collapse: separate;border-spacing: 0;margin-top: 5px;display: none">
			<table align="center" border="0" cellpadding="0" width="100%" style="height: 100%;padding: 5;">
				<tr>
					<td width="40%" style="font-family: 黑体;font-weight: bold;font-size: 15px" align="center">报警类型：</td>
					<td><input readonly="readonly" style="width:180px;height:24px;border: 2px solid #DCDCDC;" id="open_alarm_type" type="text" name="alarmType"/></td>
				</tr>
				<tr>
					<td style="font-family: 黑体;font-weight: bold;font-size: 15px" align="center">报警时间：</td>
					<td><input readonly="readonly" style="width:180px;height:24px;border: 2px solid #DCDCDC;" id="open_alarm_time" type="text" name="callTime"/></td>
				</tr>
				<tr>
					<td style="font-family: 黑体;font-weight: bold;font-size: 15px" align="center">报警内容：</td>
					<td><input readonly="readonly" style="width:180px;height:24px;border: 2px solid #DCDCDC;" id="open_alarm_msg" type="text" name="message"/></td>
				</tr>
				<tr>
					<td style="font-family: 黑体;font-weight: bold;font-size: 15px"  align="center">处理报警方式：</td>
					<td>
						<span style="height: 25px;font-size: 15px;display: block;margin-top: 10px"><input type="radio" id="open_process_mode_one" name="methods" value="1"/> 报警信息确认</span>
						<span style="height:25px;font-size: 15px;display: block"><input type="radio" id="open_process_mode_two" name="methods" value="2"/> 电话沟通确认</span>
<!-- 						<span style="height: 25px;font-size: 15px;display: block"><input type="radio" id="open_process_mode_thr" name="methods" value="3"/> 短信通知确认</span> -->
					</td>
				</tr>
			</table>
		</form>
		<!-- 导出用Form -->
		<form action="../report/show" method="post" target="_blank" id="reportForm">
			<input type="hidden" name="fparam" id="fparam"></input>
			<input type="hidden" name="fcolumn" id="fcolumn"></input>
			<input type="hidden" name="fname" id="fname"></input>
			<input type="hidden" name="fclassname" id="fclassname"></input>
			<input type="hidden" name="fmenu" id="fmenu"></input>
			<input type="hidden" name="ftitle" id="ftitle"></input>
			<input type="hidden" name="fexport" id="fexport"></input>
			<input type="hidden" name="furl" id="furl"></input>
		</form>
	</div>
</div>

<script type="text/javascript">
var grid;
var authWin;
var authTree;
var menu;//右键菜单

$(function() {
    //布局设置
    $("#layout1").ligerLayout({
    leftWidth: 200,
    });
    //变电所树检索
    substationSearch();
    //初始化右键菜单
    menu = $.ligerMenu({ width: 120, items:
        [
        { text: '确认', click: itemclick, icon: 'edit' }
        ]
        });
    //初始化表格
    initGrid();
    // 绑定查询按钮
    $("#btnSearch").click(function () {
        $("#substationSn").val($("#org_sn").val());//左侧树传值SN
        var parms = serializeObject($("#searchForm"));
           for(var p in parms){
               grid.setParm(p,parms[p].trim());
           }
        grid.loadData();
    });

    // 绑定导出按钮
    $("#doReport").click(function() {
        // 用于获取列信息和搜索信息
        $('#fparam').val(JSON.stringify(grid.options.parms));
        $('#fcolumn').val(JSON.stringify(grid.getColumns()));
        // 报表的唯一标示
        $('#fname').val("alarmReport");
        // 报表容器的className
        $('#fclassname').val("fr.cn.com.edu.taoyuan.alarm.bean.AlarmBean");
        // 报表的默认标题
        $('#ftitle').val("报警信息报表");
        $('#fmenu').val("alarm"); 
        // pdf的加载地址【必输项】
        $('#furl').val("reportpdf"); //新版本reportnew.js无默认值，必须填写
        // 报表导出的页面地址【必输项】
        $('#fexport').val("report"); //新版本reportnew.js无默认值，必须填写
        // 导出
        $('#reportForm').submit();
    });
    //刷新报警列表
    //window.setInterval("initGrid()", 3000);
});
//绑定变电所查询按钮
$("#org_btnSearch").click(function () {
	substationSearch();
});

//绑定变电所收缩
$("#org_btn_collapse").click(function () {
	orgTree.collapseAll();
});

//绑定变电所收缩
$("#endAddr,#beginAddr").blur(function () {
    $("#lenth").val($("#endAddr").val()-$("#beginAddr").val());
});
//右键点击事件
function itemclick(item, i){
    confirm();
}
var value = 1;
//左侧树收缩触发事件
$(function(){
    $(".l-layout-header-toggle").click(function(){
      	//记住左侧选择的变电所
        if($("#org_sn").val() != ""){
            $("#substationSn").val($("#org_sn").val());
        }
        changeTheme();
        var parms = serializeObject($("#searchForm"));
        for(var p in parms){
            grid.setParm(p,parms[p].trim());
        }
     	grid.loadData();
    })
})
//收起/展开
function show(){
  	//记住左侧选择的变电所
    if($("#org_sn").val() != ""){
        $("#substationSn").val($("#org_sn").val());
    }
    if(value == 1){
        //显示和隐藏
        $("#title1").hide();
        $("#hr1").hide();
        $("#grid_search").hide();
        $("#br1").hide();
        //更改主题
        changeTheme();
        //记住检索条件
        var parms = serializeObject($("#searchForm"));
        for(var p in parms){
            grid.setParm(p,parms[p].trim());
        }
        //加载表格  
     	grid.loadData();
     	//更改显示和隐藏状态
        value = 2;
    }else{
        $("#title1").show();
        $("#hr1").show();
        $("#grid_search").show();
        $("#br1").show();
        changeTheme();
        var parms = serializeObject($("#searchForm"));
        for(var p in parms){
            grid.setParm(p,parms[p].trim());
        }
     	grid.loadData();
        value = 1;
    }
    
    
}
//初始化表格
function initGrid() {
    //隐藏
    $("#grid0").show();
    $("#grid1").hide();
    $("#grid2").hide();
    $("#grid3").hide();
    //引入CSS
    grid = $("#grid0").ligerGrid({
        title: '&nbsp;&nbsp;<span style="color:#386792">报警信息一览</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
        '<i class="fa fa-square" style="color:#FF0000"></i>--故障&nbsp;&nbsp;'+
        '<i class="fa fa-square" style="color:#FFC12E"></i>--报警&nbsp;&nbsp;'+
        '<i class="fa fa-square" style="color:#4EE561"></i>--警告'+
        '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
        '<button type="button" onclick="show()" class="btn btn-success" style="height:20px;width:75px;padding:0px">'+
        '<font style="font-size:12px">收起&nbsp;/&nbsp;展开</font></button>',
        headerImg: '/hmdl/static/images/list.png',
        columns: [
            {display: '序号',name: 'rownum',width: 60},
        	{display: '报警时间',name: 'callTime',width: 170,dateFormat:'yyyy-MM-dd HH:mm:ss',type:'DATE'},
        	{display: '名称',name: 'equName',width: 185},
        	{display: '报警类型',name: 'alarmType',width: 100,
    	    	render: function(row) {
                    var html = row.alarmType;
                    if (html == 1) {
                        return "HH-超高报警";
                    }else if (html == 2) {
                        return "H-高报警";
                    }else if (html == 3) {
                        return "L-低报警";
                    }else if (html == 4) {
                        return "LL-超低报警";
                    }else if (html == 5) {
                        return "0->1变位";
                    }else if (html == 6) {
                        return "1->0变位";
                    }else if (html == 7) {
                        return "离线";
                    }
            	}
        	},
        	{display: '报警值',name: 'alarmValue',width: 75,
        	    	render: function(row) {
	                    var html = row.alarmValue;
	                    if (html == "Z") {
	                        return "合闸";
	                    }else if (html == "F") {
	                        return "分闸";
	                    }else if (html == "L") {
	                        return "0";
	                    }else{
	                        return html;
	                    }
	                }
        	},
	        {display: '报警等级',name: 'alarmLevel',width: 90,
	                render: function(row) {
	                    var html = row.alarmLevel;
	                    if (html == "4") {
	                        return "离线";
	                    }else if (html == "3") {
	                        return "3-警告";
	                    }else if (html == "2") {
	                        return "2-报警";
	                    }else if (html == "1") {
	                        return "1-故障";
	                    }
	                }
        	},
            {display: '状态',name: 'state',width: 75,
            	render: function(row) {
	                    var html = row.alarmLevel;//获得报警级别
	                    var html0 = row.state;//获得状态
	                    if (html0 == "3") {
	                        return '已处理';
	                    }
	                    if (html0 == "2") {
	                        return '已确认';
	                    }
	                    if (html0 == "1") {
	                        return '未确认';
	                    }
            	}
        	},
        	{display: '处理方式',name: 'methods',width: 110,
	                render: function(row) {
	                    var html = row.methods;
	                    if (html == "3") {
	                        return "短信通知确认";
	                    }
	                    if (html == "2") {
	                        return "电话沟通确认";
	                    }
	                    if (html == "1") {
	                        return "报警信息确认";
	                    }
	                    if (html == "") {
	                        return "";
	                    }
	                }
        	},
        	{display: '信息',name: 'message',width: 150},
        	{display: '确认恢复时间',name: 'recTime',width: 170},
        	{display: '变电所名称',name: 'substationName',width: 180},
        	{display: '位置',name: 'local',width: 180},
        	{display: ''},
        ],
        dataAction: 'server',
        url: 'searchAlarm',
        root: 'data',
        parms:[{name:"substationSn",value:$("#substationSn").val()}],
        record: 'count',
        height: '97%',
        pageSize: 20,
        checkbox: false,
        rownumbers: false,
        alternatingRow: false,
        onRClickToSelect:true,
        onContextmenu : function (parm,e)
        {
         	// 右键也是选中行
            this.select(parm.row);
            menu.show({ top: e.pageY, left: e.pageX });
            return false;
        },
        rowAttrRender: function(rowdata) {
            if(rowdata.alarmType == "7"){
                return "style='color:#00ffde;'";//#f310c0,#10e6f3
            }
            if (rowdata.alarmLevel == "1") {
                if(rowdata.state == 3){
                    return "style='color:gray;'"; 
                }else if(rowdata.state == 2){
                    return "style='color:#DCDCDC;'"; 
                }
                return "style='color:#FF0000;'";
            }else if (rowdata.alarmLevel == "2") {
                if(rowdata.state == 3){
                    return "style='color:gray;'"; 
                }else if(rowdata.state == 2){
                    return "style='color:#DCDCDC;'"; 
                }
                return "style='color:#FFC12E;'";
            }else if (rowdata.alarmLevel == "3") {
                if(rowdata.state == 3){
                    return "style='color:gray;'"; 
                }else if(rowdata.state == 2){
                    return "style='color:#DCDCDC;'"; 
                }
                return "style='color:#4EE561;'";
            }
        },
        onDblClickRow: function (data,rowindex,rowobj){
            f_open();
        }
    });
}
//确认选中行信息
function confirm() {
    var rows = grid.getSelectedRows();
    var ids = "";
    if (rows == "" || rows.length != 1) {
        $.ligerDialog.warn("请选择一条记录！", "警告");
        return;
    }
    for (var i = 0; i < rows.length; i++) {
        if (i > 0) {
            ids += "|";
        }
        ids += rows[i].id + "," + rows[i].updateDate;
    }
    f_open();

}

//弹出窗口
function f_open() {
    var row = grid.getSelectedRow();
    //判断是否已确认，确认就变成已处理，已处理的进行页面刷新
    if (row.state == "2") {
        $.ligerDialog.confirm("确认信息？", "提示",
	            function(confirm) {
	                if (confirm) {
	                    $.ajax({
	                        type: 'post',
	                        url: 'changestate?id=' + row.id,
	                        dataType: "json",
	                        success: function(data) {
	                            if (data && data.retCode == 100) {
	                                var success = $.ligerDialog.success('操作成功！', "提示");
	                                setTimeout(function(){success.close();},500);
	                                setTimeout(function(){grid.loadData();},900);
	                            } else {
	                                var error = $.ligerDialog.error('操作失败！', "提示");
	                                setTimeout(function(){error.close();},600);
	                            }
	                        },
	                        error: function() {
	                            $.ligerDialog.error("操作错误！", "提示");
	                        }
	                    });
	                }
        });
    } else if (row.state == "3") {
        $.ligerDialog.error("您已确认，请勿重新操作！", "提示");
    } else {
        //初始化表单
        $("#open_equ_name").val(row.name);
        $("#open_alarm_position").val(row.local);
        $("#open_alarm_time").val(row.callTime);
        if(row.alarmType=="1"){
            $("#open_alarm_type").val("HH-超高报警");
        }else if(row.alarmType=="2"){
            $("#open_alarm_type").val("H-高报警");
        }else if(row.alarmType=="3"){
            $("#open_alarm_type").val("L-低报警");
        }else if(row.alarmType=="4"){
            $("#open_alarm_type").val("LL-超低报警");
        }else if(row.alarmType=="5"){
            $("#open_alarm_type").val("0->1变位");
        }else if(row.alarmType=="6"){
            $("#open_alarm_type").val("1->0变位");
        }
        $("#open_alarm_msg").val(row.message);
        var value = "";
        var name = '---  '+row.equName;
        var equName = row.equName==value?value:name;
        //弹出新窗口
        $.ligerDialog.open({
            target: $("#confirmForm"),
            height: 250,
            width: 450,
            title: "<img src='/hmdl/static/images/alarm.png' style='width:25px;height:25px;float:left;margin-bottom:5px;'>"
            +"<span style='float:left;width:auto;height:25px;font-size:15px;font-family:微软雅黑;margin-top:2px;'>&nbsp;"+row.local+"</span>"
            +"<div style='padding:0px;height:25px;margin:2px;float:left;white-space:nowrap;text-overflow:ellipsis;width:180px;overflow:hidden;border:0px solid #000000;'>"+equName+"</div>",
            showMax: false,
            showToggle: false,
            allowClose:false,
            showMin: false,
            isResize: false,
            slide: false,
            buttons: [{
                text: '确认',cls:'l-dialog-btn-confirm',
                onclick: function(item, dialog) {
                    var data = serializeObject($("#confirmForm"));
                    if(data.methods!=null){
                        $.ajax({
                            type: 'post',
                            url: 'methodsconfirm',
                            data: {
                                id: row.id,
                                methods: data.methods
                            },
                            dataType: "json",
                            success: function(data) {
                                if(data && data.retCode==100){
                                    $("#grid").show();
                                    $("#grid_search").show();
                                    $.ligerDialog.hide();
                                    var success = $.ligerDialog.success('操作成功！', "提示");
                                    setTimeout(function(){success.close();},500);
                                    setTimeout(function(){grid.reload();},900);
                                }else{
                                    var error = $.ligerDialog.error('操作失败！', "提示");
                                    setTimeout(function(){error.close();},600);
                                }
                            },
                            error: function(data) {
                                $.ligerDialog.error("操作错误！", "提示");
                            }
                        });
                    }else{
                        $.ligerDialog.warn("请先选择处理方式！", "提示");
                    }
                }
            },
            {
                text: '取消',cls:'l-dialog-btn-back',
                onclick: function(item, dialog) {
                    $("#grid").show();
                    $("#grid_search").show();
                    $.ligerDialog.hide();
                }
            }]
        });
    }

}

function resetAll() {
    // FORM清空
    $("#confirmForm")[0].reset();
    // 隐藏项目清空
    $("#open_equ_name").val("");
    $("#open_alarm_position").val("");
    $("#open_alarm_time").val("");
    $("#open_alarm_level").val("");
    $("#open_alarm_msg").val("");
    $("#open_process_mode_one").removeAttr("selected");
    $("#open_process_mode_one").val("");
    $("#open_process_mode_two").removeAttr("selected");
    $("#open_process_mode_two").val("");
    $("#open_process_mode_three").removeAttr("selected");
    $("#open_process_mode_three").val("");
}
//主体切换
function changeTheme(){
    var theme = $("#theme").val();
    if(theme==1){
        //隐藏
        $("#grid0").hide();
        $("#grid1").show();
        $("#grid2").hide();
        $("#grid3").hide();
        //引入CSS
        $("#link1").attr("href","/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all1.css"); 
        $("#link2").attr("href","/hmdl/static/js/ligerUI/skins/Gray2014/css/all1.css");
        //加载表格
            grid = $("#grid1").ligerGrid({
                title: '&nbsp;&nbsp;<span style="color:#386792">报警信息一览</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
                '<i class="fa fa-square" style="color:#FF0000"></i>--故障&nbsp;&nbsp;'+
                '<i class="fa fa-square" style="color:#FFC12E"></i>--报警&nbsp;&nbsp;'+
                '<i class="fa fa-square" style="color:#4EE561"></i>--警告'+
                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
                '<button type="button" onclick="show()" class="btn btn-success" style="height:20px;width:75px;padding:0px">'+
                '<font style="font-size:12px">收起&nbsp;/&nbsp;展开</font></button>',
                headerImg: '/hmdl/static/images/list.png',
                columns: [
					{display: '序号',name: 'rownum',width: 60},
					{display: '报警时间',name: 'callTime',width: 170,dateFormat:'yyyy-MM-dd HH:mm:ss',type:'DATE'},
					{display: '名称',name: 'equName',width: 185},
					{display: '报警类型',name: 'alarmType',width: 100,
						render: function(row) {
					    var html = row.alarmType;
					    console.log(html);
					    if (html == 1) {
					        return "HH-超高报警";
					    }else if (html == 2) {
					        return "H-高报警";
					    }else if (html == 3) {
					        return "L-低报警";
					    }else if (html == 4) {
					        return "LL-超低报警";
					    }else if (html == 5) {
					        return "0->1变位";
					    }else if (html == 6) {
					        return "1->0变位";
					    }
						}
					},
					{display: '报警值',name: 'alarmValue',width: 75,
					    	render: function(row) {
					            var html = row.alarmValue;
					            if (html == "1") {
					                return "合闸";
					            }else if (html == "0") {
					                return "分闸";
					            }else{
					                return html;
					            }
					        }
					},
					{display: '报警等级',name: 'alarmLevel',width: 90,
					        render: function(row) {
					            var html = row.alarmLevel;
					            if (html == "3") {
					                return "3-警告";
					            }
					            if (html == "2") {
					                return "2-报警";
					            }
					            if (html == "1") {
					                return "1-故障";
					            }
					        }
					},
					{display: '状态',name: 'state',width: 75,
						render: function(row) {
					            var html = row.alarmLevel;//获得报警级别
					            var html0 = row.state;//获得状态
					            if (html0 == "3") {
					                return '已处理';
					            }
					            if (html0 == "2") {
					                return '已确认';
					            }
					            if (html0 == "1") {
					                return '未确认';
					            }
						}
					},
					{display: '处理方式',name: 'methods',width: 110,
					        render: function(row) {
					            var html = row.methods;
					            if (html == "3") {
					                return "短信通知确认";
					            }
					            if (html == "2") {
					                return "电话沟通确认";
					            }
					            if (html == "1") {
					                return "报警信息确认";
					            }
					            if (html == "") {
					                return "";
					            }
					        }
					},
					{display: '信息',name: 'message',width: 150},
					{display: '确认恢复时间',name: 'recTime',width: 170},
					{display: '变电所名称',name: 'substationName',width: 180},
					{display: '位置',name: 'local',width: 180},
					{display: ''},
                ],
                dataAction: 'server',
                url: 'searchAlarm',
                root: 'data',
                parms:[
                       {name:"substationSn",value:$("#substationSn").val()},
                       {name:"callTime",value:$("#callTime").val()},
                       {name:"alarmEndTime",value:$("#alarmEndTime").val()},
                       {name:"state",value:$("#state").val()},
                       {name:"alarmLevel",value:$("#alarmLevel").val()}
                ],
                record: 'count',
                height: '97%',
                pageSize: 20,
                checkbox: false,
                rownumbers: false,
                alternatingRow: false,
                onRClickToSelect:true,
                onContextmenu : function (parm,e)
                {
                 	// 右键也是选中行
                    this.select(parm.row);
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                },
                
                rowAttrRender: function(rowdata) {
                    if (rowdata.alarmLevel == "1") {
                        if(rowdata.state == 3){
                            return "style='color:gray;'"; 
                        }
                        if(rowdata.state == 2){
                            return "style='color:#DCDCDC;'"; 
                        }
                        return "style='color:#FF0000;'";
                    }
                    if (rowdata.alarmLevel == "2") {
                        if(rowdata.state == 3){
                            return "style='color:gray;'"; 
                        }
                        if(rowdata.state == 2){
                            return "style='color:#DCDCDC;'"; 
                        }
                        return "style='color:#FFC12E;'";
                    }
                    if (rowdata.alarmLevel == "3") {
                        if(rowdata.state == 3){
                            return "style='color:gray;'"; 
                        }
                        if(rowdata.state == 2){
                            return "style='color:#DCDCDC;'"; 
                        }
                        return "style='color:#4EE561;'";
                    }
                },
                onDblClickRow: function (data,rowindex,rowobj){
                    f_open();
                }
            });
        
    }else if(theme==2){
       //隐藏
        $("#grid0").hide();
        $("#grid1").hide();
        $("#grid2").show();
        $("#grid3").hide();
        //引入CSS
        $("#link1").attr("href","/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all2.css"); 
        $("#link2").attr("href","/hmdl/static/js/ligerUI/skins/Gray2014/css/all2.css");
        //初始化表格
            grid = $("#grid2").ligerGrid({
                title: '&nbsp;&nbsp;<span style="color:#386792">报警信息一览</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
                '<i class="fa fa-square" style="color:#FF0000"></i>--故障&nbsp;&nbsp;'+
                '<i class="fa fa-square" style="color:#FFC12E"></i>--报警&nbsp;&nbsp;'+
                '<i class="fa fa-square" style="color:#4EE561"></i>--警告'+
                '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
                '<button type="button" onclick="show()" class="btn btn-success" style="height:20px;width:75px;padding:0px">'+
                '<font style="font-size:12px">收起&nbsp;/&nbsp;展开</font></button>',
                headerImg: '/hmdl/static/images/list.png',
                columns: [
				{display: '序号',name: 'rownum',width: 60},
				{display: '报警时间',name: 'callTime',width: 170,dateFormat:'yyyy-MM-dd HH:mm:ss',type:'DATE'},
				{display: '名称',name: 'equName',width: 185},
				{display: '报警类型',name: 'alarmType',width: 100,
					render: function(row) {
				    var html = row.alarmType;
				    console.log(html);
				    if (html == 1) {
				        return "HH-超高报警";
				    }else if (html == 2) {
				        return "H-高报警";
				    }else if (html == 3) {
				        return "L-低报警";
				    }else if (html == 4) {
				        return "LL-超低报警";
				    }else if (html == 5) {
				        return "0->1变位";
				    }else if (html == 6) {
				        return "1->0变位";
				    }
					}
				},
				{display: '报警值',name: 'alarmValue',width: 75,
				    	render: function(row) {
				            var html = row.alarmValue;
				            if (html == "1") {
				                return "合闸";
				            }else if (html == "0") {
				                return "分闸";
				            }else{
				                return html;
				            }
				        }
				},
				{display: '报警等级',name: 'alarmLevel',width: 90,
				        render: function(row) {
				            var html = row.alarmLevel;
				            if (html == "3") {
				                return "3-警告";
				            }
				            if (html == "2") {
				                return "2-报警";
				            }
				            if (html == "1") {
				                return "1-故障";
				            }
				        }
				},
				{display: '状态',name: 'state',width: 75,
					render: function(row) {
				            var html = row.alarmLevel;//获得报警级别
				            var html0 = row.state;//获得状态
				            if (html0 == "3") {
				                return '已处理';
				            }
				            if (html0 == "2") {
				                return '已确认';
				            }
				            if (html0 == "1") {
				                return '未确认';
				            }
					}
				},
				{display: '处理方式',name: 'methods',width: 110,
				        render: function(row) {
				            var html = row.methods;
				            if (html == "3") {
				                return "短信通知确认";
				            }
				            if (html == "2") {
				                return "电话沟通确认";
				            }
				            if (html == "1") {
				                return "报警信息确认";
				            }
				            if (html == "") {
				                return "";
				            }
				        }
				},
				{display: '信息',name: 'message',width: 150},
				{display: '确认恢复时间',name: 'recTime',width: 170},
				{display: '变电所名称',name: 'substationName',width: 180},
				{display: '位置',name: 'local',width: 180},
				{display: ''},
                ],
                dataAction: 'server',
                url: 'searchAlarm',
                root: 'data',
                parms:[
                       {name:"substationSn",value:$("#substationSn").val()},
                       {name:"callTime",value:$("#callTime").val()},
                       {name:"alarmEndTime",value:$("#alarmEndTime").val()},
                       {name:"state",value:$("#state").val()},
                       {name:"alarmLevel",value:$("#alarmLevel").val()}
                ],
                record: 'count',
                height: '97%',
                pageSize: 20,
                checkbox: false,
                rownumbers: false,
                alternatingRow: false,
                onRClickToSelect:true,
                onContextmenu : function (parm,e)
                {
                 	// 右键也是选中行
                    this.select(parm.row);
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                },
                
                rowAttrRender: function(rowdata) {
                    if (rowdata.alarmLevel == "1") {//故障
                        if(rowdata.state == 3){//已处理
                            return "style='background:#F5F5F5;'"; 
                        }
                        if(rowdata.state == 2){
                            return "style='background:#BABABA;'"; //已确认
                        }
                        return "style='background:#00CCFF;'";//未确认
                    }
                    if (rowdata.alarmLevel == "2") {//报警
                        if(rowdata.state == 3){
                            return "style='background:#F5F5F5;'"; 
                        }
                        if(rowdata.state == 2){
                            return "style='background:#BABABA;'"; 
                        }
                        return "style='background:#FF6600;'";
                    }
                    if (rowdata.alarmLevel == "3") {//警告
                        if(rowdata.state == 3){
                            return "style='background:#F5F5F5;'"; 
                        }
                        if(rowdata.state == 2){
                            return "style='background:#BABABA;'"; 
                        }
                        return "style='background:#FF0000;'";
                    }
                },
                onDblClickRow: function (data,rowindex,rowobj){
                    f_open();
                }
            });
        
    }else if(theme==3){
        //隐藏
        $("#grid0").hide();
        $("#grid1").hide();
        $("#grid2").hide();
        $("#grid3").show();
        //引入CSS
        $("#link1").attr("href","/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all3.css"); 
        $("#link2").attr("href","/hmdl/static/js/ligerUI/skins/Gray2014/css/all3.css");
        //初始化表格
        grid = $("#grid3").ligerGrid({
            title: '&nbsp;&nbsp;<span style="color:#386792">报警信息一览</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
            '<i class="fa fa-square" style="color:#FF0000"></i>--故障&nbsp;&nbsp;'+
            '<i class="fa fa-square" style="color:#FFC12E"></i>--报警&nbsp;&nbsp;'+
            '<i class="fa fa-square" style="color:#4EE561"></i>--警告'+
            '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
            '<button type="button" onclick="show()" class="btn btn-success" style="height:20px;width:75px;padding:0px">'+
            '<font style="font-size:12px">收起&nbsp;/&nbsp;展开</font></button>',
            headerImg: '/hmdl/static/images/list.png',
            columns: [
			{display: '序号',name: 'rownum',width: 60},
			{display: '报警时间',name: 'callTime',width: 170,dateFormat:'yyyy-MM-dd HH:mm:ss',type:'DATE'},
			{display: '名称',name: 'equName',width: 185},
			{display: '报警类型',name: 'alarmType',width: 100,
				render: function(row) {
			    var html = row.alarmType;
			    console.log(html);
			    if (html == 1) {
			        return "HH-超高报警";
			    }else if (html == 2) {
			        return "H-高报警";
			    }else if (html == 3) {
			        return "L-低报警";
			    }else if (html == 4) {
			        return "LL-超低报警";
			    }else if (html == 5) {
			        return "0->1变位";
			    }else if (html == 6) {
			        return "1->0变位";
			    }
				}
			},
			{display: '报警值',name: 'alarmValue',width: 75,
			    	render: function(row) {
			            var html = row.alarmValue;
			            if (html == "1") {
			                return "合闸";
			            }else if (html == "0") {
			                return "分闸";
			            }else{
			                return html;
			            }
			        }
			},
			{display: '报警等级',name: 'alarmLevel',width: 90,
			        render: function(row) {
			            var html = row.alarmLevel;
			            if (html == "3") {
			                return "3-警告";
			            }
			            if (html == "2") {
			                return "2-报警";
			            }
			            if (html == "1") {
			                return "1-故障";
			            }
			        }
			},
			{display: '状态',name: 'state',width: 75,
				render: function(row) {
			            var html = row.alarmLevel;//获得报警级别
			            var html0 = row.state;//获得状态
			            if (html0 == "3") {
			                return '已处理';
			            }
			            if (html0 == "2") {
			                return '已确认';
			            }
			            if (html0 == "1") {
			                return '未确认';
			            }
				}
			},
			{display: '处理方式',name: 'methods',width: 110,
			        render: function(row) {
			            var html = row.methods;
			            if (html == "3") {
			                return "短信通知确认";
			            }
			            if (html == "2") {
			                return "电话沟通确认";
			            }
			            if (html == "1") {
			                return "报警信息确认";
			            }
			            if (html == "") {
			                return "";
			            }
			        }
			},
			{display: '信息',name: 'message',width: 150},
			{display: '确认恢复时间',name: 'recTime',width: 170},
			{display: '变电所名称',name: 'substationName',width: 180},
			{display: '位置',name: 'local',width: 180},
			{display: ''},
            ],
            dataAction: 'server',
            url: 'searchAlarm',
            root: 'data',
            parms:[
                   {name:"substationSn",value:$("#substationSn").val()},
                   {name:"callTime",value:$("#callTime").val()},
                   {name:"alarmEndTime",value:$("#alarmEndTime").val()},
                   {name:"state",value:$("#state").val()},
                   {name:"alarmLevel",value:$("#alarmLevel").val()}
            ],
            record: 'count',
            height: '97%',
            pageSize: 20,
            checkbox: false,
            rownumbers: false,
            alternatingRow: false,
            onRClickToSelect:true,
            onContextmenu : function (parm,e)
            {
                // 右键也是选中行
                this.select(parm.row);
                menu.show({ top: e.pageY, left: e.pageX });
                return false;
            },
            
            rowAttrRender: function(rowdata) {
                if (rowdata.alarmLevel == "1") {//故障
                    if(rowdata.state == 3){//已处理
                        return "style='background:white;'"; 
                    }
                    if(rowdata.state == 2){
                        return "style='background:#BABABA;'"; //已确认
                    }
                    return "style='background:#1AA3FF;'";//未确认
                }
                if (rowdata.alarmLevel == "2") {//报警
                    if(rowdata.state == 3){
                        return "style='background:white;'"; 
                    }
                    if(rowdata.state == 2){
                        return "style='background:#BABABA;'"; 
                    }
                    return "style='background:#FF7C24;'";
                }
                if (rowdata.alarmLevel == "3") {//警告
                    if(rowdata.state == 3){
                        return "style='background:white;'"; 
                    }
                    if(rowdata.state == 2){
                        return "style='background:#BABABA;'"; 
                    }
                    return "style='background:#FF2E2E;'";
                }
            },
            onDblClickRow: function (data,rowindex,rowobj){
                f_open();
            }
        });
    }
}
//回车键搜索
$("input,select").keydown(function (e) {//当按下按键时
    if (e.which == 13) {//.which属性判断按下的是哪个键，回车键的键位序号为13
        var parms = serializeObject($("#searchForm"));
		for(var p in parms){
	        grid.setParm(p,parms[p].trim());
	    }
	    grid.loadData();
    }
});
// //提示
// $(function(){
//     var ii = 1;
//     $("#theme").click(function(){
//         if(ii%2!=0){ //首次出现提示
//             $("#floatDiv").fadeIn();
//         }
//         ii++;
//         setTimeout(function(){$("#floatDiv").fadeOut();},3000);
//     })
// })
//快速检索
function checkFast(){
    var parms = serializeObject($("#searchForm"));
    for(var p in parms){
        grid.setParm(p,parms[p].trim());
    }
 	grid.loadData();
}
</script>
</body>  
</html>

