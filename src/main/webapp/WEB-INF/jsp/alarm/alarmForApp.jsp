<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link id="link1" href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all1.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link id="link2" href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all1.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/My97DatePicker/WdatePicker.js"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerDrag.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerLayout.js" type="text/javascript"></script>
    <style>
	.l-grid-body l-grid-body2 l-scroll{background-color:#071F23}
	.l-dialog-tc {
	    background: -webkit-gradient(linear, 0 0, 0 100%, from(#FFFF66), to(#FFFF66));
	    height:45px;
	    border-bottom: 0px;
	    border-radius: 10px 10px 0px 0px;
	    border-collapse: separate;
	    border-spacing: 0;
	    cursor: move;
	}
	.l-dialog-cc {
		border-radius: 0px 0px 10px 10px;
	    border-collapse: separate;
	    border-spacing: 0;
	}
	.l-dialog-buttons{
		background: #FFFFCB;
		margin:0px;
		height:30px;
		border-radius: 0px 0px 10px 10px;
	    border-collapse: separate;
	    border-spacing: 0;
	}
	.l-dialog-content{
		background: #FFFFCB;
		border-top: 0px;
	}
	.l-dialog td{
		font-family:微软雅黑;
		border-top: 0px;
	}
	.l-grid td div
	{
	    font-size: 15px;
	    height: auto !important;
	}
	.l-dialog{
		border-radius: 10px;
	}
	.l-dialog td div{
		
	}
	.l-dialog-table{
		
	}
	.l-dialog-buttons-inner{
		text-align: center;
	}
	.l-dialog-btn-confirm {
	box-shadow: 0 1px 1px rgba(0,0,0,0.3);
	background: #337fa9;
	background: -moz-linear-gradient(top, #4994be, #337fa9);
	background: -webkit-gradient(linear, 0 0, 0 100%, from(#E1A38F), to(#E1A38F));
	background: -o-linear-gradient(top, #4994be, #337fa9);
	background: -ms-linear-gradient(top, #4994be 0%,#337fa9 100%);
	background: linear-gradient(top, #4994be, #337fa9);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#4994be', endColorstr='#337fa9');
	font-weight: bold;
	color: black;
	text-shadow: 0 2px 2px rgba(0,0,0,0.22);
	margin-left: -20px;
	}
	.l-dialog-btn-back {
	box-shadow: 0 1px 1px rgba(0,0,0,0.3);
	background: #337fa9;
	background: -moz-linear-gradient(top, #4994be, #337fa9);
	background: -webkit-gradient(linear, 0 0, 0 100%, from(#BDBDAB), to(#BDBDAB));
	background: -o-linear-gradient(top, #4994be, #337fa9);
	background: -ms-linear-gradient(top, #4994be 0%,#337fa9 100%);
	background: linear-gradient(top, #4994be, #337fa9);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#4994be', endColorstr='#337fa9');
	font-weight: bold;
	color: black;
	text-shadow: 0 2px 2px rgba(0,0,0,0.22);.
	margin-right: 50px;
	}
	 .l-dialog-btn-confirm:hover
	{
	    background: #E1A38F;
	    color: #555;
	}
	 .l-dialog-btn-back:hover
	{
	    background: #BDBDAB;
	    color: #555;
	}
    .l-dialog-btn-highlight:hover
    {
        filter: progid:DXImageTransform.Microsoft.Gradient(enabled=false);
        background: #3286b4;
        color: #fff;
    }
    .l-grid-body{
    	background: #292929;
    }
    </style>
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">
	<!-- 列表部分 -->
	<div id="grid" style="display: none;height:100%"></div>
<script type="text/javascript">
var grid;

$(function() {
    //初始化表格
    initGrid();
    //刷新报警列表
    //window.setInterval("initGrid()", 3000);
});
//初始化表格
function initGrid() {
    //隐藏
    $("#grid").show();
    //引入CSS
    grid = $("#grid").ligerGrid({
//         title: '&nbsp;&nbsp;<span style="color:#386792">报警信息一览</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
//         '<i class="fa fa-square" style="color:#FF0000"></i>--故障&nbsp;&nbsp;'+
//         '<i class="fa fa-square" style="color:#FFC12E"></i>--报警&nbsp;&nbsp;'+
//         '<i class="fa fa-square" style="color:#4EE561"></i>--警告',
//         headerImg: '/hmdl/static/images/list.png',
        columns: [
			{display: '序号',name: 'rownum',width: 60},
			{display: '确认恢复时间',name: 'recTime',width: 170},
			{display: '报警时间',name: 'callTime',width: 170,dateFormat:'yyyy-MM-dd HH:mm:ss',type:'DATE'},
			{display: '名称',name: 'equName',width: 185},
			{display: '报警值',name: 'alarmValue',width: 75,
			    	render: function(row) {
			            var html = row.alarmValue;
			            if (html == "1") {
			                return "合闸";
			            }else if (html == "0") {
			                return "分闸";
			            }else{
			                return html;
			            }
			        }
			},
			{display: '报警等级',name: 'alarmLevel',width: 90,
			        render: function(row) {
			            var html = row.alarmLevel;
			            if (html == "3") {
			                return "3-警告";
			            }
			            if (html == "2") {
			                return "2-报警";
			            }
			            if (html == "1") {
			                return "1-故障";
			            }
			        }
			},
			{display: '状态',name: 'state',width: 75,
				render: function(row) {
			            var html = row.alarmLevel;//获得报警级别
			            var html0 = row.state;//获得状态
			            if (html0 == "3") {
			                return '已处理';
			            }
			            if (html0 == "2") {
			                return '已确认';
			            }
			            if (html0 == "1") {
			                return '未确认';
			            }
				}
			},
			{display: '处理方式',name: 'methods',width: 110,
			        render: function(row) {
			            var html = row.methods;
			            if (html == "3") {
			                return "短信通知确认";
			            }
			            if (html == "2") {
			                return "电话沟通确认";
			            }
			            if (html == "1") {
			                return "报警信息确认";
			            }
			            if (html == "") {
			                return "";
			            }
			        }
			},
			{display: '信息',name: 'message',width: 185},
			{display: '变电所名称',name: 'substationName',width: 205},
			{display: '位置',name: 'local',width: 205},
        ],
        dataAction: 'server',
        url: 'searchAlarm',
        parms:[{name:"substationSn",value:'${substationSn}'},
               {name:"substationName",value:'${substationName}'},
               {name:"callTime",value:'${callTime}'},
               {name:"alarmEndTime",value:'${alarmEndTime}'},
               {name:"equName",value:'${equName}'},
               {name:"alarmLevel",value:'${alarmLevel}'},
               {name:"userSn",value:'${userSn}'},
               {name:"roleSn",value:'${roleSn}'}],
        root: 'data',
        record: 'count',
        height: '97%',
        pageSize: 10,
        checkbox: false,
        rownumbers: false,
        alternatingRow: false,
        onRClickToSelect:true,
        rowAttrRender: function(rowdata) {
            if (rowdata.alarmLevel == "1") {
                if(rowdata.state == 3){
                    return "style='color:gray;'"; 
                }
                if(rowdata.state == 2){
                    return "style='color:#DCDCDC;'"; 
                }
                return "style='color:#FF0000;'";
            }
            if (rowdata.alarmLevel == "2") {
                if(rowdata.state == 3){
                    return "style='color:gray;'"; 
                }
                if(rowdata.state == 2){
                    return "style='color:#DCDCDC;'"; 
                }
                return "style='color:#FFC12E;'";
            }
            if (rowdata.alarmLevel == "3") {
                if(rowdata.state == 3){
                    return "style='color:gray;'"; 
                }
                if(rowdata.state == 2){
                    return "style='color:#DCDCDC;'"; 
                }
                return "style='color:#4EE561;'";
            }
        },
    });
}

</script>
</body>  
</html>

