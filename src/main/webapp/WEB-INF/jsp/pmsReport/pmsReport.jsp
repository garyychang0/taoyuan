<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/My97DatePicker/WdatePicker.js"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerDrag.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script>
    <style type="text/css">
        .l-case-title
        {
            font-weight: bold;
            margin-top: 20px;
            margin-bottom: 20px;
        }
		.l-dialog-win .l-dialog-content{
			background: #fcfcfc;
		}
		
    </style>
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">
<div id="layout1">
	<!-- 左侧树 -->
	<div position="left" title="变电所一览" style="height:calc(100% - 40px);overflow: auto; ">
		<!-- 变电所查询条件部分 -->
		<div id="org_search" style="margin-left:8px;margin-top:0px;">
			<ul>
				<li class="label">
					<input type="text" placeholder="输入名称进行搜索" style="text-indent:5px;height:23px;border-radius: .25em;width:60%;margin-left:0;margin-top: 5px" name="search_substationName" id="search_substationName"  />					
					<button type="button" id="org_btnSearch" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x"><i class="fa fa-search"></i></button>
					<button type="button" id="org_expend" onclick="expend()" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x"><i class="fa fa-plus"></i></button>
					<button type="button" id="org_unexpend" onclick="unexpend()" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x;display:none"><i class="fa fa-minus"></i></button>
				</li>
			</ul>
		</div><div id="orgInfoTree"></div><br/><br/>
		<input type="hidden" id="org_sn" name="org_sn" value=""/>
		<input type="hidden" id="org_name" name="org_name" value=""/>
	</div>
	<div position="center"  title="变电所" >
		<!-- 标题部分 -->
		<h3 class="hmdl_title_h3">报表管理
		    <span class="hmdl_title_span">
		        &nbsp;/&nbsp;报表一览
		    </span>
		</h3>
		<hr class="hmdl_title_hr"/>
		
		<!-- 查询条件部分 -->
		<div id="grid_search" style="margin-left:8px;margin-top:20px;">
		    <form name="searchForm" id="searchForm">
		        <!-- 隐藏域传值 -->
		        <input type="hidden" id="search_substationSn" name="substationSn"/>
		        &emsp;变电所：<input id="search_substationName" name="substationName" type="text" style="text-indent:5px;width:150px;height:25px;border-radius: .25em;" placeholder="搜索变电所名称"/>
		        &emsp;报表名称：<input id="search_reportName" name="reportName"  type="text" placeholder="搜索报表名称" style="text-indent:5px;width:150px;height:25px;border-radius: .25em;"/>
		        &emsp;报表类型：
		        <select id="search_reportType" name="reportType" style="text-indent:5px;width:150px;height:28px;border-radius: .25em;">
		        	<option selected="selected" value="">==选择报表类型==</option>
		        	<option value="1">日</option>
		        	<option value="2">月</option>
		        	<option value="3">年</option>
				    <option value="4">自定义天报表</option>
		        </select>
		        &emsp;<button id="btnSearch" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
		    </form>
		</div>
		<br/>
		<!-- 列表部分 -->
		<div id="grid"></div>
		
		<!-- 增改部分计算变量 -->
		<div id="editConfig" style="display:none;">
		    <table class="default">
				<tr style="height:50px" >
		            <td colspan="6" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
		            <i class="fa fa-address-book"></i>&emsp;报表列配置</td>
		        </tr>
		    </table>
			<!-- 配置部分 -->
			<div id="gridConfig"></div>
			<table align="center" style="margin-top: 10px">
		        <tr>
		            <td>
		                <button type="submit" class="btn btn-success" onclick="configConfirm()"><i class="fa fa-check"></i>&nbsp;&nbsp;提交&nbsp;&nbsp;</button>
		                &emsp;
		                <button type="button" class="btn btn-warning" onclick="configCancel()"><i class="fa fa-close"></i>&nbsp;&nbsp;返回&nbsp;&nbsp;</button>
		            </td>
		        </tr>
		    </table>
		</div>
		<!-- 新增 -->
		<div id="editWin" style="display:none;">
		    <form name="editWinForm" id="editWinForm">
		    <input type="hidden" id="reportId" name="id" />
		    <table class="default">
		        <tr style="height:60px" >
		            <td colspan="4" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
		                <i class="fa fa-edit"></i>&emsp;编辑
		            </td>
		        </tr>
		        <tr>
		            <td width="100px" align="right">报表名称：</td>
		            <td>
		               <input id="reportName" name="reportName" placeholder="输入报表名称" style="text-indent:5px;width:250px;height:36px;border-radius: .25em;" ></input>
		               <font color="red">*</font>
		            </td>
		            <td width="100px" align="right">模板类型：</td>
		            <td>
		               <select id="templateType" name="templateType" style="text-indent:5px;width:250px;height:36px;border-radius: .25em;">
				        	<option selected="selected" value="0">多变量报表</option>
				        	<option value="1">单变量报表</option>
				        	<option value="2">EP变量报表</option>
				        </select>
				        <font color="red">*</font>
		            </td>
		        </tr>
		        <tr>
		            <td width="100px" align="right">报表类型：</td>
		            <td>
		               <select id="reportType" name="reportType" style="text-indent:5px;width:250px;height:36px;border-radius: .25em;">
				        	<option selected="selected" value="">==选择报表类型==</option>
				        	<option value="1">日报表</option>
				        	<option value="2">月报表</option>
				        	<option value="3">年报表</option>
				        	<option value="4">自定义天报表</option>
				        </select>
				        <font color="red">*</font>
		            </td>
		            <td width="100px" align="right">选择变电所：</td>
		            <td>
		               <input id="createSubstationName" name="createSubstationName" onclick="selectStation()" placeholder="请选择变电所" readonly="readonly"  style="text-indent:5px;width:250px;height:36px;border-radius: .25em;"></input>
		               <font color="red">*</font>
		               <input id="createSubstationSn" name="substationSn" type="hidden" ></input>
		               <input id="createShortName" name="shortName" type="hidden"></input>
		            </td>
		        </tr>
		        <tr>
		            <td width="100px" align="right">报表标题：</td>
		            <td>
		               <input id="reportTitle" name="reportTitle" placeholder="输入报表标题" style="text-indent:5px;width:250px;height:36px;border-radius: .25em;" ></input>
		               <font color="red">*</font>
		            </td>
		            <td width="100px" align="right">标题颜色：</td>
		            <td>
		               <select id="titleColor" name="titleColor" style="text-indent:5px;width:250px;height:36px;border-radius: .25em;">
				        	<option selected="selected" value="black">黑色</option>
				        	<option value="red">红色</option>
				        	<option value="blue">蓝色</option>
				        	<option value="green">绿色</option>
				        	<option value="yellow">黄色</option>
				        	<option value="purple">紫色</option>
				        </select>
				        <font color="red">*</font>
		            </td>
		        </tr>
		        <tr>
		            <td width="100px" align="right">标题宽度：</td>
		            <td>
		               <input id="titleWidth" name="titleWidth" value="20" type="number" style="text-indent:5px;width:80px;height:36px;border-radius: .25em;"/>
		               <font color="red">*</font>
		            </td>
		            <td width="100px" align="right">标题字号：</td>
		            <td>
		               <input id="titleSize" name="titleSize" value="20" type="number" style="text-indent:5px;width:80px;height:36px;border-radius: .25em;"/>
		               <font color="red">*</font>
		            </td>
		        </tr>
		        <tr>
		            <td width="100px" align="right" >备注信息：</td>
		            <td colspan="3"  rowspan="2" >
		               <textarea id="remark" name="remark" placeholder="填写备注信息" style="text-indent:5px;width:500px;height:72px;line-height:36px;border-radius: .25em;"></textarea>
		            </td>
		        </tr>
		        <tr>
		        </tr>
		        <tr>
		            <td colspan="4" style="text-align: center">
		                <button type="submit" id="editWinSave" class="btn btn-primary"><i class="fa fa-check"></i>&nbsp;&nbsp;提交&nbsp;&nbsp;</button>
		                &emsp;
		                <button type="button" id="editCancel" onclick="cancel()" class="btn btn-danger"><i class="fa fa-undo"></i>&nbsp;&nbsp;关闭&nbsp;&nbsp;</button>
		            </td>
		        </tr>
		    </table>
		    </form>
		</div>
</div>
<div id="varConfig"  style="display: none;overflow:hidden;">
	<form  name="varForm" id="varForm">
		<table id= "table_var_edit" class="default">
		</table>
	</form>
</div>
<!-- 选择时间 -->
<div id='selectDate' style="display:none">
	<form id="am-form" enctype="multipart/form-data" method="post" action="report"  target="upframe">
		<table class="default">
			<tr>
				<td align="right">开始日期</td>
				<td align="left" id="search_begin_date_tr"><input type="text" autocomplete="off" class="Wdate" style="width:150px" name="search_begin_date" id="search_begin_date"  onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
				</td>
			</tr>
			<tr style="display:none" id="tr_search_end_date_tr">
				<td align="right">结束日期</td>
				<td align="left" id="search_end_date_tr"><input type="text" autocomplete="off" class="Wdate" style="width:150px" name="search_end_date" id="search_end_date"  onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
				</td>
			</tr>
		</table>
	</form>
</div>
<!-- 导出用Form -->
<form action="../report/show" method="post" target="_blank" id="reportForm">
	<input type="hidden" name="fparam" id="fparam"></input>
	<input type="hidden" name="fcolumn" id="fcolumn"></input>
	<input type="hidden" name="fname" id="fname"></input>
	<input type="hidden" name="fclassname" id="fclassname"></input>
	<input type="hidden" name="fmenu" id="fmenu"></input>
	<input type="hidden" name="ftitle" id="ftitle"></input>
	<input type="hidden" name="fexport" id="fexport"></input>
	<input type="hidden" name="furl" id="furl"></input>
</form>
<script type="text/javascript">
var grid;
var editGrid;
var selectSubstationWin;//选择变电所
var editWin;// 编辑属性窗口
var globalForm;
var globalDialog;
$(function(){
    //布局设置
    $("#layout1").ligerLayout({
    leftWidth: 200,
    });
    //变电所树检索
    substationSearch();
    //初始化表格
    initGrid();
    // 绑定查询按钮
	$("#btnSearch").click(function () {
	    $("#search_substationSn").val($("#org_sn").val());//左侧树传值SN
	    var parms = serializeObject($("#searchForm"));
   	    for(var p in parms){
   	        grid.setParm(p,parms[p].trim());
   	    }
   	    grid.loadData();
   	});
    //验证
    var validator = $("#editWinForm").validator({
        ignore: ":hidden", theme: "yellow_right", timely: 1, stopOnError: true,
        rules: { digits: [/^\d+(\d+)?$/, '{0}必须为大于0数字！'],  
           		 english: [/^\w+$/,'{0}只能英文和数字！'],
           		 email:[/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,'{0}格式不正确！']
        },
        fields: {
            reportName: '报表名称:required,length[1~30]',
            reportTitle: '报表标题:required,length[1~30]',
            reportType: '报表类型:required,length[1~30]',
            createSubstationName: '站点名称:required,length[1~30]',
            shortname: '简称:required,length[1~30]',
            titleSize: '字体大小:required,length[1~30],digits',
            titleWidth: '字体宽度:required,length[1~30],digits',
            titleColor: '字体颜色:required,length[1~30]',
        },
        valid: function (form) {
            save();
           }
    });
});
//绑定变电所查询按钮
$("#org_btnSearch").click(function () {
	substationSearch();
});

//绑定变电所收缩
$("#org_btn_collapse").click(function () {
	orgTree.collapseAll();
});

//绑定变电所收缩
$("#endAddr,#beginAddr").blur(function () {
    $("#lenth").val($("#endAddr").val()-$("#beginAddr").val());
});
//确认选中行信息
function confirm(){
 	var rows = grid.getSelectedRows();
 	var ids="";
 	if (rows == "" || rows.length!=1) {
 		$.ligerDialog.warn("请选择一条记录！","警告");
 		return;
 	}
 	for(var i=0 ; i < rows.length ; i++){
 	     if(i>0){
 	 			ids += "|";
 	     }
 	   	 ids += rows[i].id + "," + rows[i].updateDate;
 	}
}

//初始化表格
function initGrid(){
	grid=$("#grid").ligerGrid({
	    title:'&nbsp;&nbsp;<span style="color:#386792">报表信息一览</span>',
	    headerImg:'/hmdl/static/images/list.png',
	    columns: [
				{ display: '变电所', name: 'substationName',width:100},
				{ display: '报表名称', name: 'reportName',width:130},
				{ display: '报表标题', name: 'reportTitle',width:130},
				{ display: '报表类型', name: 'reportType',width:130,render:function(row){
				    if(row.reportType=="1"){
				        return "日报表";
				    }else if(row.reportType=="2"){
				        return "月报表";
				    }else if(row.reportType=="3"){
				        return "年报表";
				    }else if(row.reportType=="4"){
				        return "自定义天报表";
				    }
				}},
				{ display: '创建人', name: 'createrName',width:120},
				{ display: '创建时间', name: 'createDate',width:150},
				{ display: '更新人', name: 'updaterName',width:120},
				{ display: '更新时间', name: 'updateDate',width:150}
	    ],
	    toolbar: {
	        items: [
					{ line: true },{ line: true },<%=request.getAttribute("btn")%>,
	        ]
	    },
	    dataAction: 'server',
	    url:'search',
	    root:'data',
	    record:'count',
	    height: '97%',
	    pageSize: 10,
	    checkbox: true,
	    rownumbers: true
	});
}

//回车键搜索
$("input,select").keydown(function (e) {//当按下按键时
    if (e.which == 13) {//.which属性判断按下的是哪个键，回车键的键位序号为13
        var parms = serializeObject($("#searchForm"));
		for(var p in parms){
	        grid.setParm(p,parms[p].trim());
	    }
	    grid.loadData();
    }
});
//新增或修改报表
function operate(type){
    $('#reportType').removeAttr("disabled");
    if(type==1){
      	//重置表单
        $("#editWinForm")[0].reset();
      	//清空隐藏域ID
        $("#reportId").val("");
        //新增
        if(!editWin){
            editWin=$.ligerDialog.open({
                target:$("#editWin"),
                height: 500,
                width:900,
                title: "新增报表",
                isHidden:true,
                allowClose:false,
                isResize:false,
                showMax:false,
                showMin:false
            });
        }else{
            editWin.show();
        }
    }else{
	   	var rows = grid.getSelectedRows();
	 	if (rows == "" || rows.length!=1) {
	 		$.ligerDialog.warn("请选择一条记录！","警告");
	 		return;
	 	}
	 	//重置表单
 	    $("#editWinForm")[0].reset();
        //回显
        getReportInfo(rows);
        //修改
        if(!editWin){
            editWin=$.ligerDialog.open({
                target:$("#editWin"),
                height: 500,
                width:900,
                title: "新增报表",
                isHidden:true,
                allowClose:false,
                isResize:false,
                showMax:false,
                showMin:false
            });
        }else{
            editWin.show();
        }
		if($("#templateType").val()==1){
		    $("#reportType").val("4");
		    $('#reportType').attr("disabled",true);
		}
    }
}


//回显
function getReportInfo(rows){
    $("#reportId").val(rows[0].id);
    $("#reportName").val(rows[0].reportName);
    $("#reportTitle").val(rows[0].reportTitle);
    $("#reportType").val(rows[0].reportType);
    $("#templateType").val(rows[0].templateType);
    $("#createSubstationName").val(rows[0].substationName);
    $("#createSubstationSn").val(rows[0].substationSn);
    $("#createShortName").val(rows[0].shortName);
    $("#titleSize").val(rows[0].titleSize);
    $("#titleWidth").val(rows[0].titleWidth);
    $("#titleColor").val(rows[0].titleColor);
    $("#remark").val(rows[0].remark);
}
//删除报表
function del(){
   	var rows = grid.getSelectedRows();
   	if (rows == "") {
		$.ligerDialog.warn("请选择一条记录！","警告");
		return;
	}
   	var ids="";
   	for(var i=0 ; i < rows.length ; i++){
       	ids += rows[i].id + ",";
   	}
	$.ligerDialog.confirm("确定要删除选中记录？","提示", function(confirm){
    	if(confirm){
     		$.post("doDelete",{ids:ids},function(data){
            		if (data.retCode==100) {
            			var success = $.ligerDialog.success('删除成功！',"提示");
            	        setTimeout(function(){success.close();},500);
            	    	grid.reload();
            		}
            		else if(data.retCode==-700){
                      		$.ligerDialog.error("删除失败！存在", "提示");
                   }else{
                      		$.ligerDialog.error("删除失败！", "提示");
                   }
           },"json")
  		}
	});
}
//绑定变电所查询按钮
$("#templateType").change(function () {
	if($("#templateType").val()==1){
	    $("#reportType").val("4");
	    $('#reportType').attr("disabled",true);
	}else{
	    $('#reportType').removeAttr("disabled");
	}
});
//绑定变电所查询按钮
$("#reportType").change(function () {
	if($("#templateType").val()==2 && $("#reportType").val()==1){
	    $.ligerDialog.warn("EP不能选日报表！","提示");
	    $("#reportType").val("2");
	    return;
	}
});
//预览报表
function preview(){
    var warn = $.ligerDialog.warn("预览");
    setTimeout(function(){warn.close();},300);
}
//取消
function cancel(){
    if(editWin){editWin.hide();}
  	//重置表单
    $("#editWinForm")[0].reset();
}
//提交表单
function save(){
    $('#reportType').removeAttr("disabled");
    var data = serializeObject($("#editWinForm"));
    $.ajax({
        type: 'post',
        url: "save",
        data: data,
        cache: false,
        dataType: 'json',
        success: function (data) {
            if (data && data.retCode == 100) {
                cancel();
                var success = $.ligerDialog.success('保存成功！', "提示");
                setTimeout(function(){success.close();},500);
             	grid.reload();
            } else {
                var text = "";
                if (data.errors && data.errors.length) {
                    for (var i = 0; i < data.errors.length; i++) {
                        text = text + "<br/>" + data.errors[i];
                    }
                }
                $.ligerDialog.error(text, "提示");
            }
        }
    });
    
}

//选择变电所
function selectStation(type){
    // 转向网页的地址;
    var url='../picturePart/selectStationData';
    $.ligerDialog.open({
        //右上角的X隐藏，否则会有bug
        allowClose: false,
        height: 500,
        url: url,
        width: 1000,
        name:'choseKey',
        title:'变电站选择',
        isResize:true,
        buttons: [  {  text: '确认', onclick: function (item, dialog){
                    var arrM = new Array();
                    //获取子页面
                    arrM=document.getElementById('choseKey').contentWindow;
                    var contentgrid = arrM.$("#grid").ligerGrid();
                    var contentrows = contentgrid.getSelectedRows();
                    if (contentrows == null || contentrows.length!=1) {
                        $.ligerDialog.warn("请选择一条记录！","提示");
                        return;
                    };
                    $("#createSubstationSn").val(contentrows[0].substationSn);
                    $("#createShortName").val(contentrows[0].shortname);
                    $("#createSubstationName").val(contentrows[0].substationName);
                    if(contentrows[0].resource_type == '0'){
                        $.ligerDialog.warn("请选择具体变电所！", "提示");
                        return;
                    }
                    dialog.close();
                    }
                },
                    { text: '关闭', onclick: function (item, dialog){dialog.close();}}
                ]
    }); 
}

//计算公式关联
function changeVar(index,rowdata,val){
    var check = gridConfig.getRow(index);
    var reg = /[A-Z]/g;
    var val_new = val.match(reg);
    var array = new Array(); 
    var j=0;
    for(var i=0;i<val_new.length;i++){ 
        if((array == "" || array.toString().match(new RegExp(val_new[i],"g")) == null)&&val_new[i]!=""){ 
        array[j] =val_new[i]; 
        j++; 
        } 
     }
	$("#table_var_edit").html("");
	    addCparm(array);
		$.ligerDialog.open({
			target: $("#varConfig"),
			height: 300,
			width:600,
			isHidden: true,
			isResize:false,
			cls:"connectClass",
			allowClose: false,
			title: "设置公式",
		    buttons: [
		              { text: '确定', onclick: function (item, dialog) {
						  var valList = serializeObject($("#varForm"));
						  var showVal = "";
						  for (var k in valList) {
						      if(valList[k]==""){
				                  $.ligerDialog.error(k+"请选择变量", "提示", function (opt) {                      
				                  });
				                  return;
						      }
						      showVal = showVal+ k + ":"+valList[k]+",";//输出如:name 
						  }
						  showVal = showVal.substr(0,showVal.length-1);
		                  gridConfig.updateCell('fieldType', val, index);
		                  gridConfig.updateCell('fieldName', showVal, index);
		                  dialog.hide();
		              } },
		              { text: '取消', onclick: function (item, dialog) { dialog.hide(); } }
		          ]
	})
}

//添加表格行
function addCparm(data){
  for(var i = 0;i<data.length;i++){
      var tr = $("<tr>"); 
		$("<td align='right'colspan='1'>").html("&emsp;"+data[i]+":&emsp;<span style='color:red'>*</span>").appendTo(tr);
		$("<td align='left'>").html('<input onclick=selectVar('+i+') readonly="readonly" id="table_edit_'+i+
		        '" name='+data[i]+' type="" value =""/>').appendTo(tr);
		$("#table_var_edit").append(tr);
  }
}

//选择变量
function selectVar(obj){
	// 转向网页的地址;
	var url='../stationDataDetail/selectVar';    
   	var rows = grid.getSelectedRows();
   	if (rows == "") {
		$.ligerDialog.warn("请选择一条记录！","警告");
		return;
	}
	$.ligerDialog.open({
		//右上角的X隐藏，否则会有bug
	  allowClose: false,
	  height: 600,  
	  url: url,  
	  width: 800,  
	  name:'choseKey',  
	  title:'变量选择',  
      data: {
          funcCode: 03,
          substationSn:rows[0].substationSn,
          shortName:rows[0].shortName
      },
	  isResize:true,  
	  buttons: [  {  text: '确认', onclick: function (item, dialog){
					    var arrM = new Array();
						//获取子页面
						arrM=document.getElementById('choseKey').contentWindow;
						var contentgrid = arrM.$("#grid").ligerGrid();
						var contentrows = contentgrid.getSelectedRows();
					    if (contentrows == null || contentrows.length!=1) {
					        $.ligerDialog.warn("请选择一条记录！","提示");
					        return;
					    };
						$("#table_edit_"+obj).val(contentrows[0].numberName);
						dialog.close();
	  			}
	  		},  
	              { text: '关闭', onclick: function (item, dialog){dialog.close();}}
	          ]                                     
		}); 
}

//变量
function f_calculate(rowdata, index, value){
    return "<div><select onchange=changeVar("+index+","+value+","+"this.value"+") id='report_calculate"+index+"' name='report_calculate"+index+"' style='vertical-align:0!important;height:26px'>"+
    "<option value=''> == 请选择  == </option><c:forEach items='${report_calculate}' var='i'>"+
    "<option value='${i['value']}'>${i['value']}</option></c:forEach></select></div>";
}

//配置
function config(){
	var rows = grid.getSelectedRows();
 	if (rows == "" || rows.length!=1) {
 		$.ligerDialog.warn("请选择一条记录！","提醒");
 		return;
 	}
 	var type = 0;
	if(rows[0].templateType==1 || rows[0].templateType==2){
	    type = 1;
	}
    $("#grid").hide();
    $("#editConfig").show();
    $("#grid_search").hide();
    var unit = '${unit}';
    unit = JSON.parse(unit);
    var columnAlignData = [{ columnAlign: "Center", text: '居中' }, { columnAlign: "Left", text: '左对齐'},{ columnAlign: "Right", text: '右对齐'}];
	// 列表画面定义
	gridConfig=$("#gridConfig").ligerGrid({
		headerImg:'/hmdl/static/images/list.png',
		    columns: [
				{ display: '报表id', name: 'reportId',width:100,hide:true},
				{ display: '变电站编号', name: 'substationSn',width:100,hide:true},
				{ display: '列名', name: 'columnName',width:100,
				    editor: { type: 'text' }
				},
				{ display: '单位', name: 'columnName2',width:100,
				    editor: { type: 'select', data:  unit, valueField: 'unit'},
                    render: function (item)
                    {
                	    for (var i in unit) {
           			    	if(item.columnName2== unit[i].unit){
           			    		return  unit[i].text;
           			    	}
                	    }
                    }  
				},
				{ display: '列宽', name: 'columnWidth',width:100,
				    editor: { type: 'int' }
				},
				{ display: '对齐方式', name: 'columnAlign',width:50,type:'int',
                    editor: { type: 'select', data: columnAlignData, valueField: 'columnAlign'},
                    render: function (item)
                    {
                        if (item.columnAlign == "Left"){
                            return '左对齐';
                        }else if (item.columnAlign == "Right"){
                            return '右对齐';
                        }else if (item.columnAlign == "Center"){
                            return '居中';
                        }else{
                            item.columnAlign = "Center"
                            return '居中';
                        }
                    }
				},
				{ display: '求值', name: 'columnSum',width:50,type:'int',
                    editor: { type: 'select', data:  [{columnSum:"true",text:'是'},{columnSum:"false",text:'否'}], valueField: 'columnSum'},
                    render: function (item)
                    {
                        if (item.columnSum == "true"){
                            return '是';
                        }else if (item.columnSum == "false"){
                            return '否';
                        }else{
                            item.columnSum = "false";    
                            return '否';
                        }
                    }  
				},
				{ display: '计算公式选择', name: 'fieldTypeSelect',width:200,
				    render:f_calculate
				},
				{ display: '计算公式', name: 'fieldType',width:150},
				{ display: '变量', name: 'fieldName',width:250}
	    ],
	    toolbar: {
	        items: [
				{ text: '添加',click: function(){configAdd(type);}, icon: 'fa fa-plus',color:'success' },
				{ text: '删除',click: function(){configDelete();}, icon: 'fa fa-trash',color:'danger' },
	        ]
	    },
	    dataAction: 'server',
	    url:'searchDetail',
	    root:'data',
    	parms:[{name:"reportId",value:rows[0].id}],
    	usePager:false,
        enabledEdit: true,isScroll: true, 
	    height: '300px',
    	checkbox: true,
	    rownumbers: true
	});
}
//删除行
function configDelete(){
	var row = gridConfig.getSelectedRow();            
	if (!row) { 
		$.ligerDialog.warn("请选择行","提示"); 
		return; 
	} 
	gridConfig.deleteSelectedRow();
} 

//添加行
function configAdd(type){
	var row = grid.getSelectedRow();
	if((type==1 || type==2) && gridConfig.getData().length>=1){
	    $.ligerDialog.warn("单变量只能添加一个变量","提示");
		return;
	}else{
	    gridConfig.addRow({
	        reportId :row.id,
	        substationSn :row.substationSn,
	        columnName : "",
	        columnWidth: 100,
	        columnAlign: "",
	        columnSum: "",
	        fieldType: "",
	        fieldName: ""
		});
	}
} 

//配置提交
function configConfirm(){
	gridConfig.endEdit();
	$.ligerDialog.confirm("是否确认提交？","提示", function(confirm){
		if(confirm){
			var rows = gridConfig.getData();
			var hasKey = false;
			if(rows.length<1){
				$.ligerDialog.warn("至少添加一条数据","提示");
				return;
			}
			for(var i = 0;i < rows.length;i++){
				if(rows[i].columnName == "" || rows[i].columnName == null){
					$.ligerDialog.warn("必须输入列名,行"+(i+1),"提示");
					return;
				}
				if(rows[i].fieldType == "" || rows[i].fieldType == null){
					$.ligerDialog.warn("必须选择计算公式,行"+(i+1),"提示");
					return;
				}
				if(rows[i].fieldName == "" || rows[i].fieldName == null){
					$.ligerDialog.warn("必须选择变量,行"+(i+1),"提示");
					return;
				}
			}
			var rows = gridConfig.getData();
			rows = JSON.stringify(rows);
	     	$.ajax({
	             type : 'post',
	             url : "saveDetail",
	             data : { retList : rows},
	             cache : false,
	             dataType : 'json',
	             success : function(data) {
	                 $.ligerDialog.success('保存成功！', "提示", function (opt) {
	                     configCancel();
	                 });
	             },
	             error : function() {
	             	$.ligerDialog.warn("编辑失败！","请联系管理员");
	             }
	         });
		}
	});
}

function configCancel(){
    $("#grid").show();
    $("#editConfig").hide();
    $("#grid_search").show();
    gridConfig.reload();
  	//重置表单
    $("#varForm")[0].reset();
	$("#table_var_edit").html("");
}

//配置提交
function report(){
	var rows = grid.getSelectedRows();
	if (rows == null || rows.length!=1) {
	    $.ligerDialog.warn("请选择一条记录！","提示");
	    return;
	};
	if(rows[0].reportType==1){
		$("#search_begin_date_tr").html('<input type="text" autocomplete="off" class="Wdate"'+
		        ' style="width:150px" name="search_begin_date" id="search_begin_date"  onclick=WdatePicker({dateFmt:"yyyy-MM-dd"})>');
		$("#tr_search_end_date_tr").hide();
	}else if(rows[0].reportType==2){
		$("#search_begin_date_tr").html('<input type="text" autocomplete="off" class="Wdate"'+
	        ' style="width:150px" name="search_begin_date" id="search_begin_date"  onclick=WdatePicker({dateFmt:"yyyy-MM"})>');
		$("#tr_search_end_date_tr").hide();
	}else if(rows[0].reportType==3){
		$("#search_begin_date_tr").html('<input type="text" autocomplete="off" class="Wdate"'+
	        ' style="width:150px" name="search_begin_date" id="search_begin_date"  onclick=WdatePicker({dateFmt:"yyyy"})>');
		$("#tr_search_end_date_tr").hide();
	}else if(rows[0].reportType==4){
		$("#search_begin_date_tr").html('<input type="text" autocomplete="off" class="Wdate"'+
	        ' style="width:150px" name="search_begin_date" id="search_begin_date"  onclick=WdatePicker({dateFmt:"yyyy-MM-dd"})>');
		$("#tr_search_end_date_tr").show();
		$("#search_end_date_tr").html('<input type="text" autocomplete="off" class="Wdate"'+
	        ' style="width:150px" name="search_end_date" id="search_end_date"  onclick=WdatePicker({dateFmt:"yyyy-MM-dd"})>');
	}
	var import_date;
	if(!import_date){
		import_date=$.ligerDialog.open({
		target:$("#selectDate"),
	   	height: 150,
		width:300,
	    title: "选择时间范围",
	    isHidden: true,
	    isResize:true ,
	    allowClose:false,
	    buttons: [
			{ text: '确定', onclick: function (item, dialog) { 
				if($("#search_begin_date").val()==""){
				    $.ligerDialog.error("请选择时间！", "提示");
				 return ;
				}
				dialog.hide();
				// 用于获取id
				var fid = {};
				fid.fid = rows[0].id;
				if(rows[0].reportType=="4"){
					fid.ftime = $('#search_begin_date').val()+","+$('#search_end_date').val();
				}else{
					fid.ftime = $('#search_begin_date').val();
				}
				$('#fparam').val(JSON.stringify(fid));
				$('#fcolumn').val(JSON.stringify(grid.getColumns()));
				// 报表的唯一标示
				$('#fname').val("pmsReport");
				// 报表容器的className
				$('#fclassname').val("");
				// 报表的默认标题
				$('#ftitle').val(rows[0].reportName);
				$('#fmenu').val("pmsReport");
				// pdf的加载地址【必输项】
				$('#furl').val("reportpdf");//新版本reportnew.js无默认值，必须填写
				// 报表导出的页面地址【必输项】
				$('#fexport').val("report");//新版本reportnew.js无默认值，必须填写
				// 导出
				$('#reportForm').submit();
	            }},
	              { text: '取消', onclick: function (item, dialog) { dialog.hide(); } }
	          ]
		});
	}
}
</script>
</body>  
</html>

