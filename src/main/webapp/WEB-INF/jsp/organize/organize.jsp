<%@ page language="java" import="java.util.*"
         contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="/hmdl/static/treetable/layui/css/layui.css"  media="all">
    <link rel="stylesheet" href="/hmdl/layuiAdmin/src/layuiadmin/layui/css/layui.css" media="all">
    <link rel="stylesheet" href="/hmdl/layuiAdmin/src/layuiadmin/style/admin.css" media="all">
    <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-form layui-card-header layuiadmin-card-header-auto">
            <div class="layui-form-item">
                <div class="layui-inline">
                    <button type="button" class="layui-btn layui-btn-primary layui-btn-sm get-checked">获取选中</button>
                    <button type="button" class="layui-btn layui-btn-primary layui-btn-sm refresh">刷新（新增）</button>
                    <button type="button" class="layui-btn layui-btn-primary layui-btn-sm open-all">全部展开</button>
                    <button type="button" class="layui-btn layui-btn-primary layui-btn-sm close-all">全部关闭</button>
                    <button type="button" class="layui-btn layui-btn-primary layui-btn-sm change-icon">随机更换小图标</button>
                </div>
            </div>
        </div>
        <div class="layui-card-body">
            <div class="layui-progress" lay-filter="organize">
                <div class="layui-progress-bar organize_progress" lay-percent="0%"></div>
            </div>
            <table class="layui-table layui-form" id="tree-table" lay-size="bg"></table>
        </div>
    </div>
</div>
<script src="/hmdl/static/treetable/layui/layui.js" charset="utf-8"></script>
<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script>

    layui.config({
        base: '/hmdl/static/treetable/js/',
    });
    layui.use(['treeTable','layer','form','element'],function(){//注意进度条依赖 element 模块，否则无法进行正常渲染和功能性操作
        var o = layui.$,
            form = layui.form,
            element = layui.element,
            layer = layui.layer,
            treeTable = layui.treeTable;
        // 直接下载后url: './data/table-tree.json',这个配置可能看不到数据，改为data:[],获取自己的实际链接返回json数组
        var	re = treeTable.render({
            elem: '#tree-table',
            url:'search',
            icon_key: 'departmentName',
            is_checkbox: true,
            checked: {
                key: 'id',
                data: [0,1,4,10,11,5,2,6,7,3,8,9],
            },
            end: function(e){
                form.render();
            },
            cols: [
                {
                    key: 'departmentName',
                    title: '名称',
                    width: '250px',
                    template: function(item){
                        if(item.level == 0){
                            return '<span style="color:red;">'+item.departmentName+'</span>';
                        }else if(item.level == 1){
                            return '<span style="color:green;">'+item.departmentName+'</span>';
                        }else if(item.level == 2){
                            return '<span style="color:#aaa;">'+item.departmentName+'</span>';
                        }
                    }
                },
                {
                    key: 'id',
                    title: '编号',
                    width: '80px',
                    align: 'center',
                },
                {
                    key: 'address',
                    title: '地址',
                    width: '200px',
                    align: 'center',
                },
                {
                    key: 'contactUser',
                    title: '联系人',
                    width: '60px',
                    align: 'center',
                },
                {
                    key: 'contactTel',
                    title: '联系电话',
                    width: '60px',
                    align: 'center',
                },
                {
                    key: 'type',
                    title: '类型',
                    width: '60px',
                    align: 'center',
                },
                {
                    title: '开关',
                    width: '80px',
                    align: 'center',
                    template: function(item){
                        return '<input type="checkbox" name="close" lay-skin="switch" lay-text="ON|OFF">';
                    }
                },
                {
                    title: '操作',
                    align: 'center',
                    template: function(item){
                        return '<a lay-filter="add">添加</a> | <a target="_blank" href="/detail?id='+item.id+'">编辑</a>';
                    }
                }
            ]
        });
        // 监听展开关闭
        treeTable.on('tree(flex)',function(data){
            console.log(data);
            layer.msg(JSON.stringify(data));
        })
        // 监听checkbox选择
        treeTable.on('tree(box)',function(data){
            if(o(data.elem).parents('#tree-table').length){
                var text = [];
                o(data.elem).parents('#tree-table').find('.cbx.layui-form-checked').each(function(){
                    o(this).parents('[data-pid]').length && text.push(o(this).parents('td').next().find('span').text());
                })
                o(data.elem).parents('#tree-table').prev().find('input').val(text.join(','));
            }
            layer.msg(JSON.stringify(data));
        });
        // 监听自定义
        treeTable.on('tree(add)',function(data){
            layer.msg(JSON.stringify(data));
        });
        // 获取选中值，返回值是一个数组（定义的primary_key参数集合）
        o('.get-checked').click(function(){
            layer.msg('选中参数'+treeTable.checked(re).join(','))
        });
        // 刷新重载树表（一般在异步处理数据后刷新显示）
        o('.refresh').click(function(){
            //re.data.push({"id":50,"pid":0,"departmentName":"1-4"},{"id":51,"pid":50,"departmentName":"1-4-1"});
            element.progress('organize', '100%');//设置100%进度
            treeTable.render(re);
            setTimeout(function(){
                element.progress('organize', '0%');//设置0%进度
            },350);
        });
        // 全部展开
        o('.open-all').click(function(){
            treeTable.openAll(re);
        });
        // 全部关闭
        o('.close-all').click(function(){
            treeTable.closeAll(re);
        });
        // 随机更换小图标
        o('.change-icon').click(function(){
            var arr = [
                {
                    open: 'layui-icon layui-icon-set',
                    close: 'layui-icon layui-icon-set-fill',
                    left: 16,
                },
                {
                    open: 'layui-icon layui-icon-rate',
                    close: 'layui-icon layui-icon-rate-solid',
                    left: 16,
                },
                {
                    open: 'layui-icon layui-icon-tread',
                    close: 'layui-icon layui-icon-praise',
                    left: 16,
                },
                {
                    open: 'layui-icon layui-icon-camera',
                    close: 'layui-icon layui-icon-camera-fill',
                    left: 16,
                },
                {
                    open: 'layui-icon layui-icon-user',
                    close: 'layui-icon layui-icon-group',
                    left: 16,
                },
            ];
            var round = Math.round(Math.random()*(arr.length - 1));
            re.icon = arr[round];
            treeTable.render(re);
        });
    })
</script>

</body>
</html>
