<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/images/taoyuanlogo.png" rel="alternate icon" type="image/png" />
    <link rel="stylesheet" href="/hmdl/static/ui/layui/css/layui.css"  media="all">
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">

<!-- 标题部分 -->
<span class="layui-breadcrumb">
  <a href="">首页</a>
  <a href="">演示</a>
  <a><cite>导航元素</cite></a>
</span>

<hr class="hmdl_title_hr"/>

<!-- 查询条件部分 -->
<div id="grid_search" style="margin-left:8px;margin-top:20px;">

</div>
</body>
</html>
