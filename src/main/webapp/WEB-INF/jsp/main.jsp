<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>桃园小学综合办公管理系统</title>
	<link href="/hmdl/static/images/taoyuanlogo.png" rel="alternate icon" type="image/png" />
	<link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/application/style.css" rel="stylesheet" />
	<link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<!-- <link href="/hmdl/static/js/application/ne.css" rel="stylesheet" type="text/css" /> -->
	<link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
	<script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
	<script src="/hmdl/static/layui/lay/modules/layer.js"></script>
	<script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
	<script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
	<script src="/hmdl/static/js/application/ligerui.expand.js"></script>
	<script src="/hmdl/static/js/application/ne.js" type="text/javascript"></script>
	<script src="/hmdl/static/js/jquery-validation/messages_cn.js"  type="text/javascript"></script>
	<script src="/hmdl/static/js/ligerUI/js/plugins/ligerDrag.js" type="text/javascript"></script>
	<script src="/hmdl/static/js/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script>
	<script src="/hmdl/static/js/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
	<style>
	    .seltBk {
	       background: #C24AB1;
	    }
	    .seltSk {
	       background: #4a8bc2;
	    }
	    #showBal:HOVER{
	    	border:1px solid silver;
	    }
	    .l-grid-header{
	    	display: none
	    }
	    /* .l-dialog-tc{
	    	background: -webkit-gradient(linear, 0 0, 0 100%, from(#FFFF99), to(#FFFF99));
	    } */
	    .l-dialog td, .l-dialog tr, .l-dialog td div{
    	    font-size: 13px;
	    }
	    .l-grid-row-cell{
	    	border-right: 0px solid #DFDFDF;
	    }
	    button{
	    	background-color: #999999;
	    }
	    .l-taskbar{
    	    width: auto;
    	    background-image:none;
    	    position:absolute;
	    }
	    .l-taskbar-task-active{
	    	margin-left:-109px;
	    	position: fixed;
	    }
	    .l-dialog-content-noimage{
	    	padding:0px;
	    }
	    .l-grid-body-table{
	    	width: 100% !important;
	    	background: #FFFFFF;
	    }
	    /* 自动换行 */
	    .l-grid td div{
	    	height: auto !important;
	    }
	    /* 隐藏分页页面 */
	    .l-bar-group select{
	    	display: none !important;
	    }
	</style>
</head>
<body style="padding: 0">
    <div id="header" class="navbar navbar-inverse" style="position: relative;display:block;">
        <div class="navbar-inner">
            <div class="container-fluid">
                <div class="brand" style="color:#fff">
                <img src="static/images/taoyuan.png" style="height:28px"/></div>
                <div class="topmenu">
                    <ul class="menu"></ul>
                </div>
                <div class="top-nav ">
                    <ul class="nav pull-right top-menu" id="myInfo">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" style="display:block;padding:0px;margin:0;margin-top: -5px;">
                                <span style="width:32px;height:32px; overflow:hidden;border-radius: 50% !important; border: 2px solid rgba(255,255,255,0.5);float:left;">
                                    <img width="32px" height="32px" src="static/images/user_info.png" />
                                </span>
                                <span style="float:left;margin-left:10px;line-height: 28px;margin-top: 2px;">
                                    <span class="username" style="font-family:微软雅黑;font-weight:800">${username}</span>
                                    &nbsp;<b class="caret"></b>
                                </span>
                                <span style="clear:both;"></span>
                            </a>
                            <ul class="dropdown-menu logout" style="font-family:微软雅黑;">
                                <li><a href="javascript:getInfo()"><img style="margin-top:-3px" width="18px" height="18px" src="static/images/hmdl/grzl.png" />&emsp;个人资料</a></li>
                                <li><a href="javascript:changePwd()"><img style="margin-top:-3px" width="18px" height="18px" src="static/images/hmdl/xgmm.png" />&emsp;修改密码</a></li>
                                <li><a href="javascript:loginOut()"><img style="margin-top:-3px" width="18px" height="18px" src="static/images/hmdl/tc.png" />&emsp;退出系统</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>    
<div id="bottom" style="position: relative;">
	<!-- 展开显示 -->
	<div id="sidebar">
        <ul class="sidebar-menu" data-index="0">
            <li class="sub-menu active">
                <a id="tab_a" href="javascript:void(0);">
                    <i class="icon-dashboard"></i>
                    <span class="currentmenu"><i class="fa fa-home"></i>&nbsp;&nbsp;导航菜单</span>
                </a>
            </li>
            <!-- 菜单权限显示  -->
			<c:forEach var="pm" items="${pmlist}">
				<li class="sub-menu">
					<a href="javascript:;" class="tab_a"><i class="${pm.icons}" aria-hidden="true"></i>&nbsp;&nbsp;<span>${pm.name}</span><span class="arrow"></span></a>
					<ul class="sub">
					    <c:forEach var="cm" items="${cmlist}">
					        <c:set var="pmid" scope="session" value="${pm.id}"/>
					        <c:set var="cmparentId" scope="session" value="${cm.parentId}"/>
					        <c:if test="${pmid == cmparentId}">
					            <li><a class="link" href="javascript:;"  url="${cm.action}" tabid="setting-report${cm.id}"><i class="${cm.icons}" aria-hidden="true"></i>&nbsp;&nbsp;${cm.name}</a></li>
					        </c:if>
					    </c:forEach>
					</ul>
				</li>
			</c:forEach>
        </ul>
    </div>
    <span id="showBal" style="text-align:center;width: 16px;height: 22px;position:absolute ;margin:7px 0px 0px 0px;padding:6px 0px 0px 0px;z-index: 200">
     	<a href="javascript:;" onclick="btnHide()"><img width="14px" height="14px" src="/hmdl/static/images/icon14.png" id="btn"></img></a>
    </span>
    <div id="maincontent"style="width:100%">
        <div tabid="home" title="学校主页" >
            <div style="font-size:36px;font-family:微软雅黑;font-weight:bold;">
                <div style="width: 100%;margin:auto;">
					<img src="static/images/main1.jpg" style="width: 100%;height: 100%;margin: 0rem;padding: 0rem" alt="school">
                    <%--<div style="text-align:left;margin-left:50px;height:80px;">桃园小学综合办公管理系统</div>--%>
                </div>
            </div>
        </div>
        
    </div>
    
    
    <!-- 修改密码 -->
	<div id="changePwd" style="display:none;">
	    <form name="changePwdForm" id="changePwdForm" >
	        <table  class="default">
		        <tr style="height:50px" >
		            <td colspan="4" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
		            <i class="fa fa-cog"></i>&emsp;修改密码</td>
		        </tr>
		        <tr>
		            <td align="right">&emsp;原密码：</td>
		            <td align="left" colspan="3">
		               <input id="old_password" style="width:302px;text-indent:5px;" name="old_password" placeholder="请输入原密码" type="password"/>
		               &emsp;<span style="color:red">*</span>&emsp;6~16个字符，区分大小写
		            </td>
		        </tr>
		        <tr>
		            <td align="right">&emsp;新密码：</td>
		            <td align="left" colspan="3">
		               <input id="new_password" style="width:302px;text-indent:5px;" name="new_password" placeholder="请输入新密码" type="password"/>
		               &emsp;<span style="color:red">*</span>&emsp;6~16个字符，区分大小写
		            </td>
		        </tr>
		        <tr>
		            <td align="right">&emsp;密码确认：</td>
		            <td align="left" colspan="3">
		                <input id="repassword" style="width:302px;text-indent:5px;" name="repassword" placeholder="请再次输入新密码" type="password"/>
		                &emsp;<span style="color:red">*</span>&emsp;请再次填写密码
		            </td>
		        </tr>
		        <tr style="height:50px" >
		            <td colspan="4" align="center">		   
		                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;确认&nbsp;&nbsp;</button>
		                &emsp;
		                <button type="button" class="btn btn-warning" id="changePwdCancel"><i class="fa fa-close"></i>&nbsp;&nbsp;关闭&nbsp;&nbsp;</button>
		            </td>
		         </tr>
		     </table>
	    </form>
	</div>
	
	<!-- 个人信息 -->
	<div id="userInfoWin" style="display:none;">
	    <table  class="default">
		    <tr style="height:50px" >
		        <td colspan="4" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
		        <i class="fa fa-cog"></i>&emsp;个人资料</td>
		    </tr>
		    <tr>
		        <td align="right">&emsp;系统编号：</td>
		        <td align="left" colspan="3">
		           <input id="user_sn" style="width:200px;text-indent:5px;" name="user_sn" disabled="disabled" type="text"/>
		        </td>
		   </tr>
		    <tr>
		        <td align="right">&emsp;角色名称：</td>
		        <td align="left">
		           <input id="role_name" style="width:200px;text-indent:5px;" name="role_name" disabled="disabled" type="text"/>
		        </td>
		        <td align="right">&emsp;用户昵称：</td>
		        <td align="left">
		           <input id="user_name" style="width:200px;text-indent:5px;" name="user_name" disabled="disabled" type="text"/>
		        </td>
		   </tr>
		   <tr>
		        <td align="right">&emsp;登录名称：</td>
		        <td align="left">
		           <input id="login_name" style="width:200px;text-indent:5px;" name="login_name" disabled="disabled" type="text"/>
		        </td>
		        <td align="right">&emsp;联系方式：</td>
		        <td align="left">
		           <input id="user_tel" style="width:200px;text-indent:5px;" name="user_tel" disabled="disabled" type="text"/>
		        </td>
		   </tr>
		   <tr style="height:50px" >
		        <td colspan="4" align="center">
		        <button type="button" id="userInfoWinClose" class="btn btn-info"><i class="fa fa-close"></i>&nbsp;&nbsp;关闭&nbsp;&nbsp;</button></td>
		    </tr>
		</table>
	</div>
</div>
<!-- 实时报警窗口 -->
<div id="showWarning" style="display: none">
	<div id="grid"></div>
</div>    
    
<script type="text/javascript">
$.ligerui.controls.Dialog.prototype._borderX = 2;
$.ligerui.controls.Dialog.prototype._borderY = 0;
var grid;
var warningDialog;
var varConfigList = new Array();//所有变量配置信息
var url = 'ws://192.168.62.225:8090/fr';
var ws = null;


$(function() {
  	//获取所有的变量报警配置
	getVarConfigList();
    $("#btnSetIcon").bind("click",
    function() {
        doChart();
    });
    var headerHeight = $("#header").outerHeight();
    var winH = $(window).height(),
    winW = $(window).width();
    var jsidebar = $("#sidebar"),
    jmaincontent = $("#maincontent");
    jsidebar.height(winH - headerHeight);
    jmaincontent.height(winH - headerHeight);
    jmaincontent.width(winW - jsidebar.outerWidth());

    //布局初始化
    var bodyHeight = $(window).height() - 65;

    var homepage = true;
    var tab;
    tab = jmaincontent.ligerTab({
        height: bodyHeight,
        contextmenu: true
    });

    //tabid计数器，保证tabid不会重复
    var tabidcounter = 0;
    window.setTabTitle = function(tabid, title) {
        tab.setTabItemTitle(tabid, title);
    };
    window.setTabSrc = function(tabid, url) {
        url = ne.getAppUrl(url);
        tab.setTabItemSrc(tabid, url);
    };
    window.openTab = function(options) {
        var p = options || {};
        p.url = ne.getAppUrl(p.url);
        var tabid = p.tabid,
        text = p.text || p.title,
        url = p.url;
        if (tab.isTabItemExist(tabid)) {
            var tabTitle = tab.getTabItemTitle(tabid),
            tabSrc = tab.getTabItemSrc(tabid);
            if (tabTitle != text) {
                tab.setTabItemTitle(tabid, text);
            }
            if (tabSrc != url) {
                tab.setTabItemSrc(tabid, url);
            }
            tab.selectTabItem(tabid);
            return;
        }
        tab.addTabItem(p);
    };

    //刷新 iframe方法
    window.callFrame = function(frameName, fnName) {
        var ele = $("#" + frameName);
        if (!ele.length) return;
        ele = ele[0];
        if (!ele.contentWindow || !ele.contentWindow[fnName]) return;
        ele.contentWindow[fnName]();
    }

    window.openDialog = function(options) {
        var w = $(window).width(),
        h = $(window).height();
        var p = $.extend({
            height: h * 0.9,
            width: w * 0.9,
            showMax: false,
            showToggle: true,
            showMin: false,
            isResize: true,
            slide: false
        },
        options);
        return $.ligerDialog.open(p);
    }

    $("#sidebar a.link").click(function() {
        $('.link').removeClass("seltBk");
        $(this).addClass("seltBk");
        var jlink = $(this);
        var tabid = jlink.attr("tabid");
        var url = jlink.attr("url");
        if (!url) return;
        if (!tabid) {
            tabidcounter++;
            tabid = "tabid" + tabidcounter;
            jlink.attr("tabid", tabid);
            if (url.indexOf('?') > -1) url += "&";
            else url += "?";
            url += "MenuNo=" + jlink.attr("menuno");
            jlink.attr("url", url);
        }
        openTab({
            tabid: tabid,
            text: jlink.text(),
            url: url
        });
    });

    $('#sidebar .sub-menu > a').click(function() {
        var last = jQuery('.sub-menu.open', $('#sidebar'));
        last.removeClass("open");
        jQuery('.arrow', last).removeClass("open");
        jQuery('.sub', last).slideUp(200);
        var sub = jQuery(this).next();
        if (sub.is(":visible")) {
            jQuery('.arrow', jQuery(this)).removeClass("open");
            jQuery(this).parent().removeClass("open");
            sub.slideUp(200);
        } else {
            jQuery('.arrow', jQuery(this)).addClass("open");
            jQuery(this).parent().addClass("open");
            sub.slideDown(200);
        }
    });

    $("#myInfo").mouseover(function() {
        var isOpen = $(this).parent().hasClass("open");
        $(".dropdown").addClass("open");
      
        /* if (isOpen) {
            $(this).parent().removeClass("open");
        } else {
            $(this).parent().addClass("open");
        } */
    });
    $("#myInfo").mouseout(function() {
        var isOpen = $(this).parent().hasClass("open");
        $(".dropdown").removeClass("open");
        /* if (isOpen) {
            $(this).parent().removeClass("open");
        } else {
            $(this).parent().addClass("open");
        } */
    });
    //加载实时报警信息
    var dialog;
    //显示报警弹窗
    //showWarning();
  	//右下角小窗口
    //var tip = null;
    //开启WebSocket
    //openClick();
});
//打开链接
function openClick() {
    ws = new WebSocket(url);
    ws.onopen = function (e) {
        console.log('链接打开');
        ws.send('{action:"connect",bds:"waringlist"}');
    }
    ws.onmessage = function (e) {
        console.info('接受到数据：' + e.data);
        var d = JSON.parse(e.data);
        //console.info('接受到数据2：' + d.smd_id);
        //刷新表格
        grid.loadData();
        //弹出报警列表
        popUp();
        //右下角报警提示
        //tip(d);
        //播报声音
        warning(d);
        
    }
    ws.onclose = function (e) {
        //e  CloseEvent对象
        //e.code 关闭代码编号标识
        //e.reason 关闭原因
        console.info(e);
        //console.log('链接已经关闭');
    }
    ws.onerror = function (e) {
        console.info(e);
        //console.log('发生异常:'+e.message);
    }
}
//弹出报警对话框
function popUp(){
    //有报警就弹出
    warningDialog.active();
}
/* //右下角提示
function tip(d) {
    var substationName;//变电所
    var varName;//变量名称
    var remark;//描述
    var warningMessage;//报警信息
    var warningBrief;//报警备注
    var funcCode;//功能码
  	//遍历
    for(var i = 0;i<varConfigList.length;i++){
	    if(varConfigList[i].id==d.smd_id){
	        substationName = varConfigList[i].substationName;
	        varName = varConfigList[i].varName;
	        remark = varConfigList[i].remark;
	        warningMessage = varConfigList[i].warningMessage;
	        warningBrief = varConfigList[i].warningBrief;
	        funcCode = varConfigList[i].funcCode;
		}
    }
    //拼接显示内容
    var content = "<p style='font-weight:bold'>变电所："+substationName+"</p>"+
    			  "<p>变量："+varName+"</p>"+
    			  "<p>名称："+remark+"</p>"+
    			  "<p>信息："+warningMessage+"</p>"+
    			  "<p>功能码："+funcCode+"</p>";
    //弹出提示
    tip = $.ligerDialog.tip({ title: '新报警信息', content: content });
} */
//声音报警
function warning(d){
    for(var i = 0;i<varConfigList.length;i++){
        //遍历
        if(varConfigList[i].id==d.smd_id && varConfigList[i].warningIsVoice==1){
            var audio = new Audio(varConfigList[i].warningVoiceType);//加载声音文件
            if(varConfigList[i].warningTimes==1){//播报1次
                audio.play();
            }else if(varConfigList[i].warningTimes==3){//播报3次
                audio.play();
                audio.play();
                audio.play();
            }
        }
    }
}
//获取所有变量配置
function getVarConfigList(){
    $.ajax({
	    type: 'get',
	    url: "${pageContext.request.contextPath}/getVarConfigList",
	    cache: false,
	    dataType: 'json',
	    success: function(data) {
	        if (data && data.retCode == 100) {
	            varConfigList = data.retObj;
	            /* for(var i = 0;i<varConfigList.length;i++){
	    	    	console.log("变量名："+varConfigList[i].varName+"/NUMBER_NAME:"+varConfigList[i].numberName+"/SMD_ID:"+varConfigList[i].id+"/WARNING_TYPE:"+varConfigList[i].warningType);
	    		}
	            console.log(varConfigList); */
	        } else {
				console.log("获取不到变量配置！");
			}
	    }
	});
}
//加载实时报警信息
function showWarning() {
    //初始化表格
    initGrid();
    //打开窗口
    warningDialog = $.ligerDialog.open({
        top: 95,
        left: (window.screen.width - 1031),
        width: 1031,
        target: $("#showWarning"),
        //width: null,
        height: 382,
        showMax: false,
        showMin: true,
        //showToggle: true, 
        allowClose: false,
        show: false,
        //初始化时不显示
        isResize: true,
        modal: false,
        //窗口外可以操作，true时无法操作
        title: "实时报警信息"
    });
}
$(function() {
   $("#ssbj").click(function(){
       alert();
   }) 
});
function pop() {
    alert("1222");
}
//加载表格
function initGrid() {
    grid = $("#grid").ligerGrid({
        columns: [{
            display: '报警时间',
            name: 'callTime',
            width: 170
        },
        {
            display: '变电所',
            name: 'substationName',
            width: 200
        },
        {
            display: '名称',
            name: 'equName',
            width: 300
        },
        {
            display: '信息',
            name: 'message',
            width: 220
        },
        {
            display: '操作',
            width: 131,
            render: function(item) {
                return "<button type='button' onclick=confirmInfo('" + item.id + "') style='background-color: #999999;border:0;z-index:100px;'  class='btn btn-default  btn-small btn-mini'>确&nbsp;&nbsp;&nbsp;&nbsp;认</button>"
                + "&nbsp;<button type='button' onclick=playCall('" + item.id + "') style='background-color: #999999;border:0;z-index:100px;'  class='btn btn-default  btn-small btn-mini'>电&nbsp;&nbsp;&nbsp;&nbsp;话</button>";
            }
        }/* ,
        {
            display: ''
        } */
        ],
        dataAction: 'server',
        url: 'getWarningList',
        root: 'data',
        record: 'count',
        height:410,
        pageSize: 10,
        isScroll: false,
        checkbox: false,
        rownumbers: false,
        rowAttrRender: function(rowdata) {
            if (rowdata.alarmLevel == "3") {
                return "style='color:#0134FF;font-weight:bold;'";
            }
            if (rowdata.alarmLevel == "2") {
                return "style='color:#FF7D23;font-weight:bold;'";
            }
            if (rowdata.alarmLevel == "1") {
                return "style='color:#CE0606;font-weight:bold;'";
            }
        },
    });

}
//确认报警信息按钮
function confirmInfo(id) {
    if(id == null || id == ''){
        $.ligerDialog.error("确认失败！","提示");
        return;
    }else{
	    $.ajax({
		    type: 'get',
		    url: "${pageContext.request.contextPath}/alarm/changestate",
		    data: {
		        id: id
		    },
		    cache: false,
		    dataType: 'json',
		    success: function(data) {
		        if (data && data.retCode == 100) {
		            var success = $.ligerDialog.success("确认成功！","提示");
		            setTimeout(function(){success.close();},500);
		            grid.loadData();
		        } else {
		            $.ligerDialog.warn("确认失败！","提示");
		            return;
				}
		    }
		});
    }
}
//推送消息(该功能暂时不用)
/* function sendMessage(id) {
    if(id == null || id == ''){
        $.ligerDialog.error("推送消息确认失败！","提示");
        return;
    }else{
	    $.ajax({
		    type: 'get',
		    url: "${pageContext.request.contextPath}/alarm/sendMessage",
		    data: {
		        id: id
		    },
		    cache: false,
		    dataType: 'json',
		    success: function(data) {
		        if (data && data.retCode == 100) {
		            var success = $.ligerDialog.success("推送消息确认成功！","提示");
		            setTimeout(function(){success.close();},500);
		            grid.loadData();
		        } else {
		            $.ligerDialog.warn("推送消息确认失败！","提示");
		            return;
				}
		    }
		});
    }
} */
//打电话
function playCall(id) {
    if(id == null || id == ''){
        $.ligerDialog.error("电话确认失败！","提示");
        return;
    }else{
	    $.ajax({
		    type: 'get',
		    url: "${pageContext.request.contextPath}/alarm/playCall",
		    data: {
		        id: id
		    },
		    cache: false,
		    dataType: 'json',
		    success: function(data) {
		        if (data && data.retCode == 100) {
		            var success = $.ligerDialog.success("电话确认成功！","提示");
		            setTimeout(function(){success.close();},500);
		            grid.loadData();
		        } else {
		            $.ligerDialog.warn("电话确认失败！","提示");
		            return;
				}
		    }
		});
    }
}
//用户信息窗口
var userInfoWin;
// 个人资料
function getInfo() {
    $.ajax({
        type: 'get',
        url: "getUserInfo",
        cache: false,
        dataType: 'json',
        success: function(data) {
            if (data.retCode == -1) {
                $.ligerDialog.warn(data.msg, "提示");
                return;
            } else {
                $("#user_sn").val(data.retObj.userSn);
                $("#user_name").val(data.retObj.userName);
                $("#user_tel").val(data.retObj.phoneNum);
                $("#role_name").val(data.retObj.roleName);
                $("#login_name").val(data.retObj.loginName);
            }

            if (!userInfoWin) {
                userInfoWin = $.ligerDialog.open({
                    target: $("#userInfoWin"),
                    height: 280,
                    width: 805,
                    title: "个人资料",
                    isHidden: true,
                    isResize: true,
                    showMax: false,
                    showMin: false
                });
                //绑定事件
                $("#userInfoWinClose").click(function() {
                    userInfoWin.hide();
                });
            } else {
                userInfoWin.show();
            }
        }
    });
}

var changePwdWin;
// 修改密码
function changePwd() {
    $("#changePwdForm")[0].reset();
    if (!changePwdWin) {
        changePwdWin = $.ligerDialog.open({
            target: $("#changePwd"),
            height: 280,
            width: 805,
            title: "更改密码",
            isHidden: true,
            isResize: true,
            showMax: false,
            showMin: false
        });
        //绑定事件
        $("#changePwdCancel").click(function() {
            changePwdWin.hide();
        });
    } else {
        changePwdWin.show();
    }
}

// 新增表单提交验证
$('#changePwdForm').validate({
    rules: {
        old_password: {
            required: true,
            password: true,
        },
        new_password: {
            required: true,
            password: true,
        },
        repassword: {
            required: true,
            equalTo: "#new_password"
        }
    },
    messages: {
        old_password: {
            required: "请输入旧密码！"
        },
        new_password: {
            required: "请输入新密码！"
        },
        repassword: {
            required: "请再次输入新密码！",
            equalTo: "两次密码输入的不一致！"
        }
    },
    showErrors: function(errorMap, errorList) {
        var msg = "";
        $.each(errorList,
        function(i, v) {
            $("#" + v.element.id).ligerTip({
                content: v.message,
                callback: function() {
                    setTimeout(function() {
                        $("#" + v.element.id).ligerHideTip(); //1.5秒延迟后关闭tip
                    },
                    1500);
                }
            });
        });
        if (msg != "") {
            $.ligerDialog.warn(msg, "提示");
        }
    },
    submitHandler: function(form) {
        if ($("#new_password").val() == $("#old_password").val()) {
            $("#new_password").ligerTip({
                content: "新密码不能与老密码一致！",
                callback: function() {
                    setTimeout(function() {
                        $("#new_password").ligerHideTip(); //1.5秒延迟后关闭tip
                    },
                    1500);
                }
            });
            return;
        }
        $.ajax({
            type: 'post',
            url: "changePwd",
            data: $("#changePwdForm").serialize(),
            cache: false,
            dataType: 'json',
            success: function(data) {
                if (data && data.msg == "") {
                    $.ligerDialog.success('修改成功！', "提示",
                    function(opt) {
                        changePwdWin.hide();
                    });
                } else {
                    $.ligerDialog.error(data.msg, "提示");
                }
            }
        });
    },
    onfocus: false,
    onfocusout: false,
    onkeyup: false,
    onkeydown: false
});

// 登出操作
function loginOut() {
    $.ligerDialog.confirm("确认退出当前账号吗？", "提示",
    function(confirm) {
        if (confirm) {
            // 注销后台session登录信息
            $.ajax({
                type: 'post',
                cache: false,
                url: "loginOut",
                success: function(result) {
                    // 进行迁移到登录画面
                    location.href = decodeURIComponent("login");
                }
            });
        }
    });
}
function btnHide() {
    var dis = $("#header").prop("display");
    $("#header").hide();
    $("#sidebar").hide();
    document.getElementById("maincontent").style.height = (window.innerHeight) + 'px';
    document.getElementById("maincontent").style.width = (window.innerWidth) + 'px';
    $(".l-tab-content").height((window.innerHeight - 40) + 'px');
    $(".l-tab-content-item").height((window.innerHeight - 40) + 'px');
    $("#showBal").html("<a href='javascript:;' style='color: black' onclick='btnShow()'><img width='14px' height='14px' src='/hmdl/static/images/icon13.png' id='btn'></img></a>");

}
function btnShow() {
    var dis = $("#header").prop("display");
    $("#header").show();
    $("#sidebar").show();
    document.getElementById("maincontent").style.height = '';
    document.getElementById("maincontent").style.width = '';
    $(".l-tab-content").height((window.innerHeight - 90) + 'px');
    $(".l-tab-content-item").height((window.innerHeight - 90) + 'px');
    $("#showBal").html("<a href='javascript:;' style='color: black' onclick='btnHide()'><img width='14px' height='14px' src='/hmdl/static/images/icon14.png' id='btn'></img></a>");

}
//报警菜单淡入/淡出
/* $(function() {
    var left = -109; //margin-left初始值
    var temp; //临时存
    var set1; //定时器
    var set2; //定时器
    //淡出
    $(".l-taskbar-task-active").mouseenter(function() {
        set1 = setInterval(function() {
            if (left >= 0) {
                window.clearInterval(set1); //清除
                return;
            }
            left += 1;
            temp = left;
            $(".l-taskbar-task-active").css("marginLeft", left + "px"); //设置
        },
        0.1);
    });
    //淡入
    $(".l-taskbar-task-active").mouseleave(function() {
        window.clearInterval(set1);
        set2 = setInterval(function() {
            if (temp <= -109) {
                window.clearInterval(set2); //清除
                left = -109; //恢复初始值
                return;
            }
            temp -= 1;
            $(".l-taskbar-task-active").css("marginLeft", temp + "px"); //设置
        },
        0.1);
    });
}) */

//报警菜单淡入/淡出
$(function() {
    //<i style='font-size:30px;color:white;' class='fa fa-step-forward'></i>
    $('.l-taskbar-task-active').mouseenter(function(){
        $('.l-taskbar-task-active').animate({left:'109px'});//菜单块向右移动
    });
    $('.l-taskbar-task-active').click(function(){
        $('.l-taskbar-task-active').animate({left:'0px'}); //菜单块向左移动
    });
})

function layer(){
    $.layer.msg('hello');
}
</script>
</body>
</html>