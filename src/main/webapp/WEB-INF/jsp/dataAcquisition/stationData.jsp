<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script> 
    <script src="/hmdl/static/js/jquery/jquery.form.js" type="text/javascript"></script> 
</head>
<style>
.connectClass .l-dialog-body{
	text-align:center;
}
.connectClass .l-dialog-buttons{
}
</style>
<!-- body部分 -->
<body id="body" style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">
<div id="layout1">
	<!-- 左侧树 -->
	<div position="left" title="变电所一览" style="height:calc(100% - 40px);overflow: auto; ">
		<!-- 变电所查询条件部分 -->
		<div id="org_search" style="margin-left:8px;margin-top:0px;">
			<ul>
				<li class="label">
					<input type="text" placeholder="输入名称进行搜索" style="text-indent:5px;height:23px;border-radius: .25em;width:60%;margin-left:0;margin-top: 5px" name="search_substationName" id="search_substationName"  />					
					<button type="button" id="org_btnSearch" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x"><i class="fa fa-search"></i></button>
					<button type="button" id="org_expend" onclick="expend()" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x"><i class="fa fa-plus"></i></button>
					<button type="button" id="org_unexpend" onclick="unexpend()" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x;display:none"><i class="fa fa-minus"></i></button>
				</li>
			</ul>
		</div>
		<div id="orgInfoTree"></div><br/><br/>
		<input type="hidden" id="org_sn" name="org_sn" value=""/>
		<input type="hidden" id="org_name" name="org_name" value=""/>
	</div>
	<div position="center"  title="数据采集" >
		<!-- 标题部分 -->
		<h3 class="hmdl_title_h3">数据采集
		    <span class="hmdl_title_span">
		        &nbsp;/&nbsp;站点库数据
		    </span>
		</h3>
		<hr class="hmdl_title_hr"/>
		
		<!-- 查询条件部分 -->
		<div id="grid_search" style="margin-left:8px;margin-top:20px;">
		    <form name="searchForm" id="searchForm">
		        <ul>
		            <li class="label">
		                &emsp;变电站名称：<input type="text" id="search_substationName" name="substationName" placeholder="搜索变电站" style="text-indent:5px;height:23px;border-radius: .25em;"/>
						&emsp;简称：<input type="text" id="search_shortname" name="shortname" placeholder="搜索简称" style="text-indent:5px;height:23px;border-radius: .25em;"/>
						&emsp;ip地址：<input type="text" id="search_ipAddr" name="ipAddr" placeholder="搜索ip地址" style="text-indent:5px;height:23px;border-radius: .25em;"/>
						&emsp;<button id="btnSearch" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
		                &emsp;<button id="doReport" type="button" class="btn btn-success  btn-small btn-mini"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;导出&nbsp;&nbsp;</button>
		            </li>
		        </ul>
		    </form>
		</div>
		<br/>
	
		<!-- 列表部分 -->
		<div id="grid"></div>
		
		<!-- 增改部分 -->
		<div id="edit" style="display:none;">
		    <form name="saveForm" id="saveForm" >
		    <input type="hidden" id="id" name="id" />
		    <table  class="default">
		        <tr style="height:50px" >
		            <td colspan="4" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
		            <i class="fa fa-address-book"></i>&emsp;站点库数据信息</td>
		        </tr>  		       
		        <tr>
		            <td align="right">&emsp;选择站点：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="1">
		                <input id="substationName" name="substationName" style="text-indent:5px;width:180px" readonly="readonly" placeholder="请选择"/>
		                <input type="hidden" name="substationSn" id="substationSn"/>
		            </td>
		         </tr>
		         <tr>
		            <td align="right">&emsp;站点缩写英文：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="3">
		                <input id="shortname" name="shortname"  style="text-indent:5px;width:180px" placeholder="请输入站点缩写英文"/>
		            </td>
		         </tr>
		         <tr>
		            <td align="right">&emsp;ip地址：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="3">
		                <input id="ipAddr" name="ipAddr"  style="text-indent:5px;width:180px" placeholder="请输入ip地址"/>
		            </td>
		         </tr>
		         <tr>
		            <td align="right">&emsp;端口：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="3">
		                <input id="port" name="port"  style="text-indent:5px;width:180px" placeholder="请输入端口"/>
		            </td>
		         </tr>		         
		         <tr>
		            <td align="right">&emsp;遥信数量：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="3">
		                <input id="yxAmount" name="yxAmount"  style="text-indent:5px;width:180px" placeholder="请输入遥信数量"/>
		            </td>
		         </tr>
		         <tr>
		            <td align="right">&emsp;遥测数量：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="3">
		                <input id="ycAmount" name="ycAmount"  style="text-indent:5px;width:180px" placeholder="请输入遥测数量"/>
		            </td>
		         </tr>
		         <tr>
		            <td align="right">&emsp;遥信功能码：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="3">            
		                <select id="yxFuncode" name="yxFuncode" style="width:180px">
							  <option value=""> == 请选择  == </option>
							  <option value="01"> == 读线圈01  == </option>
							  <option value="02"> == 读离散量输入02 == </option>
					    </select>
		            </td>
		         </tr>
		         <tr>
		            <td align="right">&emsp;遥测功能码：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="3">
		                <select id="ycFuncode" name="ycFuncode" style="width:180px">
							  <option value=""> == 请选择  == </option>
							  <option value="03"> == 寄存器03  == </option>
							  <option value="04"> == 读寄存器04  == </option>
					    </select></td>
		         </tr>
		         <tr id="edit_add_yx" style="display:none">
		            <td align="right">&emsp;增加遥信数量：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="3">
		                <input id="yxAmount_add" name="yxAmount" disabled="disabled" style="text-indent:5px;width:180px" placeholder="请输入遥信数量"/>
		            </td>
		         </tr>
		         <tr id="edit_add_yc" style="display:none">
		            <td align="right">&emsp;增加遥测数量：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="3">
		                <input id="ycAmount_add" name="ycAmount" disabled="disabled" style="text-indent:5px;width:180px" placeholder="请输入遥测数量"/>
		            </td>
		         </tr>
		    </table>
		    <table align="center" style="margin-top: 10px">
		        <tr>
		            <td>
		                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;提交&nbsp;&nbsp;</button>
		                &emsp;
		                <button type="button" class="btn btn-warning" onclick="editCancel()"><i class="fa fa-close"></i>&nbsp;&nbsp;返回&nbsp;&nbsp;</button>
		            </td>
		        </tr>
		    </table>
		    </form>
		</div>
	</div>
</div>
<!-- 导出用Form -->
<form action="../report/show" method="post" target="_blank" id="reportForm">
	<input type="hidden" name="fparam" id="fparam"></input>
	<input type="hidden" name="fcolumn" id="fcolumn"></input>
	<input type="hidden" name="fname" id="fname"></input>
	<input type="hidden" name="fclassname" id="fclassname"></input>
	<input type="hidden" name="fmenu" id="fmenu"></input>
	<input type="hidden" name="ftitle" id="ftitle"></input>
	<input type="hidden" name="fexport" id="fexport"></input>
	<input type="hidden" name="furl" id="furl"></input>
</form>
<div id="connectTest"  style="display: none;overflow:hidden;"><a>开始连接测试：&emsp;</a><input type="text" id="connectIp" readonly="readonly"/></div>
<!-- 导入配置 -->
<div id="btn_file_div" style="display: none">
	<form id="excelForm" enctype="multipart/form-data">
		<input id="btn_file" name="excelFile" type="file" accept=".xls,.xlsx"/>
	</form>
</div>
<!-- excel模板下载 -->
<form id="target" action="${pageContext.request.contextPath}/stationData/download" method="post"></form>
<!-- 增改部分计算变量 -->
<div id="editConfig" style="display:none;">
	<!-- 配置部分 -->
	<div id="gridConfig"></div>
	<table align="center" style="margin-top: 10px">
        <tr>
            <td>
                <button type="submit" class="btn btn-success" onclick="configConfirm()"><i class="fa fa-check"></i>&nbsp;&nbsp;提交&nbsp;&nbsp;</button>
                &emsp;
                <button type="button" class="btn btn-warning" onclick="configCancel()"><i class="fa fa-close"></i>&nbsp;&nbsp;返回&nbsp;&nbsp;</button>
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
var grid;
var searchWin;
var gridConfig;
var ycWin;
$(function(){
	//布局设置
    $("#layout1").ligerLayout({
    leftWidth: 200,
    });
    //变电所树检索
    substationAll();
    initGrid();
    // 绑定查询按钮
	$("#btnSearch").click(function () {
		var parms = serializeObject($("#searchForm"));
   	    for(var p in parms){
   	        grid.setParm(p,parms[p].trim());
   	    }
   	    grid.loadData();
   	});
	//绑定变电所查询按钮
	$("#org_btnSearch").click(function () {
	    substationAll();
	});
	// 绑定导出按钮
	$("#doReport").click(function () {
	 	// 用于获取列信息和搜索信息
		$('#fparam').val(JSON.stringify(grid.options.parms));
		$('#fcolumn').val(JSON.stringify(grid.getColumns()));
		// 报表的唯一标示
		$('#fname').val("stationDataReport");
		// 报表容器的className
		$('#fclassname').val("fr.cn.com.edu.taoyuan.dataAcquisition.bean.StationDataBean");
		// 报表的默认标题
		$('#ftitle').val("站点库数据报表");
		$('#fmenu').val("stationData");
		// pdf的加载地址【必输项】
		$('#furl').val("reportpdf");//新版本reportnew.js无默认值，必须填写
		// 报表导出的页面地址【必输项】
		$('#fexport').val("report");//新版本reportnew.js无默认值，必须填写
		// 导出
		$('#reportForm').submit();
   	});
    
    function initGrid(){
        grid=$("#grid").ligerGrid({
            title:'&nbsp;&nbsp;<span style="color:#386792">站点库数据信息一览</span>',
            headerImg:'/hmdl/static/images/list.png',
            columns: [
				{ display: '变电站名称', name: 'substationName',width:150},
				{ display: '简称', name: 'shortname',width:120},
                { display: 'ip地址', name: 'ipAddr',width:120},
                { display: '端口', name: 'port',width:120},
                { display: '遥信数量', name: 'yxAmount',width:120},
                { display: '遥测数量', name: 'ycAmount',width:120},
                { display: '回路', name: 'circuit',width:120},
                { display: '更新时间', name: 'updateDate',dateFormat: "yyyy-MM-dd hh:mm:ss",width:150},
            ],
            toolbar: {
                items: [
					<%=request.getAttribute("btn")%>,
					{ text: '刷新',click: function(){grid.reload();}, icon: 'fa fa-refresh',color:'primary' },
				]
         	},
            dataAction: 'server',
            url:'search',
        	root:'data',
          	record:'count',
          	height: '97%',
          	pageSize: 10,
            parms:[{name:"isConfig",value:'3'}],
        	enabledSort:true,
            sortnameParmName:'sortname',
            sortorderParmName:'sortorder',
          	pageSizeOptions:[10,15,20,25,30,35],
          	checkbox: false,
          	rownumbers: true,
            onDblClickRow : function (data, rowindex, rowobj)
            {
            	operate(2,data.id);
            } ,
        	onAfterShowData: function ()
            { 
                grid.collapseAll();
            }
        });
    }
    $(function () {
	    $("#saveForm").validator({
	        ignore: ":hidden", theme: "yellow_right", timely: 1, stopOnError: true,
	        rules: { digits: [/^\d+(\d+)?$/, '{0}必须为数字！'],  
	           		 english: [/^\w+$/,'{0}只能英文和数字！'],
	           		 ip:[/^\d+(\.\d+)*$/,'{0}必须为数字！'],
	           		 email:[/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,'{0}格式不正确！']
	        },
	        fields: {
	            shortname: '简称:required,length[1~30],english',
	            port: '端口:required,length[1~10],digits',
	            ipAddr: 'ip地址:required,length[1~30],ip',
	            yxAmount: '遥信数量:required,length[1~30],digits',
	            ycAmount: '遥测数量:required,length[1~30],digits',
	            yxFuncode: '遥信功能码:required',
	            ycFuncode: '遥测功能码:required',
	        },
	        valid: function (form) {formSave(form);}
	    });
    })
});

//新增OR编辑处理
function operate(type,id){
    resetAll();
    $("#shortname").removeAttr("disabled");
	$("#edit_add_yx").hide();
	$("#edit_add_yc").hide();
    $("#yxAmount").removeAttr("disabled");
    $("#ycAmount").removeAttr("disabled");
	$("#yxAmount_add").attr("disabled",true);
	$("#ycAmount_add").attr("disabled",true);
	if(id==null && (type ==2 || type == 5))
	{ 
	    var rows = grid.getSelectedRows();
	    if ((rows == null || rows.length!=1) && type != 1 ) {
	        $.ligerDialog.warn("请选择一条记录！","提示");
	        return;
	    };
	    id = rows[0].id;
	}
	if(type == 1){
	    if($("#org_sn").val()==''){
	        $.ligerDialog.warn("请先选择左侧变电站！","提示");
	        return;
	    }
	    $("#substationName").val($("#org_name").val());
	    $("#substationSn").val($("#org_sn").val());
	}
    if(type == 2){
        showOrHideEdit(2,id);
    }else if(type == 5){
        showOrHideEdit(5,id);
    }else{
        showOrHideEdit(1);
    }
}
//保存表单数据
function formSave(form) {
    var data = serializeObject($("#saveForm"));
    $.ajax({
        type: 'post',
        url: "save",
        data: data,
        cache: false,
        dataType: 'json',
        success: function (data) {
            if (data && data.retCode == 100) {
                $.ligerDialog.success('保存成功！', "提示", function (opt) {
                    $("#grid").show();
                    $("#grid_search").show();
                    $("#edit").hide();
                    grid.reload();
                });
            } else {
                var text = "";
                if (data.errors && data.errors.length) {
                    for (var i = 0; i < data.errors.length; i++) {
                        text = text + "<br/>" + data.errors[i];
                    }
                }
                $.ligerDialog.error(text, "提示");
            }
        }
    });
}
function showOrHideEdit(type,id){
    // 0显示编辑页面
    if(type==1){
        $("#grid").hide();
        $("#grid_search").hide();
        $("#edit").show();
    // 1隐藏编辑页面
    }else if(type==2){
        getDetail(id);
    }else if(type==5){
        getDetail(id,5);  
    }else{
        $("#grid").show();
        $("#grid_search").show();
        $("#edit").hide();
    }
}
//获取表单明细数据
function getDetail(id,type) {
    $.ajax({
        type: 'get',
        url: "getDetail",
        data: { id: id },
        cache: false,
        dataType: 'json',
        success: function (data) {
            loadData(data.retObj);
            $("#grid").hide();
            $("#grid_search").hide();
            $("#edit").show();
            $("#shortname").attr("disabled","disabled");
            $("#yxAmount").attr("disabled","disabled");
            $("#ycAmount").attr("disabled","disabled");  
            if(type==5){
        		$("#edit_add_yx").show();
        		$("#edit_add_yc").show();
        		$("#yxAmount_add").val("");
        		$("#ycAmount_add").val("");
       		    $("#yxAmount_add").removeAttr("disabled");
       		    $("#ycAmount_add").removeAttr("disabled");
            }
        }
    });
}
//删除
function del(){
   	var rows = grid.getSelectedRows();
    if (rows == null || rows.length!=1 ) {
        $.ligerDialog.warn("请选择一条记录！","提示");
        return;
    };
   	var ids="";
   	for(var i=0 ; i < rows.length ; i++){
       	if(i>0){
				ids += "|";
       	}
       	ids += rows[i].id;
   	}
	$.ligerDialog.confirm("将会删除对应所有变量，确定要删除选中记录？","提示", function(confirm){
    	if(confirm){
     		$.post("doDelete",{ids:ids},function(data){
            		if (data.retCode==100) {
            			$.ligerDialog.success('删除成功！',"提示",function(opt){
                    		grid.reload();
                 	});}
            		else if(data.retCode==-700){
                      		$.ligerDialog.error("删除失败！存在", "提示");
                   }else{
                      		$.ligerDialog.error("删除失败！", "提示");
                   }
           },"json")
  		}
	});
}
function editCancel(){
    showOrHideEdit(3);
    resetAll();
}

function resetAll(){
    // FORM清空
    $("#saveForm")[0].reset();
    // 隐藏项目清空
    $("#id").val("");
    $("#edit_type").val("");
}

//展开
function expend(){
    orgTree.expandAll();
    $("#org_unexpend").show();
    $("#org_expend").hide();
}

//折叠
function unexpend(){
    orgTree.collapseAll();
    $("#org_unexpend").hide();
    $("#org_expend").show();
}

//查询变电所type 特殊执行方法
function substationAll(selectid){
	   $.ajax({
	           type : 'post',
	           url : "../stationData/stationDataSearch",
	           data : {tempdata:Date.parse(new Date()),id:selectid,substationName:$("#search_substationName").val().trim()},
	           cache : false,
	           dataType : 'json',
	           success : function(data) {  //如果没有数据，清空变电所树
	                   if(data==null||data==false){
	                           orgTree.clear();
	                   }else{   //加载变电所树
	                           orgTree = $("#orgInfoTree").ligerTree({
	                           nodeWidth: 220,
	                           data: data,
	                           checkbox: false,
	                           idFieldName: 'id',
	                           textFieldName: "text",
	                           parentIDFieldName: "pid",
	                           isExpand: 1,//展开层数1层，区域
	                           slide: true,//是否显示动画
	                           onSelect: function (node, e) {
	                               var data = node.data;
	                               /*改变标题*/
	                               $(".l-layout-center .l-layout-header").html('&emsp;<a style="color:#428bca">站点名称：['+
	                                       data.text+']</a>'
	                                       );
	                               var sn = data.id;
	                               if(sn!=""){
	                                   //获取变电站编号
	                                   $("#org_sn").val(sn);
	                                   //获取变电站名称
	                                   $("#org_name").val(data.text);
	                                   grid.setParm("substationSn", sn);
	                                   grid.setParm("substationName", data.text);
	                                   grid.loadData();
	                                   //跳至第一页
	                                   grid.changePage('first');
	                               }
	                           }
	                       });
	                   }
	           }
	   });
}

//连接测试
function connectTest(){
  var rows = grid.getSelectedRows();
  if (rows == null || rows.length!=1) {
      $.ligerDialog.warn("请选择一条记录！","提示");
      return;
  };
  $("#connectIp").val(rows[0].ipAddr);
	$.ligerDialog.open({
		target: $("#connectTest"),
		height: 100,
		width:300,
		isHidden: true,
		isResize:false,
		cls:"connectClass",
		allowClose: false,
		title: "通讯管理机连接参数设置",
 	    buttons: [
	              { text: '确定', onclick: function (item, dialog) {
	                  dialog.hide();
	                  $.ligerDialog.waitting(rows[0].ipAddr+':'+rows[0].port+'通信连接测试中,请稍候...'); 
	                  // 发送请求至后台验证
	                  $.ajax({
	                      type: "post",
	                      url: "connectTest",
	                      data: { ipAddr: rows[0].ipAddr,port:rows[0].port},
	                      cache: false,
	                      dataType: 'json',
	                      success: function (data) {
	                          $.ligerDialog.closeWaitting(); 
	                          if(data&&data.retCode>0){
		                          $.ligerDialog.success('连接成功！耗时：'+data.msg+'ms', "提示", function (opt) {});
	                          }else{
	                              $.ligerDialog.error('连接失败！耗时：'+data.msg+'ms', "提示");
	                          }

	                      }
	                  });
	              } },
	              { text: '取消', onclick: function (item, dialog) { dialog.hide(); } }
	          ]
         });
}

//导入Excel表格信息
function readExcel(){
    var rows = grid.getSelectedRows();
    if (rows == null || rows.length!=1) {
        $.ligerDialog.warn("请选择一个变电所！","提示");
        return;
    };
    
    $.ligerDialog.open({
		target:$("#btn_file_div"),
        allowClose: false,
		height: 100,
		width: 300,
		title:'<font style="color:blue"><i class="fa fa-table" aria-hidden="true"></i>&nbsp;导入数据（EXCEL）</font>',
		buttons: [
            {  text: '确认导入', onclick: function (item, dialog){
                //路径判空
                if($("#btn_file").val() == ""){
                    $.ligerDialog.warn("请选择表格！","提示");
                    return;
                }
                //获取当前所选变电所的信息，赋值传参
                var substationSn = rows[0].substationSn;
                var ipId = rows[0].id;
                var shortname = rows[0].shortname;
                //表单提交
                $.ligerDialog.waitting('导入中,请稍候...');
                document.getElementById("body").style.cursor="wait";
                setTimeout(function () {
	                $("#excelForm").ajaxSubmit({
	                    type: 'post', // 提交方式 get/post
	                    url: '${pageContext.request.contextPath}/stationData/readExcel', // 需要提交的 url
	                    cache: false,
	                    dataType: 'json',
	                    data:{substationSn:substationSn,ipId:ipId,shortname:shortname},
	                    success:function(data){
	                        $.ligerDialog.closeWaitting();
	                        document.getElementById("body").style.cursor="default";
	                        if(data && data.retCode==100){
	                            $.ligerDialog.success(""+data.msg,"提示");
	                        }else if(data && data.retCode==(-500)){
	                            $.ligerDialog.warn(""+data.msg,"提示");
	                        }else if(data && data.retCode==(-350)){
	                            $.ligerDialog.warn(""+data.msg,"提示");
	                        }
	                    },
	                    error: function() {
						    $.ligerDialog.closeWaitting();
						    document.getElementById("body").style.cursor="default";
						    $.ligerDialog.error("导入操作发生错误，请尝试刷新页面再继续操作！","错误");
						}
	                });
	              	//清空表单
	                document.getElementById("excelForm").reset();
                },2000);
              	
                //隐藏对话框
                dialog.hide();
			}},
			{ text: '关闭', onclick: function (item, dialog){
			    //清空
			    var file = $("#btn_file");
			    file.after(file.clone().val(""));      
			    file.remove();
			  	//隐藏对话框
			    dialog.hide();
			}}
		]
    });
}
//excel模板下载
function download(){
    //document.getElementById("target").click();
    $("#target").submit();
}
//其他配置
function setOther(){
    var rows = grid.getSelectedRows();
    if (rows == null || rows.length!=1) {
        $.ligerDialog.warn("请选择一个变电所！","提示");
        return;
    };
    //初始化
    var typeData = [{ type: "16", text: '16位' }, { type: "32", text: '32位'}];
	// 列表画面定义
	if(!gridConfig){
		gridConfig=$("#gridConfig").ligerGrid({
		    title:'&nbsp;&nbsp;<span style="color:#386792">&nbsp;['+rows[0].substationName+']&nbsp;的遥测总数：'+rows[0].ycAmount+'个</span>',
		    headerImg:'/hmdl/static/images/list.png',
		    columns: [
				{ display: 'id', name: 'id',width:100,hide:true},
				{ display: 'ipId', name: 'ipId',width:100,hide:true},
				{ display: '摇测数量', name: 'amount',width:100,
				    editor: { type: 'int' }
				},
				{ display: '遥测类型', name: 'ycType',width:150,type:'int',
	                editor: { type: 'select', data: typeData, valueField: 'type'},
	                render: function (item)
	                {
	                    if (item.ycType == "16"){
	                        return '16位';
	                    }else if (item.ycType == "32"){
	                        return '32位';
	                    }else{
	                        item.ycType = "32"
	                        return '32位';
	                    }
	                }
				},
		    ],
		    toolbar: {
		        items: [
					{ text: '添加',click: function(){configAdd();}, icon: 'fa fa-plus',color:'success' },
					{ text: '删除',click: function(){configDelete();}, icon: 'fa fa-trash',color:'danger' },
		        ]
		    },
		    dataAction: 'server',
		    url:'ycconfig',
		    root:'data',
			parms:[{name:"ipId",value:rows[0].id}],
			usePager:false,
	    	enabledEdit: true,
	    	isScroll: true, 
		    height: '400px',
			checkbox: true,
		    rownumbers: true
		});
	}else{
	    gridConfig.setParm("ipId",rows[0].id);
	    gridConfig.loadData();
	}
	//重新载入标题
	$("#gridConfig .l-panel-header-text").html('&nbsp;&nbsp;<span style="color:#386792">&nbsp;['+rows[0].substationName+']&nbsp;的遥测总数：'+rows[0].ycAmount+'个</span>');
	// 弹出窗口
    if(!ycWin){
	    ycWin = $.ligerDialog.open({
			target: $("#editConfig"),
			height: 500,
			width:700,
			isHidden: true,
			isResize:false,
			allowClose: true,
			title: "遥测数据配置"
		});
    }else{
        ycWin.show();
    }
}
//删除行
function configDelete(){
	var row = gridConfig.getSelectedRow();            
	if (!row) { 
		$.ligerDialog.warn("请选择行","提示"); 
		return; 
	} 
	gridConfig.deleteSelectedRow();
} 
//添加行
function configAdd(){
	var rows = grid.getSelectedRows();
    gridConfig.addRow({
        ipId :rows[0].id,
        amount : "",
        ycType : "32",
	});
//     var amountAvailable = Number(row.ycAmount)-value;
//   	//重新载入标题
// 	$("#gridConfig .l-panel-header-text").html('&nbsp;&nbsp;<span style="color:#386792">&nbsp;['+rows[0].substationName+']&nbsp;的遥测总数：'+rows[0].ycAmount+'个，还可新增'+amountAvailable+'个</span>');
	//$("#gridConfig").change();
}
//配置提交
function configConfirm(){
	gridConfig.endEdit();
	$.ligerDialog.confirm("是否确认提交？","提示", function(confirm){
		if(confirm){
			var rows = gridConfig.getData();
			if(rows.length<1){
				$.ligerDialog.warn("至少添加一条数据","提示");
				return;
			}
			for(var i = 0;i < rows.length;i++){
				if(rows[i].amount == "" || rows[i].amount == null){
					$.ligerDialog.warn("必须输入摇信数量","提示");
					return;
				}
			}
			if(checkYCamount()){//是否超限
			    return;
			}
			if(notAll()){//是否全为16或32
			    return;
			}
			if(combine()){//相邻两行为16或32
			    return;
			}
			if(notMinus()){//遥测数量不能为0
			    return;
			}
			rows = JSON.stringify(rows);
	     	$.ajax({
	             type : 'post',
	             url : "saveYCConfig",
	             data : { retList : rows},
	             cache : false,
	             dataType : 'json',
	             success : function(data) {
	                 configCancel();
	                 var success = $.ligerDialog.success('保存成功！', "提示");
	                 setTimeout(function(){success.close();},500);
	             },
	             error : function() {
	             	$.ligerDialog.warn("编辑失败！","请联系管理员");
	             }
	         });
		}
	});
}
//取消
function configCancel(){
    //窗口隐藏
    ycWin.hide();
    //表格重载
    gridConfig.reload();
}
//判断遥测数量是否超限
function checkYCamount(){
    var rows1 = grid.getSelectedRows();
    var rows2 = gridConfig.getData();
    var totalAmount = 0;
	for(var i = 0;i < rows2.length;i++){
	    totalAmount += rows2[i].amount;
	}
	if(totalAmount!=rows1[0].ycAmount){
		$.ligerDialog.warn("遥测数量和必须等于该站点的摇信总数量！","提示");
		return true;
	}
	return false;
}
//不能全部为32或16位
function notAll(){
    var rows = gridConfig.getData();
    var total16 = 0;
    var total32 = 0;
    //遍历
	for(var i = 0;i < rows.length;i++){
	    if(rows[i].ycType=="16"){
	    	total16++;
	    }
	    if(rows[i].ycType=="32"){
	    	total32++;
	    }
	}
	//如果全为16
	if(total16==rows.length){
	    $.ligerDialog.warn("遥测单类型无需进行配置！","提示");
	    return true;
	}
	//如果全为32
	if(total32==rows.length){
	    $.ligerDialog.warn("遥测单类型无需进行配置！","提示");
	    return true;
	}
	return false;
}
//相邻两个或连续多个都是16或者32位时，进行合并
function combine(){
    var rows = gridConfig.getData();
    //遍历
	if(rows.length>2){
	    for(var i = 0;i < rows.length-1;i++){
		    if(rows[i].ycType=="16" && rows[i+1].ycType=="16"){
		        $.ligerDialog.warn("相邻行的遥测类型不能相同！","提示");
		        return true;
		    }
		    if(rows[i].ycType=="32" && rows[i+1].ycType=="32"){
		        $.ligerDialog.warn("相邻行的遥测类型不能相同！","提示");
		        return true;
		    }
		}
	}
	return false;
}
//负数不能提交
function notMinus(){
    var rows = gridConfig.getData();
    for(var i = 0;i < rows.length-1;i++){
        if(0>=rows[i].amount){
            $.ligerDialog.warn("遥测数量值必须大于0！","提示");
	        return true;
        }
    }
    return false;
}
</script>
</body>
</html>
