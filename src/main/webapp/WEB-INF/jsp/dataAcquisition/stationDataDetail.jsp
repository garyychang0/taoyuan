<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script> 
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">

<div id="layout1">
	<!-- 左侧树 -->
	<div position="left" title="变电所一览" style="height:calc(100% - 40px);overflow: auto; ">
		<!-- 变电所查询条件部分 -->
		<div id="org_search" style="margin-left:8px;margin-top:0px;">
			<ul>
				<li class="label">
					<input type="text" placeholder="输入名称进行搜索" style="text-indent:5px;height:23px;border-radius: .25em;width:60%;margin-left:0;margin-top: 5px" name="search_substationName" id="search_substationName"  />					
					<button type="button" id="org_btnSearch" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x"><i class="fa fa-search"></i></button>
					<button type="button" id="org_expend" onclick="expend()" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x"><i class="fa fa-plus"></i></button>
					<button type="button" id="org_unexpend" onclick="unexpend()" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x;display:none"><i class="fa fa-minus"></i></button>
				</li>
			</ul>
		</div><div id="orgInfoTree"></div><br/><br/>
		<input type="hidden" id="org_sn" name="org_sn" value=""/>
		<input type="hidden" id="org_name" name="org_name" value=""/>
	</div>
	<div position="center"  title="变电所" >
	
		<!-- 标题部分 -->
		<h3 class="hmdl_title_h3">数据采集
		    <span class="hmdl_title_span">
		        &nbsp;/&nbsp;站点库数据详细
		    </span>
		</h3>
		<hr class="hmdl_title_hr"/>
		
		<!-- 查询条件部分 -->
		<div id="grid_search" style="margin-left:8px;margin-top:20px;">
		    <form name="searchForm" id="searchForm">
		        <ul>
		            <li class="label">
		                &emsp;变电站名称：<input type="text" id="search_substationName" name="substationName" placeholder="搜索变电站" style="text-indent:5px;height:23px;border-radius: .25em;"/>
						&emsp;IP地址：<input type="text" onclick="selectIp(1)" readonly="readonly" id="search_ipName" placeholder="IP地址" style="text-indent:5px;height:23px;border-radius: .25em;"/>			
		 			    <input type="hidden" id="search_ipId" name="ipId" />
		                &emsp;变量名称：<input type="text" id="search_var_name" name="varName" placeholder="搜索站点名称" style="text-indent:5px;height:23px;border-radius: .25em;"/>
		                &emsp;功能码：                
		                <select id="search_funcCode" name="funcCode" style="text-indent:5px;height:23px;border-radius: .25em;">
							  <option value=""> == 请选择  == </option>
							  <option value="01"> == 读线圈01  == </option>
							  <option value="02"> == 读离散量输入02 == </option>
							  <option value="03"> == 寄存器03  == </option>
							  <option value="04"> == 读寄存器04  == </option>
					    </select>
		                &emsp;<button id="btnSearch" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
		                &emsp;<button id="clearSearch" type="button" class="btn btn-info  btn-small btn-mini"><i class="fa fa-trash"></i>&nbsp;&nbsp;清空&nbsp;&nbsp;</button>
		             	&emsp;<button id="doReport" type="button" class="btn btn-success  btn-small btn-mini"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;导出&nbsp;&nbsp;</button>
		            </li>
		        </ul>
		    </form>
		</div>
		<br/>
		<!-- 列表部分 -->
		<div id="grid"></div>
		<!-- 增改部分 -->
		<div id="edit" style="display:none;">
		    <form name="saveForm" id="saveForm" >
		    <input type="hidden" id="id" name="id" />
		    <table class="default">
		        <tr style="height:50px" >
		            <td colspan="6" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
		            <i class="fa fa-address-book"></i>&emsp;站点库数据信息</td>
		        </tr>
		        <tr>
		            <td align="right">&emsp;选择站点：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="1">
		                <input id="substationName" name="substationName" style="text-indent:5px;width:180px" readonly="readonly" placeholder="请选择"/>
		                <input type="hidden" name="substationSn" id="substationSn"/>
		            </td>
		            <td align="right">&emsp;ip地址：&emsp;<span style="color:red">*</span></td>
		             <td align="left" colspan="1">
		                <input type="text" onclick='selectIp()' readonly="readonly" name="ipShow" id="ipShow"/>
		                <input type="hidden" name="ipId" id="ipId"/>
		            </td>
		             <td align="right">&emsp;站点简称：&emsp;<span style="color:red">*</span></td>
		             <td align="left" colspan="1">
		                <input type="text" disabled="disabled" name="shortname" id="shortname"/>
		            </td>
		         </tr>
		         <tr>
		            <td align="right">&emsp;变量名称：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="1">
		                <input id="varName1" name="varName"  style="text-indent:5px;width:200px" placeholder="请输入变量名称"/>
		            </td>
		            <td align="right">&emsp;是否为电量：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="3">
		                <input name="isEp" type="radio" value="0" checked/>否
						<input name="isEp" type="radio" value="1" >是
		            </td>  	
		         </tr>
		         <tr>
		            <td align="right">&emsp;描述：&emsp;</td>
		            <td align="left" colspan="5">
		                <input id="remark" name="remark"  style="text-indent:5px;width:360px" placeholder="请输入描述"/>
		            </td>	
		         </tr>
		         <tr>
		         	<td align="right">&emsp;数据类型：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="1">
		                <select id="dataType" name="dataType" style="width:180px">
							  <option value=""> == 请选择  == </option>          	
							  <c:forEach items="${data_type}" var="i">
									<option value="${i['key']}">${i['value']}</option>  
							  </c:forEach>
					    </select>
		            </td>            
		            <td align="right">&emsp;功能码：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="1">
		                <select id="funcCode1"  onchange="changeDataType()" disabled="disabled" name="funcCode" style="width:180px">
							  <option value=""> == 请选择  == </option>
							  <option value="01"> == 读线圈01  == </option>
							  <option value="02"> == 读离散量输入02 == </option>
							  <option value="03"> == 寄存器03  == </option>
							  <option value="04"> == 读寄存器04  == </option>
					    </select>
		            </td>
		            <td align="right">&emsp;节点：&emsp;</td>
		            <td align="left" colspan="1">
		                <input disabled="disabled" id="numberName"" name="numberName"  style="text-indent:5px;width:180px" placeholder=""/>
		            </td>
		         </tr>
		         <tr>
		            <td align="right" rowspan="2">&emsp;处理方式：&emsp;</td>
		            <td align="left" colspan="1" rowspan="2">
		                <select id="dealType" name="dealType" onchange="selectDealType()" style="width:220px">
							  <option value=""> == 请选择  == </option>
							  <option value="1">E14表 y=R<sup>4</sup>(X<sup>a</sup>,X<sup>b</sup>,X<sup>c</sup>,X<sup>d</sup>) </option>
							  <option value="2">E14表 y=k*R<sup>4</sup>(X<sup>a</sup>,X<sup>b</sup>,X<sup>c</sup>,X<sup>d</sup>)+b </option>
							  <option value="3">南瑞点表 y=R<sup>4</sup>(X<sup>c</sup>,X<sup>d</sup>,X<sup>a</sup>,X<sup>b</sup>) </option>
					    	  <option value="4">南瑞点表 y=k*R<sup>4</sup>(X<sup>c</sup>,X<sup>d</sup>,X<sup>a</sup>,X<sup>b</sup>)+b </option>
				   			  <option value="5"> y=k*x+b</option>
					    </select> <br/>
		                k值：<input id="k" name="k" readonly="readonly" style="margin-top:3px;text-indent:5px;width:60px"/>
		                b值：<input id="b" name="b" readonly="readonly" style="margin-top:3px;text-indent:5px;width:60px"/>
					</td>				
		            <td align="right">&emsp;变量最小值：&emsp;</td>
		            <td align="left" colspan="1">
		                <input id="varMin" name="varMin" onkeyup="value=value.replace(/[^\d\.\-]/g,'')" disabled="disabled" class="method" style="text-indent:5px;width:180px" placeholder="请输入数值"/>
		            </td>	       				
		            <td align="right">&emsp;对应原始最小值：&emsp;</td>
		            <td align="left" colspan="1">
		                <input id="orginMin" name="orginMin" onkeyup="value=value.replace(/[^\d\.\-]/g,'')" disabled="disabled" class="method" style="text-indent:5px;width:180px" placeholder="请输入数值"/>
		            </td>	  
				 </tr>
				 <tr>	            				
		            <td align="right">&emsp;变量最大值：&emsp;</td>
		            <td align="left" colspan="1">
		                <input id="varMax" name="varMax" onkeyup="value=value.replace(/[^\d\.\-]/g,'')" disabled="disabled" class="method" style="text-indent:5px;width:180px" placeholder="请输入数值"/>
		            </td>     				
		            <td align="right">&emsp;对应原始最大值：&emsp;</td>
		            <td align="left" colspan="1">
		                <input id="orginMax" name="orginMax" onkeyup="value=value.replace(/[^\d\.\-]/g,'')" disabled="disabled" class="method" style="text-indent:5px;width:180px" placeholder="请输入数值"/>
		            </td>
				 </tr>
		    </table>
		    <table align="center" style="margin-top: 10px">
		        <tr>
		            <td>
		                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;提交&nbsp;&nbsp;</button>
		                &emsp;
		                <button type="button" class="btn btn-warning" onclick="editCancel()"><i class="fa fa-close"></i>&nbsp;&nbsp;返回&nbsp;&nbsp;</button>
		            </td>
		        </tr>
		    </table>
		    </form>
		</div>
		<div id="ip_grid"  style="display: none;overflow:hidden;"></div>
		<!-- 增改部分计算变量 -->
		<div id="edit2" style="display:none;">
		    <form name="saveForm2" id="saveForm2" >
		    <input type="hidden" id="id2" name="id" />
		    <table class="default">
		        <tr style="height:50px" >
		            <td colspan="6" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
		            <i class="fa fa-address-book"></i>&emsp;变量信息</td>
		        </tr>
		        <tr>
		            <td align="right">&emsp;选择站点：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="3">
		                <input id="substationName2" name="substationName" style="text-indent:5px;width:180px" readonly="readonly" placeholder="请选择"/>
		                <input type="hidden" name="substationSn" id="substationSn2"/>
		            </td>
		         </tr>
		         <tr>
		            <td align="right">&emsp;变量名称：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="1">
		                <input id="varName2" name="varName"  style="text-indent:5px;width:180px" placeholder="请输入变量名称"/>
		            </td>
		            <td align="right">&emsp;描述：&emsp;</td>
		            <td align="left" colspan="1">
		                <input id="ipAddr" name="remark"  style="text-indent:5px;width:180px" placeholder="请输入描述"/>
		            </td>        
		         </tr>             
		         <tr>
		            <td align="right">&emsp;数据类型：&emsp;<span style="color:red">*</span></td>
		             <td align="left" colspan="1">
		                <select id="dataType2" name="dataType" style="width:180px">
							  <option value=""> == 请选择  == </option>          	
							  <c:forEach items="${data_type}" var="i">
									<option value="${i['key']}">${i['value']}</option>  
							  </c:forEach>
					    </select>
		            </td>           		            
		            <td align="right">&emsp;计算类型：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="1">
		                <select id="calculateType" name="calculateType" style="width:180px">
							  <option value=""> == 请选择  == </option>          	
							  <c:forEach items="${calculate_type}" var="i">
									<option value="${i['value']}">${i['value']}</option>  
							  </c:forEach>
					    </select> 
					    &emsp;<a onclick="clearVar()" style="cursor:pointer;text-decoration:underline">清空变量</a>
					</td>   
		         </tr>
		         <tr>
		         	<td colspan="2" rowspan="2"></td>		
		            <td align="right">&emsp;变量a：&emsp;<span style="color:red">*</span></td>
		            <td align="left" colspan="1">
		                <input id="varA" name="varA" onclick = "selectVar('varA')" readonly="readonly" style="text-indent:5px;width:180px" placeholder="请选择"/>
		            </td> 		                  
		         </tr>
		         <tr>		           
		         	<td align="right">&emsp;变量b：&emsp;</td>
		            <td align="left" colspan="1">
		                <input id="varB" name="varB" onclick = "selectVar('varB')" readonly="readonly" style="text-indent:5px;width:180px" placeholder="请选择"/>
		            </td>  
		         </tr>
		    </table>
		    <table align="center" style="margin-top: 10px">
		        <tr>
		            <td>
		                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;提交&nbsp;&nbsp;</button>
		                &emsp;
		                <button type="button" class="btn btn-warning" onclick="editCancel()"><i class="fa fa-close"></i>&nbsp;&nbsp;返回&nbsp;&nbsp;</button>
		            </td>
		        </tr>
		    </table>
		    </form>
		</div>
	</div>
</div>
<!-- 导出用Form -->
<form action="../report/show" method="post" target="_blank" id="reportForm">
	<input type="hidden" name="fparam" id="fparam"></input>
	<input type="hidden" name="fcolumn" id="fcolumn"></input>
	<input type="hidden" name="fname" id="fname"></input>
	<input type="hidden" name="fclassname" id="fclassname"></input>
	<input type="hidden" name="fmenu" id="fmenu"></input>
	<input type="hidden" name="ftitle" id="ftitle"></input>
	<input type="hidden" name="fexport" id="fexport"></input>
	<input type="hidden" name="furl" id="furl"></input>
</form>

<script type="text/javascript">
var grid;
var searchWin;
var ipWin;	
$(function(){
	//布局设置
    $("#layout1").ligerLayout({
    leftWidth: 200,
    });
    //变电所树检索
    substationSearch();
    initGrid();
    // 绑定查询按钮
	$("#btnSearch").click(function () {
		var parms = serializeObject($("#searchForm"));
	    for(var p in parms){
	        grid.setParm(p,parms[p].trim());
	    }
	    grid.loadData();
	});
    
	// 清空查询按钮
	$("#clearSearch").click(function () {
	    // FORM清空
	    $("#searchForm")[0].reset();
		var parms = serializeObject($("#searchForm"));
	    for(var p in parms){
	        grid.removeParm(p,parms[p].trim());
	    }
	    // 隐藏项目清空
	    $("#search_ipName").val("");
	    $("#search_ipId").val("");
		document.getElementById("search_ipId").value="";
	    $("#search_funcCode").val("");
	    $("#search_var_name").val("");
	    $("#search_substationName").val("");    
   	});
    
	// 绑定导出按钮
	$("#doReport").click(function () {
	 	// 用于获取列信息和搜索信息
		$('#fparam').val(JSON.stringify(grid.options.parms));
		$('#fcolumn').val(JSON.stringify(grid.getColumns()));
		// 报表的唯一标示
		$('#fname').val("stationDataDetailReport");
		// 报表容器的className
		$('#fclassname').val("fr.cn.com.edu.taoyuan.dataAcquisition.bean.StationDataDetailBean");
		// 报表的默认标题
		$('#ftitle').val("站点库数据详细报表");
		$('#fmenu').val("stationDataDetail");
		// pdf的加载地址【必输项】
		$('#furl').val("reportpdf");//新版本reportnew.js无默认值，必须填写
		// 报表导出的页面地址【必输项】
		$('#fexport').val("report");//新版本reportnew.js无默认值，必须填写
		// 导出
		$('#reportForm').submit();
   	});
    
    function initGrid(){
        grid=$("#grid").ligerGrid({
            title:'&nbsp;&nbsp;<span style="color:#386792">站点库数据信息一览</span>',
            headerImg:'/hmdl/static/images/list.png',
            columns: [
				{ display: '站点编号', name: 'substationSn',hide:true,width:120},
				{ display: '站点名称', name: 'substationName',width:200},
				{ display: '节点', name: 'numberName',width:120},
				{ display: '变量名称', name: 'varName',width:120},
				{ display: '类型', name: 'calculateType',hide:true,width:100,
					render: function (item){
						if (item.calculateType == '' || item.calculateType == null){
						    return "<div style='width:100%;height:28px;line-height:28px;background-color:#5cb85c;color:white'>变量</div>";
						}else{
						    return "<div style='width:100%;height:28px;line-height:28px;background-color:#5bc0de;color:white'><a class='fa fa-calculator' style='vertical-align:0!important;'></a>计算型变量</div>";
						}
					}
				},
				{ display: '数据类型', name: 'dataTypeName',width:120},
				{ display: '功能码', name: 'funcCode',width:120},
				{ display: '描述', name: 'remark',width:120},
                { display: '更新时间', name: 'updateDate',dateFormat: "yyyy-MM-dd hh:mm:ss",width:150},
            ],
            toolbar: {
                items: [
					<%=request.getAttribute("btn")%>,
					{ text: '刷新',click: function(){grid.reload();}, icon: 'fa fa-refresh',color:'primary' },
                ]
            },
            dataAction: 'server',
            url:'search',
            root:'data',
            onDblClickRow : function (data, rowindex, rowobj)
            {
            	operate(2,data.id);
            } ,
            record:'count',
            height: '97%',
            pageSize: 10,
            checkbox: true,
            rownumbers: true,
        });
    }
    $(function () {
	    $("#saveForm").validator({
	        ignore: ":hidden", theme: "yellow_top", timely: 20, stopOnError: false,
	        rules: { digits: [/^(\-|\+)?\d+(\.\d+)?$/, '{0}必须为数字！'],  
		             english: [/^[A-Za-z0-9_.#]{1,}$/,'{0}只能英文和数字！'],
		             email:[/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,'{0}格式不正确！']
		    },
	        fields: {
		         substationName: '变电站:required',
		         ipShow:'ip地址:required',
		         varName: '变量名称:required,english,length[1~30]',	     
		         dataType: '数据类型:required',	  
		         dealType: '处理方式:required',	   
		         k: 'k值:digits',
		         d: 'b值:,digits'
	        }, 
	        valid: function (form) {formSave(form);}
	    });
	    $("#saveForm2").validator({
	        ignore: ":hidden", theme: "yellow_right", timely: 1, stopOnError: true,
	        rules: { digits: [/^(\-|\+)?\d+(\.\d+)?$/, '{0}必须为数字！'],  
	            	 english: [/^[A-Za-z0-9_.#]{1,}$/,'{0}只能英文和数字！'],
		             email:[/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,'{0}格式不正确！']
		    },
	        fields: {
	            substationName: '变电站:required',
	            varName: '变量名称:required,english,length[1~30]',	            
	            dataType: '数据类型:required',     
	            calculateType:'计算类型:required',
	            varA:'变量a:required',
	            
	        },
	        valid: function (form) {formSave2(form);}
	    });
    })
});
//绑定变电所查询按钮
$("#org_btnSearch").click(function () {
	substationSearch();
});

//绑定变电所收缩
$("#org_btn_collapse").click(function () {
	orgTree.collapseAll();
});

//绑定变电所收缩
$("#endAddr,#beginAddr").blur(function () {
    $("#lenth").val($("#endAddr").val()-$("#beginAddr").val());
});

// 新增OR编辑处理
function operate(type,id){
    resetAll();
	if(id==null && type ==2)
	{ 
	    var rows = grid.getSelectedRows();
	    if ((rows == null || rows.length!=1) && type != 1 ) {
	        $.ligerDialog.warn("请选择一条记录！","提示");
	        return;
	    };
	    id = rows[0].id;
	}
	if(type == 1){
	    if($("#org_sn").val()==''){
	        $.ligerDialog.warn("请先选择左侧变电站！","提示");
	        return;
	    }
	    $("#substationName").val($("#org_name").val());
	    $("#substationSn").val($("#org_sn").val());
	}
	if(type == 3){
	    if($("#org_sn").val()==''){
	        $.ligerDialog.warn("请先选择左侧变电站！","提示");
	        return;
	    }
	    $("#substationName2").val($("#org_name").val());
	    $("#substationSn2").val($("#org_sn").val());
	}
    if(type == 2){
        showOrHideEdit(2,id);
    }else if(type==3){
        showOrHideEdit(3);
    }else{
        showOrHideEdit(1);
    }
}

//选择处理方式
function selectDealType(){
    var index=document.getElementById("dealType").options.selectedIndex;
    $("#k").val("");
    $("#b").val("");
    $(".method").val("");
    if(index==2 || index==4 ||index==5){
        $(".method").removeAttr("disabled");
    }else{
        $(".method").attr("disabled","disabled");
    }
}
$(".method").blur(function(){
    var k = ($("#varMin").val()-$("#varMax").val())/($("#orginMin").val()-$("#orginMax").val());
    $("#k").val(k.toFixed(5));
    var b = $("#varMin").val()-k*($("#orginMin").val());
    $("#b").val(b.toFixed(5));
    
});

//改变处理方式显示框
function changeDataType(){
    if(document.getElementById("funcCode1").value==03){
        $("#dealType").removeAttr("disabled","disabled");
        $("#orginMax").removeAttr("disabled");
        $("#orginMin").removeAttr("disabled");
        $("#varMax").removeAttr("disabled");
        $("#varMin").removeAttr("disabled");
        $("#k").val("");
        $("#b").val("");
        $(".method").val("");
    }else{
        $("#dealType").attr("disabled","disabled");
        $("#orginMax").attr("disabled","disabled");
        $("#orginMin").attr("disabled","disabled");
        $("#varMax").attr("disabled","disabled");
        $("#varMin").attr("disabled","disabled");
        $("#k").val("");
        $("#b").val("");
        $(".method").val("");
    }
}

function clearVar(){
    document.getElementById("varA").value="";
    document.getElementById("varB").value="";
}

//保存表单数据
function formSave(form) {
	//判断验证变量最大最小
    if($(".method").prop("disabled")==false){
        var index=document.getElementById("dealType").options.selectedIndex;
        if(index==2 || index==4){
            if($("#varMax").val()-$("#varMin").val()<=0){
                $.ligerDialog.error("变量最小值不能小于最大值", "提示");
                return;
            }         
        }
    }

    var data = serializeObject($("#saveForm"));
    $.ajax({
        type: 'post',
        url: "save",
        data: data,
        cache: false,
        dataType: 'json',
        success: function (data) {
            if (data && data.retCode == 100) {
                $.ligerDialog.success('保存成功！', "提示", function (opt) {
                    $("#grid").show();
                    $("#grid_search").show();
                    $("#edit").hide();
                    grid.reload();
                });
            } else {
                var text = "";
                if (data.errors && data.errors.length) {
                    for (var i = 0; i < data.errors.length; i++) {
                        text = text + "<br/>" + data.errors[i];
                    }
                }
                $.ligerDialog.error(text, "提示");
            }
        }
    });
}
//保存表单数据
function formSave2(form) {
    var data = serializeObject($("#saveForm2"));
    $.ajax({
        type: 'post',
        url: "save",
        data: data,
        cache: false,
        dataType: 'json',
        success: function (data) {
            if (data && data.retCode == 100) {
                $.ligerDialog.success('保存成功！', "提示", function (opt) {
                    $("#grid").show();
                    $("#grid_search").show();
                    $("#edit2").hide();
                    grid.reload();
                });
            } else {
                var text = "";
                if (data.errors && data.errors.length) {
                    for (var i = 0; i < data.errors.length; i++) {
                        text = text + "<br/>" + data.errors[i];
                    }
                }
                $.ligerDialog.error(text, "提示");
            }
        }
    });
}
function showOrHideEdit(type,id){
    // 0显示编辑页面
    if(type==1){
        $("#grid").hide();
        $("#grid_search").hide();
        $("#edit").show();
    // 1隐藏编辑页面
    }else if(type==2){
        getDetail(id);
    }else if(type==3){
        $("#grid").hide();
        $("#grid_search").hide();
        $("#edit2").show();
    }else{
        $("#grid").show();
        $("#grid_search").show();
        $("#edit").hide();
        $("#edit2").hide();
    }
}
//获取表单明细数据
function getDetail(id) {
$("#dataType").val("");
$("#dealType").removeAttr("disabled");
    $.ajax({
        type: 'get',
        url: "getDetail",
        data: { id: id },
        cache: false,
        dataType: 'json',
        success: function (data) {
            $("#grid").hide();
            $("#grid_search").hide();
            if(data.retObj.calculateType==null ||data.retObj.calculateType=="" ){
                $("#edit").show();
                loadData(data.retObj);
                $("#ipShow").val(data.retObj.ipAddr+":"+data.retObj.port);
            }else{
                $("#edit2").show();
                Object.keys(data.retObj).map(function(key){
                    $('#saveForm2 input,#saveForm2 select').filter(function(){
                        return key == this.name;
                    }).val(data.retObj[key]);
                });
            }
            if(data.retObj.funcCode==01 || data.retObj.funcCode==02){
                $("#dealType").attr("disabled","disabled");
                $("#dataType").val("01");
            }
            $("#orginMax").removeAttr("disabled");
            $("#orginMin").removeAttr("disabled");
            $("#varMax").removeAttr("disabled");
            $("#varMin").removeAttr("disabled");
            $("#substationName").attr("disabled","disabled");
            $("#ipShow").attr("disabled","disabled");
        }
    });
}
//删除
function del2(){
   	var rows = grid.getSelectedRows();
   	var ids="";
   	for(var i=0 ; i < rows.length ; i++){
       	if(i>0){
				ids += "|";
       	}
       	ids += rows[i].id + "," + rows[i].createDate;
   	}
	$.ligerDialog.confirm("确定要删除选中记录？","提示", function(confirm){
    	if(confirm){
     		$.post("doDelete",{ids:ids},function(data){
            		if (data.retCode==100) {
            			$.ligerDialog.success('删除成功！',"提示",function(opt){
                    		grid.reload();
                 	});}
            		else if(data.retCode==-700){
                      		$.ligerDialog.error("删除失败！存在", "提示");
                   }else{
                      		$.ligerDialog.error("删除失败！", "提示");
                   }
           },"json")
  		}
	});
}
function editCancel(){
    showOrHideEdit(4);
    resetAll();
}

function resetAll(){
    // FORM清空
    $("#saveForm")[0].reset();
    // FORM清空
    $("#saveForm2")[0].reset();
    // 隐藏项目清空
    $("#id").val("");
    $("#id2").val("");
    $("#substationSn").val("");
    $("#substationSn2").val("");
}

//选择变量
function selectVar(obj){
	// 转向网页的地址;
	var url='selectVar';              
	$.ligerDialog.open({
		//右上角的X隐藏，否则会有bug
	  allowClose: false,
	  height: 600,  
	  url: url,  
	  width: 800,  
	  name:'choseKey',  
	  title:'变量选择',  
	  isResize:true,  
	  buttons: [  {  text: '确认', onclick: function (item, dialog){
					    var arrM = new Array();
						//获取子页面
						arrM=document.getElementById('choseKey').contentWindow;
						var contentgrid = arrM.$("#grid").ligerGrid();
						var contentrows = contentgrid.getSelectedRows();
							$("#"+obj).val(contentrows[0].varName);
							$("#"+obj).attr("disabled","disabled");
							$("#calculateParm").val($("#calculateParm").val()+contentrows[0].varName+",");
							if(contentrows[0].resource_type == '0'){
								$.ligerDialog.warn("请选择变量！", "提示");
								return;
							}
							dialog.close();
	  			}
	  		},  
	              { text: '关闭', onclick: function (item, dialog){dialog.close();}}
	          ]                                     
		}); 
}

//选择ip
function selectIp(type){
   	if(!ipWin){
   	 ipWin = $.ligerDialog.open({
               target: $("#ip_grid"),
               height: 400,
               width:800,
               isHidden: true,
               isResize:false,
			   allowClose: false,
		   	    buttons: [
			              { text: '确定', onclick: function (item, dialog) { 
								var rows = gridIp.getSelectedRows();
							    if (rows == null || rows.length!=1) {
							        $.ligerDialog.warn("请选择一条记录！","提示");
							        return;
							    };
								dialog.hide(); 
								if(type==1){
									document.getElementById("search_ipName").value=rows[0].ipAddr+":"+rows[0].port;
									document.getElementById("search_ipId").value=rows[0].id;
								}else{
									$("#ipId").val(rows[0].id);
									$("#shortname").val(rows[0].shortname);
									document.getElementById("ipShow").value=rows[0].ipAddr+":"+rows[0].port;
								}
								//$("#ipShow").val(rows[0].ipAddr+":"+rows[0].port);
			              } },
			              { text: '取消', onclick: function (item, dialog) { dialog.hide(); } }
			          ]
           });
       }   
   	//加载ip grid
   	initIpGrid();  	
   	ipWin.set("title","选择ip地址");
   	ipWin.show();
}

//ip grid 初期化
function initIpGrid(){
	// 列表画面定义
	gridIp=$("#ip_grid").ligerGrid({
            columns: [
				  { display: '简称', name: 'shortname',width:120},
			      { display: 'ip地址', name: 'ipAddr',width:120},
			      { display: '端口', name: 'port',width:120},
			      { display: '回路', name: 'circuit',width:120},
			      { display: '更新时间', name: 'updateDate',dateFormat: "yyyy-MM-dd hh:mm:ss",width:150},
			  ],
	        dataAction: 'server',
	        url:'../stationData/search',
	        parms:[{name:"substationSn",value:$("#org_sn").val()}],
	        root:'data',
	        record:'count',
	    	height: '73%',
	    	pageSize: 10,
	    	checkbox: false,
	    	rownumbers: true,
	    	//用于排序
	        sortnameParmName:'sortname',
	        sortorderParmName:'sortorder',
	        enabledSort:true
	});
}

//选择变量
function selectVar(obj){
	// 转向网页的地址;
	var url='selectVar';              
	$.ligerDialog.open({
		//右上角的X隐藏，否则会有bug
	  allowClose: false,
	  height: 600,  
	  url: url,  
	  width: 800,  
	  name:'choseKey',  
	  title:'变量选择',  
      data: {
          funcCode: 03
      },
	  isResize:true,  
	  buttons: [  {  text: '确认', onclick: function (item, dialog){
					    var arrM = new Array();
						//获取子页面
						arrM=document.getElementById('choseKey').contentWindow;
						var contentgrid = arrM.$("#grid").ligerGrid();
						var contentrows = contentgrid.getSelectedRows();
						    if (contentrows == null || contentrows.length!=1) {
						        $.ligerDialog.warn("请选择一条记录！","提示");
						        return;
						    };
							$("#"+obj).val(contentrows[0].varName);
							if(contentrows[0].resource_type == '0'){
								$.ligerDialog.warn("请选择变量！", "提示");
								return;
							}
							dialog.close();
	  			}
	  		},  
	              { text: '关闭', onclick: function (item, dialog){dialog.close();}}
	          ]                                     
		});
}

</script>
</body>
</html>
