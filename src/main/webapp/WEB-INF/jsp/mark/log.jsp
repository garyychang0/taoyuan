<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/My97DatePicker/WdatePicker.js"></script>
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">

<!-- 标题部分 -->
<h3 class="hmdl_title_h3">日志管理
    <span class="hmdl_title_span">
        &nbsp;/&nbsp;日志管理
    </span>
</h3>
<hr class="hmdl_title_hr"/>

<!-- 查询条件部分 -->
<div id="grid_search" style="margin-left:8px;margin-top:20px;">
    <form name="searchForm" id="searchForm">
        &emsp;操作用户：<input type="text" id="search_operate_user" name="userName" placeholder="输入用户名" style="text-indent:5px;width:150px;height:25px;border-radius: .25em;"/>
        &emsp;操作时间：<input type="text" class="Wdate" name="createDate" style="text-indent:5px;width:150px;height:27px;border-radius: .25em;" id="search_create_time" placeholder="输入操作时间" onclick="WdatePicker({dateFmt:'yyyy-MM-dd'})"/>
        &emsp;操作方法：<input type="text" id="search_operate_method" name="method" placeholder="输入方法名" style="text-indent:5px;width:150px;height:25px;border-radius: .25em;"/>
        &emsp;日志描述：<input type="text" id="search_logs_info" name="description" placeholder="输入描述内容" style="text-indent:5px;width:150px;height:25px;border-radius: .25em;"/>
        &emsp;<button id="btnSearch" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
        &emsp;<button id="doReport" type="button" class="btn btn-success  btn-small btn-mini"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;导出&nbsp;&nbsp;</button>
    </form>
</div>
<br/>
<!-- 列表部分 -->
<div id="grid"></div>

<!-- 导出用Form -->
<form action="../report/show" method="post" target="_blank" id="reportForm">
	<input type="hidden" name="fparam" id="fparam"></input>
	<input type="hidden" name="fcolumn" id="fcolumn"></input>
	<input type="hidden" name="fname" id="fname"></input>
	<input type="hidden" name="fclassname" id="fclassname"></input>
	<input type="hidden" name="fmenu" id="fmenu"></input>
	<input type="hidden" name="ftitle" id="ftitle"></input>
	<input type="hidden" name="fexport" id="fexport"></input>
	<input type="hidden" name="furl" id="furl"></input>
</form>

<script type="text/javascript">
var grid;
var authWin;
var authTree;
$(function(){
    initGrid();

    // 绑定查询按钮
	$("#btnSearch").click(function () {
		var parms = serializeObject($("#searchForm"));
   	    for(var p in parms){
   	        grid.setParm(p,parms[p].trim());
   	    }
   	    grid.loadData();
   	});
    
	// 绑定导出按钮
	$("#doReport").click(function () {
	 	// 用于获取列信息和搜索信息
		$('#fparam').val(JSON.stringify(grid.options.parms));
		$('#fcolumn').val(JSON.stringify(grid.getColumns()));
		// 报表的唯一标示
		$('#fname').val("markReport");
		// 报表容器的className
		$('#fclassname').val("fr.cn.com.edu.taoyuan.mark.bean.LogsBean");
		// 报表的默认标题
		$('#ftitle').val("用户操作日志信息报表");
		$('#fmenu').val("mark");//logs为springmvc中的一级路径
		// pdf的加载地址【必输项】
		$('#furl').val("reportpdf");//新版本reportnew.js无默认值，必须填写
		// 报表导出的页面地址【必输项】
		$('#fexport').val("report");//新版本reportnew.js无默认值，必须填写
		// 导出
		$('#reportForm').submit();
   	});
    
    function initGrid(){
        grid=$("#grid").ligerGrid({
            title:'&nbsp;&nbsp;<span style="color:#386792">日志信息一览</span>',
            headerImg:'/hmdl/static/images/list.png',
            columns: [
				{ display: '用户编号', name: 'userSn',width:110},
				{ display: '用户名称', name: 'userName',width:120},
				{ display: '日志描述', name: 'description',width:150},
				{ display: '操作方法', name: 'method',width:380},
				{ display: '请求参数', name: 'params',width:300},
				{ display: '登录IP', name: 'requestIp',width:120},
				{ display: '日志类型', name: 'logType',width:100,
	                render: function(row) {
	                    var html = row.logType;
	                    if (html == "0") {
	                        return "正常日志";
	                    }else if (html == "1") {
	                        return "异常日志";
	                    }
	                }
				},
				{ display: '异常代码', name: 'exceptionCode',width:100},
				{ display: '异常详细信息', name: 'exceptionDetail',width:100},
				{ display: '登录方式', name: 'loginMode',width:100,
	                render: function(row) {
	                    var html = row.loginMode;
	                    if (html == "1") {
	                        return "PC登录";
	                    }else if (html == "2") {
	                        return "APP登录";
	                    }
	                }
				},
				{ display: '操作时间', name: 'createDate',dateFormat: "yyyy-MM-dd hh:mm:ss",width:150}
            ],
            dataAction: 'server',
            url:'search',
            root:'data',
            allowUnSelectRow:'true',
            record:'count',
            height: '97%',
            pageSize: 20,
            checkbox: false,
            rownumbers: true,
        });
    }
});
/**
 * 回车键搜索
 */
$("input,select").keydown(function (e) {//当按下按键时
    if (e.which == 13) {//.which属性判断按下的是哪个键，回车键的键位序号为13
        var parms = serializeObject($("#searchForm"));
		for(var p in parms){
	        grid.setParm(p,parms[p].trim());
	    }
	    grid.loadData();
    }
});
</script>
</body>  
</html>

