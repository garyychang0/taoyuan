<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title><meta http-equiv="X-UA-Compatible" content="IE=8" />
<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
<link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
<link href="/hmdl/static/js/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" />
<script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/core/base.js" type="text/javascript"></script>
<script src="/hmdl/static/js/base.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerGrid2.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerMenuBar.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerRadioList.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerDrag.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>

<script src="/hmdl/static/js/ligerUI/js/ligerui.min.js"" type="text/javascript"></script>
<script src="/hmdl/static/js/report/ligerGrid.showFilter.js" type="text/javascript"></script>
<script src="/hmdl/static/js/report/grid.js" type="text/javascript"></script>
	
<link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css"/>
<script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
<script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script>
<style>
.tdw {
	display: inline-block;
	width: 5em;
	vertical-align: middle;
}
#rTable{
	text-align:center;
	margin-left:2em;
	margin-right:2em;
}
#rTable td{
	border:1px solid black;
	line-height:25px;
}
#rTable thead{
	font-size:14px;
	font-weight:bold;
}
	
.chk-icon{ background:url(/hmdl/static/js/ligerUI/skins/Aqua/images/controls/checkbox.gif) 0px 0px; height:13px; line-height:13px; width:13px; margin:4px 20px; display:block; cursor:pointer;}
.chk-icon-selected{ background-position:0px -13px;}
</style>
</head>

<body style="overflow-x: hidden; padding: 2px;">
	<div class="l-loading" style="display: block" id="pageloading"></div>
	
	<div id="reprotTitleSetWin" style="display: none;">
		<form name="reportTitleSetFrom" id="reportTitleSetFrom">
			<table
				style="cellspacing: 0; cellpadding: 0px; font-size: 25px; border-collapse: separate; border-spacing: 10px; width: 90%; margin-left: 2em; margin-right: 2em">
				<tr>
					<td style="width:60px;">标题：</td>
					<td>
						<input style="width:90%" type="text" id="settitlename"></input>
					</td>
				</tr>
				<tr>
					<td>文字大小：</td>
					<td>
						<select style="width:60px" id="settitlesize">
							<option value="9">9px</option>
							<option value="12">12px</option>
							<option value="14">14px</option>
							<option value="16">16px</option>
							<option value="18">18px</option>
							<option value="20">20px</option>
							<option value="22">22px</option>
							<option value="24">24px</option>
							<option value="28">28px</option>
							<option value="32">32px</option>
							<option value="40">40px</option>
							<option value="48">48px</option>
							<option value="64">64px</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>文字粗细：</td>
					<td>
						 <select style="width:60px" id="settitlebold">
							<option value="normal">正常</option>
							<option value="bold">加粗</option>
							<option value="bolder">更粗</option>
							<option value="lighter">稍淡</option>
						</select>
					</td>
				</tr>
			</table>
		</form>
	</div>
	
	<div id="reprotHeadSetWin" style="display: none;">
		<form name="reportHeadSetFrom" id="reportHeadSetFrom">
			<table
				style="cellspacing: 0; cellpadding: 0px; font-size: 25px; border-collapse: separate; border-spacing: 10px; width: 90%; margin-left: 2em; margin-right: 2em">
				<tr>
					<td style="width:60px;">左侧内容：</td>
					<td>
						<input style="width:90%" type="text" id="setheadnameleft"></input>
					</td>
				</tr>
				<tr>
					<td style="width:60px;">右侧内容：</td>
					<td>
						<input style="width:90%" type="text" id="setheadnameright"></input>
					</td>
				</tr>
				<tr>
					<td>文字大小：</td>
					<td>
						<select style="width:60px" id="setheadsize">
							<option value="9">9px</option>
							<option value="12">12px</option>
							<option value="14">14px</option>
							<option value="16">16px</option>
							<option value="18">18px</option>
							<option value="20">20px</option>
						</select>
					</td>
				</tr>
				<tr>
					<td>文字粗细：</td>
					<td>
						 <select style="width:60px" id="setheadbold">
							<option value="normal">正常</option>
							<option value="bold">加粗</option>
							<option value="bolder">更粗</option>
							<option value="lighter">稍淡</option>
						</select>
					</td>
				</tr>
			</table>
		</form>
	</div>
	
	<div id="reprotTableSetWin" style="display: none;">
		<form name="reportTableSetFrom" id="reportTableSetFrom">
			<table
				style="cellspacing: 0; cellpadding: 0px; font-size: 25px; border-collapse: separate; border-spacing: 10px; width: 90%; margin-left: 2em; margin-right: 2em">
				<tr>
					<td>
						<div id="reportTableGrid"></div>
					</td>
				</tr>
			</table>
		</form>
	</div>

	<div id="reportBar"></div>
	
	<div id="reportTitle" style="text-align:center;">标题</div>
	
	<div id="reportHead">
		<div id="reportHeadLeft" style="float:left;margin-left:20px;">左侧</div>
		<div id="reportHeadRight" style="float:right;margin-right:30px;">右侧</div>
	</div>


<div class="l-clear"></div>
	<div id="reportTable">
		<table id="rTable">
			<thead>
			</thead>
			<tbody>
			</tbody>
			<tfoot>
			</tfoot>
		</table>
	</div>


	<script type="text/javascript">

	//页面按钮点击
	$("#reportBar").ligerMenuBar({
		items: [
			{ text: '标题设置', click:function(){ButReportTitle()}},
			{ text: '表头设置', click: function(){ButReportHead()}},
			{ text: '列设置', click: function(){ButReportTable()}},
			{ text: '导出', click: function(){ButReport()}},
		]
       });
//=======================页面加载=====================

	//公共参数定义
	var dialog = frameElement.dialog; 
	var reportName=dialog.get("reportName");
	var reportTitle=dialog.get("reportTitle");
	var noReport=false;//不存在report表，需要保存初始值到表
	var noColumn=false;//不存在column表，需要保存初始值到表
	
	//Title
	var titleName="初始化报表名称，待设置"; //标题文字
	var titleSize=16; //标题大小
	var titleBold="bold";  //标题粗细
	
	//Head
	var headNameLeft=""; //Head左侧内容
	var headNameRight=""; //Head右侧内容
	var headSize=12; //Head文字大小
	var headBold="normal"; //Head文字粗细
	
	//Table
	//字段相关
	var tableColumn=[]; //表格的字段 又是 表格设置Grid的Data
	//数据相关
	var tableData; //表格的查询结果
	
	//统计相关
	var tableSum={}; //统计列
	
	//页面加载事件Title和Head
	$(function(){
		ReadReportTable();//读取数据主表 Title和Head
		ReadReportColumn();//读取数据库字段表
	});
	
//*************读取数据主表，并加载Title和Head*************
	function ReadReportTable()
	{
		$.ajax({
			type : 'get',
			url : "getReportbyName",
			data : {name:reportName},
			cache : false,
			dataType : 'json',
			success : function(d) {
				if(d.id!=null)
				{
					titleName=d.title;
				}
				else
				{
					titleName=reportTitle;
					//没有表格
					noReport=true;
				}
				titleSize=d.titleSize;
				titleBold=d.titleBold;
				headNameLeft=d.headLeft;
				headNameRight=d.headRight;
				headSize=d.headSize;
				headBold=d.headBold;
				
				ReportTitleShow();//Title显示
				ReportHeadShow();//Head显示
				if(noReport)
				{
					SaveReportTable();
					SaveReportTableToJrxml();
					noReport=false;
				}
			}
		});
	}
	

	//*************读取字段表，获取表格的设置*************
	//从数据库中获取Column属性
	function ReadReportColumn()
	{
		$.ajax({
			type : 'get',
			url : "getReportColumnbyName",
			data : {name:reportName},
			cache : false,
			dataType : 'json',
			success : function(data) {
				//列属性的拼接
				var lastdata= GetTableColumns();
				var column= GetTableColumnsWithDB(lastdata,data);
				tableColumn=column;
				if(noColumn)
				{
					
				    //表格设置页面保存进jrxml
					SaveReportColumn();//表格设置页面保存进库
					SaveReportColumnToJrxml();
					noColumn=false;
				}
				
				CreateTableSetGrid();//加载设置页面的Grid
				
				//加载表格的表头部分
				CreateTableHead();
				
				//加载表格其他部分
				CreateTableBody();
			}
		});
	}
		
	
	//根据公共变量设置标题
	function ReportTitleShow()
	{
		$("#reportTitle").html(titleName);
		$("#reportTitle").css('font-size', titleSize+'px');
		$("#reportTitle").css('font-weight', titleBold);
	}
	
	//根据公共变量设置头部
	function ReportHeadShow()
	{
		$("#reportHead").css('font-size', headSize+'px');
		$("#reportHead").css('font-weight', headBold);
		$("#reportHeadLeft").html(headNameLeft);
		$("#reportHeadRight").html(headNameRight);
	}
	
	
	//从前页中获取Column属性
	function GetTableColumns()
	{
		var newcolumns=[];
	 	var columns =dialog.get("ReportGridHead");
		for(var i=0,d;d=columns[i++];){
			if(!d.issystem && !d._hide)
			{
				var head={};
				head.name=d.columnname;
				head.title=d.display;
				head.width=d._width;
				head.align="center";
				head.sum=false;
				newcolumns.push(head);
			}
		}
		return newcolumns;
	}
	
	//将两个Column属性进行匹配，放入tableColumn中
	function GetTableColumnsWithDB(lastdata, dbdata) {
		var column=[];
		noColumn=true;
		// 长度不一致说明模板的修改
		if (lastdata.length != dbdata.length ) {
		    noColumn=true;
		}
		var isExist = false;
		for (var i = 0; i < lastdata.length; i++) {
		    var row={
				name:lastdata[i].name,
				title:lastdata[i].title,
				width:lastdata[i].width,
				align:lastdata[i].align,
				sum:lastdata[i].sum
			};
		    isExist = false;
		    for (var j=0; j < dbdata.length; j++ ) {
		        if(row.name==dbdata[j].field) {
		            isExist = true;
					row.title=dbdata[j].title;
					row.width=dbdata[j].width;
					row.align=dbdata[j].align;
					row.sum=dbdata[j].sum=="true"?true:false;
				}
		    }
		    // 列未找到就是新列
		    if (!isExist) {
		        noColumn=true;
		    }
		    column.push(row);
		}
		return column;
	}
	
	//拼接Table的列标题
	function CreateTableHead() {
		var html="<tr>";
		for(var i=0,d;d=tableColumn[i++];){ 
			html+="<td id=\"h_";
			html+=d.name;
			html+="\" ";
			html+="style=\"";
			html+="width:"+d.width+"px;";
			html+="\">";
			html+=d.title+"</td>"
		}
		html+="</tr>";
		$("#rTable thead").html(html);
	}
	
	//加载表格表体部分
	function CreateTableBody() {
		GetTableData();//获取Table的数据
	}
	
	//获取Table的查询条件（前页面）
	function GetSearchData() {
		var searchData={};
		var ReportGridCount=dialog.get('ReportGridCount');
		searchData=dialog.get('DataSearch');
		searchData.page=1;
		searchData.pagesize=ReportGridCount;
		return searchData;
	}
	//获取Table的查询URL
	function GetSearchUrl()
	{
		var searchUrl="";
		var ReportSearch=dialog.get('ReportSearch');
		var ReportMenu=dialog.get('ReportMenu');
		searchUrl="../"+ReportMenu+"/"+ReportSearch;
		return searchUrl;
	}
	//获取Table的数据
	function GetTableData()
	{
		var searchUrl=GetSearchUrl();//获取Table的查询URL
		var searchData=GetSearchData();//获取Table的查询条件（前页面）
		$.ajax({
			type : 'get',
			url : searchUrl,
			data : searchData,
			cache : false,
			dataType : 'json',
			success : function(data) {
				tableData=data.data;
				CreateTable();//拼接Table的表体部分
			}
		});
	}
	//拼接Table的表体部分
	function CreateTable()
	{
		$("#rTable tbody").html("");
		for(var k=0,r;r=tableData[k++];){ 
			var html="<tr>";
			for(var i=0,d;d=tableColumn[i++];){ 
				var value=r[d.name];
				if(!value) value="";
				html+="<td class=\"t_";
				html+=d.name;
				html+="\" style=\"text-align:"+d.align+";\">";
				html+=value+"</td>"
				if(d.name in tableSum)
				{
					if(value=="") value=0;
					tableSum[d.name]+=parseFloat(value);
				}
			}
			html+="</tr>";
			$("#rTable tbody").append(html);
		}
		$("#pageloading").hide();
		CreateTableFoot();//创建Table的底部
	}
	
	//创建Table的底部
	function CreateTableFoot()
	{
		var sunCount=0;
		$("#rTable tfoot").html("");
		var html="<tr>";
		for(var i=0,d;d=tableColumn[i++];){ 
			html+="<td id=\"f_";
			html+=d.name;
			html+="\" ";
			html+="style=\"";
			html+="font-weight:bold;text-align:"+d.align;
			html+=";\">";
			if(d.name in tableSum)
			{
				html+=tableSum[d.name]+"</td>"
				sunCount+=1;
			}
			else
				html+="</td>";
		}
		html+="</tr>";
		if(sunCount!=0)
			$("#rTable tfoot").html(html);
	}

//=======================标题和表头设置页面====================================

	//标题设置页面
	function ButReportTitle()
    {
       	if(titleName=="") titleName=$("#reportTitle").html();
       	$("#settitlename").val(titleName);
       	$("#settitlesize").val(titleSize);
       	$("#settitlebold").val(titleBold);
       	reportTitleSetWin= $.ligerDialog.open({
       		target: $("#reprotTitleSetWin"),
   	 		height: 200,
   	 		width:520,
   	 		isHidden: true,
   	        isResize:true ,
   	        showMax:false,
   	        showMin:false,
   	        buttons: [
     	        { text: '确定', onclick: function (item, dialog) { ReportTitleSet(); dialog.hidden(); },cls:'l-dialog-btn-highlight' },
     	        { text: '取消', onclick: function (item, dialog) { dialog.hidden(); } }
     	    ]
       	});
    }
	
	//标题设置页面确定按钮点击
	function ReportTitleSet()
    {
    	titleName=$("#settitlename").val();
    	titleSize=$("#settitlesize").val();
		titleBold=$("#settitlebold").val();
		ReportTitleShow();
		SaveReportTable();
    }
	
	//Head设置页面
	function ButReportHead()
	{
		if(headNameLeft=="") headNameLeft=$("#reportHeadLeft").html();
		if(headNameRight=="") headNameRight=$("#reportHeadRight").html();
		$("#setheadnameleft").val(headNameLeft);
       	$("#setheadnameright").val(headNameRight);
       	$("#setheadsize").val(headSize);
       	$("#setheadbold").val(headBold);
    	  
		reportTitleSetWin= $.ligerDialog.open({
			target: $("#reprotHeadSetWin"),
			height: 360,
			width:520,
			isHidden: true,
			isResize:true ,
			showMax:false,
			showMin:false,
			buttons: [
				{ text: '确定', onclick: function (item, dialog) { ReportHeadSet(); dialog.hidden(); },cls:'l-dialog-btn-highlight' },
				{ text: '取消', onclick: function (item, dialog) { dialog.hidden(); } }
			]
		});
	}
	
	//Head设置页面确定按钮点击
	function ReportHeadSet()
	{
		headNameLeft=$("#setheadnameleft").val();
		headNameRight=$("#setheadnameright").val();
		headSize=$("#setheadsize").val();
		headBold=$("#setheadbold").val();
		ReportHeadShow();
		SaveReportTable();
		SaveReportTableToJrxml();
	}
	
	//获取保存title和head的参数
	function GetSaveReportTabledata()
	{
		var d={};
		d.name=reportName;
		d.title=titleName;
		d.titleSize=titleSize;
		d.titleBold=titleBold;
		d.headLeft=headNameLeft;
		d.headRight=headNameRight;
		d.headSize=headSize;
		d.headBold=headBold;
		return d;
	}
	
	//Title和Head保存进数据库
	function SaveReportTable()
	{
		var d=GetSaveReportTabledata();
		$.ajax({
			type : 'post',
			url : "saveReportJrxml",
			data : d,
			cache : false,
			dataType : 'json',
			success : function(d) {
				var success=true;
			}
		});
	}
	//Title和Head保存进Jrxml
	function SaveReportTableToJrxml()
	{
		var menu=dialog.get('ReportMenu');
		var fun=dialog.get('ReportSaveMain');
		var url="../"+menu+"/"+fun;
		var d=GetSaveReportTabledata();
		$.ajax({
			type : 'post',
			url : url,
			data : d,
			cache : false,
			dataType : 'json',
			success : function(d) {
				var success=true;
			}
		});
	}

//=======================表格设置页面====================================
	//表格设置页面的Grid加载
	var alignData=[{value:"left",text:"居左"},{value:"center",text:"居中"},{value:"right",text:"居右"}];
	var reportTableGrid;
	function CreateTableSetGrid()
	{
		var GridData={};
		GridData.Rows=tableColumn;
		var gridPanle = $("#reportTableGrid");
		reportTableGrid =  gridPanle.ligerGrid({
			columns: [
				{ display: '字段名', name: 'title', align: 'center', width: 110, minWidth: 30 ,editor: { type: 'text'}},
				{ display: '宽度', name: 'width', align: 'center', width: 50, minWidth: 30, editor: { type: 'numberbox'} },
				{ display: '对齐方式', name: 'align', align: 'center', width: 80, minWidth: 30, editor: { type: 'select', data: alignData }, render: fieldTypeRender },
				{ display: '统计列', name: 'sum', width: 55, render: checkboxRender}
			],data: GridData, usePager: false,
			enabledEdit: true, clickToEdit: true, fixedCellHeight: false, inWindow: true, rownumbers: true,
			width: 666, height: 400, rowHeight: 24
		});
	}
	
	//Table设置页面加载
	function ButReportTable()
	{
		reprotTableSetWin= $.ligerDialog.open({
  			target: $("#reprotTableSetWin"),
	 			height: 500,
	 			width: 800,
	 			isHidden: true,
	        	isResize:true ,
	          	showMax:false,
	          	showMin:false,
	             buttons: [
	                { text: '确定', onclick: function (item, dialog) { ReportTableSet(); dialog.hidden(); },cls:'l-dialog-btn-highlight' },
	                { text: '取消', onclick: function (item, dialog) { dialog.hidden(); } }
	             ]
  		});
	}
	
	//Table设置页面点击确定
	function ReportTableSet()
	{
		tableColumn=reportTableGrid.getData();
		var sumChange=false;
		readSum={};
		for(var i=0,d;d=tableColumn[i++];){
			if(d.sum)
			{
				if(!(d.name in tableSum))
					sumChange=true;
				readSum[d.name]=0;
			}
			else
				if(d.name in tableSum)
					sumChange=true;
		}
		if(sumChange)
		{
			tableSum=readSum;
			CreateTable();
		}
		else
		{
			for(var i=0,d;d=tableColumn[i++];){
				var head="#h_"+d.name;
				var body=".t_"+d.name;
				var foot="#f_"+d.name;
				
				//设置宽度
				$(head).css("width",d.width);
				
				//设置对齐方式
				$(body).css("text-align",d.align);
				$(foot).css("text-align",d.align);
			}
		}
		SaveReportColumn();//表格设置页面保存进库
		SaveReportColumnToJrxml();//表格设置页面保存进jrxml
	}
	
	function GetSaveReportColumnData()
	{
		var column=[];
		$(tableColumn).each(function ()
		{
			var d={
				name:reportName,
				field:this.name,
				title:this.title,
				width:this.width,
				align:this.align,
				sum:this.sum
			};
			column.push(d);
		});
		var data=JSON.stringify(column);
		return data;
	}
	
	//表格设置页面保存进数据库
	function SaveReportColumn()
	{
		var data=GetSaveReportColumnData();
		$.ajax({
			type : 'post',
			url : "saveReportColumnTable",
			contentType:"application/json", 
			data : data,
			dataType:"json",
			success : function(d) {
				var success=true;
			}
		});
	}
	
	//表格设置页面保存进jrxml
	function SaveReportColumnToJrxml()
	{
		var data=GetSaveReportColumnData();
		$.ajax({
			type : 'post',
			url : 'saveReportJrxml',
			contentType:"application/json", 
			data : data,
			dataType:"json",
			success : function(d) {
				var success=true;
			}
		});
	}
 //=======================报表点击====================================
	function ButReport()
	{
		var menu=dialog.get('ReportMenu');
		var fun=dialog.get('ReportReport');
		var url="../"+menu+"/"+fun;
		var d=GetSearchData();//获取Table的查询条件（前页面）
		
		window.open(url);
		// $.ajax({
			// type : 'get',
			// url : url,
			// data : d,
			// cache : false,
			// dataType : 'html',
			// success : function(data) {
				window.open("").document.write(data);
			// }
		// });
	}
	
 //=======================其他函数====================================
	//Grid方法重写 -- 复选框点击事件
	$("#reportTableGrid").on("click",".chk-icon", function ()
	{
		var grid = $.ligerui.get($(this).attr("gridid"));
		var rowdata = grid.getRow($(this).attr("rowid"));
		var columnname = $(this).attr("columnname");
		var checked = rowdata[columnname];
		grid.updateCell(columnname, !checked, rowdata);
	});
   </script>
</body>
</html>
