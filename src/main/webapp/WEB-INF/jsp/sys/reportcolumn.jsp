﻿<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
<link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
<link href="/hmdl/static/js/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" />
<link href="/hmdl/static/js/ligerUI/skins/Gray/css/form.css" rel="stylesheet" type="text/css" />
<script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/core/base.js" type="text/javascript"></script>
<script src="/hmdl/static/js/base.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerTree.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerGrid2.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/CustomersData.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerButton.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerDrag.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
<script src="/hmdl/static/js/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
<link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css"/>
<script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
<script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script>
<link href="/hmdl/app2_beta/css/common.css" rel="stylesheet" type="text/css"/>
<link href="/hmdl/app2_beta/css/blue/bills.css" rel="stylesheet" type="text/css"/>
<link href="/hmdl/app2_beta/css/blue/ui.min.css" rel="stylesheet"/>
<link href="/hmdl/static/css/comm.css" rel="stylesheet" type="text/css"/>
</head>
<body style="overflow-x: hidden; padding: 2px;">
    <!--  添加、修改界面 -->
    <div id="editWin" style="display: none;">
        <form name="saveForm" id="saveForm">
			<input type="text" name="id" id="id" style="display: none;"></input>
            <table
                style="cellspacing: 0; cellpadding: 0px; font-size: 25px; border-collapse: separate; border-spacing: 10px; width: 90%; margin-left: 2em; margin-right: 2em">
					<tr>
						<td>导出表名称：</td>
						<td><input name="name" id="name" type="text"/></td>
						<td>导出字段：</td>
						<td><input name="field" id="field" type="text"/></td>
					</tr>
					<tr>
						<td>导出字段标题：</td>
						<td><input name="title" id="title" type="text"/></td>
						<td>导出字段宽度：</td>
						<td><input name="width" id="width" type="text"/></td>
					</tr>
					<tr>
						<td>对齐方式：</td>
						<td><input name="align" id="align" type="text"/></td>
						<td>是否统计：</td>
						<td><input name="sum" id="sum" type="text"/></td>
					</tr>
					<tr>
						<td>是否隐藏：</td>
						<td><input name="isDisplay" id="isDisplay" type="text"/></td>
					</tr>
            </table>
        </form>
    </div>
    
    <!--  查询界面 -->
    <div id="searchWin" style="display: none;">
        <form name="searchForm" id="searchForm">
            <table
                style="cellspacing: 0; cellpadding: 0px; font-size: 25px; border-collapse: separate; border-spacing: 10px; width: 90%; margin-left: 2em; margin-right: 2em">
				<tr>
					<td>导出表名称：</td>
					<td><input name="name" id="search_name" type="text"  /></td>
					<td>导出字段：</td>
					<td><input name="field" id="search_field" type="text"  /></td>
                </tr>
				<tr>
					<td>导出字段标题：</td>
					<td><input name="title" id="search_title" type="text"  /></td>
					<td>导出字段宽度：</td>
					<td><input name="width" id="search_width" type="text"  /></td>
                </tr>
				<tr>
					<td>对齐方式：</td>
					<td><input name="align" id="search_align" type="text"  /></td>
					<td>是否统计：</td>
					<td><input name="sum" id="search_sum" type="text"  /></td>
                </tr>
				<tr>
					<td>是否隐藏：</td>
					<td><input name="isDisplay" id="search_isDisplay" type="text"  /></td>
				</tr>
            </table>
        </form>
    </div>
    <div class="cf" id="bottomField" style="width:20em;float:right">
        <div class="fr" id="toolBottom" style="width:20em">
            <span id="groupBtn" >
			</span>
		</div>
    </div>
    <div class="l-loading" style="display:block" id="pageloading"></div>

    <div class="mod-search cf" style="padding-top:1em">
        <div class="fl">
            <form id="search_form" style="width:100%">
                <ul class="ul-inline">
					 <li>
                        <label>&nbsp;&nbsp;导出表名称：</label>
                        <input type="text"  value="" name="name" id="srch_name" class="ui-input-ph"/>
                    </li>
					 <li>
                        <label>&nbsp;&nbsp;导出字段：</label>
                        <input type="text"  value="" name="field" id="srch_field" class="ui-input-ph"/>
                    </li>
					 <li>
                        <label>&nbsp;&nbsp;导出字段标题：</label>
                        <input type="text"  value="" name="title" id="srch_title" class="ui-input-ph"/>
                    </li>
				<!-- 	 <li>
                        <label>&nbsp;&nbsp;是否统计：</label>
                        <input type="text"  value="" name="sum" id="srch_sum" class="ui-input-ph"/>
                    </li> -->
					 <li>
                        <label>&nbsp;&nbsp;是否隐藏：</label>
                        <input type="text"  value="" name="isDisplay" id="srch_isDisplay" class="ui-input-ph"/>
                    </li>
                    <li><a class="ui-btn mrb" id="search" href="javascript:doSearch();">查询</a></li>
                   <!--  <li><a class="ui-btn mrb" id="search" href="javascript:showSearch();">高级查询</a></li> -->
                </ul>
			</form>
		</div>
	</div>
	<div id="maingrid" style="margin-left:1em;"></div>
	<script type="text/javascript">

		$(function ()
		{
			$('#groupBtn').html("${btn}");
			$('#bottomField').width(parseInt('${btn_size}') * 75);
			$('#toolBottom').width(parseInt('${btn_size}') * 75);
		});

		var grid;
		var action = ""; 
		   
		var editWin;
		var searchWin;

		$(function ()
		{
			window['g'] =
			grid = $("#maingrid").ligerGrid({
				height:'99%',
				columns: [
					{ display: '导出表名称', name: 'name', align: 'center',width:120 },
					{ display: '导出字段', name: 'field', align: 'center',width:120 },
					{ display: '导出字段标题', name: 'title', align: 'center',width:120 },
					{ display: '导出字段宽度', name: 'width', align: 'center',width:120 },
					{ display: '对齐方式', name: 'align', align: 'center',width:120 },
					{ display: '是否统计', name: 'sum', align: 'center',width:120 },
					{ display: '是否隐藏', name: 'isDisplay', align: 'center',width:120 },
				], url:'search', root:'data',
				record:'count', checkbox: true,rownumbers:true
           });
            
			$(".l-bar-btnprev").trigger("click");  
           
			$("#pageloading").hide();
			// 表单提交
			$("#saveForm").validator({
				rules: {
					digits: [/^\d+(\.\d+)?$/, '{0}必须为数字！'],
					email:[/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,'{0}格式不正确！']
				},
				messages: {
					//required: "请输入{0}",
				},
				fields: {
					id: '主键:required,eltd,length[1~30]',
                    name: '导出表名称:length[0~50]',
                    field: '导出字段:length[0~50]',
                    title: '导出字段标题:length[0~100]',
                    width: '导出字段宽度:length[0~10]',
                    align: '对齐方式:length[0~30]',
                    sum: '是否统计:length[0~30]',
                    isDisplay: '是否隐藏:length[0~30]'
				},
				valid: function(form){
					$.ajax({
						type : 'post',
						url : action,
						data : $("#saveForm").serialize(),
						cache : false,
						dataType : 'json',
						success : function(data) {
							if(data&&data.retCode == 100){
								$.ligerDialog.success('操作成功！',"提示",function(opt){
									editWin.hide();
									grid.reload();
								});
							}else{
								var text = "";
								if (data.errors && data.errors.length) {
									for(var i = 0;i<data.errors.length;i++)
									{
										text = text + "<br/>" + data.errors[i];
									}
								}
								$.ligerDialog.error(text, "提示");
							}
                        }
					});
				},
				ignore: ":hidden",
				theme: "yellow_bottom",
				timely: 1,
				stopOnError: true
			}); 
		});

       
		//删除
		function del(){
			var rows = grid.getSelectedRows();
			var ids="";
			if (rows == null||rows.length==0) {
				$.ligerDialog.warn("请选择一条记录！","提示");
				return;
			}
			for(var i=0 ; i < rows.length ; i++){
				if(i>0){
					ids += "|";
				}
				ids += rows[i].id + "," + rows[i].timestamp;
			}
			$.ligerDialog.confirm("确定要隐藏选中字段？","提示", function(confirm){
				if(confirm){
					$.post("doDelete",{ids:ids},function(data){
						if (data.retCode==100) {
							$.ligerDialog.success('操作成功！',"提示",function(opt){
								grid.reload();
							});
						}else{
							$.ligerDialog.error("操作失败！", "提示");
						}
					},"json")
				}
			});
		}
		//删除
		function open(){
			var rows = grid.getSelectedRows();
			var ids="";
			if (rows == null||rows.length==0) {
				$.ligerDialog.warn("请选择一条记录！","提示");
				return;
			}
			for(var i=0 ; i < rows.length ; i++){
				if(i>0){
					ids += "|";
				}
				ids += rows[i].id + "," + rows[i].timestamp;
			}
			$.ligerDialog.confirm("确定要开启选中字段？","提示", function(confirm){
				if(confirm){
					$.post("doOpen",{ids:ids},function(data){
						if (data.retCode==100) {
							$.ligerDialog.success('操作成功！',"提示",function(opt){
								grid.reload();
							});
						}else{
							$.ligerDialog.error("操作失败！", "提示");
						}
					},"json")
				}
			});
		}
        
		function doSearch(){
			var obj=serializeObject($("#search_form"));
			for(var o in obj){
				grid.setParm(o,obj[o]);
			}
			grid.loadData();
			searchWin.hide(); 
			grid.changePage('first');
		}
        
        
		function showSearch() {
     
			if(!searchWin){
				searchWin = $.ligerDialog.open({
					target: $("#searchWin"),
					height: 320,
					width:620,
					isHidden: true,
					isResize:true ,
					showMax:true,
					showMin:true,
                  
					buttons: [
						{ text: '确定', onclick: function (item, dialog) {  doSubmit();  },cls:'l-dialog-btn-highlight' },
						{ text: '取消', onclick: function (item, dialog) { dialog.hidden(); } }
					]
				});
				searchWin.hide();
			}
         
			action="search";
			searchWin.set("title","报表字段检索");
			searchWin.show();
		}   
    
    
		function doSubmit() {
			var obj=serializeObject($("#searchForm"));
				for(var o in obj){
					grid.setParm(o,obj[o]);
				}
			grid.loadData();
			searchWin.hide(); 
			grid.changePage('first');
		}
    
      
		//追加，更新
		function operate(type) {
			$("#saveForm").validator( "cleanUp" );
			if(!editWin){
				editWin = $.ligerDialog.open({
					target: $("#editWin"),
					height: 320,
					width:620,
					isHidden: true,
					isResize:true ,
					showMax:true,
					buttons: [
						{ text: '确定', onclick: function (item, dialog) { $("#saveForm").submit(); },cls:'l-dialog-btn-highlight' },
						{ text: '取消', onclick: function (item, dialog) { dialog.hidden(); } }
					]
				});
				editWin.hide();

			}

			if(type == 1){
				action="save";
				$("#saveForm")[0].reset();
				editWin.set("title","报表字段编辑");
				editWin.show();
                
			} else if (type == 2) {
				action="save";
				var rows = grid.getSelectedRows();
				if (rows == "" || rows.length!=1) {
					$.ligerDialog.warn("请选择一条记录！","提示");
					return;
				}
				$.ajax({
					type : 'get',
					url : "getDetail",
					data : {id :rows[0].id },
					cache : false,
					dataType : 'json',
					success : function(data) {
						loadData(data.retObj);
						editWin.set("title","报表字段编辑");
						editWin.show();
					}
				});
			}
		}

	</script>
</body>
</html>
