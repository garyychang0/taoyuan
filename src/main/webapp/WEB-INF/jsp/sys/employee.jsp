<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script> 
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">

<!-- 标题部分 -->
<h3 class="hmdl_title_h3">学校管理
    <span class="hmdl_title_span">
        &nbsp;/&nbsp;员工管理
    </span>
</h3>
<hr class="hmdl_title_hr"/>

<!-- 查询条件部分 -->
<div id="grid_search" style="margin-left:8px;margin-top:20px;">
    <form name="searchForm" id="searchForm">
        <ul>
            <li class="label">
                &emsp;员工编号：<input type="text" id="search_user_sn" name="userSn" placeholder="搜索员工编号" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                &emsp;员工姓名：<input type="text" id="search_user_name" name="userName" placeholder="搜索员工姓名" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                &emsp;<button id="btnSearch" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
                &emsp;<button id="doReport" type="button" class="btn btn-success  btn-small btn-mini"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;导出&nbsp;&nbsp;</button>
            </li>
        </ul>
    </form>
</div>
<br/>

<!-- 列表部分 -->
<div id="grid"></div>

<!-- 增改部分 -->
<div id="edit" style="display:none;">
    <form name="saveForm" id="saveForm" >
    <input type="hidden" id="id" name="id" />
    <table  class="default">
        <tr style="height:50px" >
            <td colspan="4" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
            <i class="fa fa-address-book"></i>&emsp;员工信息</td>
        </tr>
        <tr>
            <td align="right">&emsp;选择部门：&emsp;</td>
            <td align="left" colspan="3">
                <input id="edit_type" name="orgSn" type="hidden" />
            </td>
        </tr>
        <tr>
            <td align="right">&emsp;员工名称：&emsp;<span style="color:red">*</span></td>
            <td align="left" colspan="3">
                <input id="userName" name="userName"  style="text-indent:5px;width:180px" placeholder="请输入员工名称"/>
            </td>
         </tr>
         <tr>
            <td align="right">&emsp;手机号码：&emsp;<span style="color:red">*</span></td>
            <td align="left" colspan="3">
                <input id="tel" name="tel"  style="text-indent:5px;width:180px" placeholder="请输入手机号码"/>
            </td>
         </tr>
         <tr>
            <td align="right">&emsp;邮箱：&emsp;</td>
            <td align="left" colspan="3">
                <input id="mail" name="mail"  style="text-indent:5px;width:180px" placeholder="请输入邮箱地址"/>
            </td>
         </tr>
         <tr>
            <td align="right">&emsp;性别：&emsp;</td>
            <td align="left" colspan="3">
                <select id="sex" name="sex" style="width:180px">
					  <option value=""> == 请选择  == </option>          	
					  <c:forEach items="${sexlist}" var="i">
							<option value="${i['key']}">${i['value']}</option>  
					  </c:forEach>
			    </select>
            </td>
        </tr>
    </table>
    <table align="center" style="margin-top: 10px">
        <tr>
            <td>
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;提交&nbsp;&nbsp;</button>
                &emsp;
                <button type="button" class="btn btn-warning" onclick="editCancel()"><i class="fa fa-close"></i>&nbsp;&nbsp;返回&nbsp;&nbsp;</button>
            </td>
        </tr>
    </table>
    </form>
</div>

<!-- 审核部分 -->
<div id="searchWin" style="display: none;">
        <form name="auditForm" id="auditForm">
        <input id='userSn' type="text" name="userSn" value="" style="display:none;float:left" />
        <table  class="default">
        <tr style="height:50px" >
            <td colspan="4" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
            <i class="fa fa-address-book"></i>&emsp;审核信息</td>
        </tr>
        <tr>
            <td align="right">&emsp;所属单位/部门 ：&emsp;<span style="color:red">*</span></td>
            <td align="left" colspan="3">
                <input id="edit_type2" name="orgSn" type="hidden" />
            </td>
        </tr>
        <tr>
            <td align="right">&emsp;所属角色 ：&emsp;<span style="color:red">*</span></td>
            <td align="left" colspan="3">
                <select id="roleId" name="roleId" style="width:180px">
					  <option value=""> == 请选择  == </option>          	
					  <c:forEach items="${rolelist}" var="i">
							<option value="${i['key']}">${i['value']}</option>  
					  </c:forEach>
			    </select>
            </td>
        </tr>
		</table>
            <ul class="operate" style='margin: 10px 5px;text-align: center;width: 100%;'>
	            <input type="button" value="通过" id="btnAdopt" class="l-button l-button-submit" />&nbsp;&nbsp;
	            <input type="button" value="不通过" id="btnUnAdopt" class="l-button l-button-submit" />&nbsp;&nbsp;
	            <input type="button" value="取消" id="btnPass" class="l-button l-button-reset" />
            </ul>
        </form>
</div>

<!-- 导出用Form -->
<form action="../report/show" method="post" target="_blank" id="reportForm">
	<input type="hidden" name="fparam" id="fparam"></input>
	<input type="hidden" name="fcolumn" id="fcolumn"></input>
	<input type="hidden" name="fname" id="fname"></input>
	<input type="hidden" name="fclassname" id="fclassname"></input>
	<input type="hidden" name="fmenu" id="fmenu"></input>
	<input type="hidden" name="ftitle" id="ftitle"></input>
	<input type="hidden" name="fexport" id="fexport"></input>
	<input type="hidden" name="furl" id="furl"></input>
</form>

<script type="text/javascript">
var grid;
var searchWin;
$(function(){
    initGrid();
    // 绑定查询按钮
	$("#btnSearch").click(function () {
		var parms = serializeObject($("#searchForm"));
   	    for(var p in parms){
   	        grid.setParm(p,parms[p].trim());
   	    }
   	    grid.loadData();
   	});
    
	// 绑定导出按钮
	$("#doReport").click(function () {
	 	// 用于获取列信息和搜索信息
		$('#fparam').val(JSON.stringify(grid.options.parms));
		$('#fcolumn').val(JSON.stringify(grid.getColumns()));
		// 报表的唯一标示
		$('#fname').val("employeeReport");
		// 报表容器的className
		$('#fclassname').val("fr.cn.com.edu.taoyuan.sys.bean.EmployeeBean");
		// 报表的默认标题
		$('#ftitle').val("员工信息报表");
		$('#fmenu').val("employee");
		// pdf的加载地址【必输项】
		$('#furl').val("reportpdf");//新版本reportnew.js无默认值，必须填写
		// 报表导出的页面地址【必输项】
		$('#fexport').val("report");//新版本reportnew.js无默认值，必须填写
		// 导出
		$('#reportForm').submit();
   	});
    
    function initGrid(){
        grid=$("#grid").ligerGrid({
            title:'&nbsp;&nbsp;<span style="color:#386792">员工信息一览</span>',
            headerImg:'/hmdl/static/images/list.png',
            columns: [
				{ display: '审核状态', name: 'userStatus',width:120,render:function(value){if(value.userStatus==0){return "未审核";}else if(value.userStatus==1){return "审核通过";}else{return "审核未通过";}}},
				{ display: '员工编号', name: 'userSn',width:120},
				{ display: '员工名称', name: 'userName',width:120},
                { display: '性别', name: 'sexName',width:50},
                { display: '联系电话', name: 'tel',width:120},
                { display: '邮箱', name: 'mail',width:150},
                { display: '单位/部门', name: 'departmentName',width:200},
                { display: '创建时间', name: 'createDate',dateFormat: "yyyy-MM-dd hh:mm:ss",width:150},
                { display: '更新时间', name: 'updateDate',dateFormat: "yyyy-MM-dd hh:mm:ss",width:150},
            ],
            toolbar: {
                  items: [
					<%=request.getAttribute("btn")%>,
					{ text: '刷新',click: function(){grid.reload();}, icon: 'fa fa-refresh',color:'primary' },
				]
            },
            dataAction: 'server',
            url:'search',
            root:'data',
            record:'count',
            height: '97%',
            pageSize: 10,
            checkbox: true,
            rownumbers: true,
        });
    }
    $(function () {
	    $("#saveForm").validator({
	        ignore: ":hidden", theme: "yellow_bottom", timely: 1, stopOnError: true,
	        rules: { digits: [/^\d+(\.\d+)?$/, '{0}必须为数字！'],  
	           		 email:[/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,'{0}格式不正确！']
	        },
	        fields: {
	            orgSn: '部门:required',
	            userName: '员工名称:required,length[1~30]',
	            tel: '手机号码:required,digits',
	        },
	        valid: function (form) {formSave(form);}
	    });
    })
});

// 新增OR编辑处理
function operate(type){
    resetAll();
    var rows = grid.getSelectedRows();
    $.ajax({
        type : 'post',
        url : 'getDepartment',
        cache : false,
        dataType : 'json',
        success : function(data) {
            $("#edit_type").ligerComboBox({
	                    data:data.retObj,
	                    valueField : 'key',
	                    textField: 'value',
	                    isMultiSelect: false,
	            	});

            $("#edit_type_txt").css("height", "25.5px");
        },
        error : function() {
            $.ligerDialog.error("加载部门失败！", "提示");
        }
    });
    if ((rows == null || rows.length!=1) && type != 1 ) {
        $.ligerDialog.warn("请选择一条记录！","提示");
        return;
    };
    if(type == 2){
        showOrHideEdit(2,rows[0].id);
    }else{
        showOrHideEdit(1);
    }
}
//保存表单数据
function formSave(form) {
    var data = serializeObject($("#saveForm"));
    $.ajax({
        type: 'post',
        url: "save",
        data: data,
        cache: false,
        dataType: 'json',
        success: function (data) {
            if (data && data.retCode == 100) {
                $.ligerDialog.success('保存成功！', "提示", function (opt) {
                    $("#grid").show();
                    $("#grid_search").show();
                    $("#edit").hide();
                    grid.reload();
                });
            } else {
                var text = "";
                if (data.errors && data.errors.length) {
                    for (var i = 0; i < data.errors.length; i++) {
                        text = text + "<br/>" + data.errors[i];
                    }
                }
                $.ligerDialog.error(text, "提示");
            }
        }
    });
}
function showOrHideEdit(type,id){
    // 0显示编辑页面
    if(type==1){
        $("#grid").hide();
        $("#grid_search").hide();
        $("#edit").show();
    // 1隐藏编辑页面
    }else if(type==2){
        getDetail(id);
    }else{
        $("#grid").show();
        $("#grid_search").show();
        $("#edit").hide();
    }
}
//获取表单明细数据
function getDetail(id) {
    $.ajax({
        type: 'get',
        url: "getDetail",
        data: { id: id },
        cache: false,
        dataType: 'json',
        success: function (data) {
            loadData(data.retObj);
            $("#grid").hide();
            $("#grid_search").hide();
            $("#edit").show();
	    	$("#edit_type").ligerGetComboBoxManager().selectValue(data.retObj['orgSn']);
        }
    });
}
//删除
function del(){
   	var rows = grid.getSelectedRows();
   	var ids="";
   	if (rows == "" || rows.length!=1) {
   		$.ligerDialog.warn("请选择一条记录！","提示");
   		return;
  	}
   	for(var i=0 ; i < rows.length ; i++){
       	if(i>0){
				ids += "|";
       	}
       	ids += rows[i].id + "," + rows[i].updateDate;
   	}
	$.ligerDialog.confirm("确定要删除选中记录？","提示", function(confirm){
    	if(confirm){
     		$.post("doDelete",{ids:ids},function(data){
            		if (data.retCode==100) {
            			$.ligerDialog.success('删除成功！',"提示",function(opt){
                    		grid.reload();
                 	});}
            		else if(data.retCode==-700){
                      		$.ligerDialog.error("删除失败！存在", "提示");
                   }else{
                      		$.ligerDialog.error("删除失败！", "提示");
                   }
           },"json")
  		}
	});
}
/* 
 *审核按钮
  */
function audit(){
 	 var rows = grid.getSelectedRows();
      if ((rows == null || rows.length!=1) && type != 1 ) {
          $.ligerDialog.warn("请选择一条记录！","提示");
          return;
      }
      if(rows[0].userStatus==1 || rows[0].userStatus==2){
     	 $.ligerDialog.warn("已审核，不需要再次审核！","提示");
          return;
      }
      if(rows[0].userStatus==null || rows[0].userStatus==''){
     	 $.ligerDialog.warn("请先创建用户！","提示");
          return;
      }
      $.ajax({
          type : 'post',
          url : 'getDepartment',
          cache : false,
          dataType : 'json',
          success : function(data) {
              $("#edit_type2").ligerComboBox({
  	                    data:data.retObj,
  	                    valueField : 'key',
  	                    textField: 'value',
  	                    isMultiSelect: false,
  	            	});
          },
          error : function() {
              $.ligerDialog.error("加载部门失败！", "提示");
          }
      });
      $('#userSn').val(rows[0].userSn);
      if(!searchWin){
     	 searchWin = $.ligerDialog.open({
              target: $("#searchWin"),
              height: 300,
              width:600,
              isHidden: true,
              isResize:true,
              showMax:true,
              showMin:true
          });
          //绑定事件
          //取消
          $("#btnPass").click(function () {
         	 searchWin.hide();
          });

          //通过
          $("#btnAdopt").click(function () {
         	 if($('#edit_type2').val()==''){
         		 $.ligerDialog.warn("请选择单位！","提示");
         		 return;
         	 }
         	 if($('#roleId').val()==''){
         		 $.ligerDialog.warn("请选择角色！","提示");
         		 return;
         	 }
         	 $.ajax({
                  type : 'post',
                  url : "audit",
                  data : $("#auditForm").serialize(),
                  cache : false,
                  dataType : 'json',
                  success : function(data) {                    
                      if (data && data.retCode == 100) {
                          $.ligerDialog.success('操作成功！', "提示", function (opt) {
	                      	 searchWin.hide();
	                         grid.reload();
                          });
                      } else {
                          var text = "";
                          if (data.errors && data.errors.length) {
                              for (var i = 0; i < data.errors.length; i++) {
                                  text = text + "<br/>" + data.errors[i];
                              }
                          }
                          $.ligerDialog.error(text, "提示");
                      }
                  }
              });
         	 searchWin.hide();
          });
          
        	//不通过
          $("#btnUnAdopt").click(function () {
         	 $.ajax({
                  type : 'post',
                  url : "unaudit",
                  data : $("#auditForm").serialize(),
                  cache : false,
                  dataType : 'json',
                  success : function(data) {
                      if (data && data.retCode == 100) {
                          $.ligerDialog.success('操作成功！', "提示", function (opt) {
	                      	 searchWin.hide();
	                         grid.reload();
                          });
                      } else {
                          var text = "";
                          if (data.errors && data.errors.length) {
                              for (var i = 0; i < data.errors.length; i++) {
                                  text = text + "<br/>" + data.errors[i];
                              }
                          }
                          $.ligerDialog.error(text, "提示");
                      }
                  }
              });
         	 searchWin.hide();
          });
      }
      searchWin.set("title","审核员工信息");
      searchWin.show();
}
function editCancel(){
    showOrHideEdit(3);
    resetAll();
}

function resetAll(){
    // FORM清空
    $("#saveForm")[0].reset();
    // 隐藏项目清空
    $("#id").val("");
    $("#edit_type").val("");
}
</script>
</body>
</html>
