<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <style>
        html, body {
            margin: 0;
            height: 100%;
        }

        #rTable {
            text-align: center;
            margin-left: 2em;
            margin-right: 2em;
        }

            #rTable td {
                border: 1px solid black;
                line-height: 25px;
            }

            #rTable thead {
                font-size: 14px;
                font-weight: bold;
            }

        .chk-icon {
            background: url(/hmdl/static/js/ligerUI/skins/Aqua/images/controls/checkbox.gif) 0px 0px;
            height: 13px;
            line-height: 13px;
            width: 13px;
            margin: 4px 20px;
            display: block;
            cursor: pointer;
        }

        .chk-icon-selected {
            background-position: 0px -13px;
        }

        #setTable td {
            height: 24px;
        }

        .setHeadTD {
            font-size: 14px;
            font-weight: bold;
        }

        .pdfReport {
            width: 99%;
            height: 90%;
        }
    </style>
</head>

<body style=" margin:auto;text-align:center;height:100%">
    <div id="VariableWin" style="display: none;">
        <form name="VariableFrom" id="VariableFrom"></form>
    </div>

    <div id="reprotAttributeWin" style="display: none;">
        <form name="reportFrom" id="reportFrom">
            <table id="setTable" style="width:600px;margin-left:80px;border-spacing: 10px;">
                <tr><td colspan="4" class="setHeadTD" style="text-align:center;">标题设置</td></tr>
                <tr>
                    <td style="width:100px;">标题内容：</td>
                    <td colspan="3" style="text-align:left;">
                        <input style="width:500px;" type="text" id="settitlename" />
                    </td>
                </tr>
                <tr>
                    <td style="width:100px;">文字大小：</td>
                    <td style="width:200px;text-align:left;">
                        <select style="width:80px" id="settitlesize">
                            <option value="9">9px</option>
                            <option value="12">12px</option>
                            <option value="14">14px</option>
                            <option value="16">16px</option>
                            <option value="18">18px</option>
                            <option value="20">20px</option>
                            <option value="22">22px</option>
                            <option value="24">24px</option>
                            <option value="28">28px</option>
                            <option value="32">32px</option>
                            <option value="40">40px</option>
                            <option value="48">48px</option>
                            <option value="64">64px</option>
                        </select>
                    </td>
                    <td style="width:100px;">文字粗细：</td>
                    <td style="width:200px;text-align:left;">
                        <select style="width:80px" id="settitlebold">
                            <option value="normal">正常</option>
                            <option value="bold">加粗</option>
                            <option value="bolder">更粗</option>
                            <option value="lighter">稍淡</option>
                        </select>
                    </td>
                </tr>
                <tr><td colspan="4" class="setHeadTD" style="text-align:center;">头部设置</td></tr>
                <tr>
                    <td>左侧内容：</td>
                    <td style="text-align:left;">
                        <input style="width:160px;" type="text" id="setheadnameleft" />
                        <!--  <a href="javascript:Variable('setheadnameleft')">插入</a>-->
                    </td>
                    <td>右侧内容：</td>
                    <td style="text-align:left;">
                        <input style="width:160px;" type="text" id="setheadnameright" />
                        <!-- <a href="javascript:Variable('setheadnameright')">插入</a>-->
                    </td>
                </tr>
                <tr>
                    <td>文字大小：</td>
                    <td style="text-align:left;">
                        <select style="width:80px" id="setheadsize">
                            <option value="9">9px</option>
                            <option value="12">12px</option>
                            <option value="14">14px</option>
                            <option value="16">16px</option>
                            <option value="18">18px</option>
                            <option value="20">20px</option>
                        </select>
                    </td>
                    <td>文字粗细：</td>
                    <td style="text-align:left;">
                        <select style="width:80px" id="setheadbold">
                            <option value="normal">正常</option>
                            <option value="bold">加粗</option>
                            <option value="bolder">更粗</option>
                            <option value="lighter">稍淡</option>
                        </select>
                    </td>
                </tr>
                <tr><td colspan="4" class="setHeadTD" style="text-align:center;">表格设置</td></tr>
                <tr>
                    <td colspan="4">
                        <div style="margin:auto;" id="reportTableGrid"></div>
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <iframe class="pdfReport" src="about:blank" ></iframe>
    <div class="l-dialog-buttons" style="height:45px">
	    <div class="l-dialog-buttons-inner">
		    <div class="l-dialog-btn"  style="width:160px;height:40px;font-size:30px" onclick="window.close();">
		    	<div class="l-dialog-btn-l"></div>
		    	<div class="l-dialog-btn-r"></div>
		    	<div class="l-dialog-btn-inner" style="margin-top:9px">关闭</div>
		    </div>
		    <div class="l-dialog-btn l-dialog-btn-highlight" style="width:160px;height:40px;font-size:30px" onclick="doRpt();">
		    	<div class="l-dialog-btn-l"></div>
		    	<div class="l-dialog-btn-r"></div>
		    	<div class="l-dialog-btn-inner" style="margin-top:9px">导出</div>
		    </div>
<!-- 		    <div class="l-dialog-btn" style="width:160px;height:40px;font-size:30px" onclick="doSet();"> -->
<!-- 		    	<div class="l-dialog-btn-l"></div> -->
<!-- 		    	<div class="l-dialog-btn-r"></div> -->
<!-- 		    	<div class="l-dialog-btn-inner"style="margin-top:9px">页面设置</div> -->
<!-- 		    </div> -->
<!-- 		    <div class="l-dialog-btn"  style="width:160px;height:40px;font-size:30px" onclick="doLoginReport();"> -->
<!-- 		    	<div class="l-dialog-btn-l"></div> -->
<!-- 		    	<div class="l-dialog-btn-r"></div> -->
<!-- 		    	<div class="l-dialog-btn-inner" style="margin-top:9px" >设置权限</div> -->
<!-- 		    </div> -->
		    <div class="l-clear"></div>
	    </div>
    </div>
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/core/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerGrid2.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerListBox.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/report/ligerGrid.showFilter.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/report/grid.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/report/reportnew.js" type="text/javascript"></script>
    <script>
	    document.addEventListener("visibilitychange", function () {
	        if (document.hidden) {
	            window.close();
	        } else  {
	
	        }
	    }, false);
	    
	    $(function () {
// 	        var data = {
	                
	                
        	setTimeout(doinit(),2000);
// 	        }

	    });
	    
	    function doinit() {
// 	        FuRenReport("${data}").create();
		var a  =JSON.parse('${fparam}');
		if(a['fid']){
		    var p =  a;
		}else{
			var p = eval('${fparam}');
		}
		var t = eval('${fcolumn}');
		var rept = doCreateReport("${fname}", "${ftitle}", t, "${fclassname}","","${fexport}",p,"${furl}","${fmenu}", $(".pdfReport"));
//			doCreateReport(pname,ptitle,pcolumn,pclassname, psearch, psexport,pparam,purl,pmenu)
//	        var dialog = window.opener.dialog;
        //加载设置页面
//	        doInitTable(FRReport.base);
//	        doInitGrid(FRReport.column);
// 	        pdfshow();
		//$(".pdfReport").attr("src", "../report/pdf?src=/station/transactionReport/historyOilTotalReportpdf?classNo=20180228003?&noCache=1522841039303");
	    }
        
		function doRpt() {
		    window.open(FRReport.xml,"","","true");
		}
		function doSet() {
		    attributeTable();
		}
        // 初期化Table
        function doInitTable(report) {
            // win标题
            $("#settitlesize").val(report.titleSize);
            $("#settitlebold").val(report.titleBold);
            $("#settitlename").val(report.title);
            // win左右
            $("#setheadnameleft").val(report.headLeft);
            $("#setheadnameright").val(report.headRight);
            $("#setheadsize").val(report.headSize);
            $("#setheadbold").val(report.headBold);
            return report;
        }
        // 初期化Grid
        var reportTableGrid;
        function doInitGrid(column) {
            var alignData = [{ value: "left", text: "居左" }, { value: "center", text: "居中" }, { value: "right", text: "居右" }];
            var widthData = [
                { value: "0", text: "0" }, { value: "30", text: "30" }, { value: "50", text: "50" }, { value: "80", text: "80" }, { value: "100", text: "100" },
                { value: "120", text: "120" }, { value: "150", text: "150" }, { value: "180", text: "180" }, { value: "200", text: "200" }, { value: "250", text: "250" },
                { value: "300", text: "300" }, { value: "350", text: "350" }, { value: "400", text: "400" }, { value: "450", text: "450" }, { value: "500", text: "500" }
            ];
            var GridData = {};

            //column格式化
            for (var c in column) {
                column[c].sum = column[c].sum == "true";
            }

            GridData.Rows = column;
            var gridPanle = $("#reportTableGrid");
            reportTableGrid = gridPanle.ligerGrid({
                columns: [
                    { display: '字段名', name: 'title', align: 'center', width: 110, minWidth: 30, editor: { type: 'text' } },
                    { display: '宽度', name: 'width', align: 'center', width: 50, minWidth: 30, editor: { type: 'select', data: widthData } },
                    { display: '对齐方式', name: 'align', align: 'center', width: 80, minWidth: 30, editor: { type: 'select', data: alignData }, render: fieldTypeRender },
                    { display: '统计列', name: 'sum', width: 55, render: checkboxRender }
                ], data: GridData, usePager: false,
                enabledEdit: true, clickToEdit: true, fixedCellHeight: false, inWindow: true, rownumbers: true,
                width: 666, height: 180, rowHeight: 24
            });
            return column;
        }
        //Grid方法重写 -- 复选框点击事件
        $("#reportTableGrid").on("click", ".chk-icon", function () {
            var grid = $.ligerui.get($(this).attr("gridid"));
            var rowdata = grid.getRow($(this).attr("rowid"));
            var columnname = $(this).attr("columnname");
            var checked = rowdata[columnname];
            grid.updateCell(columnname, !checked, rowdata);
        });

        // 设置窗口的创建
        var reprotAttributeWin;
        $(function () {
            reprotAttributeWin = $.ligerDialog.open({
                target: $("#reprotAttributeWin"),
                height: 390,
                width: 850,
                isHidden: true,
                isResize: true,
                showMax: false,
                showMin: false,
                buttons: [
                    { text: '确定', onclick: function (item, dialog) { doSaveData(); dialog.hidden(); $(".pdfReport").show(); }, cls: 'l-dialog-btn-highlight' },
                    { text: '取消', onclick: function (item, dialog) { dialog.hidden(); $(".pdfReport").show(); } }
                ]
            });
            reprotAttributeWin.hide();
        });

        //打开设置页面
        function attributeTable() {
            $(".pdfReport").hide();
            reprotAttributeWin.show();
            //reprotAttributeWin.active();
        }

        //加载pdf
        function pdfshow() {

            $(".pdfReport").attr("src", FRReport.pdf);
        }
        //加载pdf
        function pdfload(pdf) {
            //$(".pdfReport").css("height", parseInt(document.body.scrollHeight) - 20);
            //将&替换为||，否则&后面的数据无法接收
             pdf = pdf.replace(/\&/g,"||");
            //转2次码，防止中文乱码
//             pdf = encodeURI(timestamp(pdf));
//             pdf = encodeURI(pdf);
             var url = "../report/pdf?src=" + pdf;

            $(".pdfReport").attr("src", url);
            //$(".pdfReport").attr("src", "../report/pdf?src=/station/transactionReport/historyOilTotalReportpdf?classNo=20180228003?&noCache=1522841039303");
        }
        function timestamp(url) {
            //var getTimestamp=Math.random();
            var getTimestamp = new Date().getTime();
            var index = url.indexOf("?");
            if (index > -1) {
                if (index = url.length - 1)
                    url = url + "t=" + getTimestamp
                else
                    url = url + "&t=" + getTimestamp
            } else {
                url = url + "?t=" + getTimestamp
            }
            return url;
        }

        //设置保存
        function doSaveData() {
            reportTableGrid.endEdit();
            var data = {};
            data.className = dialog.report_classname;
            var report = {};
            report = {
                name: dialog.report_name, title: $('#settitlename').val(), titleSize: $('#settitlesize').val(),
                titleBold: $('#settitlebold').val(), headLeft: $('#setheadnameleft').val(),
                headRight: $('#setheadnameright').val(), headSize: $('#setheadsize').val(),
                headBold: $('#setheadbold').val()
            };
            data.report = report;
            var column = [];
            var tableColumn = reportTableGrid.getData();
            for (var i in tableColumn) {
                var width = tableColumn[i].width;
                var d = {};
                d.name = dialog.report_name;
                d.field = tableColumn[i].field;
                d.title = tableColumn[i].title;
                d.width = tableColumn[i].width;
                d.align = tableColumn[i].align;
                d.sum = tableColumn[i].sum;
                column.push(d);
            }
            data.column = column;
            var jsondata = JSON.stringify(data);
            $.ajax({
                type: 'post',
                url: "saveReport",
                contentType: "application/json",
                data: jsondata,
                dataType: "json",
                success: function (d) {
                    $('.pdfReport').attr('src', "about:back");
                    pdfshow();
                }
            });
        }
    </script>
</body>
</html>
