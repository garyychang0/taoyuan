<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script> 
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">

<!-- 标题部分 -->
<h3 class="hmdl_title_h3">系统管理
    <span class="hmdl_title_span">
        &nbsp;/&nbsp;用户管理
    </span>
</h3>
<hr class="hmdl_title_hr"/>

<!-- 查询条件部分 -->
<div id="grid_search" style="margin-left:8px;margin-top:20px;">
    <form name="searchForm" id="searchForm">
        <ul>
            <li class="label">
                &emsp;账户编号：<input type="text" id="search_user_sn" name="userSn" placeholder="搜索账户编号" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                &emsp;用户名称：<input type="text" id="search_user_name" name="userName" placeholder="搜索用户名称" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                &emsp;手机号码：<input type="text" id="search_phone_num" name="phoneNum" placeholder="搜索手机号码" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                &emsp;角色名称：<select id="search_role_id" name="roleId" style="width:160px;height:23px">
                      <option value=""> == 请选择一个角色  == </option>              
                      <c:forEach items="${rolelist}" var="i">
                            <option value="${i['key']}">${i['value']}</option>  
                      </c:forEach>
                </select>
                &emsp;<button id="btnSearch" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
                &emsp;<button id="doReport" type="button" class="btn btn-success  btn-small btn-mini"><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;导出&nbsp;&nbsp;</button>
            </li>
        </ul>
    </form>
</div>
<br/>

<!-- 列表部分 -->
<div id="grid"></div>

<!-- 增改部分 -->
<div id="edit" style="display:none;">
    <form name="saveForm" id="saveForm" >
    <input type="hidden" id="id" name="id" />
    <table  class="default">
        <tr style="height:50px" >
            <td colspan="4" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
            <i class="fa fa-address-book"></i>&emsp;用户信息</td>
        </tr>
        <!-- <tr id="select_account" style="dispaly:none">
            <td align="right">&emsp;选择账号：&emsp;<span style="color:red">*</span></td>
            <td align="left" colspan="3">
                <input id="edit_type" name="userSn" type="hidden" style="border:0;"/>
            </td>
        </tr> -->
        <tr>
            <td align="right">&emsp;用户名：&emsp;<span style="color:red">*</span></td>
            <td align="left" colspan="3">
                <input id="userName" name="userName"  style="text-indent:5px;width:180px" placeholder="请输入用户名"/>
            </td>
         </tr>
         <tr>
            <td align="right">&emsp;登录名：&emsp;<span style="color:red">*</span></td>
            <td align="left" colspan="3">
                <input id="loginName" name="loginName"  style="text-indent:5px;width:180px" placeholder="请输入登录名"/>
            </td>
         </tr>
         <tr>
            <td align="right">&emsp;手机号码：&emsp;<span style="color:red">*</span></td>
            <td align="left" colspan="3">
                <input id="phoneNum" name="phoneNum"  style="text-indent:5px;width:180px" placeholder="请输入手机号码"/>
            </td>
         </tr>
         <tr>
            <td align="right">&emsp;登录密码：&emsp;<span style="color:red">*</span></td>
            <td align="left" colspan="3">
                <input type="password" id="password" name="password"  style="text-indent:5px;width:180px" placeholder="请输入登录 密码"/>
            </td>
         </tr>
         <tr>
            <td align="right">&emsp;确认密码：&emsp;<span style="color:red">*</span></td>
            <td align="left" colspan="3">
                <input type="password" id="repassword" name="repassword"  style="text-indent:5px;width:180px" placeholder="请输入确认密码"/>
            </td>
         </tr>
         <tr>
            <td align="right">&emsp;角色选择：&emsp;<span style="color:red">*</span></td>
            <td align="left" colspan="3">
                <select id="roleId" name="roleId" style="width:180px">
                      <option value=""> == 请选择一个角色  == </option>
                      <c:forEach items="${rolelist}" var="i">
                            <option value="${i['key']}">${i['value']}</option>  
                      </c:forEach>
                </select>
            </td>
        </tr>
    </table>
    <table align="center" style="margin-top: 10px">
        <tr>
            <td>
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;提交&nbsp;&nbsp;</button>
                &emsp;
                <button type="button" class="btn btn-warning" onclick="editCancel()"><i class="fa fa-close"></i>&nbsp;&nbsp;返回&nbsp;&nbsp;</button>
            </td>
        </tr>
    </table>
    </form>
</div>
<!--  数据权限 -->
<div id="authWin" style="display: none;"></div>
<!--  图库权限 -->
<div id="authPictureWin" style="display: none;">
	<div id="gridAuth"></div>
</div>
<!-- 导出用Form -->
<form action="../report/show" method="post" target="_blank" id="reportForm">
    <input type="hidden" name="fparam" id="fparam"></input>
    <input type="hidden" name="fcolumn" id="fcolumn"></input>
    <input type="hidden" name="fname" id="fname"></input>
    <input type="hidden" name="fclassname" id="fclassname"></input>
    <input type="hidden" name="fmenu" id="fmenu"></input>
    <input type="hidden" name="ftitle" id="ftitle"></input>
    <input type="hidden" name="fexport" id="fexport"></input>
    <input type="hidden" name="furl" id="furl"></input>
</form>

<script type="text/javascript">
var grid;
var gridAuth;
var authWin;
var authTree;
var authPictureWin;
$(function(){
    // 初期化项目
    initGrid();

    // 绑定查询按钮
    $("#btnSearch").click(function () {
        var parms = serializeObject($("#searchForm"));
           for(var p in parms){
               grid.setParm(p,parms[p].trim());
           }
           grid.loadData();
       });
    
    // 绑定导出按钮
    $("#doReport").click(function () {
         // 用于获取列信息和搜索信息
        $('#fparam').val(JSON.stringify(grid.options.parms));
        $('#fcolumn').val(JSON.stringify(grid.getColumns()));
        // 报表的唯一标示
        $('#fname').val("userReport");
        // 报表容器的className
        $('#fclassname').val("fr.cn.com.edu.taoyuan.sys.bean.UserBean");
        // 报表的默认标题
        $('#ftitle').val("用户信息报表");
        $('#fmenu').val("user");
        // pdf的加载地址【必输项】
        $('#furl').val("reportpdf");//新版本reportnew.js无默认值，必须填写
        // 报表导出的页面地址【必输项】
        $('#fexport').val("report");//新版本reportnew.js无默认值，必须填写
        // 导出
        $('#reportForm').submit();
       });
    
    function initGrid(){
        grid=$("#grid").ligerGrid({
            title:'&nbsp;&nbsp;<span style="color:#386792">用户信息一览</span>',
            headerImg:'/hmdl/static/images/list.png',
            columns: [
                { display: '登录名', name: 'loginName',width:120},
                { display: '账户编号', name: 'userSn',width:100},
                { display: '用户名', name: 'userName',width:100},
                { display: '手机号码', name: 'phoneNum',width:100},
                { display: '角色名称', name: 'roleName',width:100},
                { display: '创建时间', name: 'createDate',dateFormat: "yyyy-MM-dd hh:mm:ss",width:150},
                { display: '更新时间', name: 'updateDate',dateFormat: "yyyy-MM-dd hh:mm:ss",width:150},
            ],
            toolbar: {
                items: [<%=request.getAttribute("btn")%>,
						{ text: '刷新',click: function(){grid.reload();}, icon: 'fa fa-refresh',color:'primary' },
						]
            },
            dataAction: 'server',
            url:'search',
            root:'data',
            record:'count',
            height: '97%',
            pageSize: 10,
            checkbox: true,
            rownumbers: true,
        });
    }
    $(function () {
        $("#saveForm").validator({
            ignore: ":hidden", theme: "yellow_bottom", timely: 1, stopOnError: true,
            rules: { digits: [/^\d+(\.\d+)?$/, '{0}必须为数字！'],  
                        email:[/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,'{0}格式不正确！']
            },
            fields: {
                userName: '账号名称:required,length[1~30]',
                loginName: '登录名称:required,length[1~30]',
                phoneNum: '手机号码:required,digits',
                password: '密码:required,digits,length[6~30]',
                repassword: '确认密码:required,digits,length[6~30]',
                roleId:'角色:required',
            },
            valid: function (form) {
                if ($("#repassword").val() != $("#password").val()) {
                        $.ligerDialog.warn("两次密码输入不一致！", "提示");
                        return;
                }
                // 角色必须选择项目
                formSave(form);
                }
        });
    })
});

//新增OR编辑处理
function operate(type) {
    resetAll();
    var rows = grid.getSelectedRows();
    if ((rows == null || rows.length != 1) && type != 1) {
        $.ligerDialog.warn("请选择一条记录！", "提示");
        return;
    };
    if (type == 2) {
        showOrHideEdit(2, rows[0].id);
    } else {
        $.ajax({
            type: 'post',
            url: 'getEmployee',
            cache: false,
            dataType: 'json',
            success: function(data) {
                // 下拉列表
                $("#edit_type").ligerComboBox({
                    data: data.retObj,
                    valueField: 'key',
                    textField: 'value',
                    isMultiSelect: false,
                    width: 180,
                });
                // 样式覆盖
                $("#edit_type_txt").css("height","25.5px");
            },
            error: function() {
                $.ligerDialog.error("加载账号失败！", "提示");
            }
        });
        showOrHideEdit(1);
    }
}

//保存表单数据
function formSave(form) {
    var data = serializeObject($("#saveForm"));
    $.ajax({
        type: 'post',
        url: "save",
        data: data,
        cache: false,
        dataType: 'json',
        success: function(data) {
            if (data && data.retCode == 100) {
                $.ligerDialog.success('保存成功！', "提示",
                function(opt) {
                    $("#grid").show();
                    $("#grid_search").show();
                    $("#edit").hide();
                    grid.reload();
                });
            } else if (data && data.retCode == -100) {
                $.ligerDialog.error("登录名已存在,请重新输入！", "提示");
            } else{
                $.ligerDialog.error("保存失败，请稍后再试！", "提示");
            }
        }
    });
}

// 隐藏或显示画面
function showOrHideEdit(type, id) {
    // 0显示编辑页面
    if (type == 1) {
        $("#select_account").show();
        $("#grid").hide();
        $("#grid_search").hide();
        $("#edit").show();
        
        // 1隐藏编辑页面
    } else if (type == 2) {
        $("#select_account").hide();
        getDetail(id);
    } else {
        $("#grid").show();
        $("#grid_search").show();
        $("#edit").hide();
    }
}

//获取表单明细数据
function getDetail(id) {
    $.ajax({
        type: 'get',
        url: "getDetail",
        data: {
            id: id
        },
        cache: false,
        dataType: 'json',
        success: function(data) {
            loadData(data.retObj);
            $("#grid").hide();
            $("#grid_search").hide();
            $("#edit").show();
            // 详细不显示选择账号，这个不能变
            // $("#edit_type").ligerGetComboBoxManager().setDisabled();
        }
    });
}

//删除
function del() {
    var rows = grid.getSelectedRows();
    var ids = "";
    // 这边限制了一条下面还有什么意义去掉这边的
//     if (rows == "" || rows.length != 1) {
//         $.ligerDialog.warn("请选择一条记录！", "提示");
//         return;
//     }
    for (var i = 0; i < rows.length; i++) {
        if (i > 0) {
            ids += "|";
        }
        // 数据少此处未加排他验证 MARK一下
        ids += rows[i].id;
    }
    $.ligerDialog.confirm("确定要删除选中记录？", "提示",
    function(confirm) {
        if (confirm) {
            $.post("doDelete", {
                ids: ids
            },
            function(data) {
                if (data.retCode>0) {
                    $.ligerDialog.success('删除成功！', "提示",
                    function(opt) {
                        grid.reload();
                    });
                } else {
                    $.ligerDialog.error("删除失败！", "提示");
                }
            },
            "json")
        }
    });
}

//加载权限信息
function loadAuth() {
    var rows = grid.getSelectedRows();
    if (rows == null || rows.length != 1) {
        $.ligerDialog.warn("请选择一条记录！");
        return;
    }
    $.ajax({
        type: 'post',
        url: "loadAuth",
        data: {
            userSn: getSelected()
        },
        cache: false,
        dataType: 'json',
        success: function(data) {
            markTree(data);
        },
        error: function() {
            $.ligerDialog.error("权限检索失败！", "提示");
        }
    });
}

// 创建权限列表树
function markTree(data) {
    authTree = $("#authWin").ligerTree({
        nodeWidth: 250,
        data: data,
        idFieldName: 'id',
        parentIDFieldName: 'pid',
        textFieldName: 'text',
        checkbox: true
    });
    if (!authWin) {
        authWin = $.ligerDialog.open({
            target: $("#authWin"),
            height: 350,
            width: 400,
            isResize: true,
            showMax: true,
            showMin: true,
            title: '数据权限',
            buttons: [{
                text: '确定',
                onclick: function(item, dialog) {
                    updateAuth();
                }
            },
            {
                text: '取消',
                onclick: function(item, dialog) {
                    authWin.hide();
                }
            }]
        });
    } else {
        authWin.show();
    }
}

// 更新权限
function updateAuth() {
    var user_sn = getSelected();
    var c_sns = getChecked();
    $.ajax({
        type: 'post',
        url: "updateAuth",
        data: {
            c_sns: c_sns,
            user_sn: user_sn
        },
        cache: false,
        dataType: 'json',
        success: function(data) {
            $.ligerDialog.success("权限设置成功！", "提示",
            function() {
                authWin.hide();
            });
        },
        error: function() {
            $.ligerDialog.error("权限处理失败！", "提示");
        }
    });
}

// 获取选中的权限内容
function getChecked() {
    var c_sns = "";
    var data = authTree.getChecked();
    if (data.length > 0) {
        for (var i = 0; i < data.length; i++) {
            if (c_sns != "") c_sns += ",";
            c_sns += data[i]["data"]["id"];
        }
    }
    return c_sns;
}

//获取选中项
function getSelected() {
    var user = grid.getSelected();
    return user.userSn;
}

// 返回按钮
function editCancel() {
    showOrHideEdit(3);
    resetAll();
}

// 重置
function resetAll() {
    // FORM清空
    $("#saveForm")[0].reset();
    // 隐藏项目清空
    $("#id").val("");
    $("#edit_type").val("");
}

function pictureAuth(){
    var rows = grid.getSelectedRows();
    if (rows == null || rows.length != 1) {
        $.ligerDialog.warn("请选择一条记录！");
        return;
    }
    authPictureWin = $.ligerDialog.open({
	    target: $("#authPictureWin"),
	    headerImg:'/hmdl/static/images/list.png',
	    height: 350,
	    width: 250,
	    isResize: true,
	    allowClose:false,
	    showMax: false,
	    showMin: false,
	    title: '图库权限信息',
	    buttons: [{
	        text: '确定',
	        onclick: function(item, dialog) {
	            updatePictureAuth();
	            authPictureWin.hide();
	        }
	    },
	    {
	        text: '取消',
	        onclick: function(item, dialog) {
	            authPictureWin.hide();
	        }
	    }]
	});
    gridAuth=$("#gridAuth").ligerGrid({
        columns: [
            { display: '名称', name: 'name',width:120},
        ],
        dataAction: 'server',
        usePager:false,
        width:200,
        height:300,
   		enabledSort:false,
        url:'pictureAuth',
    	parms:[{name:"userSn",value:getSelected()}],
        root:'data',
        record:'count',
        checkbox: true,
        rownumbers: true,
        isChecked: f_isChecked

    });
    //$("#gridAuth").show();
}

function f_isChecked(rowdata)
{
    if (rowdata.isChecked == true) 
        return true;
    return false;
}


//更新权限
function updatePictureAuth() {
    var user_sn = getSelected();
    var c_sns = "";
    var data = gridAuth.getSelectedRows();
    if (data.length > 0) {
        for (var i = 0; i < data.length; i++) {
            if (c_sns != "") c_sns += ",";
            c_sns += data[i]["id"];
        }
    }
    $.ajax({
        type: 'post',
        url: "updatePictureAuth",
        data: {
            c_sns: c_sns,
            user_sn: user_sn
        },
        cache: false,
        dataType: 'json',
        success: function(data) {
            $.ligerDialog.success("权限设置成功！", "提示",
            function() {
                gridAuth.hide();
            });
        },
        error: function() {
            $.ligerDialog.error("权限处理失败！", "提示");
        }
    });
}
</script>
</body>
</html>
