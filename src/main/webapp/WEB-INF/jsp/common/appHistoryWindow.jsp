<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/echarts/echarts.js" type="text/javascript"></script> 
    <script src="/hmdl/static/js/My97DatePicker/WdatePicker.js" type="text/javascript"></script> 
    <!-- 引入 vintage 主题 -->
	<script src="/hmdl/static/js/echarts/themes/dark.js"></script>
	<script src="/hmdl/static/js/echarts/themes/chalk.js"></script>
	<script src="/hmdl/static/js/echarts/themes/halloween.js"></script>
	<script src="/hmdl/static/js/echarts/themes/wonderland.js"></script>
	<script src="/hmdl/static/js/echarts/themes/essos.js"></script>
	<style>
	.l-box-dateeditor{
		position: fixed!important;
		top:0!important;
		right:10vw!important; 
	}
	.l-dialog{
		position: fixed!important;
	}
	</style>
</head>

<!-- body部分 -->
<body style="height:100%;overflow-x:hidden;overflow-y:hidden;padding-top:2px;">
<!-- 查询条件部分 -->
<div id="grid_search" style="display:none">
    <form name="searchForm" id="searchForm">
        <ul>
            <li>
				<table class="default">
					<tr>
						<td align="right">主题</td>
						<td align="left">
							<select id="themeId">
								<option value="">默认</option>
								<option value="dark">黑色</option>
								<option value="chalk">黑色曲线</option>
								<option value="walden">蓝色</option>
								<option value="wonderland">绿色</option>
								<option value="halloween">橙色</option>
								<option value="essos">红色曲线</option>
							</select>
						</td>
						<td align="right">查看实时数据</td>
						<td align="left">
		                <select  name="realTime" id="search_realTime" onchange="searchRealTime()">
							<option value="">关闭</option>
							 <option value="5">开启</option>
						</select>
						</td>
					</tr>
					<tr>
						<td align="right">时间单位</td>
						<td align="left" colspan="3">
						    <select  name="search_unit" id="search_unit">
								<option value="">请选择</option>
								<option value="h">小时</option>
								<option value="d">天</option>			
								<option value="m">月</option>					
								<option value="y">年</option>
							</select>
						</td>
					</tr>
					<tr>
						<td align="right">起始日期</td>
						<td align="left"><input type="text" autocomplete="off" readonly="readonly" name="search_begin_date" id="search_begin_date"/>
						</td>
						<td align="right">结束日期</td>
						<td align="left"><input type="text" autocomplete="off" readonly="readonly" name="search_end_date" id="search_end_date"/>
						</td>
					</tr>
				</table>
            </li>
        </ul>
    </form>
</div>
<div id="indexMain"  position="" style="width:100%;height:500px">
            <div style="font-size:36px;margin-left:40%;margin-top:200px;font-family:微软雅黑;font-weight:bold;">
                    <div style="text-align:left;height:80px;">历史曲线首页</div>
            </div>
</div>
<div id="layout1">
<div style="position:fixed;top:0px;right:50px;z-index:2">
	<button style="margin-left: 10px" onclick="search()" id="" type="button" class="btn btn-info btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;条件查找&nbsp;&nbsp;</button>
	<button style="margin-left: 10px" onclick="goback()" id="" type="button" class="btn btn-default btn-small btn-mini"><i class="fa fa-reply"></i>&nbsp;&nbsp;返回&nbsp;&nbsp;</button>
</div>
<!-- 统计图表 -->
<div id="main"  position="" style="display:none;width:96%;height:1000px"></div>
</div>
<script>
var u = navigator.userAgent, app = navigator.appVersion; 
var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //android终端或者uc浏览器 
var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端 
var searchDialog;
if(isAndroid){
	var is_height = window.screen.height+70;
	var is_width = window.screen.width;
}
if(isiOS){
	var is_height = window.screen.width;
	var is_width = window.screen.height;
}
var SHORTNAME ="${shortName}";
var VARLIST = "";
var var_grid;
var myChart;
var SHOW_STATUS;//实时模式0关闭 1开启
var p_eventId = "${eventId}";
function goback()
{
	window.history.go(-1);
}
$(function () {
    $("#main").height((window.screen.width) + 'px');
    if (!window.WebSocket) {
        window.WebSocket = window.MozWebSocket;
    }
    if (window.WebSocket) {
        ws = new WebSocket("${url}");
        ws.onopen = function (e) {
        	ws.send('{action:"connect",bds:"'+SHORTNAME+'"}');
        }
    }else {
        $.ligerDialog.error("你的浏览器不支持 WebSocket！", "提示");
    };
	//实时刷新单图
    SHOW_STATUS=1;
    $("#main").show();
    $("#indexMain").hide();
    socketInt();
});
function socketInt(){
    $.ajax({
        type: 'get',
        url: "../stationDataDetail/getDetail",
        data: { id: p_eventId},
        cache: false,
        dataType: 'json',
        success: function (data) {
            var data_numberName= data.retObj.numberName;
	    	var data_TypeName=data.retObj.dataTypeName;
	    	var data_varMin=0;
	    	var data_varMax=100;
	    	var data_varRemark=data.retObj.remark;
	    	VARLIST =new Object;
	    	VARLIST.varRemark="undefined,"+data_varRemark;
	    	VARLIST.varTypeName ="undefined,"+data_TypeName;
	    	VARLIST.varId ="undefined,"+data.retObj.id;
	    	VARLIST.varShortname ="undefined,"+data.retObj.shortName;
	    	VARLIST.varMax ="undefined,"+data.retObj.varMax;
	    	VARLIST.varMin ="undefined,"+data.retObj.varMin;
	    	VARLIST.varName ="undefined,"+data.retObj.varName;
	    	VARLIST.varNumberName ="undefined,"+data.retObj.numberName;
	    	//获取后台数据
	    	var gridLength = 1;
	    	document.getElementById('main').style.height=(is_height-70)+"px";
	    	// 基于准备好的dom，初始化echarts图表
	    	myChart = echarts.init(document.getElementById('main'));
	    	var option_grid=new Array();
	    	var option_legend=new Array();
	    	var option_dataZoom=new Array();
	    	var option_xAxis=new Array();
	    	var option_yAxis=new Array();
	    	var option_series=new Array();
	    	for(var i=0;i<gridLength;i++){
	    		option_legend.push(data_varRemark);
	    		option_dataZoom.push(i);
	    		option_series.push({ //系列名称，如果启用legend，该值将被legend.data索引相关  
	    			"name" : data_varRemark,
	    			//图表类型，必要参数！如为空或不支持类型，则该系列数据不被显示。  
	    			"type" : "line",
	    			"itemStyle" : { normal: {label : {show: true}}},
	    			//系列中的数据内容数组，折线图以及柱状图时数组长度等于所使用类目轴文本标签数组axis.data的长度，并且他们间是一一对应的。数组项通常为数值  
	    			"data" : []
	    		});		
	    	}
	    	var title_text = data.retObj.varName+'曲线';
	    	// 为echarts对象加载数据 
	    	myChart.setOption({
	    		title : {
	    			//主标题文本，'\n'指定换行  
	    			text : title_text,
	    			subtext: '',
	    			//水平安放位置，默认为左侧，可选为：'center' | 'left' | 'right' | {number}（x坐标，单位px）  
	    			x : 'center',
	    			//垂直安放位置，默认为全图顶端，可选为：'top' | 'bottom' | 'center' | {number}（y坐标，单位px）  
	    			y : 'top'
	    		},
	    		//提示框，鼠标悬浮交互时的信息提示  
	    		tooltip : {
	    			show : true,
	    			//触发类型，默认（'item'）数据触发，可选为：'item' | 'axis'  
	    			trigger : 'axis'
	    		},
	    		//图例，每个图表最多仅有一个图例  
	    		legend : {
	    			//legend的data: 用于设置图例，data内的字符串数组需要与sereis数组内每一个series的name值对应  
	    			data : option_legend,
	    			x : 'center',
	    			top:'22px'
	    		},
	    	    grid: {
	    	        top: '50px',
	    	        height: (is_height-180)+'px'
	    	    },
	    	    dataZoom: [
	    	               {
	    	                   show: true,
	    	                   realtime: true
	    	               },
	    	               {
	    	                   type: 'slider',
	    	                   yAxisIndex: 0,
	    	                   filterMode: 'empty'
	    	               },
	    	    ],
	    		//直角坐标系中横轴数组，数组中每一项代表一条横轴坐标轴，仅有一条时可省略数值  
	    		//横轴通常为类目型，但条形图时则横轴为数值型，散点图时则横纵均为数值型  
	    		xAxis : {
	    			type : 'category',
	    		    boundaryGap : false,
	    	   		axisLine: {onZero: true},
	    			name : '日期',
	    			splitLine : {show : true},
	    			//类目型坐标轴文本标签数组，指定label内容。 数组项通常为文本，'\n'指定换行 
	    			data : []
	    		},
	    		//直角坐标系中纵轴数组，数组中每一项代表一条纵轴坐标轴，仅有一条时可省略数值  
	    		//纵轴通常为数值型，但条形图时则纵轴为类目型  
	    		yAxis : {
	    		    type : 'value',
	    			name : data_TypeName
	    		},
	    		//sereis的数据: 用于设置图表数据之用。series是一个对象嵌套的结构；对象内包含对象  
	    		series : option_series
	    	});
	        myChart.showLoading();
	    	var data_x=new Array();
	    	var data_y=new Array();
	    	var openFirst = 1;
	    	//获取后台数据
	    	ws.onmessage = function (e) { 
			    if(SHOW_STATUS==1){
			    var d = JSON.parse(e.data);
				var option_xAxis=new Array();
				var option_series=new Array();
			   	// 填入数据
	            if (d.func == "03" && typeof(d[data_numberName]) != "undefined") {
	               data_x.push(formatDate(d.dtime));
	               var temp_data = d[data_numberName];
	               data_y.push(temp_data);
					if(data_x.length>100){
					    data_x= data_x.slice(1);
					    data_y= data_y.slice(1);
					}
					option_xAxis=data_x;
					option_series.push({ //系列名称，如果启用legend，该值将被legend.data索引相关  
					"name" : data_varRemark,
					//图表类型，必要参数！如为空或不支持类型，则该系列数据不被显示。  
					"type" : "line",
					"itemStyle" : { normal: {label : {show: false}}},
					//系列中的数据内容数组，折线图以及柱状图时数组长度等于所使用类目轴文本标签数组axis.data的长度，并且他们间是一一对应的。数组项通常为数值  
					"data" : data_y
					});	
			   		myChart.hideLoading();
				    if(openFirst==1){
						myChart.setOption({
							xAxis : {
								type : 'category',
							    boundaryGap : false,
						   		axisLine: {onZero: true},
								name : '日期',
								splitLine : {show : true},
								//类目型坐标轴文本标签数组，指定label内容。 数组项通常为文本，'\n'指定换行 
								data : option_xAxis
							},
				    		yAxis : {
							    min:temp_data/2,
							    max:temp_data*2,
							    type : 'value',
								name : data_TypeName
				    		},
							//sereis的数据: 用于设置图表数据之用。series是一个对象嵌套的结构；对象内包含对象  
							series : option_series
						});
						openFirst = 2;
				    }else{
				        myChart.setOption({
						xAxis : {
							type : 'category',
						    boundaryGap : false,
					   		axisLine: {onZero: true},
							name : '日期',
							splitLine : {show : true},
							//类目型坐标轴文本标签数组，指定label内容。 数组项通常为文本，'\n'指定换行 
							data : option_xAxis
						},
						//sereis的数据: 用于设置图表数据之用。series是一个对象嵌套的结构；对象内包含对象  
						series : option_series
						});
				    }
	            }
			};
			}
		},
		error : function() {
		     	$.ligerDialog.warn("查询失败！","错误");
		}
    });
}
function socketDataSingle(){
    myChart.showLoading();
	var data_x=new Array();
	var data_y=new Array();
	var data_numberName=VARLIST.varNumberName.split(","); //字符分割 
	data_numberName = data_numberName.slice(1);	    	
	var data_varName = VARLIST.varName.split(","); //字符分割 
	data_varName = data_varName.slice(1);
	var data_TypeName=VARLIST.varTypeName.split(","); //字符分割 
	data_TypeName = data_TypeName.slice(1);
	var data_varRemark=VARLIST.varRemark.split(","); //字符分割 
	data_varRemark = data_varRemark.slice(1);
	//获取后台数据
	var gridLength = data_numberName.length; 	
	for(var i=0;i<gridLength;i++){
	    data_x[i] = new Array();
	    data_y[i] = new Array();
	}
    var openFirst = 1;
	ws.onmessage = function (e) { 
	    if(SHOW_STATUS==1){
	    var d = JSON.parse(e.data);
		var option_xAxis=new Array();
		var option_series=new Array();
	   	// 填入数据
	    for(var i=0;i<gridLength;i++){
            if (d.func == "03" && typeof(d[data_numberName[i]]) != "undefined") {
               data_x[i].push(formatDate(d.dtime));
               var temp_data = d[data_numberName[i]];
               data_y[i].push(temp_data);
               //data_y[i].push(d[data_numberName[i]].substring(0,d[data_numberName[i]].indexOf(".") + 3));
				if(data_x[i].length>100){
				    data_x[i] = data_x[i].slice(1);
				    data_y[i] = data_y[i].slice(1);
				}
				option_xAxis=data_x[i];
				option_series.push({ //系列名称，如果启用legend，该值将被legend.data索引相关  
				"name" : data_varRemark[i],
				//图表类型，必要参数！如为空或不支持类型，则该系列数据不被显示。  
				"type" : "line",
				"itemStyle" : { normal: {label : {show: false}}},
				//系列中的数据内容数组，折线图以及柱状图时数组长度等于所使用类目轴文本标签数组axis.data的长度，并且他们间是一一对应的。数组项通常为数值  
				"data" : data_y[i]
				});	
	   			myChart.hideLoading();
	    		if(openFirst==1){
				myChart.setOption({
				    xAxis : {
						type : 'category',
					    boundaryGap : false,
				   		axisLine: {onZero: true},
						name : '日期',
						splitLine : {show : true},
						//类目型坐标轴文本标签数组，指定label内容。 数组项通常为文本，'\n'指定换行 
						data : option_xAxis
					},
		    		yAxis : {
					    min:temp_data/2,
					    max:temp_data*2,
					    type : 'value',
						name : data_TypeName
		    		},
					//sereis的数据: 用于设置图表数据之用。series是一个对象嵌套的结构；对象内包含对象  
					series : option_series
				});
				openFirst = 2;
	   	  		}else{
					myChart.setOption({
					xAxis : {
						type : 'category',
					    boundaryGap : false,
				   		axisLine: {onZero: true},
						name : '日期',
						splitLine : {show : true},
						//类目型坐标轴文本标签数组，指定label内容。 数组项通常为文本，'\n'指定换行 
						data : option_xAxis
					},
					//sereis的数据: 用于设置图表数据之用。series是一个对象嵌套的结构；对象内包含对象  
					series : option_series
					});
				};
            }
	    }
		}
	}
    
}

//实时刷新单图
function doChartRealSingle(){
    var data_varName = VARLIST.varName.split(","); //字符分割 
	data_varName = data_varName.slice(1);
	var data_numberName=VARLIST.varNumberName.split(","); //字符分割 
	data_numberName = data_numberName.slice(1);
	var data_TypeName=VARLIST.varTypeName.split(","); //字符分割 
	data_TypeName = data_TypeName.slice(1);
	var data_varMin=VARLIST.varMin.split(","); //字符分割 
	data_varMin = data_varMin.slice(1);
	var data_varMax=VARLIST.varMax.split(","); //字符分割 
	data_varMax = data_varMax.slice(1);
	var data_varRemark=VARLIST.varRemark.split(","); //字符分割 
	data_varRemark = data_varRemark.slice(1);
	if(data_varMin[0]==""){
	    data_varMin[0]=0;
	}else{
	    data_varMin[0]=parseFloat(data_varMin[0]);
	}
	if(data_varMax[0]==""){
	    data_varMax[0]=100;
	}else{
	    data_varMax[0]=parseFloat(data_varMax[0]);  
	}
	//获取后台数据
	var gridLength = data_numberName.length;
	// 基于准备好的dom，初始化echarts图表
	myChart = echarts.init(document.getElementById('main'), $("#themeId").val());
	var option_grid=new Array();
	var option_legend=new Array();
	var option_dataZoom=new Array();
	var option_xAxis=new Array();
	var option_yAxis=new Array();
	var option_series=new Array();
	
	for(var i=0;i<gridLength;i++){
		option_legend.push(data_varRemark[i]);
		option_dataZoom.push(i);

		option_series.push({ //系列名称，如果启用legend，该值将被legend.data索引相关  
			"name" : data_varRemark[i],
			//图表类型，必要参数！如为空或不支持类型，则该系列数据不被显示。  
			"type" : "line",
			"itemStyle" : { normal: {label : {show: true}}},
			//系列中的数据内容数组，折线图以及柱状图时数组长度等于所使用类目轴文本标签数组axis.data的长度，并且他们间是一一对应的。数组项通常为数值  
			"data" : []
		});		
	}
    var title_text = data_varName[0]+'曲线';
	// 为echarts对象加载数据 
	myChart.setOption({
		title : {
			//主标题文本，'\n'指定换行  
			text : title_text,
			subtext: '',
			//水平安放位置，默认为左侧，可选为：'center' | 'left' | 'right' | {number}（x坐标，单位px）  
			x : 'center',
			//垂直安放位置，默认为全图顶端，可选为：'top' | 'bottom' | 'center' | {number}（y坐标，单位px）  
			y : 'top'
		},
		//提示框，鼠标悬浮交互时的信息提示  
		tooltip : {
			show : true,
			//触发类型，默认（'item'）数据触发，可选为：'item' | 'axis'  
			trigger : 'axis'
		},
		//图例，每个图表最多仅有一个图例  
		legend : {
			//legend的data: 用于设置图例，data内的字符串数组需要与sereis数组内每一个series的name值对应  
			data : option_legend,
			x : 'center',
			top:'22px'
		},
	    grid: {
   	        top: '50px',
   	        height: (is_height-180)+'px'
	    },
	    dataZoom: [
	               {
	                   show: true,
	                   realtime: true
	               },
	               {
	                   type: 'slider',
	                   yAxisIndex: 0,
	                   filterMode: 'empty'
	               },
	    ],
		//直角坐标系中横轴数组，数组中每一项代表一条横轴坐标轴，仅有一条时可省略数值  
		//横轴通常为类目型，但条形图时则横轴为数值型，散点图时则横纵均为数值型  
		xAxis : {
			type : 'category',
		    boundaryGap : false,
	   		axisLine: {onZero: true},
			name : '日期',
			splitLine : {show : true},
			//类目型坐标轴文本标签数组，指定label内容。 数组项通常为文本，'\n'指定换行 
			data : []
		},
		//直角坐标系中纵轴数组，数组中每一项代表一条纵轴坐标轴，仅有一条时可省略数值  
		//纵轴通常为数值型，但条形图时则纵轴为类目型  
		yAxis : {
		    type : 'value',
			name : data_TypeName[0]
		},
		//sereis的数据: 用于设置图表数据之用。series是一个对象嵌套的结构；对象内包含对象  
		series : option_series
	});
    
}

//一张图显示
function doChartSingle(parms){
var manager = $.ligerDialog.waitting('正在加载中,数据庞大请稍候...');
$.ajax({
	type:'post',
	async:true,
	url:'../analysisChart/doChart',
	data:parms,
	dataType:"json",
	success:function(result){
	    manager.close();
		var data = result.retObj.retObj;
		//获取后台数据
		var gridLength = data.length;
		// 基于准备好的dom，初始化echarts图表
		myChart = echarts.init(document.getElementById('main'), $("#themeId").val());
		//处理最大最小值
		var varList = JSON.parse(parms.varList);
		var data_min= new Array(); //定义一数组 
		data_min=varList['varMin'].split(","); //字符分割 
		data_min = data_min.slice(1);
		var data_max= new Array(); //定义一数组 
		data_max=varList['varMax'].split(","); //字符分割 
		data_max = data_max.slice(1);
		var data_varRemark=VARLIST.varRemark.split(","); //字符分割 
		data_varRemark = data_varRemark.slice(1);
	    var data_varName = VARLIST.varName.split(","); //字符分割 
		data_varName = data_varName.slice(1);
		if(data_min[0]==""){
		    data_min[0]=0;
		}else{
		    data_min[0]=parseFloat(data_min[0]);
		}
		if(data_max[0]==""){
		    data_max[0]=100;
		}else{
		    data_max[0]=parseFloat(data_max[0]);
		}
		var option_grid=new Array();
		var option_legend=new Array();
		var option_dataZoom=new Array();
		var option_xAxis=new Array();
		var option_yAxis=new Array();
		var option_series=[];
		for(var i=0;i<gridLength;i++){
			option_legend.push(data_varRemark[i]);
			option_dataZoom.push(i);
			var data_x = new Array();
			for(var j=0;j<data[i].length;j++){
			    if(data[i][j]['x']!=""){
			    var obj = JSON.parse(data[i][j]['x']);
			     if($("#search_unit").val()=="y"){
					data_x.push(obj['year']);
			     }else if($("#search_unit").val()=="m"){
					data_x.push(obj['year']+"-"+obj['month']);
			     }else if($("#search_unit").val()=="d"){
					data_x.push(obj['year']+"-"+obj['month']+"-"+obj['day']);
			     }else if($("#search_unit").val()=="h"){
					data_x.push(obj['year']+"-"+obj['month']+"-"+obj['day']+":"+obj['hour']);
			     }else{
					data_x.push(obj['year']+"-"+obj['month']+"-"+obj['day']);
			     }
			    }
			}
			var data_y = new Array();
			for(var j=0;j<data[i].length;j++){
			    if(data[i][j]['y']!=""){
				data_y.push(data[i][j]['y'].substring(0,data[i][j]['y'].indexOf(".") + 3));
			    }
			}		
			if(data_min[i]!=''){
				var data_min = parseFloat(data_min[i]);
			}else{
				var data_min = Math.min.apply(Math, data_y);
			}
			if(data_max[i]!=''){
				var data_max = parseFloat(data_max[i]);
			}else{
				var data_max = Math.max.apply(Math, data_y);
			}
			option_series.push({ //系列名称，如果启用legend，该值将被legend.data索引相关  
				"name" : data_varRemark[i],
				//图表类型，必要参数！如为空或不支持类型，则该系列数据不被显示。  
				"type" : "line",
				"itemStyle" : { normal: {label : {show: true}}},
				//系列中的数据内容数组，折线图以及柱状图时数组长度等于所使用类目轴文本标签数组axis.data的长度，并且他们间是一一对应的。数组项通常为数值  
				"data" : data_y
			});
		}
		var title_text = data_varName[0];
		//定义图表option  
		var option = {
    		title : {
    			//主标题文本，'\n'指定换行  
    			text : title_text,
    			subtext: '',
    			//水平安放位置，默认为左侧，可选为：'center' | 'left' | 'right' | {number}（x坐标，单位px）  
    			x : 'center',
    			//垂直安放位置，默认为全图顶端，可选为：'top' | 'bottom' | 'center' | {number}（y坐标，单位px）  
    			y : 'top'
    		},
			//提示框，鼠标悬浮交互时的信息提示  
			tooltip : {
				show : true,
				//触发类型，默认（'item'）数据触发，可选为：'item' | 'axis'  
				trigger : 'axis'
			},
			//图例，每个图表最多仅有一个图例  
			legend : {
				//legend的data: 用于设置图例，data内的字符串数组需要与sereis数组内每一个series的name值对应  
				data : option_legend,
				x : 'center',
				top:'22px'
			},
		    grid: {
		        top: '50px',
    	        height: (is_height-180)+'px'
		    },
		    dataZoom: [
		               {
		                   show: true,
		                   realtime: true,
		               },
		               {
		                   type: 'slider',
		                   yAxisIndex: 0,
		                   filterMode: 'empty'
		               }
		    ],
			//直角坐标系中横轴数组，数组中每一项代表一条横轴坐标轴，仅有一条时可省略数值  
			//横轴通常为类目型，但条形图时则横轴为数值型，散点图时则横纵均为数值型  
			xAxis : {
			    	type : 'category',
				    boundaryGap : false,
			        axisLine: {onZero: true},
					name : '日期',
					splitLine : {show : true},
					//类目型坐标轴文本标签数组，指定label内容。 数组项通常为文本，'\n'指定换行 
					data : data_x
			},
			//直角坐标系中纵轴数组，数组中每一项代表一条纵轴坐标轴，仅有一条时可省略数值  
			//纵轴通常为数值型，但条形图时则纵轴为类目型  
			yAxis : {
			    type : 'value',
			    min :data_min[0],
			    max :data_max[0],
				name : data[0][0]['varType']
			},
			//sereis的数据: 用于设置图表数据之用。series是一个对象嵌套的结构；对象内包含对象  
			series : option_series
		};
		// 为echarts对象加载数据 
		myChart.setOption(option);
	},
	error : function() {
         	$.ligerDialog.warn("查询失败！","错误");
    	    manager.close();
	}
})
}

function serializeObjectNew(form){
	var o={};
	$.each(form.serializeArray(),function(index){
		o[this['name']]=o[this['name']]+","+this['value'];
	});
	return o;
}
//选择时间范围
function selectDate(){
    var import_date;
	if(!import_date){
		import_date=$.ligerDialog.open({
		target:$("#selectDate"),
    	height: 300,
		width:500,
	    title: "选择时间范围",
	    isHidden: true,
	    isResize:true ,
	    allowClose:false,
	    buttons: [
	              { text: '确定', onclick: function (item, dialog) { 
	                  if($("#search_unit").val()==""){
	                      $.ligerDialog.error("请选择时间单位！", "提示");
		                  return ;
	                  }
	                  if($("#search_begin_date").val()==""){
	                      $.ligerDialog.error("请选择起始时间！", "提示");
		                  return ;
	                  }
	                  if($("#search_end_date").val()==""){
	                      $.ligerDialog.error("请选择结束时间！", "提示");
		                  return ;
	                  }
	                  var end = new Date($("#search_end_date").val());
	                  var begin = new Date($("#search_begin_date").val());
	                  var temp =(end-begin)/1000;
	                  if(temp<0){
	                      $.ligerDialog.error("结束日期必须大于起始日期！", "提示");
		                  return ;
	                  }
		              if($("#search_unit").val()=="h" && temp>86500){
		                      $.ligerDialog.error("选择时日期范围不能超过1天！", "提示");
			                  return ;
		              }
		              if($("#search_unit").val()=="d" && temp>604800){
		                      $.ligerDialog.error("选择天日期范围不能超过一星期！", "提示");
			                  return ;
		              }
		              if($("#search_unit").val()=="m" && temp>31622400){
		                      $.ligerDialog.error("选择月日期范围不能超过一年！", "提示");
			                  return ;
		              }
		              if($("#search_unit").val()=="y" && temp>94867200){
		                      $.ligerDialog.error("选择年日期范围不能超过三年！", "提示");
			                  return ;
		              }
	                  dialog.hide();}},
	              { text: '取消', onclick: function (item, dialog) { dialog.hide(); } }
	          ]
		});
		import_date.set({ left: 20,top:0});
	}
}

//查看实时数据
function searchRealTime(){
    if($("#search_realTime").val()!=""){
        $("#search_timeRange").attr("disabled","disabled");
    }else{
        $("#search_timeRange").removeAttr("disabled");
    }
}

//时间戳转日期
function formatDate(timestamp) { 
    var date = new Date(timestamp); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
    Y = date.getFullYear() + '-';
    M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    D = change(date.getDate()) + ' ';
    h = change(date.getHours()) + ':';
    m = change(date.getMinutes()) + ':';
    s = change(date.getSeconds());
    return Y + M + D + h + m + s;
} 

function change(t) {
    if (t < 10) {
        return "0" + t;
    } else {
        return t;
    }
}
function search(){
    searchDialog = $.ligerDialog.open({
		target:$("#grid_search"),
		height:is_height-180,
		width:is_width-100,
		top:20,
	    title: "查看选择",
	    allowClose:false,
	    isResize:true,
	    buttons: [
	              { text: '查看', onclick: function (item, dialog) { 
						doSearch();
					}
	              },
	              { text: '取消', onclick: function (item, dialog) { dialog.hide(); } }
	          ]
		});
}
function doSearch(){
    $("#search_end_date").val($("#search_end_date").val()+":00");
    $("#search_begin_date").val($("#search_begin_date").val()+":00");
	var parms = serializeObject($("#searchForm"));
	parms.varList=JSON.stringify(VARLIST);
	
	console.log(VARLIST);
	console.log(parms.varList);
	if($("#search_realTime").val()==""){
				if($("#search_unit").val()==""){
                    $.ligerDialog.error("请选择时间单位！", "提示");
	                  return ;
                }
                if($("#search_begin_date").val()==""){
                    $.ligerDialog.error("请选择起始时间！", "提示");
	                  return ;
                }
                if($("#search_end_date").val()==""){
                    $.ligerDialog.error("请选择结束时间！", "提示");
	                  return ;
                }
                var end = new Date($("#search_end_date").val().replace(new RegExp("-","gm"),"/"));
                var begin = new Date($("#search_begin_date").val().replace(new RegExp("-","gm"),"/"));
                var temp =(end-begin)/1000;
                if(temp<0){
                    $.ligerDialog.error("结束日期必须大于起始日期！", "提示");
	                  return ;
                }
	              if($("#search_unit").val()=="h" && temp>86400){
	                      $.ligerDialog.error("选择时日期范围不能超过1天！", "提示");
		                  return ;
	              }
	              if($("#search_unit").val()=="d" && temp>604800){
	                      $.ligerDialog.error("选择天日期范围不能超过一星期！", "提示");
		                  return ;
	              }
	              if($("#search_unit").val()=="m" && temp>31622400){
	                      $.ligerDialog.error("选择月日期范围不能超过一年！", "提示");
		                  return ;
	              }
	              if($("#search_unit").val()=="y" && temp>94867200){
	                      $.ligerDialog.error("选择年日期范围不能超过三年！", "提示");
		                  return ;
	              }
	    SHOW_STATUS=0;
		parms.timeBegin=$("#search_begin_date").val();
		parms.timeEnd=$("#search_end_date").val();
		parms.unit=$("#search_unit").val();
	    $("#main").show();
	    $("#indexMain").hide();
		if(myChart!=null){
			myChart.dispose();
		}
		doChartSingle(parms);
	}else{
	    SHOW_STATUS=1;
	    $("#main").show();
	    $("#indexMain").hide();
		if(myChart!=null){
			myChart.dispose();
		}
		doChartRealSingle(parms);
		socketDataSingle(parms);
	}
	searchDialog.hide();
}
$("#search_begin_date").ligerDateEditor({ showTime: true, labelWidth: 100, labelAlign: 'left' });
$("#search_end_date").ligerDateEditor({ showTime: true, labelWidth: 100, labelAlign: 'left' });
</script>
</body>
</html>
