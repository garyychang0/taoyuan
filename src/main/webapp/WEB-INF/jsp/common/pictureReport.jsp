<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/My97DatePicker/WdatePicker.js"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerDrag.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script>
    <style type="text/css">
        .l-case-title
        {
            font-weight: bold;
            margin-top: 20px;
            margin-bottom: 20px;
        }
		.l-dialog-win .l-dialog-content{
			background: #fcfcfc;
		}
		
    </style>
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">
<div>
	<!-- 标题部分 -->
	<h3 class="hmdl_title_h3">报表管理
	    <span class="hmdl_title_span">
	        &nbsp;/&nbsp;报表一览
	    </span>
	</h3>
	<hr class="hmdl_title_hr"/>
	
	<!-- 查询条件部分 -->
	<div id="grid_search" style="margin-left:8px;margin-top:20px;">
	    <form name="searchForm" id="searchForm">
	        <!-- 隐藏域传值 -->
	        <input type="hidden" id="search_substationSn" name="substationSn"/>
	        &emsp;报表名称：<input id="search_reportName" name="reportName"  type="text" placeholder="搜索报表名称" style="text-indent:5px;width:150px;height:25px;border-radius: .25em;"/>
	        &emsp;报表类型：
	        <select id="search_reportType" name="reportType" style="text-indent:5px;width:150px;height:28px;border-radius: .25em;">
	        	<option selected="selected" value="">==选择报表类型==</option>
	        	<option value="1">日</option>
	        	<option value="2">月</option>
	        	<option value="3">年</option>
			    <option value="4">自定义天报表</option>
	        </select>
	        &emsp;<button id="btnSearch" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
	    </form>
	</div>
	<br/>
	<!-- 列表部分 -->
	<div id="grid"></div>
</div>
<!-- 选择时间 -->
<div id='selectDate' style="display:none">
	<form id="am-form" enctype="multipart/form-data" method="post" action="report"  target="upframe">
		<table class="default">
			<tr>
				<td align="right">开始日期</td>
				<td align="left" id="search_begin_date_tr"><input type="text" autocomplete="off" class="Wdate" style="width:150px" name="search_begin_date" id="search_begin_date"  onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
				</td>
			</tr>
			<tr style="display:none" id="tr_search_end_date_tr">
				<td align="right">结束日期</td>
				<td align="left" id="search_end_date_tr"><input type="text" autocomplete="off" class="Wdate" style="width:150px" name="search_end_date" id="search_end_date"  onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
				</td>
			</tr>
		</table>
	</form>
</div>
<!-- 导出用Form -->
<form action="../report/show" method="post" target="_blank" id="reportForm">
	<input type="hidden" name="fparam" id="fparam"></input>
	<input type="hidden" name="fcolumn" id="fcolumn"></input>
	<input type="hidden" name="fname" id="fname"></input>
	<input type="hidden" name="fclassname" id="fclassname"></input>
	<input type="hidden" name="fmenu" id="fmenu"></input>
	<input type="hidden" name="ftitle" id="ftitle"></input>
	<input type="hidden" name="fexport" id="fexport"></input>
	<input type="hidden" name="furl" id="furl"></input>
</form>
<script type="text/javascript">
var grid;
var editGrid;
var selectSubstationWin;//选择变电所
var editWin;// 编辑属性窗口
var globalForm;
var globalDialog;
var dialog = frameElement.dialog;
var dialogData = dialog.get('data');
if(dialogData.substationSn==""||dialogData.substationSn==null){
    dialogData.substationSn="";
}
$("#search_substationSn").val(dialogData.substationSn);
$(function(){
    //初始化表格
    initGrid();
    // 绑定查询按钮
	$("#btnSearch").click(function () {
	    var parms = serializeObject($("#searchForm"));
   	    for(var p in parms){
   	        grid.setParm(p,parms[p].trim());
   	    }
   	    grid.loadData();
   	});
});
//初始化表格
function initGrid(){
	grid=$("#grid").ligerGrid({
	    title:'&nbsp;&nbsp;<span style="color:#386792">报表信息一览</span>',
	    headerImg:'/hmdl/static/images/list.png',
	    columns: [
				{ display: '变电所', name: 'substationName',width:100},
				{ display: '报表名称', name: 'reportName',width:130},
				{ display: '报表标题', name: 'reportTitle',width:130},
				{ display: '报表类型', name: 'reportType',width:130,render:function(row){
				    if(row.reportType=="1"){
				        return "日报表";
				    }else if(row.reportType=="2"){
				        return "月报表";
				    }else if(row.reportType=="3"){
				        return "年报表";
				    }else if(row.reportType=="4"){
				        return "自定义天报表";
				    }
				}},
				{ display: '创建人', name: 'createrName',width:120},
				{ display: '创建时间', name: 'createDate',width:150},
				{ display: '更新人', name: 'updaterName',width:120},
				{ display: '更新时间', name: 'updateDate',width:150}
	    ],
	    toolbar: {
	        items: [
	        	{ text: '导出',click: function(){report();}, icon: 'fa fa-file-excel-o',color:'primary'},
	        	{ text: '刷新',click: function(){grid.reload();}, icon: 'fa fa-refresh',color:'success'},
	        ]
	    },
	    dataAction: 'server',
	    url:'../pmsReport/search',
	    root:'data',
        parms:[{name:"substationSn",value: dialogData.substationSn}],
	    record:'count',
	    height: '97%',
	    pageSize: 10,
	    checkbox: true,
	    rownumbers: true
	});
}

//配置提交
function report(){
	var rows = grid.getSelectedRows();
	if (rows == null || rows.length!=1) {
	    $.ligerDialog.warn("请选择一条记录！","提示");
	    return;
	};
	if(rows[0].reportType==1){
		$("#search_begin_date_tr").html('<input type="text" autocomplete="off" class="Wdate"'+
		        ' style="width:150px" name="search_begin_date" id="search_begin_date"  onclick=WdatePicker({dateFmt:"yyyy-MM-dd"})>');
		$("#tr_search_end_date_tr").hide();
	}else if(rows[0].reportType==2){
		$("#search_begin_date_tr").html('<input type="text" autocomplete="off" class="Wdate"'+
	        ' style="width:150px" name="search_begin_date" id="search_begin_date"  onclick=WdatePicker({dateFmt:"yyyy-MM"})>');
		$("#tr_search_end_date_tr").hide();
	}else if(rows[0].reportType==3){
		$("#search_begin_date_tr").html('<input type="text" autocomplete="off" class="Wdate"'+
	        ' style="width:150px" name="search_begin_date" id="search_begin_date"  onclick=WdatePicker({dateFmt:"yyyy"})>');
		$("#tr_search_end_date_tr").hide();
	}else if(rows[0].reportType==4){
		$("#search_begin_date_tr").html('<input type="text" autocomplete="off" class="Wdate"'+
	        ' style="width:150px" name="search_begin_date" id="search_begin_date"  onclick=WdatePicker({dateFmt:"yyyy-MM-dd"})>');
		$("#tr_search_end_date_tr").show();
		$("#search_end_date_tr").html('<input type="text" autocomplete="off" class="Wdate"'+
	        ' style="width:150px" name="search_end_date" id="search_end_date"  onclick=WdatePicker({dateFmt:"yyyy-MM-dd"})>');
	}
	var import_date;
	if(!import_date){
		import_date=$.ligerDialog.open({
		target:$("#selectDate"),
	   	height: 150,
		width:300,
	    title: "选择时间范围",
	    isHidden: true,
	    isResize:true ,
	    allowClose:false,
	    buttons: [
			{ text: '确定', onclick: function (item, dialog) { 
				if($("#search_begin_date").val()==""){
				    $.ligerDialog.error("请选择时间！", "提示");
				 return ;
				}
				dialog.hide();
				// 用于获取id
				var fid = {};
				fid.fid = rows[0].id;
				if(rows[0].reportType=="4"){
					fid.ftime = $('#search_begin_date').val()+","+$('#search_end_date').val();
				}else{
					fid.ftime = $('#search_begin_date').val();
				}
				$('#fparam').val(JSON.stringify(fid));
				$('#fcolumn').val(JSON.stringify(grid.getColumns()));
				// 报表的唯一标示
				$('#fname').val("pmsReport");
				// 报表容器的className
				$('#fclassname').val("");
				// 报表的默认标题
				$('#ftitle').val(rows[0].reportName);
				$('#fmenu').val("pmsReport");
				// pdf的加载地址【必输项】
				$('#furl').val("reportpdf");//新版本reportnew.js无默认值，必须填写
				// 报表导出的页面地址【必输项】
				$('#fexport').val("report");//新版本reportnew.js无默认值，必须填写
				// 导出
				$('#reportForm').submit();
	            }},
	              { text: '取消', onclick: function (item, dialog) { dialog.hide(); } }
	          ]
		});
	}
}

</script>
</body>  
</html>

