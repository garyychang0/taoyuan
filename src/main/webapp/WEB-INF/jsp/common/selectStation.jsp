<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script> 
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">
<div id="grid_search" style="margin-left:8px;margin-top:10px;">
	<form name="searchForm" id="searchForm">
		<ul>
	        <li class="label">
	        	&emsp;变电所编号：<input type="text" id="search_substation_sn" name="substationSn" value=""/>
	        	&emsp;变电所名称：<input type="text" id="search_substation_name" name="substationName" value=""/>
	       		&emsp;&nbsp;地&emsp;&emsp;&nbsp;&nbsp;&nbsp;址：<input type="text" id="search_substation_address" name="substationAddress" value=""/>
		 		<br/>&emsp;负&nbsp;&nbsp;&nbsp;责&nbsp;&nbsp;&nbsp;人：&nbsp;<input type="text" id="search_contact_user1" name="contactUser1" value=""/>
	   			&emsp;负责人电话：<input type="text" id="search_contact_tel1" name="contactTel1" value=""/>
	   			&emsp;<input type="button" value="查询" id="btnSearch" class="l-button l-button-submit" />
			</li>
		</ul>
	</form>
</div>

<br/>
<div id="grid"></div>
<script type="text/javascript">

var grid;

//接收传值
var request = (function (){
    var obj = {};
    var arr = window.location.search.slice(1).split("&");
    for (var i = 0, len = arr.length; i < len; i++) {
        var nv = arr[i].split("=");
        obj[unescape(nv[0]).toLowerCase()] = unescape(nv[1]);
    }
    return obj;
})()


var status="";
if(request.status==0){
	//变电所状态：未绑定
	status=0;
}


$(function(){
   	initGrid();
	
});


//绑定查询按钮
$("#btnSearch").click(function () {
	var parms = serializeObject($("#searchForm"));
    for(var p in parms){
        grid.setParm(p,parms[p].trim());
     	$("#am-form input[name='"+p+"']").val(parms[p].trim());
    }
    grid.loadData();
});


function initGrid(){
	grid=$("#grid").ligerGrid({
		title:'&nbsp;&nbsp;<span style="color:#386792">变电所一览</span>',
		headerImg:'/hmdl/static/images/list.png',
		columns: [
		{ display: '数据id', name: 'id',hide:true},
		{ display: '变电所名称', name: 'substationName',id:'treeNode',width:250,align:'left'},
		{ display: '变电所编号', name: 'substationSn',width:100},
		{ display: '类型', name: 'resourceType',width:100,
			render: function (item){
				if (item.resourceType == '0') return '区域';
				if (item.resourceType == '1') return '变电所';
				if (item.resourceType == '2') return '子变电所';
			}
		},
		{ display: '地址', name: 'substationAddress',width:150},
		{ display: '负责人', name: 'contactUser1',width:60},
		{ display: '负责人电话', name: 'contactTel1',width:100},
		{ display: '变电站建立时间', name: 'buildTime',width:100,hide:true},
		{ display: '维护时间起', name: 'maintainStart',width:100,hide:true},
		{ display: '维护时间止', name: 'maintainEnd',width:100,hide:true},
		{ display: '创建时间', name: 'createDate',dateFormat: "yyyy-MM-dd",width:100},
    	], 
    	toolbar: {
        	items: [
        	{ text: '刷新',click: function(){grid.reload();}, icon: 'fa fa-refresh',color:'success'},
			{ text: '收缩',click: function(){grid.collapseAll();}, icon: 'fa fa-minus-square-o',color:'warning' },
			{ text: '展开',click: function(){grid.expandAll();}, icon: 'fa fa-plus-square-o',color:'danger' },
			]
    	},
    	dataAction: 'server',
    	url:'../stationData/substationSearch',
    	parms:[{name:"status",value:status}],
    	root:'data',
    	record:'count',
    	height: '104%',
    	pageSize: 15,
    	enabledSort:true,
        sortnameParmName:'sortname',
        sortorderParmName:'sortorder',
    	pageSizeOptions:[15,20,25,30,35],
    	/* parms:function(){return serializeObject($("#searchForm"));}, */
    	tree: { columnId: 'treeNode',idField: 'substationSn',parentIDField: 'parentSn'},
    	checkbox: false,
    	autoCheckChildren:false,
    	rownumbers: true,
    	usePager:false,
    	onAfterShowData: function ()
        { 
            grid.collapseAll();
        }
	});
}
</script>
</body>
</html>
