<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script> 
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">
<div id="grid_search" style="margin-left:8px;margin-top:10px;">
	<form name="searchForm" id="searchForm">
		<ul>
	        <li class="label">
	        	&emsp;变电所编号：<input type="text" id="search_substation_sn" name="substationSn" value=""/>
	        	&emsp;变电所名称：<input type="text" id="search_substation_name" name="substationName" value=""/>
	       		&emsp;简&nbsp;&nbsp;&nbsp;称：<input type="text" id="search_shortname" name="shortname" value=""/>
		 		&emsp;<input type="button" value="查询" id="btnSearch" class="l-button l-button-submit" />
			</li>
		</ul>
	</form>
</div>

<br/>
<div id="grid"></div>
<script type="text/javascript">

var grid;

//接收传值
var request = (function (){
    var obj = {};
    var arr = window.location.search.slice(1).split("&");
    for (var i = 0, len = arr.length; i < len; i++) {
        var nv = arr[i].split("=");
        obj[unescape(nv[0]).toLowerCase()] = unescape(nv[1]);
    }
    return obj;
})()


var status="";
if(request.status==0){
	//变电所状态：未绑定
	status=0;
}


$(function(){
   	initGrid();
	
});


//绑定查询按钮
$("#btnSearch").click(function () {
	var parms = serializeObject($("#searchForm"));
    for(var p in parms){
        grid.setParm(p,parms[p].trim());
     	$("#am-form input[name='"+p+"']").val(parms[p].trim());
    }
    grid.loadData();
});


function initGrid(){
	grid=$("#grid").ligerGrid({
		title:'&nbsp;&nbsp;<span style="color:#386792">变电所一览</span>',
		headerImg:'/hmdl/static/images/list.png',
		columns: [
		{ display: '变电所名称', name: 'substationName',id:'treeNode',width:250,align:'left'},
		{ display: '变电所编号', name: 'substationSn',width:100},
		{ display: '简称', name: 'shortname',width:100},
		{ display: 'ip地址', name: 'ipAddr',width:150},
		{ display: '端口号', name: 'port',width:60},
		{ display: '创建时间', name: 'createDate',dateFormat: "yyyy-MM-dd",width:100},
    	], 
    	toolbar: {
        	items: [
        	{ text: '刷新',click: function(){grid.reload();}, icon: 'fa fa-refresh',color:'success'},
			]
    	},       
    	dataAction: 'server',
    	url:'../stationData/search',
    	root:'data',
    	record:'count',
    	height: '104%',
    	pageSize: 15,
    	enabledSort:true,
        sortnameParmName:'sortname',
        sortorderParmName:'sortorder',
    	pageSizeOptions:[15,20,25,30,35],
    	tree: { columnId: 'treeNode',idField: 'substationSn',parentIDField: 'parentSn'},
    	checkbox: false,
    	autoCheckChildren:false,
    	rownumbers: true,
    	usePager:false,
    	onAfterShowData: function ()
        { 
            grid.collapseAll();
        }
	});
}
</script>
</body>
</html>
