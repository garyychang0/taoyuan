<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script> 
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">

<!-- 查询条件部分 -->
<div id="grid_search" style="margin-left:8px;margin-top:20px;">
    <form name="searchForm" id="searchForm">
        <ul>
            <li class="label">
                &emsp;站点编号：<input type="text" id="search_substation_sn" name="substationSn" placeholder="搜索站点编号" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                <input type="hidden" id="search_shortName" name="search_shortName"/>
	        	&emsp;站点名称：<input type="text" id="search_substation_name" name="substationName" placeholder="搜索站点名称" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                &emsp;变量名称：<input type="text" id="search_user_sn" name="varName" placeholder="变量名称" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                &emsp;<button id="btnSearch" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
            </li>
        </ul>
    </form>
</div>
<br/>

<!-- 列表部分 -->
<div id="grid"></div>

<script type="text/javascript">
var grid;
var dialog = frameElement.dialog;
var dialogData = dialog.get('data');
if(dialogData.funcCode==""||dialogData.funcCode==null){
    dialogData.funcCode="";
}
if(dialogData.substationSn==""||dialogData.substationSn==null){
    dialogData.substationSn="";
}else{
    $("#search_substationSn").val(dialogData.substationSn);
}
if(dialogData.shortName==""||dialogData.shortName==null){
    dialogData.shortName="";
}else{
    $("#search_shortName").val(dialogData.shortName);
}
$(function(){
    initGrid();

    // 绑定查询按钮
	$("#btnSearch").click(function () {
		var parms = serializeObject($("#searchForm"));
   	    for(var p in parms){
   	        grid.setParm(p,parms[p].trim());
   	    }
   	    grid.loadData();
   	});
    
    function initGrid(){
        grid=$("#grid").ligerGrid({
            title:'&nbsp;&nbsp;<span style="color:#386792">变量一览</span>',
            headerImg:'/hmdl/static/images/list.png',
            columns: [
					{ display: '站点编号', name: 'substationSn',width:120},
					{ display: '站点名称', name: 'substationName',width:120},
					{ display: '变量名称', name: 'varName',width:120},
					{ display: '描述', name: 'remark',width:120},
	                { display: '数据类型', name: 'dataType',width:120},
	                { display: '功能码', name: 'funcCode',width:120},
	                { display: '长度', name: 'lenth',width:120},
	                { display: '地址域', name: 'addrArea',width:120},
	                { display: '开始地址', name: 'beginAddr',width:120},
	                { display: '结束地址', name: 'endAddr',width:120},
	                { display: '设备号', name: 'deviceNum',width:120},
	                { display: 'ip地址', name: 'ipAddr',width:120},
	                { display: '端口号', name: 'port',width:120},
					{ display: '创建时间', name: 'createDate',width:150},
				],
            toolbar: {
                items: [
					{ line: true },{ line: true },
					{ text: '刷新',click: function(){grid.reload();}, icon: 'fa fa-refresh',color:'success'},
				  ]
            },
            dataAction: 'server',
        	url:'../stationDataDetail/varSearch',
        	root:'data',
        	record:'count',
        	height: '97%',
        	pageSize: 10,
        	parms:[{name:"funcCode",value:dialogData.funcCode},{name:"substationSn",value:dialogData.substationSn},{name:"shortName",value:dialogData.shortName}],
        	enabledSort:true,
            sortnameParmName:'sortname',
            sortorderParmName:'sortorder',
        	checkbox: false,
        	rownumbers: true,
        });
    }
});

function resetAll(){
 // FORM清空
 $("#saveForm")[0].reset();
 // 隐藏项目清空
 $("#id").val("");
 $("#edit_type").val("");
}
</script>
</body>
</html>
