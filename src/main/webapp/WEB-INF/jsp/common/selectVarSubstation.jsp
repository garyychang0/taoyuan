<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script> 
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">

<!-- 查询条件部分 -->
<div id="grid_search" style="margin-left:8px;margin-top:20px;">
    <form name="searchForm" id="searchForm">
        <ul>
            <li class="label">
                &emsp;站点编号：<input type="text" id="search_substation_sn" name="substationSn" placeholder="搜索站点编号" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                &emsp;变量名称：<input type="text" id="search_user_sn" name="varName" placeholder="变量名称" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                &emsp;<button id="btnSearch" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
            </li>
        </ul>
    </form>
</div>
<br/>

<!-- 列表部分 -->
<div id="grid"></div>
<div style="overflow:auto;height:170px;">
<form  name="varForm" id="varForm">
	<table id= "table_edit" class="default">
	</table>
</form>
</div>
<script type="text/javascript">
var grid;
//接收传值
var dialog = frameElement.dialog;
var dialogData = dialog.get('data');
var ID =dialogData.ipId;
var SUBSTATIONSN =dialogData.substationSn;
$(function(){
    
    initGrid();

    // 绑定查询按钮
	$("#btnSearch").click(function () {
		var parms = serializeObject($("#searchForm"));
   	    for(var p in parms){
   	        grid.setParm(p,parms[p].trim());
   	    }
   	    grid.loadData();
   	});
    function initGrid(){
        grid=$("#grid").ligerGrid({
            title:'&nbsp;&nbsp;<span style="color:#386792">变量一览</span>',
            headerImg:'/hmdl/static/images/list.png',
            columns: [
					{ display: '站点编号', name: 'substationSn',width:120},
					{ display: '站点名称', name: 'substationName',width:120},
					{ display: '变量名称', name: 'varName',width:120},
					{ display: '描述', name: 'remark',width:120},
	                { display: '数据类型', name: 'dataType',width:120},
	                { display: '功能码', name: 'funcCode',width:120},
	                { display: '长度', name: 'lenth',width:120},
	                { display: '地址域', name: 'addrArea',width:120},
	                { display: '开始地址', name: 'beginAddr',width:120},
	                { display: '结束地址', name: 'endAddr',width:120},
	                { display: '设备号', name: 'deviceNum',width:120},
	                { display: 'ip地址', name: 'ipAddr',width:120},
	                { display: '端口号', name: 'port',width:120},
					{ display: '创建时间', name: 'createDate',width:150},
				],
		  toolbar: {
	                items: [
						{ line: true },{ line: true },
						{ text: '添加',click: function(){addCparm();}, icon: 'fa fa-plus',color:'success'}, 
						{ text: '刷新',click: function(){grid.reload();}, icon: 'fa fa-refresh',color:'primary'},
					  ]
			            },
            dataAction: 'server',
        	url:'../stationDataDetail/varSearch',
        	root:'data',
        	record:'count',
        	height: '280px',
        	pageSize: 5,
            parms:[{name:"substationSn",value:SUBSTATIONSN}],
        	enabledSort:true,
            sortnameParmName:'sortname',
            sortorderParmName:'sortorder',
        	checkbox: false,
        	rownumbers: true,
        });
    }
});

function resetAll(){
 // FORM清空
 $("#saveForm")[0].reset();
 // 隐藏项目清空
 $("#id").val("");
 $("#edit_type").val("");
}

table_edit = 0;
//动态添加计算参数
function addCparm(){
    var rows = grid.getSelectedRows();
    if (rows == null || rows.length!=1) {
        $.ligerDialog.warn("请选择一条记录！","提示");
        return;
    };
    table_edit = table_edit+1;
    var tr = $("<tr>"); 
	$("<td align='right'colspan='1'>").html("&emsp;变量:&emsp;<span style='color:red'>*</span>").appendTo(tr);
	$("<td align='left'>").html('<input name="varId" type="hidden" value = "'+rows[0].id+'"/><input type="text" style="width:100px" id="table_eidt_'+table_edit+'" name="varName" value = "'+rows[0].varName+'"/>').appendTo(tr);
	$("<td align='right'>").html('&emsp;最小值:&emsp;').appendTo(tr);
	$("<td align='left'>").html('<input class="varInput" value="" name="varMin" style="width:100px" placeholder="不填自动适应" type="text"/>').appendTo(tr);
	$("<td align='right'>").html('&emsp;最大值:&emsp;').appendTo(tr);
	$("<td align='left'>").html('<input class="varInput" value="" name="varMax" style="width:100px" placeholder="不填自动适应" type="text"/>').appendTo(tr);
	$("<td align='left'>").html( '&nbsp;'
	+'&nbsp;【<a href ="#" style="text-decoration:none;color:blue" onclick= "deleteRow('+table_edit+',this,'+'\'table_edit\''+')">删除</a>】'
	).appendTo(tr);
	$("#table_edit").append(tr);
	$(".varInput").bind("input propertychange",function(event){
	    
	});
}

//动态删除行
function deleteRow(id,obj,tablename){
	var index=obj.parentNode.parentNode.rowIndex;
	var table= document.getElementById(tablename);
	table.deleteRow(index);
}

//获取变量
function varform(){
	var parms = serializeObjectNew($("#varForm"));
	return parms;
}

function serializeObjectNew(form){
	var o={};
	$.each(form.serializeArray(),function(index){
		o[this['name']]=o[this['name']]+","+this['value'];
	});
	return o;
}
</script>
</body>
</html>
