<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>电力建图</title>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table1.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/draw/jquery.contextMenu.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/draw/palette-color-picker.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/color/spectrum.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/draw/jquery.contextMenu.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/draw/palette-color-picker.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/draw/jquery.mousewheel.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/fabricnew.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script> 
    <script src="/hmdl/static/js/color/spectrum.js" type="text/javascript"></script>
    <script src='/hmdl/static/js/color/docs.js' type='text/javascript'></script>
    <script src='/hmdl/static/js/color/toc.js' type='text/javascript'></script>
    <script src="/hmdl/static/js/bootstrap-fileinput.js" type="text/javascript"></script> 
    <script src="/hmdl/static/js/jquery.form.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerMenu.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerMenuBar.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/pako.min.js" type="text/javascript"></script>
    <style>
        .bind{
            z-index: 100;
        }
        .l-grid-body{
            width:765px
        }
        .overflow{
            overflow: hidden;
        }
    </style>
</head>

<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">
<div id="layout1">
    <!-- 左侧界面 -->
    <div position="left" title="组件图库列表" style="height: 100%; padding: 5px 0 0; overflow: auto; ">
        <!-- 检索部分 -->
        <input type="text" id="search_name" name="searchName" placeholder="请输入名称进行搜索" style="text-indent:5px;height:23px;border-radius: .25em;width:75%;margin-left: 5%"/>
        <button style="width:26px;height:26px;margin-top: -5px" id="btnSearch" type="button" class="btn btn-default  btn-small btn-mini"><i class="fa fa-search"></i></button>
        <!-- 属性调整部分 -->
        <div id="drawProperty" style="margin-top: 5px;margin-left: 5%;display:none">
            <hr/>&nbsp;粗细
            <input type="number" class="sp-replacer sp-light" style="width:30px;height:22px" id="strokeWidthSelect" onchange="strokeWidthChange()"/>
            </input>&nbsp;颜色
            <input id="colorPicker1"/>
        </div>
        <!-- 加载中显示部分 -->
        <div id="leftLoading" style="text-align: center;margin-top: 100px">
            <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
            <br/><br/><span style="color:#515151;font-size: 14px;font-weight: bolder;">数据加载中</span>
        </div>
        <!-- 未能加载到数据显示部分 -->
        <div id="leftNone" style="text-align: center;display: none">
            <img style="width: 30%;margin-top: 10px" src="/hmdl/static/images/hmdl/404.png"/>
            <div style ="color:#515151;font-size: 12px">
                <br/>非常抱歉<br/>未能检索到您的组件信息<br/>
            </div>
        </div>
        <!-- 加载到数据显示部分 -->
        <div id="leftList" style="text-align: center;display: none">
            <ul id="accordion" class="accordion" style="margin-top: 10px">
              <li>
                <div class="link">
                  <i class="fa fa-star-o"></i>基本组件
                  <i class="fa fa-chevron-down"></i></div>
                  <ul class="submenu" id="html0"></ul>
              </li>
              <li>
                <div class="link">
                  <i class="fa fa-star-half-o"></i>常用组件
                  <i class="fa fa-chevron-down"></i></div>
                  <ul class="submenu" id="html1"></ul>
              </li>
              <li>
                <div class="link">
                  <i class="fa fa-star"></i>常用大组件
                  <i class="fa fa-chevron-down"></i></div>
                  <ul class="submenu" id="html2"></ul>
              </li>
            </ul>
        </div>
    </div>
    <!-- 右侧界面 -->
    <div id="rightWin" position="center" title="电力建图">
        <!-- 全局隐藏变量 站点编号和站点名称 -->
        <input id="tempSubstationSn" type="hidden">
        <input id="tempSubstationName" type="hidden">
        <input id="tempShortname" type="hidden">
        <!-- 初始菜单界面 （后期有时间再美化）-->
        <div id="initWin" style="vertical-align: middle;text-align: center;">
            <!-- 初始化菜单标题 -->
            <div style="margin-top: 150px"><h1 style="color:#6589AA"><i class="fa fa-star"></i>华明电力<i class="fa fa-star"></i>&emsp;电力建图</h1></div>
            <!-- 初始化菜单按钮 -->
            <div style="text-align: center;margin-top: 50px">
              <a class="btn btn-lg btn-warning" href="javascript:initMenuCreate(0)">
              <i class="fa fa-microchip fa-2x pull-left"></i>　创 　建 　组 　件　<br/>(常用/常用大组件)</a>&emsp;&emsp;
              <a class="btn btn-lg btn-success" href="javascript:initMenuCreate(1)">
              <i class="fa fa-picture-o fa-2x pull-left"></i>　创 　建 　部 　件　<br/>(图形/表格/文本部件)</a>&emsp;&emsp;
              <a class="btn btn-lg btn-primary" href="javascript:initMenuCreate(2)">
              <i class="fa fa-flash fa-2x pull-left"></i>　电 　力　建 　图　<br/>(新建一个电力图库)</a>&emsp;&emsp;
              <a class="btn btn-lg btn-info" href="javascript:initMenuCreate(3)">
              <i class="fa fa-edit fa-2x pull-left"></i>　编 　辑　图 　库　<br/>(编辑已有的图库)</a>
            </div>
        </div>
        <!-- 画板 -->
        <div id="cWin" style="display: none;">
            <div style="margin-top: 10px;">
                <div id=beforeEditBtns>
                    <button style="margin-left: 10px" onclick="btnEdit()" id="btnEdit" type="button" class="btn btn-default btn-small btn-mini"><i class="fa fa-edit"></i>&nbsp;&nbsp;编辑&nbsp;&nbsp;</button>
                    <button style="margin-left: 10px" onclick="btnDel()" id="btnDel" type="button" class="btn btn-default btn-small btn-mini"><i class="fa fa-trash"></i>&nbsp;&nbsp;删除&nbsp;&nbsp;</button>
                    <button style="margin-left: 10px" onclick="btnExit()" id="btnCanel" type="button" class="btn btn-default btn-small btn-mini"><i class="fa fa-undo"></i>&nbsp;&nbsp;返回&nbsp;&nbsp;</button>
                </div>
                <div id="afterEditBtns" style="display:none">
                    <!-- 当前组件对应数据库ID -->
                    <input type="hidden" name="selectId" id="selectId"/>
                    <!-- 当前组件对应数据库名称 -->
                    <input type="hidden" name="selectName" id="selectName"/>
                    <!-- 当前组件对应数据库类型(非创建时) 1常用组件 2常用大组件 -->
                    <!-- 当前组件对应数据库类型(创建时) 0创建组件 1创建部件 2电力建图 -->
                    <input type="hidden" name="selectType" id="selectType"/>
                    <!-- 编辑按钮区域 -->
                    <button style="margin-left: 10px;" onclick="btnGroup()" id="btnGroup" type="button" class="btn btn-default btn-small btn-mini"><i class="fa fa-object-group"></i>&nbsp;&nbsp;组合&nbsp;&nbsp;</button>
                    <button style="margin-left: 10px;" onclick="btnUnGroup()" id="btnUnGroup" type="button" class="btn btn-default btn-small btn-mini"><i class="fa fa-object-ungroup"></i>&nbsp;&nbsp;拆分&nbsp;&nbsp;</button>
                    <button style="margin-left: 10px;" onclick="btnClean()" id="btnClean" type="button" class="btn btn-default btn-small btn-mini"><i class="fa fa-eraser"></i>&nbsp;&nbsp;清除&nbsp;&nbsp;</button>
                    <button style="margin-left: 10px;" onclick="undo()" id="undo" type="button" class="btn btn-default btn-small btn-mini" >&nbsp;<i class="fa fa-arrow-left"></i>&nbsp;&nbsp;撤销&nbsp;&nbsp;</button>
                    <button style="margin-left: 10px;" onclick="redo()" id="redo" type="button" class="btn btn-default btn-small btn-mini" disabled>&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;&nbsp;恢复&nbsp;&nbsp;</button>
                    
                    <!-- 表单按钮区域 -->
                    <button style="margin-left: 10px;margin-right:10px;float:right" onclick="btnExit()" id="btnExit" type="button" class="btn btn-danger  btn-small btn-mini"><i class="fa fa-undo"></i>&nbsp;&nbsp;退出&nbsp;&nbsp;</button>
                    <button style="margin-left: 10px;float:right" onclick="btnSave(0)" id="btnSave" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-save"></i>&nbsp;&nbsp;保存&nbsp;&nbsp;</button>
                    <!-- 测试功能按钮：开发用 后期删除-->
                    <button style="margin-left: 10px;float:right" onclick="toBetterClean()" id="btnTest" type="button" class="btn btn-default btn-small btn-mini"><i class="fa fa-bug"></i>&nbsp;&nbsp;测试&nbsp;&nbsp;</button>
                </div>
            </div>
            <!-- 包涵画板的DIV显示 -->
            <!-- 画板信息 -->
            <div id="canvas_border" style="margin-left:10px;margin-top:12px;overflow: auto;">
                <canvas id="c" width='1980' height='1020' style="border:1px dashed #000;"></canvas>
            </div>
            <!-- 动态文本显示区域 -->
             &emsp;&emsp;<span id="zb"></span>&emsp;&emsp;<span id="sf">画布缩放比例：100%</span>
             &emsp;&emsp;<span id="dbMsg"></span>&emsp;<span id="ebMsg"></span>
             &emsp;&emsp;<span id="saveMsg"></span>
        </div>
        <!-- 弹出右键菜单 -->
        <div id="contextmenu-output"></div>
    </div>
</div>

<!-- 普通模块编辑 -->
<div id="editWin" style="display:none;">
    <form name="editWinForm" id="editWinForm">
    <table class="default">
        <tr style="height:50px" >
            <td colspan="3" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
                <i class="fa fa-edit"></i>&emsp;编辑
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;组件类型：</td>
            <td colspan="2">
               <input id="edit_type" name="edit_type" class="readonlyInput" readonly="readonly"></input>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;组&nbsp;件&nbsp;I&nbsp;D：</td>
            <td colspan="2">
               <input id="edit_id" name="edit_id" class="readonlyInput" readonly="readonly"></input>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;组件位置：</td>
            <td colspan="2">
                &nbsp;X&nbsp;轴&nbsp;：&nbsp;<input style="width:60px" id="edit_left" name="edit_left"></input>
                &nbsp;Y&nbsp;轴&nbsp;：&nbsp;<input style="width:60px" id="edit_top" name="edit_top"></input>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;缩放比例：</td>
            <td colspan="2">
                &nbsp;横轴&nbsp;：&nbsp;<input style="width:60px" id="edit_scaleX" name="edit_scaleX"></input>
                &nbsp;纵轴&nbsp;：&nbsp;<input style="width:60px" id="edit_scaleY" name="edit_scaleY"></input>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;线条粗细：</td>
            <td colspan="2">
                <input id="edit_strokeWidth" name="edit_strokeWidth"></input>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;线条颜色：</td>
            <td colspan="2">
                <input id="colorPicker2" />
                <input type="text" name="edit_stroke" id="edit_stroke" value=""/>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;填充颜色：</td>
            <td colspan="2">
                <input id="colorPicker3" />
                <input type="text" name="edit_fill" id="edit_fill" value=""/>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: center">
                <button type="button" class="btn btn-primary btn-small btn-mini" onclick="editWinSave()"><i class="fa fa-check"></i>&nbsp;&nbsp;提交&nbsp;&nbsp;</button>
                &emsp;
                <button type="button" class="btn btn-danger btn-small btn-mini" onclick="editCancel()"><i class="fa fa-undo"></i>&nbsp;&nbsp;关闭&nbsp;&nbsp;</button>
            </td>
        </tr>
    </table>
    </form>
</div>

<!-- 文本模块编辑 -->
<div id="editTextWin" style="display:none;">
    <form name="editTextForm" id="editTextForm">
    <table class="default">
        <tr style="height:50px" >
            <td colspan="3" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
                <i class="fa fa-edit"></i>&emsp;文本编辑
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;组件类型：</td>
            <td colspan="2">
               <input id="editText_type" name="editText_type" class="readonlyInput" readonly="readonly"></input>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;组&nbsp;件&nbsp;I&nbsp;D：</td>
            <td colspan="2">
               <input id="editText_id" name="editText_id" class="readonlyInput" readonly="readonly"></input>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;组件位置：</td>
            <td colspan="2">
                &nbsp;X&nbsp;轴&nbsp;：&nbsp;<input style="width:60px" id="editText_left" name="editText_left"></input>
                &nbsp;Y&nbsp;轴&nbsp;：&nbsp;<input style="width:60px" id="editText_top" name="editText_top"></input>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;文本内容：</td>
            <td colspan="2">
                <input style="width:300px" id="editText_text" name="editText_text"></input>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;字体样式：</td>
            <td colspan="2">大小：
                <input type="number" style="width:30px" id="editText_fontSize" name="editText_fontSize"></input>加粗：
                <select id="editText_fontWeight" name="editText_fontWeight">
                        <option selected="selected" value="normal">标准</option>
                        <option value="bold">粗体</option>
                    </select>字体：
                <select id="editText_fontFamily" name="editText_fontFamily">
                        <option selected="selected" value="Times New Roman">Times New Roman</option>
                        <option value="微软雅黑">微软雅黑</option>
                        <option value="宋体">宋体</option>
                        <option value="黑体">黑体</option>
                        <option value="新宋体">新宋体</option>
                        <option value="楷体">楷体</option>
                    </select>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;文本样式：</td>
            <td colspan="2">
                <input type="checkbox" id="underline" name="underline"></input>下划线&emsp;
                <input type="checkbox" id="overline" name="overline"></input>上划线&emsp;
                <input type="checkbox" id="linethrough" name="linethrough"></input>贯穿线&emsp;&emsp; 文本位置：
               <select id="editText_textAlign" name="editText_textAlign">
                        <option value="center" selected="selected">居中</option>
                        <option value="left">靠左</option>
                        <option value="right">靠右</option>
                    </select>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;字体颜色：</td>
            <td colspan="2">
                <input id="colorPicker4" />
                <input type="text" name="editText_fill" id="editText_fill" value=""/>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: center">
                <button type="button" class="btn btn-primary btn-small btn-mini" onclick="editTextWinSave()"><i class="fa fa-check"></i>&nbsp;&nbsp;提交&nbsp;&nbsp;</button>
                &emsp;
                <button type="button" class="btn btn-danger btn-small btn-mini" onclick="editCancel()"><i class="fa fa-undo"></i>&nbsp;&nbsp;关闭&nbsp;&nbsp;</button>
            </td>
        </tr>
    </table>
    </form>
</div>

<!-- 均分编辑 -->
<div id="editDivideWin" style="display:none;">
    <form name="editDivideForm" id="editDivideForm">
    <table class="default">
        <tr style="height:50px" >
            <td colspan="3" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
                <i class="fa fa-edit"></i>&emsp;组件均分
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;组件类型：</td>
            <td colspan="2">
               <input id="editDivide_type" name="editDivide_type" class="readonlyInput" readonly="readonly"></input>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;组&nbsp;件&nbsp;I&nbsp;D：</td>
            <td colspan="2">
               <input id="editDivide_id" name="editDivide_id" class="readonlyInput" readonly="readonly"></input>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;均分方向：</td>
            <td colspan="2">
                <input type="radio" id="divide_x" name="divide"></input>水平均分&emsp;&emsp;
                <input type="radio" id="divide_y" name="divide"></input>垂直均分
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;个数/间距：</td>
            <td colspan="2">个数：
                <input type="number" min="1" style="width:40px" id="copy_num" name="copy_num"></input>
                &emsp;&emsp;间距：
              <input type="number" min="0" step="10" style="width:40px" id="spacing" name="spacing"></input>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: center">
                <button type="button" class="btn btn-primary btn-small btn-mini" onclick="editDivideWinSave()"><i class="fa fa-check"></i>&nbsp;&nbsp;提交&nbsp;&nbsp;</button>
                &emsp;
                <button type="button" class="btn btn-danger btn-small btn-mini" onclick="editCancel()"><i class="fa fa-undo"></i>&nbsp;&nbsp;关闭&nbsp;&nbsp;</button>
            </td>
        </tr>
    </table>
    </form>
</div>

<!-- 画布属性弹出窗口 -->
<div id="canvasEditWin" style="display: none">
    <table class="default">
        <tr style="height:50px" >
            <td colspan="2" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
                <i class="fa fa-edit"></i>&emsp;画布属性编辑
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;背景颜色：</td>
            <td>
                <input id="colorPicker5" />
                <input type="text" name="hb_bjys" style="width:120px"  id="hb_bjys" value=""/>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;自定义名称：</td>
            <td>
                <input type="text" name="zdyShowName" style="width:180px"  id="zdyShowName" value=""/>
            </td>
        </tr>
        <tr id="departSize1">
            <td>&emsp;部件大小：</td>
            <td>
               &nbsp;<input type="number" value="900" style="width:60px" id="departSize_width1" name="departSize_width1" />
               &nbsp;x&nbsp;<input type="number" value="600" style="width:60px" id="departSize_height1" name="departSize_height1" />
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: center">
                <button type="button" class="btn btn-primary btn-small btn-mini" onclick="canvasEdit()"><i class="fa fa-check"></i>&nbsp;&nbsp;提交&nbsp;&nbsp;</button>
                &emsp;
                <button type="button" class="btn btn-danger btn-small btn-mini" onclick="canvasCancel()"><i class="fa fa-undo"></i>&nbsp;&nbsp;关闭&nbsp;&nbsp;</button>
            </td>
        </tr>
    </table>
</div>

<!-- 确认保存 -->
<div id="partSaveWin" style="display:none">
    <form name="saveForm" id="saveForm">
        <input type="hidden" name="id" id="id"/>
        <input type="hidden" name="partMemory" id="partMemory"/>
        <table class="default">
            <tr style="height:50px" >
                <td colspan="2" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
                    <i class="fa fa-save"></i>&emsp;组件保存
                </td>
            </tr>
            <tr>
                <td align="right">组件名称</td>
                <td align="left"><input type="text" name="partName" id="partName" placeholder="请输入组件名称" style="text-indent:5px;width:250px"/>
                </td>
            </tr>
            <tr>
                <td align="right">组件类型</td>
                <td align="left">
                    <select name="partType" id="partType" style="text-indent:5px;width:250px">
                        <option value='1' >常用组件</option>
                        <option value='2' >常用大组件</option>
                    </select>
                </td>
            </tr>
        </table>
    </form>
    <table align="center" style="margin-top: 10px">
        <tr>
            <td>
                <button type="submit" class="btn btn-primary btn-small btn-mini" id="btnConfirm"><i class="fa fa-check"></i>&nbsp;&nbsp;提交&nbsp;&nbsp;</button>
                &emsp;
                <button type="button" class="btn btn-danger btn-small btn-mini" id="btnCancel"><i class="fa fa-close"></i>&nbsp;&nbsp;返回&nbsp;&nbsp;</button>
            </td>
        </tr>
    </table>
</div>

<!-- 初期化选择配置-->
<div id='canvasConfig' style="display:none">
    <form name="saveForm2" id="saveForm2">
        <input type="hidden" name="id" id="configId"/>
        <table class="default">
            <tr>
                <td align="right">选择变电站<span style="color:red">*</span></td>
                <td align="left">
                    <input style="width:200px;" id="configSubstationName" type="text" onclick="selectStation()" readonly="readonly" />
                </td>
            </tr>
            <tr>
                <td align="right">自定义名称<span style="color:red">*</span></td>
                <td align="left"><input type="text" id="configCanvasName" name="canvasName" style="width:200px;"/>
                </td>
            </tr>
            <tr id="departSize" style="display:none">
                <td align="right">&emsp;部件大小：</td>
                <td align="left">
                   &nbsp;<input type="number" value="900" style="width:60px" id="departSize_width" name="departSize_width" />
                   &nbsp;x&nbsp;<input type="number" value="600" style="width:60px" id="departSize_height" name="departSize_height" />
                </td>
            </tr>
            <tr>
                <td align="right">类型</td>
                <td align="left">
                    <select name="canvasType" id="configCanvasType" style="text-indent:5px;width:100px">
                    </select>
                    <input name="hidden_canvas_type" id="hidden_canvas_type" type="hidden"/>
                </td>
            </tr>
            <tr>
                <td align="right">背景颜色</td>
                <td align="left">
                <input id="colorPicker6" />
                <input type="text" name="canvasBackgroundColor" id="configBackground" value="" readonly="readonly"/>
                </td>
            </tr>
        </table>
    </form>
</div>
<!-- 数据绑定窗口 -->
<div id="dbDialog" style="display: none">
    <!-- 查询条件部分 -->
    <div style="margin-left:8px;">
        <form name="dbDialogForm" id="dbDialogForm">
            <ul>
                <li class="label">
                    <input type="hidden" id="search_data_substationSn" /><!-- 隐藏域传值，作为弹出事件绑定窗口时候的检索条件 -->
                      <!-- &emsp;当前变电所： --><input type="hidden" id="search_substationName" name="substationName" readonly="readonly" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                    &emsp;变量名称：<input type="text" id="search_dbDialog_name" name="varName" placeholder="搜索变量名称" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                    &emsp;描述内容：<input type="text" id="search_remark" name="remark" placeholder="搜索描述内容" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                       &emsp;功能码：
                    <select id="search_funcCode" name="funcCode" onchange="searchQuick()" style="text-indent:5px;height:23px;border-radius: .25em;">
                      <option value="">==请选择==</option>
                      <option value="01">==开入01==</option>
                      <option value="02">==开入02==</option>
                      <option value="03">==寄存器03==</option>
                      <option value="04">==寄存器04==</option>
                    </select>
                       &emsp;<button id="btn_dbDialogForm_search" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
                </li>
            </ul>
        </form>
    </div>
    <!-- 数据绑定窗口 -->
    <div id="dbDialogGrid"></div>
</div>

<!-- 事件绑定窗口 -->
<div id="ebDialog" style="display: none">
    <!-- 查询条件部分 -->
    <input type="hidden" id="search_event_id"/><!-- 隐藏域传值：绑定变量一览时，如果有数据绑定，则检索变量一览时作为检索条件 -->
    <div style="margin-left:8px;">
        <form name="ebDialogForm" id="ebDialogForm">
            <ul>
                <li class="label">
                    <input type="hidden" id="search_event_substationSn" /><!-- 隐藏域传值，作为弹出事件绑定窗口时候的检索条件 -->
                    &emsp;当前变电所：<input type="text" id="event_search_substationName" name="substationName" readonly="readonly" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                    &emsp;名称：<input type="text" id="search_ebDialog_name" name="search_ebDialog_name" placeholder="输入名称查询" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                    &emsp;事件类型：
                    <select id="search_event_type" onchange="selectEventType()" name="searchEventType" style="text-indent:5px;width:100px;height:23px;border-radius: .25em;">
                        <option id="popup_bj" selected="selected" value="1">弹出部件</option>
                        <option id="popup_fx" value="2">分析一览</option>
                    </select>
                    &emsp;<button id="btn_ebDialogForm_search" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
                </li>
            </ul>
        </form>
    </div>
    <!-- 弹出部件窗口 -->
    <div id="popUpDialogGrid"></div>
    <!-- 分析一览窗口,默认隐藏 -->
    <div id="analysisDialogGrid" style="display: none"></div>
</div>

<!-- 显示已经绑定的列表数据一览 -->
<div id="bindGridDialog" style="display: none">
    <div id="showBindDataGrid"></div>
    <div id="showBindEventGrid"></div>
</div>

<!-- 开关样式动态化编辑  -->
<div id="stateEditWin" style="display: none">
    <table class="default">
        <tr style="height:50px" >
            <td colspan="3" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
                <i class="fa fa-edit"></i>&emsp;开关编辑
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;线条颜色：</td>
            <td colspan="2">
                <input id="colorPicker7" />
                <input type="text" name="stateEditStroke" id="stateEditStroke" value="" readonly="readonly"/>&emsp;不变化不用填写
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;填充颜色：</td>
            <td colspan="2">
                <input id="colorPicker8" />
                <input type="text" name="stateEditFill" id="stateEditFill" value="" readonly="readonly"/>&emsp;不变化不用填写
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;填充文本：</td>
            <td colspan="2">
                <input type="text" name="stateEditText" id="stateEditText" value=""/>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;当前角度：</td>
            <td colspan="2">
                <input type="text" name="stateEditFromPoint" id="stateEditFromPoint" value="" style="width:60px" readonly="readonly"/>
                &emsp;旋转至&emsp;
                <input type="number" name="stateEditToPoint" id="stateEditToPoint" value="" style="width:60px"/>
                &emsp;<button type="button" onclick="stateEditPreview()" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-eye"></i>&nbsp;&nbsp;预览&nbsp;&nbsp;</button>
            </td>
        </tr>
    </table>
    <!-- 迷你预览小画板 -->
    <canvas id="p" style="border:1px dashed #000;"></canvas>
</div>
<input type="hidden" name="openSelectPicture" id="openSelectPicture"/>
<script type="text/javascript">
// 新建画板
var canvas = new fabric.Canvas('c');
// 新建预览板
var canvasp = new fabric.Canvas('p');
canvasp.setWidth(475);
canvasp.setHeight(220);
canvasp.defaultCursor = 'default';
canvasp.isDrawingMode =false;// 这个模式就是FALSE 是画笔轨迹的图形
canvasp.selectable = false;// 没有选中框不然丑
canvasp.skipTargetFind=true;// 画板元素不能被选中
canvasp.selection = false; //画板不显示选中

/*  POPUP WINDOWS(弹出窗口)   */
var editWin;// 编辑属性窗口
var editTextWin;// 文本编辑窗口
var editDivideWin;// 均分窗口
var editPicWin;//选择图库窗口
var dbDialog;//数据绑定窗口
var ebDialog;//事件绑定窗口
var bindGridDialog;//表格展示窗口
var stateEditWin;// 开关动态化配置窗口

/*  POPUP GRID(弹出列表)   */
var dbDialogGrid;//数据绑定表格
var popUpDialogGrid;//弹出部件表格
var analysisDialogGrid;//分析一览表格
var showBindDataGrid;//数据绑定显示表格
var showBindEventGrid;//事件绑定显示表格
var stateGrid;// 开关编辑grid

/*  PROPERTIES(画面缓存值)   */
var dataBindList = new Array();//数据绑定缓存
var isSelectDataBind = false;// 选中目标是否已绑定数据
var isSelectDataSwith = false;// 选中目标绑定的情况是否是开关变量
var eventBindList = new Array();//数据绑定缓存
var isSelectEventBind = false;// 选中目标是否已绑定事件

var canvasStationMemory;//画布保存值
var canvasEditWin;//画布属性窗口
var colorNum;//画布编号
var saveCanvasType;//保存类型 0 常用组件 1:部件 2:电力总图 3编辑图库
var zoomPoint;// 缩放点记录
// 画板属性设定
canvas.defaultCursor = 'default';// 鼠标样式
canvas.hoverCursor = 'pointer';// 鼠标样式
canvas.setBackgroundColor("#ffffff");//设置画布背景颜色 白色

var listData;//左侧树存放备份数据
var loadObj;// load的Json对象
var menuSelectObj;// 非画笔模式下选中的组件对象存储，只能用一次用完就清空
var isMenuSelectModel = false;// 非画笔模式下选中的组件对象存储，只能用一次用完就清空
var clearFlg = true;// 是否需要全清画面的标致
// 显示模式 0：预览模式，不能移动不能操作
// 显示模式 1：编辑模式，可以移动可以各种操作
var showModel = 0;
//回撤配置 暂时没有指定撤回最大步数
var _config = {
    canvasState             : [],
    currentStateIndex       : -1,
    undoStatus              : false,
    redoStatus              : false,
    undoFinishedStatus      : 1,
    redoFinishedStatus      : 1,
    undoButton              : document.getElementById('undo'),
    redoButton              : document.getElementById('redo'),
};
// 设定撤回记录最大步数 10步
var maxBackStep = 9;
//是否拖动(画布移动功能)
var panning = false;
// 基本组件画图相关内容
var mouseFrom = {}, mouseTo = {}, drawType = null, canvasObjectIndex = 0, textbox = null;
var moveCount = 1; //绘制移动计数器
var isDrawModel = false; // 是否画图模式
var doDrawing = false; // 绘制状态 false选择模式  true画图模式
var drawingObject = null;// 参考原来的逻辑保留
var drawData;// 画笔对应的选中obj
// 拖动画布偏移量
var movementX = 0;
var movementY = 0;
// 记录相对倍数
var mousewheel=0;
// 记录中心点XY
var mousewheelX=0;
var mousewheelY=0;

var objConfigId;//选择行ID

var createType;//创建图库类型（0为组件，1为部件，2为总图，3为编辑图库）
var selectDom;//事件绑定的判断

//鼠标按下监听事件
canvas.on('mouse:down', function (options) {
    //按住alt键非绘图才可拖动画布 
    //拖动画布 此功能暂时已经取消了，这边预留 便于后期拓展
    if(options.e.altKey && !isDrawModel) {
        panning = true;
        canvas.selection = false;
    }

    // 绘图
    if(isDrawModel){
        var xy = transformMouse(options.e.offsetX, options.e.offsetY);
        mouseFrom.x = xy.x;
        mouseFrom.y = xy.y;
        doDrawing = true;
    }
    // 编辑模式下下选中目标显示绑定提示信息
    var mouseDownObj = options.target;

    // 先清空
    clearBindMsg();
    if(mouseDownObj && showModel>0){
        // 显示是否绑定相关数据
        bulidBindMsg(mouseDownObj);
    }
});

//鼠标抬起
canvas.on('mouse:up', function (options) {

    panning = false;
    canvas.selection = true;
    // xy一直做计算
    var xy = transformMouse(options.e.offsetX, options.e.offsetY);
    mouseTo.x = xy.x;
    mouseTo.y = xy.y;

    // 绘图
    if(isDrawModel){
        drawingObject = null;
        moveCount = 1;
        doDrawing = false;
        canvas.selection = false;
        
        // 鼠标位移问题文本模块需要特殊化处理
        var dawObj = drawData.objects[0];
        var name = dawObj['id'];
        if(name=="文本"){
             // 文本BUG待解决 不适合在移动鼠标的时候新建文本只适合单独处理
             textbox = new fabric.Textbox("", {
                 left : mouseFrom.x,
                 top : mouseFrom.y,
                 width : 150,
                 fontSize : dawObj['fontSize'],
                 fontWeight:dawObj['fontWeight'],
                 fontFamily:dawObj['fontFamily'],
                 underline:dawObj['underline'],
                 overline:dawObj['overline'],
                 linethrough:dawObj['linethrough'],
                 textAlign:dawObj['textAlign'],
                 fill : dawObj['fill'],
                 text:dawObj['text']
             });
             textbox.id = getNewId();
             canvas.add(textbox);

             // 退出画笔模式呀
             changeOutDrawModel();
        }
    }

    // 如果是选中组件的状态下，下一次点击鼠标加载这个组件至合适的位置
    if(isMenuSelectModel){
         // 加载 最后一个参数0代表重新赋值ID,1或者没有代表不重新赋值
         loadObj = canvas.loadFromJSONFr(menuSelectObj,null,function(oa,oj) {
            // 编辑
            oj.selectable=true;
            // 需要重新定位位置
            oj.set("left",mouseTo.x);
            oj.set("top",mouseTo.y);
            // 重置所有ID（group采用递归方式ID全部重置）
            // 这边自己ID已在JS中重置过 type=0不重置即可
            rebuilderAllId(oj,0);
         },0);
         // 记录撤回点
         updateCanvasState();
         // 需要退出此模式调用下
         changeOutDrawModel();
    }
});

//鼠标移动
canvas.on('mouse:move', function (options) {
    if(showModel>0){
        // 显示画板坐标点
        $("#zb").html("当前坐标   X:"+options.e.offsetX+"&emsp;Y:"+options.e.offsetY);
        // 开放画布拖动功能，记录偏移量X和Y
        if (panning && options && options.e) {
            var delta = new fabric.Point(options.e.movementX, options.e.movementY);
            movementX +=  options.e.movementX*(1-mousewheel);
            movementY += options.e.movementY*(1-mousewheel);
            canvas.relativePan(delta);
        }
    }
    // 绘图
    if(isDrawModel){
        if (moveCount % 2 && !doDrawing) {
            //减少绘制频率
            return;
        }
        moveCount++;
        var xy = transformMouse(options.e.offsetX, options.e.offsetY);
        mouseTo.x = xy.x;
        mouseTo.y = xy.y;
        drawing();
    }
});

//Canvas以指定的点为中心进行缩放(此处定为中心原点缩放，自由点暂时没办法计算出画笔的偏移量)
//鼠标滚轮监听(画布放大功能) 用滚轮的方式会和画面冲突的
$(".upper-canvas").mousewheel(function(event) {
    if(showModel>0){
        // zoom
        var zoom = (event.deltaY > 0 ? 0.1 : -0.1) + canvas.getZoom();
        if(zoom>=0.3 && zoom<=3){
            // 计算比列，重要代码勿动
            mousewheel = (zoom-1)/zoom;
        }
        zoom = Math.max(0.3,zoom); //最小为原来的3/10
        zoom = Math.min(3,zoom); //最大是原来的3倍
        // 放大点的选取优化(只支持中心放大缩小，暂不支持自定义坐标)
        zoomPoint = new fabric.Point(canvas.width * 0.5 , canvas.height * 0.5);
        $("#sf").html("当前缩放比例："+toPercent(zoom));
        canvas.zoomToPoint(zoomPoint, zoom);
    }
});

//在canvas上层对象上添加右键事件监听
$(".upper-canvas").contextmenu(onContextmenu);
//初始化右键菜单
$.contextMenu({
      selector: '#contextmenu-output',
      trigger: 'none',
      build: function($trigger, e) {
          //构建菜单项build方法在每次右键点击会执行
          return {
              callback: contextMenuClick,
              items: contextMenuItems
          };
      },
});

$("#rightWin").resize(function(){
    document.getElementById("canvas_border").style.width = (document.getElementById("rightWin").offsetWidth-25)+'px';
    document.getElementById("canvas_border").style.height = (document.getElementById("rightWin").offsetHeight-65)+'px';
});

$(function() {
    selectDom = $("#popup_bj");//弹出部件的DOM
    // 布局设置
    $("#layout1").ligerLayout({
        leftWidth: 200,
    });
    
    // LINK样式动画设定
    var Accordion = function(el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;

        var links = this.el.find('.link');
        links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
    }

    Accordion.prototype.dropdown = function(e) {
        var $el = e.data.el;
            $this = $(this),
            $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
        };
    }
    
    // 编辑用的
    canvas.setWidth(document.getElementById("rightWin").offsetWidth-30);//设置显示用画布宽度
    canvas.setHeight(document.getElementById("rightWin").offsetHeight-70);//设置显示用画布高度
    // 获取中心点位置记录下来
    zoomPoint = new fabric.Point(canvas.width * 0.5 , canvas.height * 0.5);
    mousewheelX = canvas.width * 0.5;
    mousewheelY = canvas.height * 0.5;
    
    var accordion = new Accordion($('#accordion'), false);
    // 加载左侧列表
    getLeftList();
    //监听键盘事件
    document.onkeyup = function(event){
        if(showModel>0){
            var event = event || window.event;

            // ESC退出选中状态切换为选择状态快速键
            if(event.keyCode==27){
                changeOutDrawModel();
                return;
            }

            var obj = canvas.getActiveObject();
            // 若果没有选中的OBJ直接返回
            if (!obj) {
                return;
            }

            // delete删除
            if(event.keyCode==46){
                btnClean();
            // 位移
            }else{
                var yd = 1;
                // 这边SHIFT+偏移量 也可以使用其他的，
                // 不能使用ALT切记会退出画面浏览器快捷键冲突了!
                if(event.shiftKey){
                    yd  = 20;
                }
                yd = yd/canvas.getZoom();
                // 右移
                if(event.keyCode ==39){
                    obj.set('left', obj.left+yd);// X轴坐标+
                // 左移
                }else if(event.keyCode ==37){
                    obj.set('left', obj.left-yd);// X轴坐标-
                // 下移
                }else if(event.keyCode ==40){
                    obj.set('top', obj.top+yd);// y轴坐标+
                // 上移
                }else if(event.keyCode ==38){
                    obj.set('top', obj.top-yd);// Y轴坐标-
                }
                obj.setCoords();
                canvas.requestRenderAll();
            }
        }
    }

    //监听键盘持续按下事件
    document.onkeydown = function(event){
        if(showModel>0){
            // 复制
            if(event.ctrlKey && event.keyCode==86){
                btnPaste();
                return;
            }else if(event.ctrlKey && event.keyCode==67){
                btnCopy();
                return;
            }else if(event.ctrlKey && event.keyCode==90){
                undo();
                return;
            }else if(event.ctrlKey && event.keyCode==89){
                redo();
                return;
            }else if(event.ctrlKey && event.keyCode==83){
                btnSave(1);
                event.preventDefault();
                return;
            }

            var obj = canvas.getActiveObject();
            // event.keyCode 37 38 39 40
            if (!obj) {
                return;
            }
            var yd = 1;
            // 这边SHIFT+偏移量 也可以使用其他的，
            // 不能使用ALT切记会退出画面浏览器快捷键冲突了!
            if(event.shiftKey){
                yd  = 20;
            }
            yd = yd/canvas.getZoom();
            // 右移
            if(event.keyCode ==39){
                obj.set('left', obj.left+yd);// X轴坐标+
            // 左移
            }else if(event.keyCode ==37){
                obj.set('left', obj.left-yd);// X轴坐标-
            // 下移
            }else if(event.keyCode ==40){
                obj.set('top', obj.top+yd);// y轴坐标+
            // 上移
            }else if(event.keyCode ==38){
                obj.set('top', obj.top-yd);// Y轴坐标-
            }
            obj.setCoords();
            canvas.requestRenderAll();
        }
    }
});

//绑定查询按钮
$("#btnSearch").click(function () {
    var searchName =document.getElementById('search_name').value;
    var data = {"partName" : searchName};
    getLeftList(data);
});

$("#btnConfirm").click(function () {
    $("#saveForm").submit();
});

//绑定按钮事件
$("#btnCancel").click(function () {
    $.ligerDialog.hide();
});

//提交验证
$("#saveForm").validator({
    ignore: ":hidden", theme: "yellow_right", timely: 1, stopOnError: true,
    rules: { digits: [/^\d+(\.\d+)?$/, '{0}必须为数字！']},
    fields: {
        partName: '组件名称:required,length[1~50]',
    },
    valid: function (form) {formSave(form);}
});

// 按下左侧菜单触发事件
function clickMenu(type,name,id,inputId){
    // 非编辑状态下点击基本组件直接返回
    if(showModel==0 && type=="0"){
        return;
    }
    var cssFlg =false;
    // 选中CSS样式手动调整下= =
    $('#leftTable tr').each(function(i){
        $(this).children('td').each(function(j){
            if($(this).text()==name){
                // 已经是这个颜色了 退出模式
                if(colorRGB2Hex($(this).css("background-color"))=="#4a8bc2"){
                    $(this).css("background-color","");
                    cssFlg = true;
                }else{
                    $(this).css("background-color","#4a8bc2");
                }
            }else{
                $(this).css("background-color","");
            }
        });
    });

    // 返回
    if(cssFlg){
        // 退出画笔模式呀
        changeOutDrawModel();
        return;
    }

    // 非编辑模式下加载的信息
    if(showModel==0){
        var name_p1 = "";
        if(type=='1'){
            name_p1 = "常用组件";
        }else if(type=='2'){
            name_p1 = "常用大组件";
        }

        // 隐藏初始化界面，显示具体的画图界面，同时更改标题
        $(".l-layout-center .l-layout-header").html(name_p1+"【"+name+"】 预览模式");
        $("#initWin").hide();
        $("#cWin").show();
        $("#selectId").val(id);
        $("#selectName").val(name);
        $("#selectType").val(type);

        // 默认全清
        if (clearFlg){
            canvas.clear().renderAll();
        }
    }

    // 如果是编辑模式且选中的是基本组件的情况下，切换为画笔模式
    if(showModel==1 && type=='0'){
        // 更换为画笔模式
        changeToDrawModel();
        listData.forEach(function (item,index,input) {
            // 如果ID一致
            if(id == input[index].id){
                drawData = $.parseJSON(input[index].partMemory);
                if(drawData.objects[0]['type']=="textbox"){
                    $("#strokeWidthSelect").val(drawData.objects[0]['fontSize']);
                    $("#colorPicker1").spectrum("set", drawData.objects[0]['fill']);
                }else{
                    $("#strokeWidthSelect").val(drawData.objects[0]['strokeWidth']);
                    $("#colorPicker1").spectrum("set", drawData.objects[0]['stroke']);
                }
            }
        });
    }else{
        // 加载对应的组件对象
        listData.forEach(function (item,index,input) {
            // 如果ID一致
            if(id == input[index].id){
                if(showModel == 0 ){
                    // 加载 最后一个参数0代表重新赋值ID,1或者没有代表不重新赋值
                    loadObj = canvas.loadFromJSONFr(input[index].partMemory,null,function(oa,oj) {
                        if(showModel==0){
                            // 预览
                            oj.selectable=false;
                        }else{
                           // 编辑
                           oj.selectable=true;
                           // 需要重新定位位置
                           oj.set("left",oj.left+(mousewheelX-oj.left)*mousewheel-movementX);
                           oj.set("top",oj.top+(mousewheelY-oj.top)*mousewheel-movementY);
                        }
                    },1);
                    // 退出画笔模式
                    changeOutDrawModel();
                }else{
                    // 先存起来不加载
                    menuSelectObj = input[index].partMemory;
                    isMenuSelectModel = true;
                }
            }
        });
    }
}

// 取消
function btnExit(){
    //清除值
    $("#partType").val("");
    //样式显示回到初始状态
    $(".l-layout-center .l-layout-header #l-layout-pic-title").html("电力建图");
    $("#initWin").show();
    $("#cWin").hide();
    $("#beforeEditBtns").show();
    $("#afterEditBtns").hide();
    //清除saveform2    
    $("#configId").val("");
    $("#configCanvasName").val("");
    //标准清除回到初始状态
    canvas.clear().renderAll();
    clearFlg = true;
    showModel = 0;
    // 重置撤回操作
    _config = {
        canvasState             : [],
        currentStateIndex       : -1,
        undoStatus              : false,
        redoStatus              : false,
        undoFinishedStatus      : 1,
        redoFinishedStatus      : 1,
        undoButton              : document.getElementById('undo'),
        redoButton              : document.getElementById('redo'),
    };
    // 剪切板不清除了
    // 退出画笔模式
    changeOutDrawModel();
    // 改变鼠标样式
    canvas.hoverCursor = 'pointer';
    // 还原画布偏移位置信息 没搞懂要超过二次才准确 
    for(var i=0;i<2;i++){
        var delta = new fabric.Point(0, 0);
        movementX = 0;
        movementY = 0;
        canvas.absolutePan(delta);
        // 还原画布缩放比例
        zoomPoint = new fabric.Point(canvas.width * 0.5 , canvas.height * 0.5);
        $("#sf").html("当前缩放比例：100%");
        canvas.zoomToPoint(zoomPoint, 1);
        mousewheel = 0;
    }
}

function btnEdit(){
    var oldTiltle = $(".l-layout-center .l-layout-header").html();
    var newTiltle = oldTiltle.replace(/预览模式/, "编辑模式");
    $(".l-layout-center .l-layout-header").html(newTiltle);
    $("#afterEditBtns").show();
    $("#beforeEditBtns").hide();
    // 转为可编辑状态
    clearFlg = false;
    showModel = 1;
    saveCanvasType=0;
    var objAll = canvas.getObjects(); 
    objAll.forEach(function (item,index,input) {
        input[index].selectable=true;
    });
    // 编辑状态是移动
    canvas.hoverCursor = 'move';
}

// 删除
function btnDel(){
    $.ligerDialog.confirm("确定要删除选中记录？","提示", function(confirm){
        if(confirm){
             $.post("doDelete",{id:$("#selectId").val()},function(data){
                if (data.retCode==100) {
                    $.ligerDialog.success('删除成功！',"提示",function(opt){
                        btnExit();
                        getLeftList();
                    });
                }else if(data.retCode==-700){
                    $.ligerDialog.error("删除失败！存在", "提示");
                }else{
                    $.ligerDialog.error("删除失败！", "提示");
                }
            },"json")
        }
    });
}

function btnSave(type){
    $("#saveForm")[0].reset();
    canvasStationMemory ="";
    if(saveCanvasType==0){
        console.log("GroupAll begin");
        var ret = GroupAll();
        console.log("GroupAll end");
        if(ret==0){
            $.ligerDialog.error("请先画上你想要的组件再进行保存！", "提示");
            return;
        }else{
            var ojb = JSON.stringify(canvas);
            $("#partMemory").val(ojb);
            $("#id").val($("#selectId").val());
            $("#partName").val($("#selectName").val());
            // 值集成过来
            $("#partType").val($("#selectType").val());
            
            $.ligerDialog.open({
                target:$("#partSaveWin"),
                height: 300,
                width:600,
                title: "确认保存",
                isHidden: true,
                isResize:true ,
                allowClose:false
            });
        }
    }else if(saveCanvasType==1 || saveCanvasType==2 || saveCanvasType==3){
           //定义一个空字符串用于保存数据   
           var ojb = JSON.stringify(canvas);
           canvasStationMemory = ojb;
           // 绑定数据串
           var bindDataStr = "";
           // 绑定事件串
           var bindEventStr = "";
           //判断dataBindList是否为空
           if(dataBindList.length>0){
               var temp1 = 0 ;
               // 拼接字符串
               for(var i in dataBindList){
                   temp1 ++;
                   // 分隔
                   bindDataStr += dataBindList[i].substationSn+'*'+dataBindList[i].objId
                       +'*'+dataBindList[i].smdId+'*'+dataBindList[i].varName
                       +'*'+dataBindList[i].dataType+'*'+dataBindList[i].bindTable+'*'+dataBindList[i].bindColumns
                       +'*'+dataBindList[i].stateOpen+'*'+dataBindList[i].stateClose+'*'+dataBindList[i].showPoint;
                       if(temp1 < dataBindList.length){
                           bindDataStr +='|';
                       }
               }
           }
           //判断eventBindList是否为空
           if(eventBindList.length>0){
               var temp2 = 0 ;
               // 拼接字符串
               for(var i in eventBindList){
                   temp2 ++;
                   bindEventStr += eventBindList[i].substationSn+','+eventBindList[i].objId
                       +','+eventBindList[i].eventId+','+eventBindList[i].eventType;
                       if(temp2 < eventBindList.length){
                           bindEventStr +='|';
                       }
               }
               
           }
           //保存电力图
           saveElcPicture(bindDataStr,bindEventStr,type);
    }
}

//压缩
function zip(str) {
  //escape(str)  --->压缩前编码，防止中午乱码
  var binaryString = pako.gzip(escape(str), { to: 'string' });
  return binaryString;
}
//保存电力图（包含绑定的数据和事件） type:0 有弹出信息提示适用于按钮保存  1：底部MSG提示适用于快捷键保存，预览保存
function saveElcPicture(bindDataStr,bindEventStr,type){
    //提交
    var data = {};
    data.id=document.getElementById('configId').value;
    data.canvasType=document.getElementById('hidden_canvas_type').value;
    data.canvasName=document.getElementById('configCanvasName').value;
    data.substationSn=document.getElementById('tempSubstationSn').value;
    data.shortName=document.getElementById('tempShortname').value;
    data.canvasWidth = document.getElementById('departSize_width1').value;
    data.canvasHeight = document.getElementById('departSize_height1').value;
    data.canvasBackgroundColor = canvas.backgroundColor;
    data.stationMemory = canvasStationMemory;
    data.bindDataStr=bindDataStr;//绑定的数据字符串
    data.bindEventStr=bindEventStr;//绑定的事件字符串
    $("#saveMsg").html("<span style='color:green'>保存中..请稍等..</span>");
    //异步提交电力图、数据、事件
    $.ajax({
        type: 'post',
        url: "insertPictureStation",
        data: data,
        cache: false,
        dataType: 'json',
        success: function (data) {
            if (data && data.retCode == 100) {
                $("#saveMsg").html("<span style='color:green'>保存成功！</span>");
                setTimeout(function(){$("#saveMsg").html("");}, 3000);
                if(type==0){
                    $.ligerDialog.success('保存成功！', "提示", function (opt) {
                        $.ligerDialog.hide();
                        document.getElementById('configId').value= data.retObj['id'];
                        getLeftList();
                    });
                }else{
                    document.getElementById('configId').value= data.retObj['id'];
                    getLeftList();
                }
            } else {
                $("#saveMsg").html("<span style='color:red'>保存失败请稍后再试！</span>");
                var text = "";
                if (data.errors && data.errors.length) {
                    for (var i = 0; i < data.errors.length; i++) {
                        text = text + "<br/>" + data.errors[i];
                    }
                }
                $.ligerDialog.error(text, "提示");
            }
        }
    });
}
//保存表单数据
function formSave(form) {
    var data = serializeObject($("#saveForm"));
    $.ajax({
        type: 'post',
        url: "save",
        data: data,
        cache: false,
        dataType: 'json',
        success: function (data) {
            if (data && data.retCode == 100) {
                $.ligerDialog.success('保存成功！', "提示", function (opt) {
                    btnExit();
                    getLeftList();
                    $.ligerDialog.hide();
                });
            } else {
                var text = "";
                if (data.errors && data.errors.length) {
                    for (var i = 0; i < data.errors.length; i++) {
                        text = text + "<br/>" + data.errors[i];
                    }
                }
                $.ligerDialog.error(text, "提示");
            }
        }
    });
}

//右键点击事件响应
function onContextmenu(event) {
    // 非编辑状态不显示右击菜单
    if(showModel==0){
        return false;
    }else{
        var pointer = canvas.getPointer(event.originalEvent);
        var objects = canvas.getObjects();
        var check = false;
        var checkObj;
        for (var i = objects.length - 1; i >= 0; i--) {
            var object = objects[i];
            //判断该对象是否在鼠标点击处
            if (canvas.containsPoint(event, object)) {
               //选中该对象
               canvas.setActiveObject(object);
               checkObj = object;
               check = true;
               break;
            }
        }

        if(check){
            bulidBindMsg(checkObj);
            // 右键也让他选中框出现
            checkObj.setCoords();
            canvas.requestRenderAll();
            //显示菜单
            showContextMenu(event, checkObj);
        }else{
            // 清空MSG
            clearBindMsg();
            //显示全局菜单
            showContextMenuWidthOutObj(event);
        }

        //阻止系统右键菜单
        event.preventDefault();
        return false;
    }
}

//右键菜单项点击
function showContextMenu(event, object) {
    if(saveCanvasType==0){
        //定义右键菜单项
        contextMenuItems = {
           "type": {name: "类型："+object.get("type"),icon: "fa-cog", data: object},
           "id": {name: "ID值："+object.get("id"),icon: "fa-info-circle", data: object},
           "sep1": "---------",
           "clone": {name: "克隆", icon: "fa-copy", data: object},
           "edit": {name: "编辑", icon: "fa-edit", data: object},
           "delete": {name: "删除", icon: "fa-trash", data: object},
           "sep2": "---------",
           "divide": {name: "均分", icon: "fa-braille", data: object},
           "sep3": "---------",
           "right": {name: "顺时90°", icon: "fa-rotate-right", data: object},
           "left": {name: "逆时90°", icon: "fa-rotate-left", data: object},
           "sep4": "---------",
           "top": {name: "置于顶层", icon: "fa-chevron-circle-up", data: object},
           "up": {name: "上移一层", icon: "fa-chevron-up", data: object},
           "down": {name: "下移一层", icon: "fa-chevron-down", data: object},
           "bottom": {name: "置于底层", icon: "fa-chevron-circle-down", data: object},
        };
    }else{
        //定义右键菜单项
        contextMenuItems = {
           "type": {name: "类型："+object.get("type"),icon: "fa-cog", data: object},
           "id": {name: "ID值："+object.get("id"),icon: "fa-info-circle", data: object},
           "sep1": "---------",
           "clone": {name: "克隆", icon: "fa-copy", data: object},
           "edit": {name: "编辑", icon: "fa-edit", data: object},
           "delete": {name: "删除", icon: "fa-trash", data: object},
           "sep2": "---------",
           "divide": {name: "均分", icon: "fa-braille", data: object},
           "sep3": "---------",
           "right": {name: "顺时90°", icon: "fa-rotate-right", data: object},
           "left": {name: "逆时90°", icon: "fa-rotate-left", data: object},
           "sep4": "---------",
           "top": {name: "置于顶层", icon: "fa-chevron-circle-up", data: object},
           "up": {name: "上移一层", icon: "fa-chevron-up", data: object},
           "down": {name: "下移一层", icon: "fa-chevron-down", data: object},
           "bottom": {name: "置于底层", icon: "fa-chevron-circle-down", data: object},
           "sep5": "---------",
           "dataBind": {name: "数据绑定", icon: "fa-database", data: object,visible: !isSelectDataBind},
           "dataUnBind": {name: "数据解绑", icon: "fa-ban", data: object,visible: isSelectDataBind},
           "fun01Edit1": {name: "合闸配置", icon: "fa-cogs", data: object,visible: isSelectDataBind&&isSelectDataSwith},
           "fun01Edit0": {name: "分闸配置", icon: "fa-cogs", data: object,visible: isSelectDataBind&&isSelectDataSwith},
           "fun03Edit": {name: "显示配置", icon: "fa-cogs", data: object,visible: isSelectDataBind&&!isSelectDataSwith},
           "eventBind": {name: "事件绑定", icon: "fa-calendar-plus-o", data: object,visible: !isSelectEventBind},
           "eventUnBind": {name: "事件解绑", icon: "fa-minus-square", data: object,visible: isSelectEventBind},
        };
    }
    //右键菜单显示位置
    var position = {
        x: event.clientX,
        y: event.clientY
    };

    $('#contextmenu-output').contextMenu(position);
}

//右键菜单项点击
function showContextMenuWidthOutObj(event) {
    // 组件右键菜单
    if(saveCanvasType==0){
       //定义右键菜单项
       contextMenuItems = {
           "fuwei": {name: "复位", icon: "fa-undo"},
           "back": {name: "撤销", icon: "fa-arrow-left"},
           "recovery": {name: "恢复", icon: "fa-arrow-right"}
        };
    }else{
        //定义右键菜单项
        contextMenuItems = {
            "fuwei": {name: "复位", icon: "fa-undo"},
            "back": {name: "撤销", icon: "fa-arrow-left"},
            "recovery": {name: "恢复", icon: "fa-arrow-right"},
            "aligntop": {name: "向上对齐", icon: "fa-align-center"},
            "aligndown": {name: "向下对齐", icon: "fa-align-center"},
            "alignleft": {name: "向左对齐", icon: "fa-align-left"},
            "alignright": {name: "向右对齐", icon: "fa-align-right"},
            "divertX": {name: "水平分散", icon: "fa-align-right"},
            "divertY": {name: "垂直分散", icon: "fa-align-right"},
            "canvasItem": {name: "画布属性", icon: "fa-cogs"},
            "preview": {name: "画面预览", icon: "fa-play"},
            "showDataBind": {name: "查看数据绑定", icon: "fa-search"},
            "showEventBind": {name: "查看事件绑定", icon: "fa-search"}
         };
    }
    //右键菜单显示位置
    var position = {
        x: event.clientX,
        y: event.clientY
    };
    $('#contextmenu-output').contextMenu(position);
}

//删除绑定的数据和事件
function delBindDataAndEvent(object){
    //同时删除该数据绑定
    if(dataBindList.length>0){
        for(var i = 0;i<dataBindList.length;i++){
            if(dataBindList[i].objId==object.id){
                dataBindList.splice(i,1);//删除数组中指定的数据
                break;
            }
        }
    }
    if(eventBindList.length>0){
        for(var i = 0;i<eventBindList.length;i++){
            if(eventBindList[i].objId==object.id){
                eventBindList.splice(i,1);//删除数组中指定的数据
                break;
            }
        }
    }
}

//右键菜单项点击
function contextMenuClick(key, options) {
  if(showModel==0){
      $.ligerDialog.warn("操作前请点击开始编辑按钮！", "提示");
      return;
  }else{
      //得到对应的object并删除
      var object = contextMenuItems[key].data;
      // 删除
      if(key == "delete") {
          canvas.remove(object);
          delBindDataAndEvent(object);//删除绑定的数据和事件
      // 编辑
      }else if(key =="edit"){
          openEditWin(object,1);
      // 克隆
      }else if(key == "clone"){
          object.clone(function(cloned){
              cloned.set({
                  left: cloned.left + 20,
                  top: cloned.top + 20,
                  evented: true,
              });
              //重置所有ID 参数1-自身ID也要重置
              rebuilderAllId(cloned,1);
              canvas.add(cloned);
              canvas.setActiveObject(cloned);
          });
      }else if(key =="top"){
          canvas.bringToFront(object);
          canvas.requestRenderAll();
      }else if(key =="up"){
          canvas.bringForward(object,true);
          canvas.requestRenderAll();
      }else if(key =="down"){
          canvas.sendBackwards(object,true);
          canvas.requestRenderAll();
      }else if(key =="bottom"){
          canvas.sendToBack(object);
          canvas.requestRenderAll();
      // 自用打印对象json串
      }else if(key =="id"){
          console.log(JSON.stringify(object));
      // 旋转
      }else if(key =="right"){
          var temp = object.angle;
          temp+=90;
          if(temp==360){temp=0};
          object.rotate(temp);
          //刷新画面
          object.setCoords();
          canvas.requestRenderAll();
      // 旋转
      }else if(key =="left"){
          var temp = object.angle;
          temp-=90;
          if(temp==-360){temp=0};
          object.rotate(temp);
          //刷新画面
          object.setCoords();
          canvas.requestRenderAll();
      // 均分
      }else if(key == "divide"){
          // 打开属性画面
          openEditWin(object,2);
      //向上对齐
      }else if(key == "aligntop"){
          aligntop();
      //向下对齐
      }else if(key == "aligndown"){
          aligndown();
      //向左对齐
      }else if(key == "alignleft"){
          alignleft();
      //向右对齐
      }else if(key == "alignright"){
          alignright();
      //水平分散
      }else if(key == "divertX"){
          divertX();
      //垂直分散
      }else if(key == "divertY"){
          divertX();
      }else if(key == "canvasItem"){
          
          // 电力总图没有大小
          if($("#hidden_canvas_type").val() == 0){
              $("#departSize1").hide();
              $("#departSize_width1").val(canvas.width);
              $("#departSize_height1").val(canvas.height);
          // 部件图有大小
          }else{
              $("#departSize1").show();
              $("#departSize_width1").val($("#departSize_width").val());
              $("#departSize_height1").val($("#departSize_height").val());
          }

          $("#zdyShowName").val($("#configCanvasName").val());
          $("#colorPicker5").spectrum("set", canvas.backgroundColor);
          $("#hb_bjys").val(canvas.backgroundColor);
          //打开弹窗
          canvasEditWin=$.ligerDialog.open({
              target:$("#canvasEditWin"),
              height: 400,
              width:400,
              title: "画布属性设置",
              isHidden:true,
              isResize:false,
              showMax:false,
              showMin:false
          });
      }else if(key == "fuwei"){
          //没搞懂
          for(var i=0;i<2;i++){
              // 还原原点清除偏移量
              var delta = new fabric.Point(0, 0);
              movementX = 0;
              movementY = 0;
              // 有待试验 有效果不知道有没有BUG
              canvas.absolutePan(delta);
              // 大小也进行一个复位
              // 还原画布缩放比例
              zoomPoint = new fabric.Point(canvas.width * 0.5 , canvas.height * 0.5);
              $("#sf").html("当前缩放比例：100%");
              canvas.zoomToPoint(zoomPoint, 1);
              mousewheel = 0;
          }
      }else if(key == "preview"){
          var objs = canvas.getObjects();//获得所有对象
          btnSave(1);
          openNewTap(document.getElementById('configId').value);
      }else if(key == "recovery"){
          redo();
      }else if(key == "back"){
          undo();
      }else if(key == "showDataBind"){
          if(dataBindList.length==0){
              $.ligerDialog.warn("没有数据绑定！","提示");
          }else{
              showDataBind();
          }
      }else if(key == "showEventBind"){
          if(eventBindList.length==0){
              $.ligerDialog.warn("没有事件绑定！","提示");
          }else{
              showEventBind();
          }
      }else if(key == "dataBind"){
          //初始化数据绑定窗口列表
          initDataBindGrid();
      }else if(key == "dataUnBind"){
          //解绑
          for(var i = 0;i<dataBindList.length;i++){
              if(object.id==dataBindList[i].objId){
                  $.ligerDialog.confirm('确认解绑数据吗？', "提示",function(confirm) {
                      if(confirm){
                          //解绑
                          dataBindList.splice(i,1);//删除数组中指定的元素
                      }else{
                          return;
                      }
                  });
                  break;//跳出循环
              }
          }
      }else if(key == "eventBind"){
          //初始化事件绑定窗口列表
          initEventBindGrid();
      }else if(key == "eventUnBind"){
          //解绑
          for(var i = 0;i<eventBindList.length;i++){
              if(object.id==eventBindList[i].objId){
                  $.ligerDialog.confirm('确认解绑事件吗？', "提示",function(confirm) {
                      if(confirm){
                          //解绑
                          eventBindList.splice(i,1);//删除数组中指定的元素
                      }else{
                          return;
                      }
                   });
                   break;//跳出循环
              }
          }
      // 开关的合闸显示配置
      }else if(key == "fun01Edit1"){
          fun01Edit(object,"1");
      // 开关的分闸显示配置
      }else if(key == "fun01Edit0"){
          fun01Edit(object,"0");
      // 文本的显示配置
      }else if(key == "fun03Edit"){
          // 获取这个对象对应的绑定数据中小数点位数
          var showPoint = 2;
          var showIndex;
          dataBindList.forEach(function (item,index,input) {
              if(input[index].objId==object.id){
                  showPoint = input[index].showPoint;
                  showIndex = index;
              }
          });
          $.ligerDialog.prompt('显示小数点位数',showPoint+'', function (yes,value) {
              if(yes){
                  dataBindList[showIndex].showPoint = value;
              } 
          });
      }
  }
}

//解绑
function unBlindClick(row,type){
   //判断删除类型
   if(type==1){
      for(var i =0;i<dataBindList.length;i++){
          if(dataBindList[i].objId==row.objId){
              dataBindList.splice(i,1);//删除数组中指定的元素
          }
      }
   }else{
      for(var i =0;i<eventBindList.length;i++){
          if(eventBindList[i].objId==row.objId){
              eventBindList.splice(i,1);//删除数组中指定的元素
          }
      }
   }
   var success = $.ligerDialog.success("解绑成功！");
   setTimeout(function(){success.close();}, 400);
}

//跳到指定位置
function searchBlindClcik(row){
    var objId = row.objId;
    // 设定为选中状态
    canvas.getObjects().forEach(function (obj) {
        if(obj.id == objId){
            canvas.setActiveObject(obj);//设置被选中状态
            canvas.renderAll();//页面刷新
            return;
        }
    });
}

//显示数据绑定
function showDataBind(){
    //隐藏
    $("#showBindDataGrid").show();
    $("#showBindEventGrid").hide();
    //初始化表格
    showDataBindGrid();
    showBindGridDialog();
}

//显示事件绑定
function showEventBind(){
    //隐藏
    $("#showBindDataGrid").hide();
    $("#showBindEventGrid").show();
    //初始化表格
    showEventBindGrid();
    showBindGridDialog();
}

//初始化数据绑定表格
function showDataBindGrid(){
    //获得数据
    var data = {Rows:dataBindList,Total:dataBindList.length};
    if(!showBindDataGrid){
        //初始化表格数据
        showBindDataGrid=$("#showBindDataGrid").ligerGrid({
            title:'&nbsp;&nbsp;<span style="color:#386792">数据绑定信息一览</span>',
            headerImg:'/hmdl/static/images/list.png',
            columns: [
                { display: '数据类型', name: 'dataTypeName',width:80},
                { display: '组件ID', name: 'objId',width:150},
                { display: '节点', name: 'bindColumns',width:70},
                { display: '变量名称', name: 'varName',width:120},
                { display: '变量描述', name: 'remark',width:150},
                { display: '操作', name: 'dataType',width:150,render: function (item){
                    if(item.dataType =='01'||item.dataType =='02'){
                        return "<a href='javascript:dbShowListClick(\""+item.objId+"\",0)'>分闸配置</a>"
                        +"&emsp;<a href='javascript:dbShowListClick(\""+item.objId+"\",1)'>合闸配置</a>";
                    }else if(item.dataType =='03'||item.dataType =='04'){
                        return "<a href='javascript:dbShowListClick(\""+item.objId+"\",2)'>显示配置</a>";
                    }
                }},
            ],
            data: data,
            height: 450,
            checkbox: false,
            rownumbers: true,
            usePager:false,
            onDblClickRow : function (data, rowindex, rowobj)
            {
                //绑定数据
                searchBlindClcik(data);
                //关闭窗口
                bindGridDialog.hide();
            }
        });
    }
    // 加载数据
    showBindDataGrid.loadData(data);
}

//初始化数据绑定表格
function showEventBindGrid(){
    //获得数据
    var data = {Rows:eventBindList,Total:eventBindList.length};
    //初始化表格数据
    showBindEventGrid=$("#showBindEventGrid").ligerGrid({
        title:'&nbsp;&nbsp;<span style="color:#386792">事件绑定信息一览</span>',
        headerImg:'/hmdl/static/images/list.png',
        columns: [
          { display: '绑定类型', name: 'eventType',width:80,render: function (item){
              if (item.eventType == 1){
                  return "弹出部件";
              }else if(item.eventType == 2){
                  return "分析数据";
              }
          }},
          { display: '组件ID', name: 'objId',width:150},
          { display: '事件ID', name: 'eventId',width:150},
          { display: '相关描述', name: 'showName',width:200}
        ],
        data: data,
        height: 450,
        checkbox: false,
        rownumbers: true,
        usePager:false
    }); 
    showBindEventGrid.reload();
}

//初始化绑定窗口
function showBindGridDialog(){
    if(!bindGridDialog){
        //打开弹出窗口
        bindGridDialog = $.ligerDialog.open({
                target:$("#bindGridDialog"),
                height: 500,
                width:800,
                title: "绑定信息查看",
                isHidden:true,
                allowClose:false,
                isResize:true,
                showMax:false,
                showMin:false,
                buttons: [
                { text: '解绑', onclick: function (item, dialog) {
                    var rows;
                    if($("#showBindDataGrid").css("display")=="block"){
                        rows = showBindDataGrid.getSelectedRows();
                    }else{
                        rows = showBindEventGrid.getSelectedRows();
                    }
                    if (rows == null || rows.length!=1) {
                        $.ligerDialog.warn("请选择一条记录！","提示");
                        return;
                    }
                    //解绑
                    unBlindClick(rows[0],1);
                    //刷新列表
                    showBindDataGrid.reload();
                    showBindEventGrid.reload();
                }},
                { text: '查看', onclick: function (item, dialog) { 
                    var rows;
                    if($("#showBindDataGrid").css("display")=="block"){
                        rows = showBindDataGrid.getSelectedRows();
                    }else{
                        rows = showBindEventGrid.getSelectedRows();
                    }
                    if (rows == null || rows.length!=1) {
                        $.ligerDialog.warn("请选择一条记录！","提示");
                        return;
                    }
                    //跳转到绑定的位置
                    searchBlindClcik(rows[0]);
                    //关闭窗口
                    dialog.hide();
                }},
                { text: '关闭', onclick: function (item, dialog) {
                    //关闭窗口
                    dialog.hide();
                }}
                ]
        });
    }else{
        bindGridDialog.show();
    }
}

//显示可绑定数据
function initDataBindGrid(){
    if(!dbDialogGrid){
        dbDialogGrid=$("#dbDialogGrid").ligerGrid({
            title:'&nbsp;&nbsp;<span style="color:#386792">变量信息一览</span>',
            headerImg:'/hmdl/static/images/list.png',
            columns: [
                { display: '数据类型', name: 'dataTypeName',width:120},
                { display: '节点', name: 'numberName',width:70},
                { display: '变量名称', name: 'varName',width:150},
                { display: '变量描述', name: 'remark',width:250}
            ],
            dataAction: 'server',
            url:'searchDB',
            parms:[{name:"substationSn",value:$("#search_data_substationSn").val()},
                   {name:"id",value:""}],
            root:'data',
            onDblClickRow : function (data, rowindex, rowobj)
            {
                //绑定数据
                bindData(data);
                //关闭窗口
                dbDialog.hide();
            } ,
            record:'count',
            height: 390,
            pageSize: 10,
            checkbox: false,
            rownumbers: true,
        });
    }else{
        dbDialogGrid.setParm("substationSn",$("#search_data_substationSn").val());
        dbDialogGrid.setParm("id","");
        dbDialogGrid.loadData();
    }

    //打开窗口判断
    if(!dbDialog){
        //打开弹窗
        dbDialog=$.ligerDialog.open({
                target:$("#dbDialog"),
                height: 500,
                width:800,
                title: "数据绑定",
                isHidden:true,
                isResize:false,
                showMax:false,
                showMin:false,
                buttons:[
                     { text: '绑定', onclick: function (item, ebDialog) { 
                          var rows = dbDialogGrid.getSelectedRows();
                          if (rows == null || rows.length!=1) {
                              $.ligerDialog.warn("请选择一条记录！","提示");
                              return;
                           };
                           //绑定数据
                           bindData(rows[0]);
                           //窗口隐藏
                           dbDialog.hide();
                     }},
                     { text: '取消', onclick: function (item, dbDialog) { dbDialog.hide(); } }
               ]
        });

        //绑定查询按钮（数据绑定）
        $("#btn_dbDialogForm_search").click(function () {
            var parms = serializeObject($("#dbDialogForm"));
            for(var p in parms){
                dbDialogGrid.setParm(p,parms[p].trim());
            }
            dbDialogGrid.loadData();
        });
    }else{
        dbDialog.show();
    }
}

//显示可绑定事件
function initEventBindGrid(){
  	//清空隐藏域值
  	$("#search_event_id").val("");
    //获取当前选中的obj
  	var object = canvas.getActiveObject();
    //判断是否已经绑定了数据，如果绑定了，把绑定的数据id存在隐藏域中
    for(var i = 0;i<dataBindList.length;i++){
        if(dataBindList[i].objId==object.id){
            $("#search_event_id").val(""+dataBindList[i].smdId);//隐藏域赋值
        }
    }
    
    //select选择框初始化
    if(createType==1){
        $("#popup_bj").remove();
        $("#popup_fx").prop("selected",true);
    }else if(createType==2){
        $("#search_event_type").prepend(selectDom);
        $("#popup_bj").prop("selected",true);
        $("#popup_fx").prop("selected",false);
    }else if(createType==3){
        $("#search_event_type").prepend(selectDom);
        $("#popup_bj").prop("selected",true);
        $("#popup_fx").prop("selected",false);
    }
    //根据select选择框初始化对应显示的表格
    if($("#popup_fx").prop("selected")==true){
        $("#popUpDialogGrid").hide();
        $("#analysisDialogGrid").show();
    }else{
        $("#popUpDialogGrid").show();
        $("#analysisDialogGrid").hide();
    }
     
    // 加载弹出部件GRID
    if(!popUpDialogGrid){
        popUpDialogGrid=$("#popUpDialogGrid").ligerGrid({
            title:'&nbsp;&nbsp;<span style="color:#386792">部件一览</span>',
            headerImg:'/hmdl/static/images/list.png',
            columns: [
                { display: '部件名称', name: 'canvasName',width:200},
                { display: '部件类型', name: 'canvasType',width:100,
                    render: function (item){
                        if(item.canvasType == 1){
                            return "图形部件";
                        }else if(item.canvasType == 2){
                            return "表格部件";
                        }else if(item.canvasType == 3){
                            return "文本部件";
                        }
                    }
                },
                { display: '部件高度', name: 'canvasHeight',width:80},
                { display: '部件宽度', name: 'canvasWidth',width:80},
                { display: '背景色', name: 'canvasBackgroundColor',width:50,
                    render: function (item){
                        return "<div style='width:100%;height:28px;line-height:28px;background-color:"+item.canvasBackgroundColor+";'></div>";
                    }
                },
                { display: '更新时间', name: 'updateDate',dateFormat: "yyyy-MM-dd hh:mm:ss",width:150},
            ],
            dataAction: 'server',
            url:'searchBj',
            root:'data',
            parms:[{name:"substationSn",value:$("#search_event_substationSn").val()}],
            onDblClickRow : function (data, rowindex, rowobj)
            {
                //绑定事件
                bindEvent(data,1);
                //关闭窗口
                ebDialog.hide();
            } ,
            record:'count',
            height: 390,
            pageSize: 10,
            checkbox: false,
            rownumbers: true,
        });
    }else{
        popUpDialogGrid.setParm("substationSn",$("#search_event_substationSn").val());
        popUpDialogGrid.loadData();
    }

    // 加载分析用变量一览
    if(!analysisDialogGrid){
        analysisDialogGrid=$("#analysisDialogGrid").ligerGrid({
            title:'&nbsp;&nbsp;<span style="color:#386792">变量信息一览</span>',
            headerImg:'/hmdl/static/images/list.png',
            columns: [
                { display: '数据类型', name: 'dataTypeName',width:120},
                { display: '节点', name: 'numberName',width:70},
                { display: '变量名称', name: 'varName',width:150},
                { display: '变量描述', name: 'remark',width:250}
            ],
            dataAction: 'server',
            url:'searchDB',
            parms:[{name:"substationSn",value:$("#search_event_substationSn").val()},
                   {name:"id",value:$("#search_event_id").val()}],
            root:'data',
            onDblClickRow : function (data, rowindex, rowobj)
            {
                //绑定事件
                bindEvent(data,2);
                //关闭窗口
                ebDialog.hide();
            } ,
            record:'count',
            height: 390,
            pageSize: 10,
            checkbox: false,
            rownumbers: true,
        });
    }else{
        analysisDialogGrid.setParm("substationSn",$("#search_event_substationSn").val());
        analysisDialogGrid.setParm("id",$("#search_event_id").val());
        analysisDialogGrid.loadData();
    }
    
    if(!ebDialog){
        //打开弹窗
        ebDialog=$.ligerDialog.open({
            target:$("#ebDialog"),
            height: 500,
            width:800,
            title: "事件绑定",
            isHidden:true,
            isResize:false,
            showMax:false,
            showMin:false,
            buttons:[
               { text: '绑定', 
                 onclick: function (item, ebDialog) { 
                     //绑定事件
                     if($("#search_event_type").val()==1){
                         //判断是否选择
                         var rows = popUpDialogGrid.getSelectedRows();
                         if (rows == null || rows.length!=1) {
                             $.ligerDialog.warn("请选择一条记录！","提示");
                             return;
                         };
                         //绑定
                         bindEvent(rows[0],1);
                     }else{
                         //判断是否选择
                         var rows = analysisDialogGrid.getSelectedRows();
                         if (rows == null || rows.length!=1) {
                             $.ligerDialog.warn("请选择一条记录！","提示");
                             return;
                         };
                         //绑定
                         bindEvent(rows[0],2);
                     }
                     //窗口隐藏
                     ebDialog.hide();
                       // 1 弹出部件
                      $("#search_ebDialog_name").val("");//清空名称输入框
                    if($("#search_event_type").val()==1){
                        popUpDialogGrid.setParm("canvasName",$("#search_ebDialog_name").val().trim());
                        popUpDialogGrid.loadData();
                    }else{
                        analysisDialogGrid.setParm("varName",$("#search_ebDialog_name").val().trim());
                        analysisDialogGrid.loadData();
                    }
                 }
               },
               { text: '取消', onclick: function (item, ebDialog) { ebDialog.hide(); 
                    // 1 弹出部件
                    $("#search_ebDialog_name").val("");//清空名称输入框
                    if($("#search_event_type").val()==1){
                        popUpDialogGrid.setParm("canvasName",$("#search_ebDialog_name").val().trim());
                        popUpDialogGrid.loadData();
                    }else{
                        analysisDialogGrid.setParm("varName",$("#search_ebDialog_name").val().trim());
                        analysisDialogGrid.loadData();
                    }
               } }
            ]
        });

        //绑定查询按钮（事件绑定）
        $("#btn_ebDialogForm_search").click(function () {
            // 1 弹出部件
            if($("#search_event_type").val()==1){
                popUpDialogGrid.setParm("canvasName",$("#search_ebDialog_name").val().trim());
                popUpDialogGrid.loadData();
            }else{
                analysisDialogGrid.setParm("varName",$("#search_ebDialog_name").val().trim());
                analysisDialogGrid.loadData();
            }
        });
    }else{
        ebDialog.show();
    }
}

// 下拉框切换弹出部件和分析一览
function selectEventType(){
    $("#search_ebDialog_name").val("");//清空名称输入框
    if($("#search_event_type").val()==1){
        popUpDialogGrid.setParm("canvasName",$("#search_ebDialog_name").val().trim());
        popUpDialogGrid.loadData();
    }else{
        analysisDialogGrid.setParm("varName",$("#search_ebDialog_name").val().trim());
        analysisDialogGrid.loadData();
    }
    //获取类型值
    if($("#search_event_type").val()==1){
        $("#popUpDialogGrid").show();
        $("#analysisDialogGrid").hide();
    }else{
        $("#popUpDialogGrid").hide();
        $("#analysisDialogGrid").show();
    }
}

//绑定数据
function bindData(data){
    //获取当前选中的对象
    var bindedObj = canvas.getActiveObject();
    // 循环遍历如若已有就去掉
    dataBindList.forEach(function (item,index,input) {
        // 提示错误信息
        if(input[index].objId==bindedObj.id){
            $.ligerDialog.error("组件ID："+bindedObj.id+"已被绑定过，请重新绘制此组件，保证ID唯一性！");
            return;
        }
    });
    // 开关效果
    var stateOpen = "";
    var stateClose = "";
    // 指定默认值，省的他每个进行编辑
    if(isSpecicalObjSwith(bindedObj)){
        stateOpen = '{"stroke":"rgb(255, 0, 0)","angle":23}';// 红色旋转
        stateClose = '{"stroke":"rgb(12, 116, 12)","angle":0}';// 绿色不旋转
    }else{
        // 单个对象也指定默认值
        if(bindedObj.type!="group"){
            stateOpen = '{"fill":"rgb(255, 0, 0)"}';// 红色
            stateClose = '{"fill":"rgb(0, 255, 0)"}';// 绿色
        // group也指定下不然后台会爆炸
        }else{
            stateOpen = '{"angle":0}';// 不旋转
            stateClose = '{"angle":0}';// 不旋转
        }
    }

    // 寄存器不需要这些
    if(data.funcCode=='03'){
        stateOpen = "";
        stateClose = "";
    }
    var db = {
        substationSn:data.substationSn,
        objId:bindedObj.id,
        smdId:data.id,
        dataTypeName:data.dataTypeName,
        remark:data.remark,
        varName:data.varName,
        bindTable:"bind_table",//预留暂时没用
        bindColumns:data.numberName,
        dataType:data.funcCode,//功能码,01开关，03寄存器
        showPoint:2,// 默认小数位数2位 开关用不到此变量，这边统一四舍五入法，后期要改再改
        stateOpen:stateOpen,// 默认开关合
        stateClose:stateClose// 默认开关分
    };
    //向数组中添加新数据绑定
    dataBindList.push(db);
    //提示成功
    var success = $.ligerDialog.success("数据绑定成功！");
    setTimeout(function(){success.close();}, 500);
}

//绑定事件
function bindEvent(data,type){
    //获取当前选中的对象
    var bindedObj = canvas.getActiveObject();
    // 循环遍历如若已有就去掉
    eventBindList.forEach(function (item,index,input) {
        //提示错误信息
        if(input[index].objId==bindedObj.id){
            $.ligerDialog.error("组件ID："+bindedObj.id+"已被绑定过，请重新绘制此组件，保证ID唯一性！");
            return;
        }
    });
    // 创建绑定关系
    var eb = {
        substationSn:data.substationSn,
        objId:bindedObj.id,
        eventType:type==1?"1":"2",
        showName:type==1?data.canvasName:data.varName,
        eventId:data.id
    };
    //向数组中添加新事件绑定
    eventBindList.push(eb);
    //提示成功
    var success = $.ligerDialog.success("事件绑定成功！");
    setTimeout(function(){success.close();}, 500);
}

//画布属性窗口提交
function canvasEdit(){
    // 赋值
    $("#departSize_width").val($("#departSize_width1").val());
    $("#departSize_height").val($("#departSize_height1").val());
    $("#configCanvasName").val($("#zdyShowName").val());

    canvas.setBackgroundColor($("#hb_bjys").val());//设置画布背景颜色
    canvas.requestRenderAll();
    canvasEditWin.hide();
}
//关闭画布属性窗口
function canvasCancel(){
    canvasEditWin.hide();
}

// 右击菜单打开窗口 1：编辑窗口  2：均分窗口
function openEditWin(object,num){
    // 编辑窗口
    if(num==1){
        // 文本编辑窗口
        if(object.type=="textbox"){
            // 清空FORM
            $("#editTextForm")[0].reset();
            // 赋值
            $("#editText_type").val(object.type);// 组件类型
            $("#editText_id").val(object.id);// 组件ID
            $("#editText_left").val(object.left.toFixed(2));// X轴
            $("#editText_top").val(object.top.toFixed(2));// Y轴
            $("#editText_text").val(object.text);// 文本内容
            $("#editText_fontSize").val(object.fontSize);// 字号
            $("#editText_fontWeight").val(object.fontWeight);// 加粗
            $("#editText_fontFamily").val(object.fontFamily);// 字体
            $("input[name='underline']").attr("checked", object.underline);// 下划线
            $("input[name='overline']").attr("checked", object.overline);// 上划线
            $("input[name='linethrough']").attr("checked", object.linethrough);// 贯穿线
            $("#editText_textAlign").val(object.textAlign);// 对齐方式
            //字体颜色
            $("#colorPicker4").spectrum("set", object.fill);
            $("#editText_fill").val(object.fill);

            if(!editTextWin){
                editTextWin=$.ligerDialog.open({
                    target:$("#editTextWin"),
                    height: 500,
                    width:500,
                    title: "文本编辑",
                    isHidden:true,
                    isResize:false,
                    showMax:false,
                    showMin:false
                });
            }else{
                editTextWin.show();
            }
        // 组件编辑窗口
        }else{

            // 清空FORM
            $("#editWinForm")[0].reset();
            // 赋值
            $("#edit_type").val(object.type);// 组件类型
            $("#edit_id").val(object.id);// 组件ID
            $("#edit_left").val(object.left.toFixed(0));// X轴
            $("#edit_top").val(object.top.toFixed(0));// Y轴
            $("#edit_scaleX").val(object.scaleX);// 横轴缩放比例
            $("#edit_scaleY").val(object.scaleY);// 纵轴缩放比例
            // 组件group编辑更加精确化
            if(object.type=="group"){
                var g_strokeWidth;
                var g_stroke;
                var g_fill;
                object.getObjects().forEach(function(obj) {
                    if(!g_strokeWidth){g_strokeWidth = obj.strokeWidth.toFixed(0);}
                    if(!g_stroke){g_stroke = obj.stroke;}
                    if(!g_fill && obj.type!="line"&& obj.type!="polyline"){g_fill = obj.fill;}
                });
                $("#edit_strokeWidth").val(g_strokeWidth);// 线条宽度
                $("#edit_stroke").val(g_stroke);// 线条颜色
                $("#colorPicker2").spectrum("set", g_stroke);
                $("#edit_fill").val(g_fill);// 填充色
                $("#colorPicker3").spectrum("set", g_fill);
            }else{
                $("#edit_strokeWidth").val(object.strokeWidth.toFixed(0));// 线条宽度
                $("#edit_stroke").val(object.stroke);// 线条颜色
                $("#colorPicker2").spectrum("set", object.stroke);
                $("#edit_fill").val(object.fill);// 填充色
                $("#colorPicker3").spectrum("set", object.fill);
            }

            if(!editWin){
                editWin=$.ligerDialog.open({
                    target:$("#editWin"),
                    height: 500,
                    width:500,
                    title: "组件编辑",
                    isHidden:true,
                    isResize:false,
                    showMax:false,
                    showMin:false
                });
            }else{
                editWin.show();
            }
        }
    // 均分窗口
    }else{
        // 清空FORM
        $("#editDivideForm")[0].reset();
        // 赋值
        $("#editDivide_type").val(object.type);// 组件类型
        $("#editDivide_id").val(object.id);// 组件ID
        //设置默认均分方向
        $("#divide_x").attr("checked",true);//默认为水平均分
        //个数
        $("#copy_num").val(1);//默认为1
        //间距
        $("#spacing").val(10);//默认为10

        if(!editDivideWin){
            editDivideWin=$.ligerDialog.open({
                target:$("#editDivideWin"),
                height: 500,
                width:500,
                title: "均分编辑",
                isHidden:true,
                isResize:false,
                showMax:false,
                showMin:false
            });
        }else{
            editDivideWin.show();
        }
    }
}

function editCancel(){
    if(editWin){editWin.hide();}
    if(editTextWin){editTextWin.hide();}
    if(editDivideWin){editDivideWin.hide();}
}

function editWinSave(){
    var obj = canvas.getActiveObject();
    //如果不是文本，设置对应属性
    obj.set('left', Number($("#edit_left").val()));// X轴坐标
    obj.set('top', Number($("#edit_top").val()));// Y轴坐标
    obj.set('scaleX', Number($("#edit_scaleX").val()));// 横轴比例
    obj.set('scaleY', Number($("#edit_scaleY").val()));// 纵轴比例
    // group区分处理
    if(obj.type=="group"){
        obj.getObjects().forEach(function(gobj) {
            gobj.set('stroke', $("#edit_stroke").val());// 线条颜色
            gobj.set('strokeWidth', Number($("#edit_strokeWidth").val()));// 线条粗细
            gobj.set('fill', $("#edit_fill").val());// 填充色
        });
    }else{
        obj.set('stroke', $("#edit_stroke").val());// 线条颜色
        obj.set('strokeWidth', Number($("#edit_strokeWidth").val()));// 线条粗细
        obj.set('fill', $("#edit_fill").val());// 填充色
    }

    //刷新画面
    // canvas.discardActiveObject();
    obj.setCoords();
    canvas.requestRenderAll();
    // 隐藏编辑窗口
    editCancel();
    // 记录下回撤点不管变没变提交就当成一次编辑进行记录
    updateCanvasState();
}

function editTextWinSave(){
    var obj = canvas.getActiveObject();
    //如果不是文本，设置对应属性
    obj.set('left', Number($("#editText_left").val()));// X轴坐标
    obj.set('top', Number($("#editText_top").val()));// Y轴坐标
    obj.set('text', $("#editText_text").val());
    obj.set('fontSize', Number($("#editText_fontSize").val()));
    obj.set('fontWeight', $("#editText_fontWeight").val());
    obj.set('fontFamily', $("#editText_fontFamily").val());
    obj.set('textAlign', $("#editText_textAlign").val());
    obj.set('fill', $("#editText_fill").val());
    // 下划线
    if($("#underline").prop("checked")){obj.set('underline', true);}else{obj.set('underline', false);}
    // 上划线
    if($("#overline").prop("checked")){obj.set('overline', true);}else{obj.set('overline', false);}
    // 贯穿线
    if($("#linethrough").prop("checked")){obj.set('linethrough', true);}else{obj.set('linethrough', false);}
    //刷新画面
    // canvas.discardActiveObject();
    obj.setCoords();
    canvas.requestRenderAll();
    // 隐藏编辑窗口
    editCancel();
    updateCanvasState();
}

// 均分
function editDivideWinSave(){
    var boo1 = $("#divide_x").prop("checked");
    var boo2 = $("#divide_y").prop("checked");
    if(boo1 == false && boo2 == false){
        $.ligerDialog.error("未选择均分方向！");
        return;
    }
    var copy_num = $("#copy_num").val();//复制个数
    var spacing = $("#spacing").val();//排列间距
    if(boo1==true){
        //获取当前选中的obj
        var object = canvas.getActiveObject();
        //复制copy_num个object,同时生成一个ID
        for(var i = 1;i<=copy_num;i++){
            object.clone(function(cloned){//复制选中的obj
                cloned.set({
                    left: cloned.left + spacing*i + object.width*i,
                    top: cloned.top,
                    evented: true,
                });
                // 重置所有ID 1-自身ID也重置
                rebuilderAllId(cloned,1);
                // 添加到画布中
                canvas.add(cloned);
                // 设置为选中状态
                canvas.setActiveObject(cloned);
            });
        }
        // 隐藏编辑窗口
        editCancel();
    }else{
          //获取当前选中的obj
        var object = canvas.getActiveObject();
        //复制copy_num个object,同时生成一个ID
        for(var i = 1;i<=copy_num;i++){
            object.clone(function(cloned){//复制选中的obj
                cloned.set({
                    left: cloned.left,    
                    top: cloned.top + spacing*i + object.height*i,
                    evented: true,
                });
                // 重置所有ID 1-自身ID也重置
                rebuilderAllId(cloned,1);
                // 添加到画布中
                canvas.add(cloned);
                // 设置为选中状态
                canvas.setActiveObject(cloned);
            });
        }
        // 隐藏编辑窗口
        editCancel();
    }
}

// 选中的目标组合
function btnGroup(){
    if (!canvas.getActiveObject()) {
        return;
    }
    if (canvas.getActiveObject().type !== 'activeSelection') {
       return;
    }
    canvas.getActiveObject().toGroup(0);
    canvas.requestRenderAll();
    updateCanvasState();
}

// 选中目标解组
function btnUnGroup(){
    if (!canvas.getActiveObject()) {
        return;
    }
    if (canvas.getActiveObject().type !== 'group') {
        return;
    }
    canvas.getActiveObject().toActiveSelection();
    canvas.requestRenderAll();
    updateCanvasState();
}

// 全部组合（用于保存时进行的自动操作，组件是作为整体出现而不是单独的一个一个部件出现）
// 返回值1 正常  0 画板为空没必要保存 
function GroupAll(){
    var ret =1;
    // 没有对象的时候
    if(canvas.getObjects().length==0){
        ret = 0;
    }else if(canvas.getObjects().length == 1) {
        // donothing
    }else if(canvas.getObjects().length > 1) {
        // bug修复
        canvas.discardActiveObject();
        // 组件都是组合起来的控件（不可拆分性）
        // 这边是否必要 看需求加 感觉部件不用自动组合，但是组件还是要自动组合比较好一点
        var group = new fabric.Group(canvas.getObjects());
        group.id = getNewId();
        // 清除原有的添加新的
        canvas.clear().renderAll();
        canvas.add(group);
    }
    return ret;
}

function getNewId(){
    // 组合了相当于新组件给个ID（便于存库）
    var now = new Date();
    var rnd = "";
    for(var i=0;i<5;i++) {
        rnd+=Math.floor(Math.random()*10);
    }
    return now.getTime() + rnd;
}

function getLeftList(data){
    // 开始加载左侧数据
    $.ajax({
        type: 'post',
        url: 'getLeftList',
        dataType: "json",
        data: data,
        success: function(data) {
            $("#leftLoading").hide();
            // 成功加载到数据
            if(data&&data.retCode==100){
                listData = data.retObj;
                // 加载基本组件
                $("#html0").html(loadHtml(listData,'0'));
                // 加载常用组件
                $("#html1").html(loadHtml(listData,'1'));
                // 加载常用大组件
                $("#html2").html(loadHtml(listData,'2'));
                $("#leftNone").hide();
                $("#leftList").show();
            // 没有加载到数据
            }else{
                $("#leftNone").show();
                $("#leftList").hide();
            }
        }
    });
}

// 左侧采用拼接HTML的方式进行拼接
function  loadHtml(obj,type){
    var begin = '<li><table id="leftTable" class="default" style="text-align: center;">';
    var end = '</table></li>';
    var html = "";
    // 计数
    var temp = 0;
    obj.forEach(function (item,index,input) {
        // 如果类型一致
        if(type == input[index].partType){
            //2个一行进行拼接
            if(temp%2==0){
                html+= '<tr>'
            }
            var imgsrc =input[index].thumbnail;
            // 空值放个404图片显示下不然前台报错太多
            if(imgsrc == null || imgsrc==""){
                imgsrc = "/hmdl/static/images/hmdl/404.png";
            }
            html+='<td style="width:50%" onclick="clickMenu(\''+input[index].partType+'\',\''+input[index].partName+'\','+input[index].id+')">';
            html+='<img src="'+imgsrc+'" style="width:32px;height:32px"/><br/><span style="font-weight:bolder;">'+input[index].partName+'</span></td>';
            if(temp%2==1){
                html+= '</tr>'
            }
            temp++;
        }
    });
    // 如果为空则显示空的提示信息
    if(html==""){
        html = '<li style="background-color:white"><img style="width:30%;margin-top: 10px" src="/hmdl/static/images/hmdl/404.png"/>';
        html += '<div style ="color:#515151;font-size: 12px"><br/>非常抱歉<br/>未能检索到您的组件信息<br/></div><br/></li>';
    // 有值再加上首位
    }else{
        html = begin+html+end;
    }
    return html;
}

function initMenuCreate(type){
    saveCanvasType = type;
    // 隐藏创建窗口打开画布窗口
    $("#afterEditBtns input").val("");
    $("#selectType").val(type);
    $(".l-layout-center .l-layout-header").html("<a id='l-layout-pic-title'></a>&emsp;<input  type='text' readonly='readonly' value='' id='selectSubstationName' onclick='selectStation()' style='width:300px;border:none;background-color:transparent;color:#428bca;cursor:pointer;text-decoration:underline' />");
    if(saveCanvasType==0){
        $(".l-layout-center .l-layout-header #l-layout-pic-title").html("创建组件");
        $("#afterEditBtns").show();
        $("#beforeEditBtns").hide();
        // 转为可编辑状态
        clearFlg = false;
        showModel = 1;
        // 隐藏对应窗口
        $("#initWin").hide();
        $("#cWin").show();
    }else if(saveCanvasType==1){
        $(".l-layout-center .l-layout-header #l-layout-pic-title").html("创建部件");
        document.getElementById("configCanvasType").innerHTML="<option value='1' >图形部件</option><option value='2' >表格部件</option><option value='3' >文本部件</option>";
        createCanvas(saveCanvasType);
    }else if(saveCanvasType==2){
        $(".l-layout-center .l-layout-header #l-layout-pic-title").html("电力建图");
        document.getElementById("configCanvasType").innerHTML="<option value='0' >电力总图</option>";
        createCanvas(saveCanvasType);
    }else if(saveCanvasType==3){//编辑图库
        $(".l-layout-center .l-layout-header #l-layout-pic-title").html("编辑图库");
        selectStationPicture(saveCanvasType);
    }
    if(type!=0){
        if(document.getElementById("tempSubstationName").value!=""){
            document.getElementById("selectSubstationName").value=document.getElementById("tempSubstationName").value;
        }
    }
}

// 复制
function btnCopy() {
    canvas.getActiveObject().clone(function(cloned) {
        // copy的东西放到粘贴板上
        _clipboard = cloned;
    });
}

// 粘贴
function btnPaste() {
    _clipboard.clone(function(clonedObj) {
        canvas.discardActiveObject();
        clonedObj.set({
            left: clonedObj.left + 20,
            top: clonedObj.top + 20,
            evented: true,
        });
        if (clonedObj.type === 'activeSelection') {
            clonedObj.canvas = canvas;
            clonedObj.forEachObject(function(obj) {
                //重置所有ID 参数1-自身ID也要重置
                rebuilderAllId(obj,1);
                canvas.add(obj);
            });
            clonedObj.setCoords();
        } else {
            //重置所有ID 参数1-自身ID也要重置
            rebuilderAllId(clonedObj,1);
            canvas.add(clonedObj);
        }
        _clipboard.top += 20;
        _clipboard.left += 20;
        canvas.setActiveObject(clonedObj);
        canvas.requestRenderAll();
    });
}

function btnClean(){
    var obj = canvas.getActiveObject();
    // 如果是选择的类型
    if (obj.type === 'activeSelection') {
        obj.forEachObject(function(robj) {
            canvas.remove(robj);
            delBindDataAndEvent(robj);//删除绑定的数据和事件
        });
    // 其余直接清
    }else{
        canvas.remove(obj);
        delBindDataAndEvent(obj);//删除绑定的数据和事件
    }

    canvas.discardActiveObject();
    canvas.requestRenderAll();
    updateCanvasState();// 删除单独监听
}


// 切换为画笔画图模式
function changeToDrawModel(){
    canvas.defaultCursor = 'crosshair';
    isDrawModel = true;
    // 切换画板画画的一些必要属性
    canvas.isDrawingMode =false;// 这个模式就是FALSE 是画笔轨迹的图形
    canvas.selectable = true;// 没有选中框不然丑
    canvas.skipTargetFind=true;// 画板元素不能被选中
    canvas.selection = false; //画板不显示选中

    window.canvas = canvas;
    window.zoom = window.zoom ? window.zoom : 1;
    $("#drawProperty").show();
}

//切出画笔画图模式
function changeOutDrawModel(){
    canvas.defaultCursor = 'default';
    isDrawModel = false;
    // 切换画板画画的一些必要属性
    canvas.isDrawingMode =false;// 这个模式就是FALSE 是画笔轨迹的图形
    canvas.selectable = true;// 没有选中框不然丑
    canvas.skipTargetFind=false;// 画板元素不能被选中
    canvas.selection = true; //画板不显示选中
    $("#drawProperty").hide();
    // 还原左侧为全部未选中状态
    $('#leftTable tr').each(function(i){$(this).children('td').each(function(j){$(this).css("background-color","");});});
    // 清空选中的组件内容
    menuSelectObj = null;
    isMenuSelectModel = false;
}

// 开始画图
function drawing(){

    if (drawingObject) {
        canvas.remove(drawingObject);
    }
    var dawObj = drawData.objects[0];

    // 基本组件ID即是他的名字，这个后台数据和前台对应必须要固定好不能变
    var name = dawObj['id'];
    var canvasObject = null;

    switch (name) {
    case "直线": //直线
        var newx = mouseTo.x;
        var newy = mouseTo.y;
        
        // X轴偏移量/Y轴偏移量
        var xdy = (Math.abs(mouseTo.x-mouseFrom.x))/(Math.abs(mouseTo.y-mouseFrom.y));
        if(xdy>2){
            newy = mouseFrom.y;
        }else if(xdy<=2 && xdy >= 1/2){
            // 以X为基准计算偏移量
            var xmove = Math.abs(mouseTo.x-mouseFrom.x);
            if((newy-mouseFrom.y)>0){
                newy = mouseFrom.y+xmove;
            }else{
                newy = mouseFrom.y-xmove;
            }
        }else if(xdy<0.5){
            newx = mouseFrom.x;
        }
        canvasObject = new fabric.Line([ mouseFrom.x, mouseFrom.y,newx,newy], {
            stroke : dawObj['stroke'],//
            strokeWidth : dawObj['strokeWidth']
        });
        break;
    case "线": //线
        canvasObject = new fabric.Line([ mouseFrom.x,mouseFrom.y,
                mouseTo.x, mouseTo.y], {
            stroke : dawObj['stroke'],
            strokeWidth : dawObj['strokeWidth']
        });
        break;
    case "虚线": //虚线
        canvasObject = new fabric.Line([ mouseFrom.x, mouseFrom.y,
                mouseTo.x, mouseTo.y ], {
            strokeDashArray : [ 3, 1 ],
            stroke : dawObj['stroke'],
            strokeWidth : dawObj['strokeWidth']
        });
        break;
    case "正圆": //正圆
        var left = mouseFrom.x, top = mouseFrom.y;
        var radius = Math.sqrt((mouseTo.x - left)
                * (mouseTo.x - left) + (mouseTo.y - top)
                * (mouseTo.y - top)) / 2;
        canvasObject = new fabric.Circle({
            left : left,
            top : top,
            stroke : dawObj['stroke'],
            fill : dawObj['fill'],
            radius : radius,
            strokeWidth : dawObj['strokeWidth']
        });
        break;
    case "椭圆": //椭圆
        var left = mouseFrom.x, top = mouseFrom.y;
        var radius = Math.sqrt((mouseTo.x - left)
                * (mouseTo.x - left) + (mouseTo.y - top)
                * (mouseTo.y - top)) / 2;
        canvasObject = new fabric.Ellipse({
            left : left,
            top : top,
            stroke : dawObj['stroke'],
            fill : dawObj['fill'],
            rx : Math.abs(left - mouseTo.x),
            ry : Math.abs(top - mouseTo.y),
            strokeWidth : dawObj['strokeWidth']
        });
        break;
    case "矩形": //矩形
        var path = "M " + mouseFrom.x + " " + mouseFrom.y + " L "
                + mouseTo.x + " " + mouseFrom.y + " L " + mouseTo.x
                + " " + mouseTo.y + " L " + mouseFrom.x + " "
                + mouseTo.y + " L " + mouseFrom.x + " "
                + mouseFrom.y + " z";
        var left = mouseFrom.x, top = mouseFrom.y;
        canvasObject = new fabric.Path(path, {
            left : left,
            top : top,
            stroke : dawObj['stroke'],
            strokeWidth : dawObj['strokeWidth'],
            fill : dawObj['fill']
        });
        //也可以使用fabric.Rect
        break;
    case "直角三角": //直角三角形
        var path = "M " + mouseFrom.x + " " + mouseFrom.y + " L "
                + mouseFrom.x + " " + mouseTo.y + " L " + mouseTo.x
                + " " + mouseTo.y + " z";
        canvasObject = new fabric.Path(path, {
            left : left,
            top : top,
            stroke : dawObj['stroke'],
            strokeWidth : dawObj['strokeWidth'],
            fill : dawObj['fill']
        });
        break;
    case "正三角": //正三角形
        var height = mouseTo.y - mouseFrom.y;
        canvasObject = new fabric.Triangle({
            top : mouseFrom.y,
            left : mouseFrom.x,
            width : Math.sqrt(Math.pow(height, 2)
                    + Math.pow(height / 2.0, 2)),
            height : height,
            stroke : dawObj['stroke'],
            strokeWidth : dawObj['strokeWidth'],
            fill : dawObj['fill']
        });
        break;
    }

    if (canvasObject) {
        // 给个新的ID 哪怕是每条线也都是有ID的
        canvasObject.id=getNewId();
        canvas.add(canvasObject);

        drawingObject = canvasObject;
    }
}

//坐标转换
function transformMouse(mouseX, mouseY) {
    // 计算坐标加入拖动偏移量放大系数原点坐标，勿动
    return {
        x : mouseX+(mousewheelX-mouseX)*mousewheel-movementX,
        y : mouseY+(mousewheelY-mouseY)*mousewheel-movementY
    };
}

// 颜色转换函数
function colorRGB2Hex(color) {
    var hex ="";
    if(color!=""&&color!=null){
        var rgb = color.split(',');
        var r = parseInt(rgb[0].split('(')[1]);
        var g = parseInt(rgb[1]);
        var b = parseInt(rgb[2].split(')')[0]);
        hex = "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
    }
    return hex;
}

// 撤回点
canvas.on('object:modified', function(){if(!drawingObject && (moveCount>2||moveCount==1)){updateCanvasState();}});
canvas.on('mouse:up', function(){if(isDrawModel){updateCanvasState();}});

// 更新事件
function updateCanvasState() {
    if((_config.undoStatus == false && _config.redoStatus == false)){
        var jsonData = canvas.toJSON();
        var canvasAsJson = JSON.stringify(jsonData);
        if(_config.currentStateIndex < _config.canvasState.length-1){
            var indexToBeInserted                  = _config.currentStateIndex+1;
            _config.canvasState[indexToBeInserted] = canvasAsJson;
            var numberOfElementsToRetain           = indexToBeInserted+1;
            _config.canvasState                    = _config.canvasState.splice(0,numberOfElementsToRetain);
        }else{
            _config.canvasState.push(canvasAsJson);
        }
        _config.currentStateIndex = _config.canvasState.length-1;

        // 优化点，超出最大的步数后将覆盖最前面的操作，防止前台内存溢出
        if(_config.currentStateIndex>maxBackStep){
            _config.canvasState.splice(0,1);
            _config.currentStateIndex -= 1;
        }
        if((_config.currentStateIndex == _config.canvasState.length-1) && _config.currentStateIndex != -1){
            _config.redoButton.disabled= "disabled";
            // BUG修复两个按钮不同时为disabled
            _config.undoButton.removeAttribute("disabled");
        }
    }
}

// 撤销
function undo() {
    if(_config.undoFinishedStatus){
        // 加上0，第一步不让撤回，统一为已经是最初是状态了
        if(_config.currentStateIndex == -1 || _config.currentStateIndex == 0){
            _config.undoStatus = false;
        }else{
            if (_config.canvasState.length >= 1) {
                _config.undoFinishedStatus = 0;
                  if(_config.currentStateIndex != 0){
                    _config.undoStatus = true;
                    canvas.loadFromJSON(_config.canvasState[_config.currentStateIndex-1],function(){
                            var jsonData = JSON.parse(_config.canvasState[_config.currentStateIndex-1]);
                            canvas.renderAll();
                              _config.undoStatus = false;
                              _config.currentStateIndex -= 1;
                            _config.undoButton.removeAttribute("disabled");
                            if(_config.currentStateIndex !== _config.canvasState.length-1){
                                _config.redoButton.removeAttribute('disabled');
                            }
                            _config.undoFinishedStatus = 1;
                      });
                  }else if(_config.currentStateIndex == 0){
                    canvas.clear();
                    _config.undoFinishedStatus = 1;
                    _config.undoButton.disabled= "disabled";
                    _config.redoButton.removeAttribute('disabled');
                    _config.currentStateIndex -= 1;
                  }
            }
        }
    }
}

// 恢复
function redo() {
    if(_config.redoFinishedStatus){
        if((_config.currentStateIndex == _config.canvasState.length-1) && _config.currentStateIndex != -1){
            _config.redoButton.disabled= "disabled";
        }else{
              if (_config.canvasState.length > _config.currentStateIndex && _config.canvasState.length != 0){
                _config.redoFinishedStatus = 0;
                _config.redoStatus = true;
                canvas.loadFromJSON(_config.canvasState[_config.currentStateIndex+1],function(){
                    var jsonData = JSON.parse(_config.canvasState[_config.currentStateIndex+1]);
                    canvas.renderAll();
                    _config.redoStatus = false;
                      _config.currentStateIndex += 1;
                    if(_config.currentStateIndex != -1){
                        _config.undoButton.removeAttribute('disabled');
                    }
                    _config.redoFinishedStatus = 1;
                    if((_config.currentStateIndex == _config.canvasState.length-1) && _config.currentStateIndex != -1){
                          _config.redoButton.disabled= "disabled";
                    }
                  });
            }
        }
    }
}
// 更改线宽
function strokeWidthChange(){
   if(drawData.objects[0]){
       // 文本框区别对待
       if(drawData.objects[0]['type']=='textbox'){
           drawData.objects[0]['fontSize'] = Number($("#strokeWidthSelect").val());
       }else{
           drawData.objects[0]['strokeWidth'] = Number($("#strokeWidthSelect").val());
       }
   }
}

//获取颜色
function colorConfirm(colorData){
    var colorNow;
    if(colorData){
        colorNow=colorData;
    }else{
        colorNow=$('.sp-active .sp-preview .sp-preview-inner').css('background-color');
    }
    //左侧树的颜色块
    if(colorNum==1){
        if(drawData.objects[0]){
            // 文本框区别对待
            if(drawData.objects[0]['type']=='textbox'){
                drawData.objects[0]['fill'] = colorNow;
            }else{
                drawData.objects[0]['stroke'] = colorNow;
            }
        }
    //组件线条颜色
    }else if(colorNum==2){
        $("#edit_stroke").val(colorNow);
    //组件填充颜色
    }else if(colorNum==3){
        $("#edit_fill").val(colorNow);
    //字体颜色
    }else if(colorNum==4){
        $("#editText_fill").val(colorNow);
    //背景颜色
    }else if(colorNum==5){
        $("#hb_bjys").val(colorNow);
    //初期化画布颜色
    }else if(colorNum==6){
        $("#configBackground").val(colorNow);
    }else if(colorNum==7){
        $("#stateEditStroke").val(colorNow);
    }else if(colorNum==8){
        $("#stateEditFill").val(colorNow);
    }
}

// 转百分比显示
function toPercent(point){
    var str=Number(point*100).toFixed(0);
    str+="%";
    return str;
}

//创建初始化
function createCanvas(type){
    document.getElementById('configBackground').value="rgb(255, 255, 255)";// 白底
    // type为1
    if(type == 1){
        $("#departSize").show();
    }else{
        $("#departSize").hide();
    }
    $.ligerDialog.open({
        target:$("#canvasConfig"),
        height: 300,
        width:500,
        title: "初期选择",
        isHidden: true,
        isResize:true ,
        allowClose:false,
        buttons: [
              { text: '确定', onclick: function (item, dialog) { 
                  if (document.getElementById('configSubstationName').value=="") {
                      $.ligerDialog.warn("请先选择变电站！","提示");
                      return;
                  };
                  if (document.getElementById('configCanvasName').value=="") {
                      $.ligerDialog.warn("请输入制图名称！","提示");
                      return;
                  };
                  $("#afterEditBtns").show();
                  $("#beforeEditBtns").hide();
                  $("#departSize_width1").val($("#departSize_width").val());
                  $("#departSize_height1").val($("#departSize_height").val());
                  $("#hidden_canvas_type").val($("#configCanvasType").val());
                  canvas.setBackgroundColor(document.getElementById('configBackground').value);
                  //设置查询变电所名称（绑定变量时用）
                  $("#search_substationName").val($("#tempSubstationName").val());
                  $("#event_search_substationName").val($("#tempSubstationName").val());
                  $("#search_data_substationSn").val($("#tempSubstationSn").val());
                  $("#search_event_substationSn").val($("#tempSubstationSn").val());
                  //设置图库类型
                  createType = type;//全局变量，用来判断是否屏蔽右键的部件绑定功能（如果等于0和1则屏蔽，2和3不屏蔽）
                  //渲染页面
                  canvas.renderAll();
                  // 转为可编辑状态
                  clearFlg = false;
                  showModel = 1;
                  // 隐藏对应窗口
                  $("#initWin").hide();
                  $("#cWin").show();
                  dialog.hide();
                  // 双清
                  eventBindList.length = 0;
                  dataBindList.length =0;
              }},
              { text: '取消', onclick: function (item, dialog) { dialog.hide(); } }
          ]
    });
}

//选择变电所
function selectStation(type){
    // 转向网页的地址;
    var url='selectStationData';
    $.ligerDialog.open({
        //右上角的X隐藏，否则会有bug
        allowClose: false,
        height: 500,
        url: url,
        width: 1000,
        name:'choseKey',
        title:'变电站选择', 
        isResize:true,
        buttons: [  {  text: '确认', onclick: function (item, dialog){
                    var arrM = new Array();
                    //获取子页面
                    arrM=document.getElementById('choseKey').contentWindow;
                    var contentgrid = arrM.$("#grid").ligerGrid();
                    var contentrows = contentgrid.getSelectedRows();
                    if (contentrows == null || contentrows.length!=1) {
                        $.ligerDialog.warn("请选择一条记录！","提示");
                        return;
                    };
                    $("#tempSubstationSn").val(contentrows[0].substationSn);
                    $("#tempSubstationName").val(contentrows[0].substationName);;
                    $("#tempShortname").val(contentrows[0].shortname);
                    $("#configSubstationName").val(contentrows[0].substationName);
                    $("#selectSubstationName").val(contentrows[0].substationName);
                    if(contentrows[0].resource_type == '0'){
                        $.ligerDialog.warn("请选择具体变电所！", "提示");
                        return;
                    }
                    dialog.close();
                    if(type==3){
                        initMenuCreate(3); 
                    }
                    }
                },
                    { text: '关闭', onclick: function (item, dialog){dialog.close();}}
                ]
    }); 
}
//对后台传来的二进制数据进行解压
function unzip(key) {
    var strData = atob(key);
	// Convert binary string to character-number array
	var charData = strData.split('').map(function(x){return x.charCodeAt(0);});
	// Turn number array into byte-array
	var binData = new Uint8Array(charData);
	// // unzip
	var data = pako.inflate(binData);
	// Convert gunzipped byteArray back to ascii string:
	//uint8array有缓冲区大小的限制。它将崩溃在大缓冲区，粗鲁的最大大小是246300，有时它在不同的浏览器不正确
	//strData = String.fromCharCode.apply(null, Uint8Array(data));//对传来的数组对象整体一次性解析
	var strData = handleCodePoints(data);//解决Maximum call stack size exceeded问题
	return decodeURIComponent(escape(strData));//解决中文乱码问题decodeURIComponent(escape());
}
//将所有数据进行循环分块解析，不再像String.fromCharCode.apply一次性解析，避免了数据量过大造成栈溢出
function handleCodePoints(array) {
    var CHUNK_SIZE = 0x8000; // arbitrary number here, not too small, not too big（不能太小，也不能太大）
    var index = 0;
    var length = array.length;
    var result = '';
    var slice;
    var arr = [];
    for (var i = 0, _i = array.length; i < _i; i++) {
        arr[i] = array[i];
    }
    while (index < length) {
        slice = arr.slice(index, Math.min(index + CHUNK_SIZE, length)); // `Math.min` is not really necessary here I think
        result += String.fromCharCode.apply(null, slice);
        index += CHUNK_SIZE;
    }
    return result;
}
//变电所部件合并
function selectStationPicture(type){
    
    // 转向网页的地址;
    var url='selectStationPicture';
    $.ligerDialog.open({
        //右上角的X隐藏，否则会有bug
        allowClose: false,
        height: 500,  
        url: url,  
        width: 1000,  
        name:'choseKey',  
        title:'选择部件', 
        data: {
            substationSn: $("#openSelectPicture").val()
        },
        isResize:true,  
        buttons: [  {  text: '开始编辑', onclick: function (item, dialog){
                        var arrM = new Array();
                        //获取子页面
                        arrM=document.getElementById('choseKey').contentWindow;
                        var contentgrid = arrM.$("#grid").ligerGrid();
                        var contentrows = contentgrid.getSelectedRows();
                        if (contentrows == null || contentrows.length!=1) {
                            $.ligerDialog.warn("请选择一条记录！","提示");
                            return;
                        };
                        $.ligerDialog.waitting('图库加载中,请稍候...'); 
                        setTimeout(function () {
                          	//开始时间
                            var start = new Date();
                            getBindData(contentrows[0].id);//获取绑定的数据
                            getEventData(contentrows[0].id);//获取绑定的事件
                            $("#saveForm2")[0].reset();
                            $("#openSelectPicture").val(contentrows[0].substationSn);
                            /*$("#tempSubstationSn").val(contentrows[0].substationSn);
                            $("#tempSubstationName").val(contentrows[0].substationName);
                            $("#configSubstationName").val(contentrows[0].substationName);
                            $("#selectSubstationName").val(contentrows[0].substationName);*/
                            //设置查询变电所名称（绑定变量和事件时，作为检索条件用，只搜索当前变电所下的所有变量和所有部件绑定）
                            $("#search_substationName").val(contentrows[0].substationName);
                            $("#search_data_substationSn").val(contentrows[0].substationSn);
                            $("#search_event_substationSn").val(contentrows[0].substationSn);
                            $("#event_search_substationName").val(contentrows[0].substationName);
                            //打开画面
                            $("#configCanvasName").val(contentrows[0].canvasName);
                            $("#hidden_canvas_type").val(contentrows[0].canvasType);
                            $("#afterEditBtns").show();
                            $("#beforeEditBtns").hide();
                            canvas.setBackgroundColor(contentrows[0].canvasBackgroundColor);
                            canvas.renderAll();
                              //设置图库类型
                              if(contentrows[0].canvasType==0){//全局变量，用来判断是否屏蔽右键的部件绑定功能（如果createType等于1则屏蔽，2和3不屏蔽;canvasType:0总图，1图形部件，2表格部件，3文本部件）
                                  createType = 2;
                              }else{//为部件的话
                                  createType = 1;
                              }
                            // 转为可编辑状态
                            clearFlg = false;
                            showModel = 1;
                            // 隐藏对应窗口
                            $("#initWin").hide();
                            $("#cWin").show();
                            $.ajax({
                                type: 'get',
                                url: "../pictureStation/getDetail",
                                data: { id: contentrows[0].id },
                                async:false,
                                cache: false,
                                dataType: 'json',
                                success: function (data) {
                                    /*解压获取画图
                                  	*/
                                    var stationMemory = unzip(data.retObj[0]['stationMemoryBlob']);
                                  	//载入画图
                                    loadObj = canvas.loadFromJSON(stationMemory);
                                    //解压
                                  	
                                  	/*直接获取画图
                                    //载入画图
                                    loadObj = canvas.loadFromJSON(data.retObj[0]['stationMemory']);
                                  	*/
                                  	
                                    //记录初始记录点
                                    updateCanvasState();
                                  	
                                },
                                error : function() {
                                     $.ligerDialog.warn("查询失败！","错误");
                                     loadObj = "";
                                }
                            });
                            //loadObj = canvas.loadFromJSON(contentrows[0].stationMemory);
                            $("#configId").val(contentrows[0].id);
                            dialog.close();
                            $.ligerDialog.closeWaitting(); 
                         	//结束时间
                            var end = new Date();
                         	//打印时间
                     		console.log("【加载绑定数据、获取blob、解压、canvas加载】共计耗时："+(end-start)+"毫秒！");
                        }, 100);
                    }
                },
                    { text: '关闭', onclick: function (item, dialog){dialog.close();}}
                ]
    }); 
  	
  	
}
//获取数据绑定数据
function getBindData(id){
  $.ajax({
      type: 'get',
      url: "getDataBind",
      data: { psId: id },
      async:false, 
      cache: false,
      dataType: 'json',
      success: function (data) {
          dataBindList.length = 0;
          dataBindList = data.retObj;
      }
  });
  
}

//获取事件绑定数据
function getEventData(id){
  $.ajax({
      type: 'get',
      url: "getEventBind",
      data: { psId: id },
      async:false, 
      cache: false,
      dataType: 'json',
      success: function (data) {
          // 优先清空
          eventBindList.length = 0;

          var list=data.retObj;
          //重新改造eventBindList
          for(var i = 0;i<list.length;i++){
              // 创建绑定关系
              var eb = {
                  substationSn:list[i].substationSn,
                  objId:list[i].objId,
                  eventType:list[i].eventType,
                  showName:list[i].eventType==1?list[i].canvasName:list[i].varName,
                  eventId:list[i].eventId
              };
              //向数组中添加新事件绑定
              eventBindList.push(eb);
          }
      }
  });
}

//画布预览
var preview = 1;//tab新增id 
function openNewTap(id,title){
    if(!title){
        title = document.getElementById('configCanvasName').value;
    }
    parent.parent.openTab({
        tabid: 'preview'+id,
        text: '预览'+title,
        url: "/hmdl/pictureStation/preview?parentId=301&id="+id
    });
}

// 重置OBJ所有ID(递归方法group中还有group的情况下)
// type0:自身ID不重置   1:自身ID也重置
function rebuilderAllId(loadFrObj,type){
    if(type==1){
        loadFrObj.id= getNewId();
    }
    if(loadFrObj.type=="group"){
        loadFrObj.getObjects().forEach(function(loadNextFrObj) {
            if(loadNextFrObj.type=="group"){
                // 递归调用 直到所有的obj都有自己的唯一的ID
                rebuilderAllId(loadNextFrObj,1);
            }else{
                loadNextFrObj.id = getNewId();
            }
        });
    }
}

// 加载绑定信息
function bulidBindMsg(bindObj){
    // 清空所有的显示信息
    clearBindMsg();
    // 定义按下选中id
    var id = bindObj.id;

    // 数据绑定显示信息
    dataBindList.forEach(function (item,index,input) {
        if(id==input[index].objId){
            isSelectDataBind = true;
            if(input[index].dataType=='01' || input[index].dataType=='02' ){
                isSelectDataSwith = true;
            }else{
                isSelectDataSwith = false;
            }
            $("#dbMsg").html("<font style='color:green;font-weight:800'>数据绑定【"+input[index].varName+"】</font>");
            return;//跳出循环
        }
    });

    //事件绑定显示信息
    eventBindList.forEach(function (item,index,input) {
        if(id==input[index].objId){
            isSelectEventBind = true;
            var eventType = input[index].eventType==1?"弹出部件【":"分析数据【";
            $("#ebMsg").html("<font style='color:blue;font-weight:800'>"+eventType+"&nbsp;"+input[index].showName+"】</font>");
            return;//跳出循环
        }
    });
}

// 清空绑定信息
function clearBindMsg(){
    // 先清空
    $("#dbMsg").html("");
    $("#ebMsg").html("");
    isSelectDataBind = false;
    isSelectEventBind = false
}

// 01-开关显示配置
var fun01EditType;

// 列表中点击配置
function dbShowListClick(objId,type){
    var dbShowObj;
    canvas.getObjects().forEach(function (obj) {
        if(obj.id == objId){
            dbShowObj = obj;
        }
    });
    if(dbShowObj){
        if(type=='0' || type=='1'){
            fun01Edit(dbShowObj,type);
        }else{
            // 获取这个对象对应的绑定数据中小数点位数
            var showPoint = 2;
            var showIndex;
            dataBindList.forEach(function (item,index,input) {
                if(input[index].objId==dbShowObj.id){
                    showPoint = input[index].showPoint;
                    showIndex = index;
                }
            });
            $.ligerDialog.prompt('显示小数点位数',showPoint+'', function (yes,value) {
                if(yes){
                    dataBindList[showIndex].showPoint = value;
                } 
            });
        }
    }
}

function  fun01Edit(stateObj,type){
    fun01EditType = type;
    // 先清空
    $("#stateEditFromPoint").val("");
    $("#stateEditStroke").val("");
    $("#stateEditFill").val("");
    $("#stateEditToPoint").val("");
    $("#stateEditText").val("");
    $("#colorPicker7").spectrum("set", "#ffffff");
    $("#colorPicker8").spectrum("set", "#ffffff");
    // 显示弹出窗口
    initStateEditWin();
    if(type=="0"){
        stateEditWin.set("title","开关-分闸0配置");
    }else if(type =="1"){
        stateEditWin.set("title","开关-合闸1配置");
    }
    // 加载开关效果预览
    canvasp.clear().renderAll();
    var stateJson ='{"version":"2.3.5","objects":[' +JSON.stringify(stateObj)+'],"background":"rgb(255, 255, 255)"}';
    // 加载数据
    canvasp.loadFromJSON(stateJson);
    var previewObj = canvasp.getObjects()[0]; 
    previewObj.center();
    previewObj.setCoords();
    canvasp.renderAll();
    // 加载原始角度
    $("#stateEditFromPoint").val(stateObj.angle);
    // 获取已经绑定的数据
    var stateData;
    // 从绑定数据列表中取得相关数据
    dataBindList.forEach(function (item,index,input) {
        var bindData = input[index];
        //删除数组中指定的数据
        if(bindData.objId==stateObj.id){
            if(type=="0"){
                stateData = bindData.stateClose;
            }else if(type =="1"){
                stateData = bindData.stateOpen;
            }
        }
    });
    // 取得数据，默认都没有值不显示信息
    if(stateData){
        // 加载数据
        var stateJsonObj = JSON.parse(stateData);
        if(stateJsonObj.stroke){
            $("#stateEditStroke").val(stateJsonObj.stroke);
            $("#colorPicker7").spectrum("set", stateJsonObj.stroke);
        }
        if(stateJsonObj.fill){
            $("#stateEditFill").val(stateJsonObj.fill);
            $("#colorPicker8").spectrum("set", stateJsonObj.fill);
        }
        if(stateJsonObj.angle || stateJsonObj.angle==0){
            $("#stateEditToPoint").val(stateJsonObj.angle);
        }
        if(stateJsonObj.text){
            $("#stateEditText").val(stateJsonObj.text);
        }
        // 赋值完毕 预览
        stateEditPreview();
    }
}

function initStateEditWin(){
    // 初始化编辑窗口
    if(!stateEditWin){
        //打开弹出窗口
        stateEditWin = $.ligerDialog.open({
            target:$("#stateEditWin"),
            height: 500,
            width:500,
            isHidden:true,
            allowClose:false,
            isResize:true,
            showMax:false,
            showMin:false,
            buttons: [{ text: '提交', onclick: function (item, dialog) {stateEditWinCommit();}},
                      { text: '关闭', onclick: function (item, dialog) {dialog.hide();}}]
        });
    }else{
        stateEditWin.show();
    }
}

// 提交编辑的内容
function stateEditWinCommit(){
    // 开关效果
    var stateStr = "";
    // 画面上的角度颜色信息
    var toPoint = $("#stateEditToPoint").val();
    var stroke = $("#stateEditStroke").val();
    var fill = $("#stateEditFill").val();
    var text = $("#stateEditText").val();
    if(stroke){stateStr+= '"stroke":"'+stroke+'",';}
    if(fill){stateStr+= '"fill":"'+fill+'",';}
    if(text){stateStr+= '"text":"'+text+'",';}
    if(toPoint){stateStr+= '"angle":'+toPoint;}
    
    // 拼接串
    if(stateStr){
        if(stateStr.substr(stateStr.length-1,1) ==","){
            stateStr = stateStr.substr(0,stateStr.length-1);
        }
        stateStr ='{'+stateStr+'}'; 
    }
    var stateObj = canvas.getActiveObject();
    dataBindList.forEach(function (item,index,input) {
        //重置动态效果
        if(input[index].objId==stateObj.id){
            if(fun01EditType=='0'){
                input[index].stateClose = stateStr;
            }else if(fun01EditType=='1'){
                input[index].stateOpen = stateStr;
            }
        }
    });
    stateEditWin.hide();
}

// 预览开关效果
function stateEditPreview(){
    // 画面上的角度颜色信息
    var toPoint = $("#stateEditToPoint").val();
    var stroke = $("#stateEditStroke").val();
    var fill = $("#stateEditFill").val();
    var text = $("#stateEditText").val();
    var previewObj = canvasp.getObjects()[0];
    if(previewObj.type=="group"){
        var isSpecFlg = isSpecicalObjSwith(previewObj);
        // 如果是非特殊的 直接整体旋转
        if(!isSpecFlg && toPoint){previewObj.rotate(Number($("#stateEditToPoint").val()));}
        previewObj.getObjects().forEach(function(gobj) {
            // 只旋转线
            if(isSpecFlg && gobj.type=='line' && toPoint){gobj.set({angle:Number($("#stateEditToPoint").val())});}
            if(stroke && gobj.type!='textbox'){gobj.set('stroke', $("#stateEditStroke").val());}// 线条颜色
            if(fill && gobj.type!='textbox' ){gobj.set('fill', $("#stateEditFill").val());}// 填充色
            if(text && gobj.type=='textbox' ){gobj.set('text', $("#stateEditText").val());}// 文本
        });
    }else{
        if(toPoint){previewObj.rotate(Number($("#stateEditToPoint").val()));}
        if(stroke){previewObj.set('stroke', $("#stateEditStroke").val());}
        if(fill){previewObj.set('fill', $("#stateEditFill").val());}
        if(text && previewObj.type=='textbox'){previewObj.set('text', $("#stateEditText").val());}
    }

    //刷新画面
    previewObj.setCoords();
    canvasp.requestRenderAll();
}

// 判断是否特殊开关 两圆一线
function isSpecicalObjSwith(swObj){

     var xcount = 0;// 线的数量
     var ycount = 0;// 圆的数量
     var tcount = 0 ;// 总数量
     if(swObj.type=="group"){
         swObj.getObjects().forEach(function(gobj) {
             tcount++;
             if(gobj.type=='line'){
                 xcount++;
             }else if(gobj.type=='circle'){
                 ycount++;
             }
         });
         if(xcount==1 && ycount==2 && tcount==3){
             return true;
         }else{
             return false;
         }
     }else{
         return false;
     }
}

//测试按钮
function test(){
    // 重置所有的ID
//  var objAll = canvas.getObjects(); 
//  objAll.forEach(function (item,index,input) {
//      rebuilderAllId(input[index],1);
//  });
//dataBindList = new Array();//数据绑定缓存
//eventBindList = new Array();//数据绑定缓存
	
	
}
//解决模糊问题
function toBetterClean(){
  	//所有text文本坐标取整
    var objAll = canvas.getObjects();
    objAll.forEach(function (item,index,input) {
        input[index].objectCaching = false;
    });
    canvas.renderAll();
}
//数据绑定弹窗检索条件切换
function searchItem(){
    var value = $("#search_item").val();
    if(value==1){
        $("#search_dbDialog_name").css("display","block");
        $("#search_remark").css("display","none");
    }else{
        $("#search_dbDialog_name").css("display","none");
        $("#search_remark").css("display","block");
    }
}
//按下esc键关闭弹窗
$(function(){
    $(document).keyup(function(e){
        if (e.keyCode == 27){
            if(ebDialog){
                ebDialog.hide();
            }
            if(dbDialog){
                dbDialog.hide(); 
            }
        }
    });
})
//select快速搜索
function searchQuick(){
    var parms = serializeObject($("#dbDialogForm"));
	for(var p in parms){
	    dbDialogGrid.setParm(p,parms[p].trim());
	}
	dbDialogGrid.loadData();
}
//回车键搜索
$("#search_dbDialog_name,#search_remark").keydown(function (e) {//当按下按键时
    if (e.which == 13) {//.which属性判断按下的是哪个键，回车键的键位序号为13
        searchQuick();
    }
});
//回车键搜索
$("#search_ebDialog_name").keydown(function (e) {//当按下按键时
    if (e.which == 13) {//.which属性判断按下的是哪个键，回车键的键位序号为13
		// 1 弹出部件
        if($("#search_event_type").val()==1){
            popUpDialogGrid.setParm("canvasName",$("#search_ebDialog_name").val().trim());
            popUpDialogGrid.loadData();
        }else{
            analysisDialogGrid.setParm("varName",$("#search_ebDialog_name").val().trim());
            analysisDialogGrid.loadData();
        }
    }
});
//相上对齐
function aligntop(){
    //获取当前选中的obj
    var objects = canvas.getActiveObjects();
    if(objects.length > 1 ){
        //获得第一个矩形的四个属性值
        var rect = objects[0].getBoundingRect();
        //获得第一个矩形的左偏移
        var mintop = rect.top;
        //遍历获得最小的左偏移
        for(var i = 1; i<objects.length; i++){
          rect = objects[i].getBoundingRect();
          mintop = Math.min(mintop, rect.top);
        }
        //遍历将所有选中的对象设置最小左偏移
        for(var i = 0; i<objects.length; i++){
            objects[i].set('top', mintop);// Y轴坐标
        }
        //刷新画布
        canvas.renderAll();
    }    
}
//相下对齐
function aligndown(){
    //获取当前选中的obj
    var objects = canvas.getActiveObjects();
    if(objects.length > 1 ){
        //获得第一个矩形的四个属性值
        var rect = objects[0].getBoundingRect();
        //获得第一个矩形的上偏移+高度
        var maxtop = rect.top+rect.height;
        //遍历获得最大的上偏移
        for(var i = 1; i<objects.length; i++){
          rect = objects[i].getBoundingRect();
          maxtop = Math.max(maxtop, rect.top+rect.height);
        }
        //遍历将所有选中的对象设置最大上偏移
        for(var i = 0; i<objects.length; i++){
            objects[i].set('top', maxtop-objects[i].getBoundingRect().height);// Y轴坐标
        }
        //刷新画布
        canvas.renderAll();
    }    
}
//相左对齐
function alignleft(){
    //获取当前选中的obj
    var objects = canvas.getActiveObjects();
    if(objects.length > 1 ){
        //获得第一个矩形的四个属性值
        var rect = objects[0].getBoundingRect();
        //获得第一个矩形的左偏移
        var minleft = rect.left;
        //遍历获得最小的左偏移
        for(var i = 1; i<objects.length; i++){
          rect = objects[i].getBoundingRect();
          minleft = Math.min(minleft, rect.left);
//           minY= Math.min(minY, rect.top);
//           maxX = Math.max(maxX, rect.left + rect.width);
//           maxY= Math.max(maxY, rect.top + rect.height);
        }
        //遍历将所有选中的对象设置最小左偏移
        for(var i = 0; i<objects.length; i++){
            objects[i].set('left', minleft);// X轴坐标
        }
        //刷新画布
        canvas.renderAll();
    }
}
//相右对齐
function alignright(){
    //获取当前选中的obj
    var objects = canvas.getActiveObjects();
    if(objects.length > 1 ){
        //获得第一个矩形的四个属性值
        var rect = objects[0].getBoundingRect();
        //获得第一个矩形的左偏移
        var maxleft = rect.left+rect.width;
        //遍历获得最大的左偏移
        for(var i = 1; i<objects.length; i++){
          rect = objects[i].getBoundingRect();
          maxleft = Math.max(maxleft, rect.left+rect.width);
        }
        //遍历将所有选中的对象设置最大左偏移
        for(var i = 0; i<objects.length; i++){
            objects[i].set('left', maxleft-objects[i].getBoundingRect().width);// X轴坐标
        }
        //刷新画布
        canvas.renderAll();
    }    
}
//水平分散
function divertX(){
//   	//获取当前选中的obj
//     var objects = canvas.getActiveObjects();
//     if(objects.length > 1 ){
//         //获得第一个矩形的四个属性值
//         var rect = objects[0].getBoundingRect();
//         //获得第一个矩形的左偏移
//         var maxleft = rect.left;
//         //遍历获得最大的左偏移
//         for(var i = 1; i<objects.length; i++){
//           rect = objects[i].getBoundingRect();
//           maxleft = Math.max(maxleft, rect.left);
//         }
//         //遍历将所有选中的对象设置最大左偏移
//         for(var i = 0; i<objects.length; i++){
//             objects[i].set('left', maxleft);// X轴坐标
//         }
//         //刷新画布
//         canvas.renderAll();
//     }
}
//垂直分散
function divertY(){
    
}

</script>
</body>
</html>