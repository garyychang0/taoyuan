<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/draw/jquery.mousewheel.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/fabricnew.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/pako.min.js" type="text/javascript"></script>
    <style>
	.fullScreen{
	    margin:auto auto;
	}
	.l-dialog-content{
	    overflow: hidden;
	    margin:0px;
	    padding:0px;
	}
	.l-dialog-content-noimage{
	    margin:0px;
	    padding:0px;
	}
	.l-dialog-tc {
	    background: -webkit-gradient(linear, 0 0, 0 100%, from(#d1ecf3), to(#4A8BC2));
	}
	.l-dialog-buttons{
	    background: #d2e3ec;
	    margin:0px;
	}
	.upper-canvas{
	    width:100%;
	    height:100%;
	    padding:0;
	    margin:0px;
	}
	.canvas-container{
	    width:100%;
	    height:100%;
	    padding:0;
	    margin:0px;
	}
	.l-dialog-close{
	background: url(/hmdl/static/images/draw/olympic_games10.png) no-repeat;
	height: 14px;
	width:14px;
	background-size:100% 100%;  
	}
	</style>
</head>

<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;background-color:#F9F9F9;height:100%;position:fixed">
<input type="hidden" id="substationSn" value="${content[0].substationSn}"/><!-- 隐藏域传值 -->
<div id="rightWin" position="center"  title="变电所" >
    <div id="hmdl_title_top">
    </div>
    <!-- 画板 -->
    <div id="cWin">
         <div id="canvas_bottom" style="position:fixed;top:10px;z-index:2">
<!--                <button style="margin-left: 10px" onclick="refesh()" id="" type="button" class="btn btn btn-primary btn-small btn-mini"><i class="fa fa-arrows-alt"></i>&nbsp;&nbsp;全屏&nbsp;&nbsp;</button> -->
               <button style="margin-left: 10px" onclick="refesh()" id="" type="button" class="btn btn-default btn-small btn-mini"><i class="fa fa-undo"></i>&nbsp;&nbsp;刷新&nbsp;&nbsp;</button>
           </div>
        <!-- 包涵画板的DIV显示 -->
        <!-- 画板信息 -->
        <div id="canvas_border" style="z-index:1">
            <canvas id="c" width='1980' height='1020' style=""></canvas>
        </div>
    </div>
</div>
<!-- 弹出新画布窗口 -->
<div id="popUpCanvasDialog" style="margin0:0px;margin0:0px;overflow: hidden;padding:0px;width: 100%;height:100%;">
    <canvas id="newCanvas" width="708px" height="429px"></canvas>
</div>

<script type="text/javascript">
var u = navigator.userAgent, app = navigator.appVersion; 
var is_height = 600;
var is_width = 800;
var is_p_height = 600;
var is_p_width = 800;
var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Linux') > -1; //android终端或者uc浏览器 
var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端 
if(isAndroid){
	is_height = window.screen.height;
	is_width = window.screen.width;
	is_p_width = document.body.scrollWidth;
	is_p_height = document.body.scrollHeight+20;
}
if(isiOS){
	is_height = window.screen.width;
	is_width = window.screen.height;
	is_p_height = is_height;
	is_p_width = is_width;
}
var canvas = new fabric.Canvas('c');// 新建画板
var newCanvas = new fabric.Canvas('newCanvas');//新画布
var lastPoint;
var tem =0;//用于判断是否成功从数据库获取了数据绑定和事件绑定
var warningArray = new Array();//格式[{objId:objId,eventId:eventId,stationMemory:stationMemory},...]

var isPopup = false;//是否打出弹出窗口
//是否拖动(画布移动功能)
var panning = false;
//鼠标按下
canvas.on('mouse:down', function (e) {
    panning = true;
    canvas.selection = true;
});

//鼠标按下
newCanvas.on('mouse:down', function (e) {
    panning = true;
    newCanvas.selection = false;
});

//鼠标抬起
canvas.on('mouse:up', function (e) {
    panning = false;
    canvas.selection = true;
});

//鼠标抬起
newCanvas.on('mouse:up', function (e) {
    panning = false;
    newCanvas.selection = true;
});
//监听主图
var startX = startX1 = startY1 = startY = endX = endX1= endY = endY1 = fingers= 0 ;
var canvasBox = document.querySelector('#canvas_border');
canvasBox.addEventListener('touchstart',function (e) {
    panning = true;
    canvas.selection = true;
    fingers = e.touches.length;
    if(fingers==1){
	    startX = e.touches[0].pageX;
	    startY = e.touches[0].pageY;
    }else if(fingers==2){
	    startX = e.touches[0].pageX;
	    startY = e.touches[0].pageY;
	    startX1 = e.touches[1].pageX;
	    startY1 = e.touches[1].pageY;
	}
});
canvasBox.addEventListener('touchmove',function (e) {
    fingers = e.touches.length;
    endX = e.touches[0].pageX;
    endY = e.touches[0].pageY;
    if(fingers==1){
	    var x =(endX-startX)/20;
	    var y =(endY-startY)/20;
	    if (panning) {
	    	var delta = new fabric.Point(x,y);
			canvas.relativePan(delta);
	    }
	}else if(fingers==2){
	    endX1 = e.touches[1].pageX;
	    endY1 = e.touches[1].pageY;
	    if(!isPopup){
	         var zoom =  canvas.getZoom();
	         zoom = Math.max(0.3,zoom); //最小为原来的1/10
	         zoom = Math.min(3,zoom); //最大是原来的3倍
	         var zoomPoint = new fabric.Point(canvas.width * 0.5 , canvas.height * 0.5);
	         if(Math.abs(startX1-startX)-Math.abs(endX1-endX)>0){
		         zoom = zoom-0.02; 
	         }else{
		         zoom = zoom+0.02; 
	         }
	         canvas.zoomToPoint(zoomPoint, zoom);
	     }
	}
    
});
canvasBox.addEventListener('touchend',function (e) {
    panning = false;
    canvas.selection = true;
});
//监听弹出画布
var p_startX = p_startX1 = p_startY = p_startY1 = p_endX = p_endX1 = p_endY = p_endY1 = p_fingers= 0 ;
var p_canvasBox = document.querySelector('#popUpCanvasDialog');
p_canvasBox.addEventListener('touchstart',function (e) {
    panning = true;
    newCanvas.selection = true;
    p_fingers = e.touches.length;
    if(p_fingers==1){
        p_startX = e.touches[0].pageX;
        p_startY = e.touches[0].pageY;
    }else if(p_fingers==2){
        p_startX = e.touches[0].pageX;
        p_startY = e.touches[0].pageY;
        p_startX1 = e.touches[1].pageX;
        p_startY1 = e.touches[1].pageY;
	}
});
p_canvasBox.addEventListener('touchmove',function (e) {
    p_fingers = e.touches.length;
    p_endX = e.touches[0].pageX;
    p_endY = e.touches[0].pageY;
    if(p_fingers==1){
	    var p_x =(p_endX-p_startX)/20;
	    var p_y =(p_endY-p_startY)/20;
	    if (panning) {
	    	var p_delta = new fabric.Point(p_x,p_y);
	   		newCanvas.relativePan(p_delta);
	    }
	}else if(p_fingers==2){
		p_endX1 = e.touches[1].pageX;
		p_endY1 = e.touches[1].pageY;
	    if(isPopup){
	         var zoom =  newCanvas.getZoom();
	         zoom = Math.max(0.3,zoom); //最小为原来的1/10
	         zoom = Math.min(3,zoom); //最大是原来的3倍
	         var zoomPoint = new fabric.Point(newCanvas.width * 0.5 , newCanvas.height * 0.5);
	         if(Math.abs(p_startX1-p_startX)-Math.abs(p_endX1-p_endX)>0){
		         zoom = zoom-0.05; 
	         }else{
		         zoom = zoom+0.05; 
	         }
	   		 newCanvas.zoomToPoint(zoomPoint, zoom);
	     }
	}
    
});
p_canvasBox.addEventListener('touchend',function (e) {
    panning = false;
    newCanvas.selection = true;
});
var dataBindList = new Array();//数据绑定缓存
var eventBindList = new Array();//事件绑定缓存
var popUpDataBindList = new Array();//弹出画布中的绑定的变量数据
var popUpEventBindList = new Array();//弹出画布中的绑定的事件绑定-->变量分析一览

var popUpCanvasDialog;//弹出新画布窗口

var textData = new Array();//导入数据缓存(底层画布)
var popUpTextData = new Array();//导入数据缓存（弹出画布）

var url = '${url}';
var ws = null;
var lockReconnect = false;//避免重复连接

//监听键盘事件
document.onkeyup = function(event){
    var event = event || window.event;
    // ESC退出popup防止图片过大关不了了
    if(event.keyCode==27){
        popUpDataBindList.length = 0;//清空 
        popUpTextData.length = 0;//清空 
        if(popUpCanvasDialog){popUpCanvasDialog.hide();}
        isPopup = false;
    }
}

//生成页面开关的绑定数据---->目的是存放一个包含有obj对象及其所绑定的数据的数组（底层画布）
function getData() {
    var objs = canvas.getObjects();
    //遍历查询绑定的数据
    if (dataBindList.length > 0) {
        for (var i = 0; i < dataBindList.length; i++) {
            var objId = dataBindList[i].objId; //获取当前绑定数据的文本的id
            for (var k = 0; k < objs.length; k++) { //遍历查询
                if (objs[k].id == objId) { //找到该obj（所有）
                    var td = {
                        obj: objs[k],//画布上是的对象
                        dataType:dataBindList[i].dataType,//数据类型
                        value:dataBindList[i].bindColumns,//传输来的数据的numberName
                        showPoint:dataBindList[i].showPoint,//文本保留几位小数
                        stateOpen:dataBindList[i].stateOpen,//开关闭合1
                        stateClose:dataBindList[i].stateClose//开关分开0
                    }
                    textData.push(td);//存入数组textData中
                    break;//跳出循环
                }
            }
        }
    }
}
//生成弹窗页面开关的绑定数据---->目的是存放一个包含有obj对象及其所绑定的数据的数组（弹出画布）
function getPopUpData(popUpData) {
    var objs = newCanvas.getObjects();
    
    //遍历查询绑定的数据
    if (popUpData.length > 0) {
        for (var i = 0; i < popUpData.length; i++) {
            var objId = popUpData[i].objId; //获取当前绑定数据的文本的id
            for (var k = 0; k < objs.length; k++) { //遍历查询
                if (objs[k].id == objId) { //找到该obj（所有）
                    var td = {
                        obj: objs[k],//画布上是的对象
                        dataType:popUpData[i].dataType,//数据类型
                        value:popUpData[i].bindColumns,//传输来的数据的numberName
                        showPoint:popUpData[i].showPoint,//文本保留几位小数
                        stateOpen:popUpData[i].stateOpen,//开关闭合1
                        stateClose:popUpData[i].stateClose//开关分开0
                    }
                    popUpTextData.push(td);//存入数组textData中
                    break;//跳出循环
                }
            }
        }
    }
}

function createWebSocket(url) {
    try {
        ws = new WebSocket(url);
        initEventHandle();
    } catch (e) {
        reconnect(url);
    }
    
}

function initEventHandle() {
    ws.onclose = function () {
        console.info("连接关闭");
        reconnect(url);
    };
    ws.onerror = function () {
        console.info("传输异常");
        reconnect(url);
    };
    ws.onopen = function () {
        console.info("连接开启");
        //心跳检测重置
        heartCheck.reset().start();
        ws.send('{action:"connect",bds:"${content[0].shortName}"}');
    };
    ws.onmessage = function (e) {
        
        //如果获取到消息，心跳检测重置
        heartCheck.reset().start();

        var d = JSON.parse(e.data);
        
        //如果有弹出画布，并且有绑定的数据
        if(popUpTextData.length>0){
            if (d.func == "03" || d.func == "04") {//如果是寄存器
                for(var i=0;i<popUpTextData.length;i++){
	                    if(popUpTextData[i].dataType=="03" || popUpTextData[i].dataType=="04"){//如果绑定的是遥测数据
	                        var cur_value = popUpTextData[i].value;
	                        var cur_obj = popUpTextData[i].obj;
	                        var show_point = popUpTextData[i].showPoint;// 保留几位小数
	                        if(!show_point){show_point='2'};
	                        if((e.data).indexOf('"'+cur_value+'"')>-1){
	                            cur_obj.set('text',roundPoint(d[cur_value],show_point)+"");
	                        }
	                    }
	                }
    	    }else if (d.func == "01" || d.func == "02") {//如果是开入
    	        for(var i=0;i<popUpTextData.length;i++){
	                    if(popUpTextData[i].dataType=="01" || d.func == popUpTextData[i].dataType=="02"){//如果绑定的是遥信数据
	                        var curValue = popUpTextData[i].value;
	                        var previewObj = popUpTextData[i].obj;
	                        if((e.data).indexOf('"'+curValue+'"')>-1){
	                            var stateJsonObj;
	                            // 开和关
	                            if(d[curValue]==1){
	                                if(popUpTextData[i].stateOpen!=null && popUpTextData[i].stateOpen!=''){
	                                    stateJsonObj = JSON.parse(popUpTextData[i].stateOpen);
	                                }
	                            }else{
	                                if(popUpTextData[i].stateClose!=null && popUpTextData[i].stateClose!=''){
	                                    stateJsonObj = JSON.parse(popUpTextData[i].stateClose);
	                                }
	                            }
	                            //刷新开关
	                            if(stateJsonObj){
	                                var toPoint = stateJsonObj.angle;
	                                var stroke = stateJsonObj.stroke;
	                                var fill = stateJsonObj.fill;
	                                var text = stateJsonObj.text;
	                                if(previewObj.type=="group"){
	                                    var isSpecFlg = isSpecicalObjSwith(previewObj);
	                                    // 如果是非特殊的 直接整体旋转
	                                    if(!isSpecFlg && toPoint){previewObj.rotate(Number(toPoint));}
	                                    previewObj.getObjects().forEach(function(gobj) {
	                                        // 只旋转线
	                                        if(isSpecFlg && gobj.type=='line' && toPoint){gobj.set({angle:Number(toPoint)});}
	                                        if(stroke && gobj.type!='textbox'){gobj.set('stroke', stroke);}// 线条颜色
	                                        if(fill && gobj.type!='textbox'){gobj.set('fill', fill);}// 填充色
	                                        if(text && gobj.type=='textbox'){gobj.set('text', text);}// 文本
	                                    });
	                                }else{
	                                    if(toPoint){previewObj.rotate(Number(toPoint));}
	                                    if(stroke){previewObj.set('stroke', stroke);}
	                                    if(fill){previewObj.set('fill', fill);}
	                                    if(text && previewObj.type=='textbox'){previewObj.set('text', text);}// 文本
	                                }
	                            }
	                        }
	                    }
	                }
    	    }
    	}
        if (d.func == "03" || d.func == "04") {//如果是寄存器
            for(var i=0;i<textData.length;i++){
                if(textData[i].dataType=="03" || textData[i].dataType=="04"){//如果绑定的是遥测数据
                    var cur_value = textData[i].value;
                    var cur_obj = textData[i].obj;
                    var show_point = textData[i].showPoint;// 保留几位小数
                    if(!show_point){show_point='2'};
                    if((e.data).indexOf('"'+cur_value+'"')>-1){
                        cur_obj.set('text',roundPoint(d[cur_value],show_point)+"");
                    }
                }
            }
        }else if (d.func == "01" || d.func == "02") {//如果是开入
            
            for(var i=0;i<textData.length;i++){
                if(textData[i].dataType=="01" || textData[i].dataType =="02"){//如果绑定的是遥信数据
                    var curValue = textData[i].value;
                    var previewObj = textData[i].obj;
                    if((e.data).indexOf('"'+curValue+'"')>-1){
                        var stateJsonObj;
                        // 开和关
                        if(d[curValue]==1){
                            if(textData[i].stateOpen!=null && textData[i].stateOpen!=''){
                                stateJsonObj = JSON.parse(textData[i].stateOpen);
                            }
                        }else{
                            if(textData[i].stateClose!=null && textData[i].stateClose!=''){
                                stateJsonObj = JSON.parse(textData[i].stateClose);
                            }
                        }
                        //刷新开关
                        if(stateJsonObj){
                            var toPoint = stateJsonObj.angle;
                            var stroke = stateJsonObj.stroke;
                            var fill = stateJsonObj.fill;
                            var text = stateJsonObj.text;
                            if(previewObj.type=="group"){
                                var isSpecFlg = isSpecicalObjSwith(previewObj);
                                // 如果是非特殊的 直接整体旋转
                                if(!isSpecFlg && toPoint){previewObj.rotate(Number(toPoint));}
                                previewObj.getObjects().forEach(function(gobj) {
                                    // 只旋转线
                                    if(isSpecFlg && gobj.type=='line' && toPoint){gobj.set({angle:Number(toPoint)});}
                                    if(stroke && gobj.type!='textbox'){gobj.set('stroke', stroke);}// 线条颜色
                                    if(fill && gobj.type!='textbox'){gobj.set('fill', fill);}// 填充色
                                    if(text && gobj.type=='textbox'){gobj.set('text', text);}// 文本
                                });
                            }else{
                                if(toPoint){previewObj.rotate(Number(toPoint));}
                                if(stroke){previewObj.set('stroke', stroke);}
                                if(fill){previewObj.set('fill', fill);}
                                if(text && previewObj.type=='textbox'){previewObj.set('text', text);}// 文本
                            }
                        }
                    }
                }
            }
        }
    }
}

function reconnect(url) {
    if(lockReconnect) return;
    lockReconnect = true;
    //没连接上会一直重连，设置延迟避免请求过多
    setTimeout(function () {
        console.info("尝试重连..." + new Date().format("yyyy-MM-dd hh:mm:ss"));
        createWebSocket(url);
        lockReconnect = false;
    }, 5000);
}


//心跳检测,每10s心跳一次
var heartCheck = {
    timeout: 10000,
    timeoutObj: null,
    serverTimeoutObj: null,
    reset: function(){
        clearTimeout(this.timeoutObj);
        clearTimeout(this.serverTimeoutObj);
        return this;
    },
    start: function(){
        var self = this;
        this.timeoutObj = setTimeout(function(){
            //这里发送一个心跳，后端收到后，返回一个心跳消息，
            //onmessage拿到返回的心跳就说明连接正常
            ws.send('{action:"connect",bds:"${content[0].shortName}"}');
            console.info("客户端发送心跳：" + new Date().format("yyyy-MM-dd hh:mm:ss"));
            self.serverTimeoutObj = setTimeout(function(){//如果超过一定时间还没重置，说明后端主动断开了
                ws.close();//如果onclose会执行reconnect，我们执行ws.close()就行了.如果直接执行reconnect 会触发onclose导致重连两次
            }, self.timeout)
        }, this.timeout)
    }
}

//js中格式化日期，调用的时候直接：new Date().format("yyyy-MM-dd hh:mm:ss")
Date.prototype.format = function(fmt) {
     var o = {
        "M+" : this.getMonth()+1,                 //月份 
        "d+" : this.getDate(),                    //日 
        "h+" : this.getHours(),                   //小时 
        "m+" : this.getMinutes(),                 //分 
        "s+" : this.getSeconds(),                 //秒 
        "S"  : this.getMilliseconds()             //毫秒 
   	}; 
    if(/(y+)/.test(fmt)) {
            fmt=fmt.replace(RegExp.$1, (this.getFullYear()+"").substr(4 - RegExp.$1.length)); 
    }
    for(var k in o) {
       if(new RegExp("("+ k +")").test(fmt)){
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length)));
         }
     }
    return fmt; 
}

$(function(){
    canvasConfig();
    show();
    getData();
    createWebSocket(url);//开启数据传输
    // 定时刷新画面
    setInterval(refreshCanvas, 1000);
});

// 四舍五入保留多少位小数
function roundPoint(curValue,showPoint){
    var cv = Math.pow(10,Number(showPoint));
    return Math.round(curValue * cv) / cv;
}

//判断是否特殊开关 两圆一线
function isSpecicalObjSwith(swObj){

     var xcount = 0;// 线的数量
     var ycount = 0;// 圆的数量
     var tcount = 0 ;// 总数量
     if(swObj.type=="group"){
         swObj.getObjects().forEach(function(gobj) {
             tcount++;
             if(gobj.type=='line'){
                 xcount++;
             }else if(gobj.type=='circle'){
                 ycount++;
             }
         });
         if(xcount==1 && ycount==2 && tcount==3){
             return true;
         }else{
             return false;
         }
     }else{
         return false;
     }
}

function refreshCanvas() {
    canvas.renderAll();
    newCanvas.renderAll();
}

//画布初期配置
function canvasConfig(){
  //canvas.defaultCursor = 'pointer';
  canvas.defaultCursor = 'default';
  canvas.isDrawingMode =false;// 这个模式就是FALSE 是画笔轨迹的图形
  canvas.selectable = true;// 没有选中框不然丑
  canvas.skipTargetFind=false;// 画板元素不能被选中
  canvas.selection = false; //画板不显示选中
  canvas.allowTouchScrolling = true;
  //鼠标按下监听事件
  canvas.on('mouse:down', function (options) {
      var objData = options.target;
            
      //弹出新画布
      popUpCanvas(objData);
      
  });
  //鼠标浮上监听事件
  canvas.on('mouse:over', function (options) {
      var objData = options.target;
      if(objData!=null){//&&objData!=undefined
          changeMouseType(objData);
      }else{
          canvas.hoverCursor = 'default';
      }
  });
  //鼠标移除监听事件
  canvas.on('mouse:out', function (options) {
      canvas.hoverCursor = 'default';
  });
  
  newCanvas.defaultCursor = 'default';
  newCanvas.isDrawingMode =false;// 这个模式就是FALSE 是画笔轨迹的图形
  newCanvas.selectable = true;// 没有选中框不然丑
  newCanvas.skipTargetFind=false;// 画板元素不能被选中
  newCanvas.selection = false; //画板不显示选中

  //鼠标浮上监听事件
  newCanvas.on('mouse:over', function (options) {
      newCanvas.hoverCursor = 'default';
  });
  //鼠标按下监听事件
  newCanvas.on('mouse:down', function (options) {
      var objData = options.target;
      //alert(objData);      
      //弹出变量曲线
      popUpHistoryWindow(objData);
      
  });
  //鼠标浮上监听事件
  newCanvas.on('mouse:over', function (options) {
      var objData = options.target;
      if(objData!=null){//&&objData!=undefined
          changeMouseTypePopUp(objData);
      }else{
          newCanvas.hoverCursor = 'default';
      }
  });
}

//改变鼠标样式（底层画布）
function changeMouseType(objData){
    canvas.hoverCursor = 'default';
    if(dataBindList.length>0){
        for(var i = 0;i<dataBindList.length;i++){
            if(dataBindList[i].objId==objData.id){
                canvas.hoverCursor = 'pointer';
                break;
            }
        }
    }
    if(eventBindList.length>0){
        for(var i = 0;i<eventBindList.length;i++){
            if(eventBindList[i].objId==objData.id){
                canvas.hoverCursor = 'pointer';
                break;
            }
        }
    }
    
}
//改变鼠标样式（弹出画布）
function changeMouseTypePopUp(objData){
    newCanvas.hoverCursor = 'default';
    if(popUpDataBindList.length>0){
        for(var i = 0;i<popUpDataBindList.length;i++){
            if(popUpDataBindList[i].objId==objData.id){
                newCanvas.hoverCursor = 'pointer';
                break;
            }
        }
    }
    if(popUpEventBindList.length>0){
        for(var i = 0;i<popUpEventBindList.length;i++){
            if(popUpEventBindList[i].objId==objData.id){
                newCanvas.hoverCursor = 'pointer';
                break;
            }
        }
    }
    
}
//获取绑定的弹出部件中绑定的数据（数据绑定）
function getPopupDataBind(id){
    //获得绑定数据
    $.ajax({
        type: 'get',
        url: "getBindData",
        data: { psId: id },
        async:false, 
        cache: false,
        dataType: 'json',
        success: function (data) {
            popUpDataBindList = data.retObj;
            getPopUpData(popUpDataBindList);
        }
    });
}
//获取事件绑定数据（事件绑定-->变量一览）
function getPopupEventBind(id){
  $.ajax({
      type: 'get',
      url: "getEventData",
      data: { psId: id },
      async:false,
      cache: false,
      dataType: 'json',
      success: function (data) {
          popUpEventBindList=data.retObj;
      }
  });
}
//弹出画布
function popUpCanvas(objData){
    if(objData!=null&&objData!=undefined){
        if(eventBindList.length>0){
            for(var i = 0;i<eventBindList.length;i++){
                if(eventBindList[i].eventType=="1"){//1、弹出部件
                    if(objData.id==eventBindList[i].objId){
                        // 重置弹出窗口
                        resetCanvas(newCanvas);
                        if(eventBindList[i].stationMemoryBlob==""||eventBindList[i].stationMemoryBlob==null){//部件在数据库中已被删除
                            //提示加载失败
                            $.ligerDialog.error("加载失败！该事件绑定的部件或变量信息可能被删除！","加载错误");
                        }else{
                            isPopup = true;
                            // 先弹出后加载
                            if(!popUpCanvasDialog){
                                popUpCanvasDialog = $.ligerDialog.open({
                                     target:$("#popUpCanvasDialog"),
                                     height: (is_p_height-80),
                                     width: (is_p_width-30),
                                     //height: eventBindList[i].canvasHeight,//Number(eventBindList[i].canvasHeight)
                                     //width: eventBindList[i].canvasWidth,//Number(eventBindList[i].canvasWidth)
                                     title: "事件查看",
                                     cls:"l-dialog-highlight",
                                     allowClose:false,
                                     buttons:[
                                          { text: '关闭', cls:'l-dialog-btn-highlight',
                                              onclick: function (item, popUpCanvasDialog) { 
                                                  popUpDataBindList.length = 0;//清空 
                                                  popUpTextData.length = 0;//清空 
                                                  popUpCanvasDialog.hide();
                                                  isPopup = false;
                                              }
                                          }
                                      ]
                                  }); 
                            }else{
                                //确定弹窗的高度、宽度以及窗口居中位置
                                var winH = is_p_height-80;
                                var winW =is_p_width-30;
                                var evH = eventBindList[i].canvasHeight;
                                var evW = eventBindList[i].canvasWidth;
                                //确定窗口左偏移和右偏移
                                var top = Math.abs(winH-evH)/2;
                                var left = Math.abs(winW-evW)/2;
                                //设置窗口显示的位置
                                popUpCanvasDialog.set({ left: 20,top:0, width: winW, height:winH });
                                popUpCanvasDialog.show();
                                resetCanvas(newCanvas);
                            }
                            //根据部件宽和高设定画布大小
                            newCanvas.setWidth(is_p_width-30);
                            newCanvas.setHeight(is_p_height-150);
	                        //解压
		                    var stationMemory = unzip(eventBindList[i].stationMemoryBlob);
	                        //加载选中的对象
	                        newCanvas.loadFromJSONFr(stationMemory);
                            //zoomToFitCanvas(newCanvas);
                            //获取绑定的弹出部件中绑定的数据
                            getPopupDataBind(eventBindList[i].eventId);
                          	//获取绑定的弹出部件中绑定的事件（变量一览）
                            getPopupEventBind(eventBindList[i].eventId);
                            
                            var objAll = newCanvas.getObjects();
                            objAll.forEach(function (item,index,input) {
                                if(input[index].type=="textbox"){
                                    input[index].editable = false;
                                }
                                input[index].selectable=false;
                            });
                            newCanvas.renderAll();
                            zoomToFitCanvas(newCanvas);
                            toBetterClean(2);
                        }
                    }
                }
                if(eventBindList[i].eventType=="2"){//如果是弹出变量一览
                    if(objData.id==eventBindList[i].objId){
                        historyWindow(eventBindList[i].eventId);
                    }
                }
            }
            
        }
    }
}
//弹出变量曲线一览
function popUpHistoryWindow(objData){
    if(objData!=null && objData!=undefined){
        if(popUpEventBindList.length>0){
            for(var i = 0;i<popUpEventBindList.length;i++){
                if(popUpEventBindList[i].eventType=="2"){//如果是弹出变量一览
                    if(objData.id==popUpEventBindList[i].objId){
                        historyWindow(popUpEventBindList[i].eventId);
                    }
                }
            }
        }
    }
}
function resetCanvas(myCanvas){
    //标准清除回到初始状态
    myCanvas.clear().renderAll();
    var delta = new fabric.Point(0, 0);
    myCanvas.absolutePan(delta);
    // 还原画布缩放比例
    myCanvas.zoomToPoint(lastPoint, 1);
}
//对后台传来的二进制数据进行解压
function unzip(key) {
    var strData = atob(key);
	// Convert binary string to character-number array
	var charData = strData.split('').map(function(x){return x.charCodeAt(0);});
	// Turn number array into byte-array
	var binData = new Uint8Array(charData);
	// // unzip
	var data = pako.inflate(binData);
	// Convert gunzipped byteArray back to ascii string:
	//uint8array有缓冲区大小的限制。它将崩溃在大缓冲区，粗鲁的最大大小是246300，有时它在不同的浏览器不正确
	//strData = String.fromCharCode.apply(null, Uint8Array(data));//对传来的数组对象整体一次性解析
	var strData = handleCodePoints(data);//解决Maximum call stack size exceeded问题
	return decodeURIComponent(escape(strData));//解决中文乱码问题decodeURIComponent(escape());
}
//将所有数据进行循环分块解析，不再像String.fromCharCode.apply一次性解析，避免了数据量过大造成栈溢出
function handleCodePoints(array) {
    var CHUNK_SIZE = 0x8000; // arbitrary number here, not too small, not too big（不能太小，也不能太大）
    var index = 0;
    var length = array.length;
    var result = '';
    var slice;
    var arr = [];
    for (var i = 0, _i = array.length; i < _i; i++) {
        arr[i] = array[i];
    }
    while (index < length) {
        slice = arr.slice(index, Math.min(index + CHUNK_SIZE, length)); // `Math.min` is not really necessary here I think
        result += String.fromCharCode.apply(null, slice);
        index += CHUNK_SIZE;
    }
    return result;
}
//展示预览
function show(){
    var array = new Array();
    document.getElementById("canvas_border").style.width =  (window.innerWidth)+'px';
    document.getElementById("canvas_border").style.height = (window.innerHeight)+'px';
    canvas.clear();
    getBindData("${content[0].id}");
    getEventData("${content[0].id}");
    //getAllVarConfig("${content[0].substationSn}");//获取所有变量配置
    //canvas.setWidth("${content[0].canvasWidth}");
    //canvas.setHeight("${content[0].canvasHeight}");
    canvas.setWidth(window.innerWidth);
    canvas.setHeight(window.innerHeight);
    canvas.setBackgroundColor("${content[0].canvasBackgroundColor}");

  	//再次获取画图
    $.ajax({
        type: 'get',
        url: "../pictureStation/getDetail",
        data: { id: '${content[0].id}' },
        async:false,
        cache: false,
        dataType: 'json',
        success: function (data) {
//             console.log("压缩后的stationMemory:"+data.retObj[0]['stationMemoryBlob']);
            //解压
            var stationMemory = unzip(data.retObj[0]['stationMemoryBlob']);
            //载入画图
            loadObj = canvas.loadFromJSON(stationMemory);
        }
    });
  	
    var objAll = canvas.getObjects();
    

    objAll.forEach(function (item,index,input) {
        if(input[index].type=="textbox"){
            input[index].editable = false;
        }
        input[index].selectable=false;
    });

    canvas.renderAll();
    zoomToFitCanvas(canvas);
    toBetterClean(1);//解决显示模糊问题 
}

//获取数据绑定数据
function getBindData(id){
  $.ajax({
      type: 'get',
      url: "getBindData",
      data: { psId: id },
      async:false, 
      cache: false,
      dataType: 'json',
      success: function (data) {
          dataBindList = data.retObj;
      }
  });
}
//获取事件绑定数据
function getEventData(id){
  $.ajax({
      type: 'get',
      url: "getEventData",
      data: { psId: id },
      async:false,
      cache: false,
      dataType: 'json',
      success: function (data) {
          eventBindList=data.retObj;//包含id,psId,substationSn,objId,eventType,eventId,stationMemory
      }
  });
}

//缩放移动视图，使其适应Canvas大小
function zoomToFitCanvas(myCanvas) {
  var maxX = 0;
  var minX = 0;
  var maxY = 0;
  var minY = 0;
  //遍历所有对对象，获取最小坐标，最大坐标
  var objects = myCanvas.getObjects();
  if(objects.length > 0 ){
    var rect = objects[0].getBoundingRect();
    var minX = rect.left;
    var minY = rect.top;
    var maxX = rect.left + rect.width;
    var maxY = rect.top + rect.height;
    for(var i = 1; i<objects.length; i++){
      rect = objects[i].getBoundingRect();
      minX = Math.min(minX, rect.left);
      minY= Math.min(minY, rect.top);
      maxX = Math.max(maxX, rect.left + rect.width);
      maxY= Math.max(maxY, rect.top + rect.height);
    }
  }
  //计算平移坐标
  var panX = (maxX - minX - myCanvas.width)/2 + minX;
  var panY = (maxY - minY - myCanvas.height)/2 + minY;
  //开始平移
  myCanvas.absolutePan({x:panX, y:panY});
  // 直接写进去会报错
  var myCanvasWidth = myCanvas.width;
  var myCanvasHeight = myCanvas.height;
  var xMM = myCanvasWidth/(maxX - minX);
  var yMM = myCanvasHeight/(maxY - minY);
  //计算缩放比例留0.05的边距
  var zoom = Math.min(xMM,yMM)-0.05;
  //计算缩放中心
  var zoomPoint = new fabric.Point(myCanvasWidth * 0.5 , myCanvasHeight * 0.5);
  //开始缩放
  myCanvas.zoomToPoint(zoomPoint, zoom);
  lastPoint = zoomPoint;
}

//Canvas以指定的点为中心进行缩放
//鼠标滚轮监听(画布放大功能) 用滚轮的方式会和画面冲突的
$(".upper-canvas").mousewheel(function(event) {
     if(!isPopup){
         var zoom = (event.deltaY > 0 ? 0.1 : -0.1) + canvas.getZoom();
         zoom = Math.max(0.3,zoom); //最小为原来的1/10
         zoom = Math.min(3,zoom); //最大是原来的3倍
         var zoomPoint = new fabric.Point(canvas.width * 0.5 , canvas.height * 0.5);
         canvas.zoomToPoint(zoomPoint, zoom);
     }else{
         var zoom = (event.deltaY > 0 ? 0.1 : -0.1) + newCanvas.getZoom();
         zoom = Math.max(0.3,zoom); //最小为原来的1/10
         zoom = Math.min(3,zoom); //最大是原来的3倍
         var zoomPoint = new fabric.Point(newCanvas.width * 0.5 , newCanvas.height * 0.5);
         newCanvas.zoomToPoint(zoomPoint, zoom);
     }
});

//转百分比显示
function toPercent(point){
    var str=Number(point*100).toFixed(0);
    str+="%";
    return str;
}

//页面刷新
function refesh(){
    location.reload();
}

//不用
function fullScreen() {
    document.getElementById("canvas_border").style.width = (window.screen.width) + 'px';
    document.getElementById("canvas_border").style.height = (window.screen.height) + 'px';
    canvas.setWidth(window.screen.width);
    canvas.setHeight(window.screen.heigh);
    canvas.setBackgroundColor("${content[0].canvasBackgroundColor}");
    loadObj = canvas.loadFromJSONFr('${content[0].stationMemory}');
    canvas.renderAll();
    zoomToFitCanvas(canvas);
    $(document).keyup(function(e) {
        if (e.keyCode == 27) {
            fullWin.hide();
        }
    });
    var el = document.documentElement,
    rfs = el.requestFullScreen || el.webkitRequestFullScreen || el.mozRequestFullScreen || el.msRequestFullScreen,
    wscript;
    if (typeof rfs != "undefined" && rfs) {
        rfs.call(el);
        return;
    }

    if (typeof window.ActiveXObject != "undefined") {
        wscript = new ActiveXObject("WScript.Shell");
        if (wscript) {
            wscript.SendKeys("{F11}");
        }
    }
}

//历史曲线窗口
function historyWindow(eventId) {
    window.location.href="appHistoryWindow?id="+eventId+"&shortName="+"${content[0].shortName}";  
    /*if(!eventId){
        eventId=0;
    }
    // 转向网页的地址;
    var url = 'appHistoryWindow?id=' + "${content[0].id}";
    $.ligerDialog.open({
        //右上角的X隐藏，否则会有bug
        height: (is_p_height-30),
        url: url,
        width: (is_p_width-30),
        name: 'choseKey',
        title: '历史曲线',
        data: {
            pid: "${content[0].id}",
            substationSn: "${content[0].substationSn}",
            shortName: "${content[0].shortName}",
            eventId:eventId
        },
        allowClose: true
    });*/
}
//历史曲线窗口(弹出画布)
function historyWindowPopUp(eventId) {
    if(!eventId){
        eventId=0;
    }
    // 转向网页的地址;
    var url = 'historyWindow?id=' + "${content[0].id}";
    $.ligerDialog.open({
        //右上角的X隐藏，否则会有bug
        allowClose: false,
        showMax :true,
        height: (window.screen.height - 250),
        url: url,
        width: (window.screen.width - 200),
        name: 'choseKey',
        title: '历史曲线',
        data: {
            pid: "${content[0].id}",
            substationSn: "${content[0].substationSn}",
            shortName: "${content[0].shortName}",
            eventId:eventId
        },
        isResize: true,
        buttons: [{
            text: '确认',
            onclick: function(item, dialog) {
                dialog.close();
            }
        },
        {
            text: '关闭',
            onclick: function(item, dialog) {
                dialog.close();
            }
        }]
    });
}
//报表查看窗口
function report() {
    // 转向网页的地址;
    var url = 'pictureReport?id=' + "${content[0].id}";
    $.ligerDialog.open({
        //右上角的X隐藏，否则会有bug
        allowClose: false,
        showMax :true,
        height: (window.screen.height - 250),
        url: url,
        width: (window.screen.width - 200),
        name: 'choseKey',
        title: '报表查看',
        data: {
            substationSn: "${content[0].substationSn}"
        },
        isResize: true,
        buttons: [{
            text: '确认',
            onclick: function(item, dialog) {
                dialog.close();
            }
        },
        {
            text: '关闭',
            onclick: function(item, dialog) {
                dialog.close();
            }
        }]
    });
}
//报警事件显示
function alarmEvent(){
    var substationSn = $("#substationSn").val();
    //打开弹窗
    $.ligerDialog.open({
            url:"alarmEvent?substationSn="+substationSn,
            height: (window.screen.height-250),
            width:(window.screen.width-200),
            title: "报警事件",
            name:"alarmEvent",
            isHidden:true,
            allowClose: false,
            isResize:false,
            showMax:false,
            showMin:false,
            buttons:[
                 { text: '关闭', onclick: function (item, dialog) { dialog.close(); } }
            ]
    });
}
//获取所有变量配置(站点下所有变电所)
function getAllVarConfig(id){
//     $.ajax({
//         type: 'post',
//         url: "getAllVarConfig",
//         data: { psId: id },
//         async:false,
//         cache: false,
//         dataType: 'json',
//         success: function (data) {
//             warningArray=data.retObj;
//         }
//     });
}
//解决模糊问题
function toBetterClean(type){
  	if(type==1){
  	  	var objAll = canvas.getObjects();
  	    objAll.forEach(function (item,index,input) {
  	        input[index].objectCaching = false;
  	    });
  	    canvas.renderAll();
  	}else if(type==2){
  	  	var objAll = newCanvas.getObjects();
	    objAll.forEach(function (item,index,input) {
	        input[index].objectCaching = false;
	    });
	    newCanvas.renderAll();
  	}
    
}
</script>
</body>
</html>
