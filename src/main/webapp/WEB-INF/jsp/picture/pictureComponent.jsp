<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table1.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/draw/jquery.contextMenu.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/draw/palette-color-picker.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/draw/jquery.contextMenu.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/draw/palette-color-picker.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/draw/jquery.mousewheel.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/fabricnew.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script> 
    <style>
    .readonlyInput{
        border:1px solid #DDD;
        background-color:#F5F5F5;
        color:#ACA899;
    }
    .accordion {
         width: 100%;
         max-width: 360px;
         margin: 0px auto 20px;
         background: #FFF;
         -webkit-border-radius: 4px;
         -moz-border-radius: 4px;
         border-radius: 4px;
         border:1px solid #CCC;
     }
    
    .accordion .link {
        cursor: pointer;
        display: block;
        padding: 15px 15px 15px 42px;
        color: #4D4D4D;
        font-size: 14px;
        font-weight: 700;
        border-bottom: 1px solid #CCC;
        position: relative;
        -webkit-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }
    
    .accordion li:last-child .link {
        border-bottom: 0;
    }
    
    .accordion li i {
        position: absolute;
        top: 16px;
        left: 12px;
        font-size: 18px;
        color: #595959;
        -webkit-transition: all 0.4s ease;
        -o-transition: all 0.4s ease;
        transition: all 0.4s ease;
    }
    
    .accordion li i.fa-chevron-down {
        right: 12px;
        left: auto;
        font-size: 16px;
    }
    
    .accordion li.open .link {
        color: #b63b4d;
    }
    
    .accordion li.open i {
        color: #b63b4d;
    }
    .accordion li.open i.fa-chevron-down {
        -webkit-transform: rotate(180deg);
        -ms-transform: rotate(180deg);
        -o-transform: rotate(180deg);
        transform: rotate(180deg);
    }
    .submenu {
         display: none;
         background: #444359;
         font-size: 14px;
     }

     .submenu li {
         border-bottom: 1px solid #4b4a5e;
     }
    
     .submenu a {
         display: block;
         text-decoration: none;
         color: #d9d9d9;
         padding: 12px;
         padding-left: auto;
         -webkit-transition: all 0.25s ease;
         -o-transition: all 0.25s ease;
         transition: all 0.25s ease;
     }
    
     .submenu a:hover {
         background: #b63b4d;
         color: #FFF;
     }
    </style>
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">
<div id="layout1">
    <!-- 左侧界面 -->
    <div position="left" title="组件图库列表" style="height: 100%; padding: 5px 0 0; overflow: auto; ">
        <input type="text" id="search_name" name="searchName" placeholder="请输入名称进行搜索" style="text-indent:5px;height:23px;border-radius: .25em;width:75%;margin-left: 5%"/>
        <button style="width:26px;height:26px;margin-top: -5px" id="btnSearch" type="button" class="btn btn-default  btn-small btn-mini"><i class="fa fa-search"></i></button>
        <div id="leftLoading" style="text-align: center;margin-top: 100px">
            <i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
            <br/><br/><span style="color:#515151;font-size: 14px;font-weight: bolder;">数据加载中</span>
        </div>
        <div id="leftNone" style="text-align: center;display: none">
            <img style="width: 30%;margin-top: 10px" src="/hmdl/static/images/hmdl/404.png"/>
            <div style ="color:#515151;font-size: 12px">
                <br/>非常抱歉<br/>未能检索到您的组件信息<br/>
            </div>
        </div>
        
        <div id="leftList" style="text-align: center;display: none">
            <ul id="accordion" class="accordion" style="margin-top: 10px">
              <li>
                <div class="link">
                  <i class="fa fa-star-o"></i>基本组件
                  <i class="fa fa-chevron-down"></i></div>
                  <ul class="submenu" id="html0"></ul>
              </li>
              <li>
                <div class="link">
                  <i class="fa fa-star-half-o"></i>常用组件
                  <i class="fa fa-chevron-down"></i></div>
                  <ul class="submenu" id="html1"></ul>
              </li>
              <li>
                <div class="link">
                  <i class="fa fa-star"></i>常用大组件
                  <i class="fa fa-chevron-down"></i></div>
                  <ul class="submenu" id="html2"></ul>
              </li>
            </ul>
        </div>
    </div>
    <!-- 右侧界面 -->
    <div id="rightWin" position="center" title="请选择一个组件进行预览或者编辑" style="width:100%">
        <!-- 标题 -->
        <div id="initWin" style="vertical-align: middle;display: table-cell;text-align: center;">
            <div>
                <div style="margin-top: 150px"><h1>组件图库管理</h1></div>
                <div style="width: 100%; height: 100%;text-align: center;margin-top: 50px">
                  <a class="btn btn-lg btn-success" href="javascript:createComponents(0)">
                  <i class="fa fa-star-o fa-2x pull-left"></i>  创 建 基 本 组 件&emsp;　&emsp;<br/>BASIC COMPONENTS</a>&emsp;&emsp;&emsp;&emsp;&emsp;
                  <a class="btn btn-lg btn-warning" href="javascript:createComponents(1)">
                  <i class="fa fa-star-half-o fa-2x pull-left"></i>  创 建 常 用 组 件&emsp;　　　&emsp;<br/>COMMON COMPONENTS</a>&emsp;&emsp;&emsp;&emsp;&emsp;
                  <a class="btn btn-lg btn-primary" href="javascript:createComponents(2)">
                  <i class="fa fa-star fa-2x pull-left"></i>  创 建 常 用 大  组 件&emsp;　　　　&emsp;<br/>COMMON BIG COMPONENTS</a>
                </div>
            </div>
        </div>
        <!-- 画板 -->
        <div id="cWin" style="display: none">
            <div style="margin-top: 10px;">
                <div id="model0">
                    <button style="margin-left: 10px" onclick="btnEdit()" id="btnEdit" type="button" class="btn btn-warning  btn-small btn-mini"><i class="fa fa-edit"></i>&nbsp;&nbsp;开始编辑&nbsp;&nbsp;</button>
                </div>
                <div id="model1" style="display:none">
	                <button style="margin-left: 10px;" onclick="btnSave()" id="btnSave" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-save"></i>&nbsp;&nbsp;保存编辑&nbsp;&nbsp;</button>
<!-- 拆分暂时没有找到合适的方法，暂且不要 -->
	                <button style="margin-left: 10px;" onclick="btnGroup()" id="btnGroup" type="button" class="btn btn-success  btn-small btn-mini"><i class="fa fa-save"></i>&nbsp;&nbsp;组合&nbsp;&nbsp;</button>
	                <button style="margin-left: 10px;" onclick="btnUnGroup()" id="btnUnGroup" type="button" class="btn btn-warning  btn-small btn-mini"><i class="fa fa-save"></i>&nbsp;&nbsp;拆分&nbsp;&nbsp;</button>
	                <button style="margin-left: 10px;" onclick="btnCanel()" id="btnCanel" type="button" class="btn btn-default  btn-small btn-mini"><i class="fa fa-undo"></i>&nbsp;&nbsp;取消编辑&nbsp;&nbsp;</button>
                    &emsp;&emsp;<span id="zb"></span>&emsp;&emsp;<span id="sf">当前缩放比例：100%</span>
                </div>
                <hr />
            </div>
            <!-- 画板信息 -->
            <canvas id="c" width='1010' height='440' style='border:1px solid #000'></canvas>
        </div>
        <!-- 弹出右键菜单 -->
        <div id="contextmenu-output"></div>
    </div>
</div>

<!-- 颜色调整板 -->
<div id="editWin" style="display:none;">
    <table class="default">
        <tr style="height:50px" >
            <td colspan="3" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
            	<i class="fa fa-edit"></i>&emsp;组件编辑
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;组件类型：</td>
            <td colspan="2">
               <input id="zjlx" name="zjlx" class="readonlyInput" readonly="readonly"></input>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;组&nbsp;件&nbsp;I&nbsp;D：</td>
            <td colspan="2">
               <input id="zjid" name="zjid" class="readonlyInput" readonly="readonly"></input>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;组件位置：</td>
            <td colspan="2">
                &nbsp;X&nbsp;轴&nbsp;：&nbsp;<input style="width:60px" id="xPosition" name="xPosition"></input>
                &nbsp;Y&nbsp;轴&nbsp;：&nbsp;<input style="width:60px" id="yPosition" name="yPosition"></input>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;组件大小：</td>
            <td colspan="2">
                &nbsp;高度&nbsp;：&nbsp;<input style="width:60px" id="zjgd" name="zjgd"></input>
                &nbsp;宽度&nbsp;：&nbsp;<input style="width:60px" id="zjkd" name="zjkd"></input>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;线条粗细：</td>
            <td colspan="2">
                <input id="xtcx" name="xtcx"></input>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;线条颜色：</td>
            <td colspan="2">
                <input type="text" name="xtys" id="xtys"
                data-palette='["#FFFFFF","#000000","#D50000","#304FFE","#00B8D4","#00C853","#FFD600","#FF6D00","#FF1744","#3D5AFE","#00E5FF","#00E676","#FFEA00","#FF9100","#FF5252","#536DFE","#69F0AE","#FFFF00","#FFAB40"]' 
                value=""/>
            </td>
        </tr>
        <tr>
            <td width="100px">&emsp;填充颜色：</td>
            <td colspan="2">
                <input type="text" name="tcys" id="tcys"
                data-palette='["#FFFFFF","#000000","#D50000","#304FFE","#00B8D4","#00C853","#FFD600","#FF6D00","#FF1744","#3D5AFE","#00E5FF","#00E676","#FFEA00","#FF9100","#FF5252","#536DFE","#69F0AE","#FFFF00","#FFAB40"]' 
                value=""/>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: center">
                <button type="button" class="btn btn-primary btn-small btn-mini" onclick="editCommit()"><i class="fa fa-check"></i>&nbsp;&nbsp;提交&nbsp;&nbsp;</button>
                &emsp;
                <button type="button" class="btn btn-default btn-small btn-mini" onclick="editCancel()"><i class="fa fa-undo"></i>&nbsp;&nbsp;返回&nbsp;&nbsp;</button>
            </td>
        </tr>
    </table>
</div>
<script type="text/javascript">
var canvas = new fabric.Canvas('c');
canvas.defaultCursor = 'crosshair'; //默认光标改成十字
canvas.hoverCursor = 'pointer'; //悬浮光标改成手型
var listData;
var obj;
var clearFlg = true;
// 显示模式 0：预览模式，不能移动不能操作
// 显示模式 1：编辑模式，可以移动可以各种操作
var showModel = 0;

//是否拖动(画布移动功能)
var panning = false;
//鼠标按下
canvas.on('mouse:down', function (e) {
    //按住alt键才可拖动画布
    if(e.e.altKey) {
      panning = true;
      canvas.selection = false;
    }
});

//鼠标抬起
canvas.on('mouse:up', function (e) {
    panning = false;
    canvas.selection = true;
});

//鼠标移动
canvas.on('mouse:move', function (e) {
    if(showModel==1){
        // 显示画板坐标点
        $("#zb").html("当前坐标 - X:"+e.e.offsetX+"&emsp;Y:"+e.e.offsetY);
        if (panning && e && e.e) {
            var delta = new fabric.Point(e.e.movementX, e.e.movementY);
            canvas.relativePan(delta);
        }
    }
});

//Canvas以指定的点为中心进行缩放
//鼠标滚轮监听(画布放大功能)
$(".upper-canvas").mousewheel(function(event) {
    if(showModel==1){
	    var zoom = (event.deltaY > 0 ? 0.1 : -0.1) + canvas.getZoom();
	    zoom = Math.max(0.3,zoom); //最小为原来的1/10
	    zoom = Math.min(3,zoom); //最大是原来的3倍
	    var zoomPoint = new fabric.Point(event.pageX, event.pageY);
	    $("#sf").html("当前缩放比例："+toPercent(zoom));
	    canvas.zoomToPoint(zoomPoint, zoom);
    }
});

//在canvas上层对象上添加右键事件监听
$(".upper-canvas").contextmenu(onContextmenu);
//初始化右键菜单
$.contextMenu({
      selector: '#contextmenu-output',
      trigger: 'none',
      build: function($trigger, e) {
          //构建菜单项build方法在每次右键点击会执行
          return {
              callback: contextMenuClick,
              items: contextMenuItems
          };
      },
});

$(function() {
    document.getElementById("initWin").style.width = document.getElementById("rightWin").offsetWidth+'px';
    document.getElementById("initWin").style.height = document.getElementById("rightWin").offsetHeight+'px';
    // 布局设置
    $("#layout1").ligerLayout({
        leftWidth: 200,
    });
    
    // LINK样式动画设定
    var Accordion = function(el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;

        var links = this.el.find('.link');
        links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
    }

    Accordion.prototype.dropdown = function(e) {
        var $el = e.data.el;
            $this = $(this),
            $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
        };
    }    

    var accordion = new Accordion($('#accordion'), false);
    // 加载左侧列表
    getLeftList();
    // 线条颜色
    $('[name="xtys"]').paletteColorPicker({
        clear_btn: 'last'
    });
    // 填充颜色
    $('[name="tcys"]').paletteColorPicker({
        clear_btn: 'last'
    });
        
});
//绑定查询按钮
$("#btnSearch").click(function () {
	var searchName =document.getElementById('search_name').value;
    var data = {"partName" : searchName};
	getLeftList(data);
});
//画图drawing方法
function clickMenu(type,name,id){
    if(showModel==0){
        var name_p1 = "";
        if(type=='0'){
            name_p1 = "基本组件";
        }else if(type=='1'){
            name_p1 = "常用组件";
        }else if(type=='2'){
            name_p1 = "常用大组件";
        }
        // 隐藏初始化界面，显示具体的画图界面，同时更改标题
        $(".l-layout-center .l-layout-header").html(name_p1+"【"+name+"】 预览模式");
        $("#initWin").hide();
        $("#cWin").show();

        // 默认全清
        if (clearFlg){
            canvas.clear().renderAll();
        }
    }
 	// 加载对应的组件对象
    listData.forEach(function (item,index,input) {
        // 如果ID一致
        if(id == input[index].id){
            // 加载 最后一个参数0代表重新赋值ID（测试版本-重新生成最新 的ID）
            obj = canvas.loadFromJSONFr(input[index].partMemory,null,function(oa,oj) {
                if(showModel==0){
                    // 预览
                    oj.selectable=false;
                }else{
                     // 编辑
                    oj.selectable=true;
                }
            },0);
        }
    });
}

function toPercent(point){
    var str=Number(point*100).toFixed(0);
    str+="%";
    return str;
}

// 取消
function btnCanel(){
    $(".l-layout-center .l-layout-header").html("请选择一个组件进行预览或者编辑");
    $("#initWin").show();
    $("#cWin").hide();
    $("#model0").show();
    $("#model1").hide();
    clearFlg = true;
//    BUG待解决
//    canvas.setZoom(1);
    canvas.clear().renderAll();
    showModel = 0;
}

function btnEdit(){
    var oldTiltle = $(".l-layout-center .l-layout-header").html();
    var newTiltle = oldTiltle.replace(/预览模式/, "编辑模式");
    $(".l-layout-center .l-layout-header").html(newTiltle);
    $("#model1").show();
    $("#model0").hide();
    // 转为可编辑状态
    clearFlg = false;
    showModel = 1;
    var objAll = canvas.getObjects(); 
	objAll.forEach(function (item,index,input) {
	    input[index].selectable=true;
	});
}

function btnSave(){
    var ojb = JSON.stringify(canvas);
    alert(ojb);
    console.log(ojb);
}

//右键点击事件响应
function onContextmenu(event) {
  var pointer = canvas.getPointer(event.originalEvent);
  var objects = canvas.getObjects();
  for (var i = objects.length - 1; i >= 0; i--) {
    var object = objects[i];
     //判断该对象是否在鼠标点击处
     if (canvas.containsPoint(event, object)) {
       //选中该对象
       canvas.setActiveObject(object);
       //显示菜单
       showContextMenu(event, object);
       continue;
     }
  }

  //阻止系统右键菜单
  event.preventDefault();
  return false;
}

//右键菜单项点击
function showContextMenu(event, object) {
  //定义右键菜单项
  contextMenuItems = {
     "type": {name: "类型："+object.get("type"),icon: "fa-info-circle", data: object},
     "id": {name: "ID值："+object.get("id"),icon: "fa-info-circle", data: object},
     "sep1": "---------",
     "copy": {name: "复制", icon: "fa-copy", data: object},
     "edit": {name: "编辑", icon: "fa-edit", data: object},
     "delete": {name: "删除", icon: "fa-trash", data: object}
  };
  //右键菜单显示位置
  var position = {
      x: event.clientX,
      y: event.clientY
  }
  $('#contextmenu-output').contextMenu(position);
}
var editWin;

//右键菜单项点击
function contextMenuClick(key, options) {
  if(showModel==0){
      $.ligerDialog.warn("操作前请点击开始编辑按钮！", "提示");
  }else{
   // 删除
      if(key == "delete") {
        //得到对应的object并删除
        var object = contextMenuItems[key].data;
        canvas.remove(object);
      // 编辑
      }else if(key =="edit"){
          var object = contextMenuItems[key].data;
          clearZjData();
          // 组件类型
          $("#zjlx").val(object.type);
          // 组件ID
          $("#zjid").val(object.id);
          // X轴位置
          $("#xPosition").val(object.left.toFixed(0));
          // Y轴位置
          $("#yPosition").val(object.top.toFixed(0));
          // 组件高度
          $("#zjgd").val(object.height.toFixed(0));
          // 组件宽度
          $("#zjkd").val(object.width.toFixed(0));
          // 线条粗细
          $("#xtcx").val(object.strokeWidth.toFixed(0));
          // 线条颜色
          $("#xtys").val(object.stroke);
          // 填充颜色
          $("#tcys").val(colorRGB2Hex(object.fill));
          
          if(!editWin){
              editWin=$.ligerDialog.open({
                  target:$("#editWin"),
                  height: 450,
                  width:500,
                  title: "组件编辑",
                  isHidden:true,
                  isResize:false,
                  showMax:false,
                  showMin:false
              });
              
          }else{
              editWin.show();
          }
      // 复制+粘贴
      }else if(key == "copy"){
          // 优化下选中的全部复制过去
          canvas.getActiveObject().clone(function(cloned){
              cloned.set({
                  left: cloned.left + 20,
                  top: cloned.top + 20,
                  evented: true,
              });
              cloned.id = getNewId();
              // 给个新ID
              canvas.add(cloned);
              canvas.setActiveObject(cloned);
          })
      }else if(key =="property"){
          // 设置X坐标
//           canvas.getActiveObject().set('left',300);
//           // 设置Y坐标
//           canvas.getActiveObject().set('top',200);
//           canvas.getActiveObject().selectable=true;
          //object.setTop(200),
          //canvas.getActiveObject().scale(0.1);
          canvas.renderAll();
          //alert(canvas.getActiveObject().width);
          //alert(canvas.getActiveObject().height);
      }
  }
}
function clearZjData(){
    // 组件类型
    $("#zjlx").val("");
    // 组件ID
    $("#zjid").val("");
    // X轴位置
    $("#xPosition").val("");
    // Y轴位置
    $("#yPosition").val("");
    // 组件高度
    $("#zjgd").val("");
    // 组件宽度
    $("#zjkd").val("");
    // 线条粗细
    $("#xtcx").val("");
    // 线条颜色
    $("#xtys").val("");
    // 填充颜色
    $("#tcys").val("");
}

function editCancel(){
    editWin.hide();
}

function editCommit(){
    var obj = canvas.item(0);
    alert(obj);
    obj.set('left', Number($("#xPosition").val()));// X轴坐标
    obj.set('top', Number($("#yPosition").val()));// Y轴坐标
    obj.set('height', Number($("#zjgd").val()));// 高度
    obj.set('width', Number($("#zjkd").val()));// 宽度
    obj.set('stroke', $("#xtys").val());// 线条颜色
    obj.set('fill', $("#tcys").val());// 填充色
    //刷新画面
    //canvas.discardActiveObject();
    obj.setCoords();
    canvas.requestRenderAll();
    //清除内容
    clearZjData();
    // 隐藏编辑窗口
    editWin.hide();
}

// 选中的目标组合
function btnGroup(){
    if (!canvas.getActiveObject()) {
        return;
    }
    if (canvas.getActiveObject().type !== 'activeSelection') {
       return;
    }
    canvas.getActiveObject().toGroup(0);
    canvas.requestRenderAll();
}

// 选中目标解组
function btnUnGroup(){
    if (!canvas.getActiveObject()) {
        return;
    }
    if (canvas.getActiveObject().type !== 'group') {
        return;
    }
    canvas.getActiveObject().toActiveSelection();
    canvas.requestRenderAll();
}

// 全部组合（用于保存时进行的自动操作，组件是作为整体出现而不是单独的一个一个部件出现）
function GroupAll(){
    // 组件都是组合起来的控件（不可拆分性）
    var group = new fabric.Group(canvas.getObjects());
    group.id = getNewId();
    // 清除原有的添加新的
    canvas.clear().renderAll();
    canvas.add(group);
}

function getNewId(){
    // 组合了相当于新组件给个ID（便于存库）
    var now = new Date();
    var rnd = "";
    for(var i=0;i<5;i++) {
        rnd+=Math.floor(Math.random()*10);
    }
    return now.getTime() + rnd;
}

function getLeftList(data){
    // 开始加载左侧数据
    $.ajax({
        type: 'post',
        url: 'getLeftList',
        dataType: "json",
        data: data,
        success: function(data) {
            $("#leftLoading").hide();
            // 成功加载到数据
            if(data&&data.retCode==100){
                listData = data.retObj;
                // 加载基本组件
                $("#html0").html(loadHtml(listData,'0'));
                // 加载常用组件
                $("#html1").html(loadHtml(listData,'1'));
                // 加载常用大组件
                $("#html2").html(loadHtml(listData,'2'));
                
                $("#leftNone").hide();
                $("#leftList").show();
            // 没有加载到数据
            }else{
                $("#leftNone").show();
                $("#leftList").hide();
            }
        }
    });
}

// 左侧采用拼接HTML的方式进行拼接
function  loadHtml(obj,type){
    var begin = '<li><table class="default" style="text-align: center;">';
    var end = '</table></li>';
    var html = "";
    // 计数
    var temp = 0;
    obj.forEach(function (item,index,input) {
        // 如果类型一致
        if(type == input[index].partType){
            //2个一行进行拼接
            if(temp%2==0){
                html+= '<tr>'
            }
            var imgsrc =input[index].thumbnail;
            // 空值放个404图片显示下不然前台报错太多
            if(imgsrc == null || imgsrc==""){
                imgsrc = "/hmdl/static/images/hmdl/404.png";
            }
            html+='<td style="width:50%" onclick="clickMenu(\''+input[index].partType+'\',\''+input[index].partName+'\','+input[index].id+')">';
            html+='<img src="'+imgsrc+'" style="width:32px;height:32px"/><br/><span style="font-weight:bolder;">'+input[index].partName+'</span></td>';
            if(temp%2==1){
                html+= '</tr>'
            }
            temp++;
        }
    });
    // 如果为空则显示空的提示信息
    if(html==""){
        html = '<li style="background-color:white"><img style="width:30%;margin-top: 10px" src="/hmdl/static/images/hmdl/404.png"/>';
        html += '<div style ="color:#515151;font-size: 12px"><br/>非常抱歉<br/>未能检索到您的组件信息<br/></div><br/></li>';
    // 有值再加上首位
    }else{
        html = begin+html+end;
    }
    return html;
}

function createComponents(type){
    // 隐藏创建窗口打开画布窗口
    var name= "创建";
    if(type==0){
        name+= "基本组件";
    }else if(type==1){
        name+= "常用组件";
    }else if(type==2){
        name+= "常用大组件";
    }
    $(".l-layout-center .l-layout-header").html(name);
    $("#model1").show();
    $("#model0").hide();
    // 转为可编辑状态
    clearFlg = false;
    showModel = 1;
    $("#initWin").hide();
    $("#cWin").show();
}

function colorRGB2Hex(color) {
    var hex ="";
    if(color!=""&&color!=null){
        if(color.indexOf("#") != -1 ){
            hex = color;
        }else{
            var rgb = color.split(',');
	        var r = parseInt(rgb[0].split('(')[1]);
	        var g = parseInt(rgb[1]);
	        var b = parseInt(rgb[2].split(')')[0]);
	        hex = "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
        }
    }
    return hex;
 }
</script>
</body>
</html>
