<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script> 
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">

<div id="layout1">
	<!-- 左侧树 -->
	<div id="leftWin" position="left" title="变电所一览" style="height:calc(100% - 40px);overflow: auto; ">
		<!-- 变电所查询条件部分 -->
		<div id="org_search" style="margin-left:8px;margin-top:0px;">
			<ul>
				<li class="label" style="float: left;padding-right: 0">
					<input type="text" placeholder="输入名称进行搜索" style="text-indent:5px;height:23px;border-radius: .25em;width:60%;margin-left:0;margin-top: 5px" name="search_substationName" id="search_substationName"  />					
					<button type="button" id="org_btnSearch" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x"><i class="fa fa-search"></i></button>
					<button type="button" id="org_expend" onclick="expend()" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x"><i class="fa fa-plus"></i></button>
					<button type="button" id="org_unexpend" onclick="unexpend()" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x;display:none"><i class="fa fa-minus"></i></button>
				</li>
			</ul>
		</div>
		<div id="orgInfoTree"></div><br/><br/>
		<input type="hidden" id="org_sn" name="org_sn" value=""/>
		<input type="hidden" id="org_name" name="org_name" value=""/>
	</div>
	<div id="rightWin" position="center"  title="数据配置" >
		<!-- 查询条件部分 -->
		<div id="grid_search" style="margin-left:8px;margin-top:20px;">
		    <form name="searchForm" id="searchForm">
		        <ul>
		            <li class="label">
		                &emsp;变电站名称：<input type="text" id="search_substationName" name="substationName" placeholder="搜索变电站" style="text-indent:5px;height:23px;border-radius: .25em;"/>
						&emsp;简称：<input type="text" id="search_shortname" name="shortname" placeholder="搜索简称" style="text-indent:5px;height:23px;border-radius: .25em;"/>
						&emsp;ip地址：<input type="text" id="search_ipAddr" name="ipAddr" placeholder="搜索ip地址" style="text-indent:5px;height:23px;border-radius: .25em;"/>
						&emsp;<button id="btnSearch" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
		            </li>
		        </ul>
		    </form>
		</div>
		<br/>
		<!-- 列表部分 -->
		<div id="grid"></div>
	</div>
</div>
<script type="text/javascript">
var grid;
var searchWin;
$(function(){
	//布局设置
	$("#layout1").ligerLayout({
		leftWidth: 200,
	});
	//变电所树检索
	substationSearch();
	initGrid();
    // 绑定查询按钮
	$("#btnSearch").click(function () {
		var parms = serializeObject($("#searchForm"));
   	    for(var p in parms){
   	        grid.setParm(p,parms[p].trim());
   	    }
   	    grid.loadData();
   	});
	    function initGrid(){
	        grid=$("#grid").ligerGrid({
	            title:'&nbsp;&nbsp;<span style="color:#386792">站点库数据信息一览</span>',
	            headerImg:'/hmdl/static/images/list.png',
	            columns: [
					{ display: '变电站名称', name: 'substationName',width:150},
					{ display: '简称', name: 'shortname',width:120},
	                { display: 'ip地址', name: 'ipAddr',width:120},
	                { display: '端口', name: 'port',width:120},
	                { display: '遥信数量', name: 'yxAmount',width:120},
	                { display: '遥测数量', name: 'ycAmount',width:120},
	                { display: '回路', name: 'circuit',width:120},
	                { display: '更新时间', name: 'updateDate',dateFormat: "yyyy-MM-dd hh:mm:ss",width:150},
	            ],
	            toolbar: {
	                items: [
						{ text: '刷新',click: function(){grid.reload();}, icon: 'fa fa-refresh',color:'primary' }
					]
	         	},
	            dataAction: 'server',
	            url:'../stationData/search',
	        	root:'data',
	          	record:'count',
	          	height: '97%',
	          	pageSize: 10,
	            parms:[{name:"isConfig",value:'3'}],
	        	enabledSort:true,
	            sortnameParmName:'sortname',
	            sortorderParmName:'sortorder',
	          	pageSizeOptions:[10,15,20,25,30,35],
	          	checkbox: false,
	          	rownumbers: true,
	        	onAfterShowData: function ()
	            { 
	                grid.collapseAll();
	            }
	        });
	    }
});
//绑定变电所查询按钮
$("#org_btnSearch").click(function () {
	substationSearch('changeTitle');
});

//绑定变电所收缩
$("#org_btn_collapse").click(function () {
	orgTree.collapseAll();
});

//展开
function expend(){
    orgTree.expandAll();
    $("#org_unexpend").show();
    $("#org_expend").hide();
}
//折叠
function unexpend(){
    orgTree.collapseAll();
    $("#org_unexpend").hide();
    $("#org_expend").show();
}

</script>
</body>
</html>
