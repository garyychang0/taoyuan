<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script> 
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">

<div id="layout1">
	<!-- 左侧树 -->
	<div position="left" title="变电所一览" style="height:calc(100% - 40px);overflow: auto; ">
		<!-- 变电所查询条件部分 -->
		<div id="org_search" style="margin-left:8px;margin-top:0px;">
			<ul>
				<li class="label" style="float: left;padding-right: 0">
					<input type="text" placeholder="输入名称进行搜索" style="text-indent:5px;height:23px;border-radius: .25em;width:60%;margin-left:0;margin-top: 5px" name="search_substationName" id="search_substationName"  />					
					<button type="button" id="org_btnSearch" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x"><i class="fa fa-search"></i></button>
					<button type="button" id="org_expend" onclick="expend()" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x"><i class="fa fa-plus"></i></button>
					<button type="button" id="org_unexpend" onclick="unexpend()" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x;display:none"><i class="fa fa-minus"></i></button>
				</li>
			</ul>
		</div>
		<div id="orgInfoTree"></div><br/><br/>
		<input type="hidden" id="org_sn" name="org_sn" value=""/>
		<input type="hidden" id="org_name" name="org_name" value=""/>
	</div>
	<div position="center"  title="变电所" >
		<!-- 标题部分 -->
		<h3 class="hmdl_title_h3">图形制作
		    <span class="hmdl_title_span">
		        &nbsp;/&nbsp;站点图库
		    </span>
		</h3>
		<hr class="hmdl_title_hr"/>
		<br/>
	</div>
</div>
<script type="text/javascript">
var grid;
var searchWin;
$(function(){
	//布局设置
	$("#layout1").ligerLayout({
		leftWidth: 200,
	});
	//变电所树检索
	substationSearch('changeTitle');
});
//绑定变电所查询按钮
$("#org_btnSearch").click(function () {
	substationSearch('changeTitle');
});

//绑定变电所收缩
$("#org_btn_collapse").click(function () {
	orgTree.collapseAll();
});
//获取选择变电站详细
function getSubstationDetail(id) {
    var row = '';
    $.ajax({
        type: 'get',
        url: "../stationData/search",
        data: { substationSn: id },
        cache: false,
        dataType: 'json',
        async: false,
        success: function (data) {
            row =  data.data[0];
        }
    });
    return row;
}
//标题栏标题
function changeTitle(id){
    var data = getSubstationDetail(id);
    $(".l-layout-center .l-layout-header").html(
            data.substationName+'&emsp;<a style="color:#428bca">缩写简称：['+
            data.shortname+']</a>'+'&emsp;<a style="color:#428bca">ip地址：['+data.ipAddr+']</a>'+'&emsp;<a style="color:#428bca">端口号：['+data.port+']</a>'
            );
}
//展开
function expend(){
    orgTree.expandAll();
    $("#org_unexpend").show();
    $("#org_expend").hide();
}
//折叠
function unexpend(){
    orgTree.collapseAll();
    $("#org_unexpend").hide();
    $("#org_expend").show();
}

</script>
</body>
</html>
