<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script> 
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">

<!-- 标题部分 -->
<h3 class="hmdl_title_h3">学校管理
    <span class="hmdl_title_span">
        &nbsp;/&nbsp;部门管理
    </span>
</h3>
<hr class="hmdl_title_hr"/>

<!-- 查询条件部分 -->
<div id="grid_search" style="margin-left:8px;margin-top:20px;">
    <form name="searchForm" id="searchForm">
        <ul>
            <li class="label">
                &emsp;部门编号：<input type="text" id="search_user_sn" name="userSn" placeholder="搜索部门编号" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                &emsp;部门名称：<input type="text" id="search_department_name" name="departmentName" placeholder="搜索部门名称" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                &emsp;<button id="btnSearch" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
            </li>
        </ul>
    </form>
</div>
<br/>

<!-- 列表部分 -->
<div id="grid"></div>

<!-- 增改部分 -->
<div id="edit" style="display:none;">
    <form name="saveForm" id="saveForm" >
    <input type="hidden" id="id" name="id" />
    <table  class="default">
        <tr style="height:50px" >
            <td colspan="4" align="center" style="color:#fff;font-size: 15px;font-weight:bold;background-color:rgb(74, 139, 194);">
            <i class="fa fa-address-book"></i>&emsp;部门信息</td>
        </tr>
        <tr>
            <td align="right">&emsp;部门名称：&emsp;<span style="color:red">*</span></td>
            <td align="left" colspan="3">
                <input id="departmentName" name="departmentName"  style="text-indent:5px;width:180px" placeholder="请输入部门名称"/>
            </td>
         </tr>
         <tr>
            <td align="right">&emsp;选择上级部门：&emsp;</td>
            <td align="left" colspan="3">
                <input id="edit_type" name="parentSn" type="hidden" style="border: 0px"/>
            </td>
        </tr>
         <tr>
            <td align="right">&emsp;负责人：&emsp;</td>
            <td align="left" colspan="3">
                <input id="contactUser" name="contactUser"  style="text-indent:5px;width:180px" placeholder="请输入负责人"/>
            </td>
         </tr>
         <tr>
            <td align="right">&emsp;部门联系电话：&emsp;</td>
            <td align="left" colspan="3">
                <input id="contactTel" name="contactTel"  style="text-indent:5px;width:180px" placeholder="请输入手机号码"/>
            </td>
         </tr>
         <tr>
            <td align="right">&emsp;部门所在位置：&emsp;</td>
            <td align="left" colspan="3">
                <input id="address" name="address"  style="text-indent:5px;width:180px" placeholder="请输入位置"/>
            </td>
         </tr>
         <tr>
            <td align="right">&emsp;性质：&emsp;</td>
            <td align="left" colspan="3">
                <input id="type" name="type"  style="text-indent:5px;width:180px" placeholder="请输入性质"/>
            </td>
         </tr>
         <tr>
            <td align="right">&emsp;备注：&emsp;</td>
            <td align="left" colspan="3">
                <input id="remark" name="remark"  style="text-indent:5px;width:180px" placeholder="请输入备注"/>
            </td>
         </tr>
    </table>
    <table align="center" style="margin-top: 10px">
        <tr>
            <td>
                <button type="submit" class="btn btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;提交&nbsp;&nbsp;</button>
                &emsp;
                <button type="button" class="btn btn-warning" onclick="editCancel()"><i class="fa fa-close"></i>&nbsp;&nbsp;返回&nbsp;&nbsp;</button>
            </td>
        </tr>
    </table>
    </form>
</div>

<script type="text/javascript">
var grid;
$(function(){
    initGrid();

    // 绑定查询按钮
	$("#btnSearch").click(function () {
		var parms = serializeObject($("#searchForm"));
   	    for(var p in parms){
   	        grid.setParm(p,parms[p].trim());
   	    }
   	    grid.loadData();
   	});
    
    function initGrid(){
        grid=$("#grid").ligerGrid({
            title:'&nbsp;&nbsp;<span style="color:#386792">部门信息一览</span>',
            headerImg:'/hmdl/static/images/list.png',
            columns: [
					{ display: '部门名称', name: 'departmentName',id:'treeNode',width:200,align:'left'},
					{ display: '部门编号', name: 'departmentSn',width:100},
					{ display: '负责人', name: 'contactUser',width:100},
					{ display: '部门联系电话', name: 'contactTel',width:100},
					{ display: '部门所在位置', name: 'address',width:150},
					{ display: '性质', name: 'type',width:100},
					{ display: '备注', name: 'remark',width:100},
					{ display: '创建时间', name: 'createDate',width:150},
					{ display: '更新时间', name: 'updateDate',width:150},
				],
            toolbar: {
                items: [
					<%=request.getAttribute("btn")%>,
					{ text: '收缩',click: function(){grid.collapseAll();}, icon: 'fa fa-minus-square-o',color:'primary' },
					{ text: '展开',click: function(){grid.expandAll();}, icon: 'fa fa-plus-square-o',color:'info' },
					{ text: '刷新',click: function(){grid.reload();}, icon: 'fa fa-refresh',color:'primary' },
					]

            },
            dataAction: 'server',
            url:'search',
            root:'data',
            record:'count',
            height: '97%',
            pageSize: 10,
            rownumbers: true,
           	enabledSort:false,
           	pageSizeOptions:[15,20,25,30,35],
           	parms:function(){return serializeObject($("#searchForm"));},
           	tree: { columnId: 'treeNode',idField: 'departmentSn',parentIDField: 'parentSn'},
           	checkbox: true,
           	autoCheckChildren:false,
           	rownumbers: true,
           	usePager:false
        });
    }
    $(function () {
	    $("#saveForm").validator({
	        ignore: ":hidden", theme: "yellow_bottom", timely: 1, stopOnError: true,
	        rules: { digits: [/^\d+(\.\d+)?$/, '{0}必须为数字！'],  
	           		 email:[/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/,'{0}格式不正确！']
	        },
	        fields: {
	            departmentName: '部门名称:required,length[1~30]'
	        },
	        valid: function (form) {formSave(form);}
	    });
})
});

//新增OR编辑处理
function operate(type){
 resetAll();
 var rows = grid.getSelectedRows();
 $.ajax({
     type : 'post',
     url : 'getDepartment',
     cache : false,
     dataType : 'json',
     success : function(data) {
         $("#edit_type").ligerComboBox({
	                    data:data.retObj,
	                    valueField : 'key',
	                    textField: 'value',
	                    isMultiSelect: false,
	                    width:180,
	            	});
         $("#edit_type_txt").css("height", "25.5px");
     },
     error : function() {
         $.ligerDialog.error("加载部门失败！", "提示");
     }
 });
 if ((rows == null || rows.length!=1) && type != 1 ) {
     $.ligerDialog.warn("请选择一条记录！","提示");
     return;
 };
 if(type == 2){
     showOrHideEdit(2,rows[0].id);
 }else{
     showOrHideEdit(1);
 }
}
//保存表单数据
function formSave(form) {
 var data = serializeObject($("#saveForm"));
 $.ajax({
     type: 'post',
     url: "save",
     data: data,
     cache: false,
     dataType: 'json',
     success: function (data) {
         if (data && data.retCode == 100) {
             $.ligerDialog.success('保存成功！', "提示", function (opt) {
                 $("#grid").show();
                 $("#grid_search").show();
                 $("#edit").hide();
                 grid.reload();
             });
         } else {
             var text = "";
             if (data.errors && data.errors.length) {
                 for (var i = 0; i < data.errors.length; i++) {
                     text = text + "<br/>" + data.errors[i];
                 }
             }
             $.ligerDialog.error(text, "提示");
         }
     }
 });
}
function showOrHideEdit(type,id){
 // 0显示编辑页面
 if(type==1){
     $("#grid").hide();
     $("#grid_search").hide();
     $("#edit").show();
 // 1隐藏编辑页面
 }else if(type==2){
     getDetail(id);
 }else{
     $("#grid").show();
     $("#grid_search").show();
     $("#edit").hide();
 }
}
//获取表单明细数据
function getDetail(id) {
 $.ajax({
     type: 'get',
     url: "getDetail",
     data: { id: id },
     cache: false,
     dataType: 'json',
     success: function (data) {
         loadData(data.retObj);
         $("#grid").hide();
         $("#grid_search").hide();
         $("#edit").show();
	     $("#edit_type").ligerGetComboBoxManager().selectValue(data.retObj['parentSn']);
     }
 });
}
//删除
function del(){
	var rows = grid.getSelectedRows();
	var ids="";
	if (rows == "" || rows.length!=1) {
		$.ligerDialog.warn("请选择一条记录！","提示");
		return;
	}
	for(var i=0 ; i < rows.length ; i++){
    	if(i>0){
				ids += "|";
    	}
    	ids += rows[i].id + "," + rows[i].updateDate;
	}
	$.ligerDialog.confirm("确定要删除选中记录？","提示", function(confirm){
 	if(confirm){
  		$.post("doDelete",{ids:ids},function(data){
         		if (data.retCode==100) {
         			$.ligerDialog.success('删除成功！',"提示",function(opt){
                 		grid.reload();
              	});}
         		else if(data.retCode==-700){
                   		$.ligerDialog.error("删除失败！存在", "提示");
                }else{
                   		$.ligerDialog.error("删除失败！", "提示");
                }
        },"json")
		}
	});
}
function editCancel(){
 showOrHideEdit(3);
 resetAll();
}

function resetAll(){
 // FORM清空
 $("#saveForm")[0].reset();
 // 隐藏项目清空
 $("#id").val("");
 $("#edit_type").val("");
}
</script>
</body>
</html>
