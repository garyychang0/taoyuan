<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script> 
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">

<div id="layout1">
	<div id="rightWin" position="center"  title="变电所" >
		<!-- 查询条件部分 -->
		<div id="grid_search" style="margin-left:8px;margin-top:20px;">
		    <form name="searchForm" id="searchForm">
		        <ul>
		            <li class="label">
		                &emsp;制图名称：<input type="text" id="search_canvasName" name="canvasName" placeholder="搜索制图名" style="text-indent:5px;height:23px;border-radius: .25em;"/>
						&emsp;制图类型：
		                <select  name="canvasType" id="search_canvasType">
							<option value="">请选择</option>
							<option value="0">电力总图</option>
							<option value="1">图形部件</option>
							<option value="2">表格部件</option>
							<option value="3">文本部件</option>
						</select>
						&emsp;<button id="btnSearch" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
		             </li>
		        </ul>
		    </form>
		</div>
		<br/>
		<!-- 列表部分 -->
		<div id="grid"></div>
	</div>
</div>
<script type="text/javascript">
var grid;
var searchWin;
var dialog = frameElement.dialog;
var dialogData = dialog.get('data');
$(function(){
	//布局设置
	$("#layout1").ligerLayout({
	});
	initGrid();
    // 绑定查询按钮
	$("#btnSearch").click(function () {
		var parms = serializeObject($("#searchForm"));
   	    for(var p in parms){
   	        grid.setParm(p,parms[p].trim());
   	    }
   	    grid.loadData();
   	});
    function initGrid(){
        grid=$("#grid").ligerGrid({
            title:'&nbsp;&nbsp;<span style="color:#386792">站点图库一览</span>',
            headerImg:'/hmdl/static/images/list.png',
            columns: [
                { display: '操作', width:100,
                        render: function (item){
                           return "<div onclick=openNewTap("+item.id+",'"+item.canvasName+"') style='float:left;cursor:pointer;width:100%;height:28px;line-height:28px;background-color:#5bc0de;color:white'><a class='fa fa-search-plus' style='vertical-align:0!important;'></a>预览</div>"; 
                        }
                },
				{ display: '站点编号', name: 'substationSn',hide:true,width:120},
				{ display: '站点名称', name: 'substationName',width:200},
				{ display: '制图名称', name: 'canvasName',width:200},
				{ display: '类型', name: 'canvasType',width:100,
					render: function (item){
						if (item.canvasType == 0){
						    return "电力总图";
						}else if(item.canvasType == 1){
						    return "图形部件";
						}else if(item.canvasType == 2){
						    return "表格部件";
						}else if(item.canvasType == 3){
						    return "文本部件";
						}
					}
				},
		        { display: '背景色', name: 'canvasBackgroundColor',width:50,
					render: function (item){
					    return "<div style='width:100%;height:28px;line-height:28px;background-color:"+item.canvasBackgroundColor+";'></div>";
					}
                },
                { display: '画板高度', name: 'canvasHeight',width:120},
                { display: '画板宽度', name: 'canvasWidth',width:120},
                { display: '更新时间', name: 'updateDate',dateFormat: "yyyy-MM-dd hh:mm:ss",width:150},
            ],
            dataAction: 'server',
            url:'../pictureStation/search',
            root:'data',
            record:'count',
            height: '97%',
            parms:[{name:"substationSn",value:dialogData.substationSn}],
            pageSize: 5,
            pageSizeOptions:[5,10, 20, 30, 50],
            checkbox: false,
            rownumbers: true,
        });
    }
});

//展示预览
function show(){
  var rows = grid.getSelectedRows();
  if (rows == null || rows.length!=1) {
      $.ligerDialog.warn("请选择一条记录！","提示");
      return;
  };
  parent.parent.openTab({
      tabid: 'preview'+rows[0].id,
      text: '预览'+rows[0].canvasName,
      url: "/hmdl/pictureStation/preview?parentId=301&id="+rows[0].id
  });
}

//画布预览
var preview = 1;//tab新增id 
function openNewTap(id,title){
    if(!title){
        title = document.getElementById('configCanvasName').value;
    }
    parent.parent.openTab({
        tabid: 'preview'+id,
        text: '预览'+title,
        url: "/hmdl/pictureStation/preview?parentId=301&id="+id
    });
}
</script>
</body>
</html>
