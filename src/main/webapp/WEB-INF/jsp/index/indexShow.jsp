<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" /> 
	<link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/indexShow/<%=request.getAttribute("themes")%>.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/echarts/echarts.js" type="text/javascript"></script> 
    <script src="/hmdl/static/js/My97DatePicker/WdatePicker.js" type="text/javascript"></script> 
    <!-- 引入 vintage 主题 -->
	<script src="/hmdl/static/js/echarts/themes/dark.js"></script>
	<script src="/hmdl/static/js/echarts/themes/chalk.js"></script>
	<script src="/hmdl/static/js/echarts/themes/halloween.js"></script>
	<script src="/hmdl/static/js/echarts/themes/wonderland.js"></script>
	<script src="/hmdl/static/js/echarts/themes/essos.js"></script>
	<!-- 引入 百度地图 -->
	<script type="text/javascript" src="http://api.map.baidu.com/api?v=3.0&ak=i7Ai8zur9Hl8kjfCkRkwnkqfB3Y0SSVR"></script>
	<script type="text/javascript" src="http://api.map.baidu.com/library/InfoBox/1.2/src/InfoBox_min.js"></script>
	<script src="/hmdl/static/js/indexShow/mapThemes.js"></script>
</head>
<!-- body部分 -->
<body style="background-color:#F9F9F9;">
<div class="show_body">
	<!-- 标题部分 -->
	<div class="show_title">
		<h3 class="show_title_h3">华明电力运维/首页
		</h3>
	</div>
	<div class="show_menu">
		<input id="search_name" style="width:100px" placeholder="名称查找" />
		<select id="themeId">
			<option value="">默认</option>
			<option value="1">夜间</option>
		</select>
		<a>当前在线变电所数量</a>100		
		<select id="substationStatus">
			<option value="all" selected="selected">全部</option>
			<option value="1">在线</option>
			<option value="0">离线</option>
		</select>
		<i class="fa fa-search fa-lg" onclick="getPoint()" style="margin-left:2px"></i>
	</div>
	<div class="show_main">
		<div class="show_main_left">
			<div class="show_map" id="container">
			</div>
			<div class="show_alarm">
				<div class="show_alarm_log">
					<div id="alarm_log_list">
					    <ul id="alarm_log_list_ul">
					        
					    </ul>
					</div>
				</div>
				<div class="show_alarm_other">
					<div id="chart2"></div>
				</div>
			</div> 
		</div>
		<div class="show_main_right">
			<div class="show_count_top">
				<div class="row_total">
					<div class="row_total_col">
						<div class="row_total_col_num"><a id="count_substation">--</a></div>
						<div class="row_total_col_text"><a>变电所数量</a></div>
					</div>
					<div class="row_total_col">
						<div class="row_total_col_num"><a id="count_ep" >--</a></div>
						<div class="row_total_col_text"><a>变电所总用电量(kwh)</a></div>
					</div>
					<div class="row_total_col">
						<div class="row_total_col_num"><a id="count_people">--</a></div>
						<div class="row_total_col_text"><a>平台在线人数</a></div>
					</div>
					<div class="row_total_col">
						<div class="row_total_col_num"><a id="count_substation">--</a></div>
						<div class="row_total_col_text"><a>变电所总用电量</a></div>
					</div>
					<div class="row_total_col">
						<div class="row_total_col_num"><a id="count_task_reform">--</a></div>
						<div class="row_total_col_text"><a>改造任务数量</a></div>
					</div>
					<div class="row_total_col">
						<div class="row_total_col_num"><a id="count_task_patrol">--</a></div>
						<div class="row_total_col_text"><a>巡查任务数量</a></div>
					</div>
					<div class="row_total_col">
						<div class="row_total_col_num"><a id="count_task_duty">--</a></div>
						<div class="row_total_col_text"><a>值班任务数量</a></div>
					</div>
					<div class="row_total_col">
						<div class="row_total_col_num"><a id="count_task_other">--</a></div>
						<div class="row_total_col_text"><a>其他任务数量</a></div>
					</div>
				</div>
			</div>
			<div class="show_count_center">
				<div id="chart" ></div>
			</div>
			<div class="show_count_bottom">
				<div id="chart1"></div>
			</div>
		</div>
	</div>	
</div>
<div id="alarm_dialog" style="display: none;color: gray;font-family: 黑体">
	<!-- 确认处理方式弹出窗口 -->
	<form id="alarmForm" name="alarmForm" style="border-radius: 8px;border-collapse: separate;border-spacing: 0;margin-top: 5px;">
		<table align="center" border="0" cellpadding="0" width="100%" style="height: 100%;padding: 5;">
			<tr>
				<td width="40%" style="font-weight: bold;font-size: 15px" align="center">报警类型：</td>
				<td><input readonly="readonly" style="width:180px;height:24px;" id="open_alarm_type" type="text" name="alarmType"/></td>
			</tr>
			<tr>
				<td style="font-weight: bold;font-size: 15px" align="center">报警时间：</td>
				<td><input readonly="readonly" style="width:180px;height:24px;" id="open_alarm_time" type="text" name="callTime"/></td>
			</tr>
			<tr> 
				<td style="font-weight: bold;font-size: 15px" align="center">报警内容：</td>
				<td><input readonly="readonly" style="width:180px;height:24px;" id="open_alarm_equName" type="text" name="equName"/></td>
			</tr>
			<tr>
				<td width="40%" style="font-weight: bold;font-size: 15px" align="center">位置：</td>
				<td><input readonly="readonly" style="width:180px;height:24px;" id="open_alarm_local" type="text" name="local"/></td>
			</tr>
			<tr>
				<td style="font-weight: bold;font-size: 15px"  align="center">处理报警方式：</td>
				<td>
					<span style="height: 25px;font-size: 15px;display: block;margin-top: 10px"><input type="radio" id="open_process_mode_one" name="methods" value="1" style="cursor: pointer"/><span onclick="selectSapn(1)" style="cursor: pointer"> 报警信息确认</span></span>
					<span style="height:25px;font-size: 15px;display: block"><input type="radio" id="open_process_mode_two" name="methods" value="2" style="cursor: pointer"/><span onclick="selectSapn(2)" style="cursor: pointer"> 电话沟通确认</span></span>
				</td>
			</tr>
		</table>
	</form>
</div>
<script>
var infoBox;
var styleJson="";
var chartStyle="";
$(function () {
	if('${themes}'=="dark"){
	    styleJson=mapThemes_styleJson_dark;
	    chartStyle="halloween";
	    $("#themeId").val(1);
	}else{
	    styleJson = "";
	}
    getPoint();
    doChart();
    doChart1();
    getAlarmCounts();//加载报警信息
    scroll();
    doCount();
    OnlineUser();
})
$('#search_name').bind('keypress',function(event){    
         if(event.keyCode == 13){  
             getPoint();
         }  
});
function initMap(longitude,latitude){
    MAP = new BMap.Map("container");
    if(longitude!=null){
        //创建地图实例  
        var point = new BMap.Point(longitude,latitude);
    }else{
        //创建地图实例  
        var point = new BMap.Point(120.2558,31.913902);
    }
    //创建点坐标  
    MAP.centerAndZoom(point, 15);
    MAP.enableScrollWheelZoom(true);
    MAP.addControl(new BMap.NavigationControl()); 
    if(styleJson!=""){
    MAP.setMapStyleV2({styleJson:styleJson});
    }
}

function getPoint(){
    var search_data = {substationName:$("#search_name").val()};
    $.ajax({
        type: 'post',
        url: "search",
        cache: true,
        dataType: 'json',
        data:search_data,
        success: function (result) {
           var data = result.data;
           if(data.length>0){
               initMap(data[0]['longitude'],data[0]['latitude']);
           }else{
               initMap();
           }
           for(var i=0;i<data.length;i++){
               var point = new BMap.Point(data[i]['longitude'],data[i]['latitude']);
               var substationSn = data[i]['substationSn'];
               var substationName = data[i]['substationName'];
               mapMark(point,substationSn,substationName);
           }
        }
    });
}
function mapMark(point,substationSn,substationName){
    if($("#substationStatus").val()==0){
        var myIcon = new BMap.Icon("/hmdl/static/images/marker2.png", new BMap.Size(32, 45), {    
            // 指定定位位置。   
            // 当标注显示在地图上时，其所指向的地理位置距离图标左上    
            // 角各偏移10像素和25像素。您可以看到在本例中该位置即是   
            // 图标中央下端的尖角位置。    
            anchor: new BMap.Size(0, 0),    
            // 设置图片偏移。   
            // 当您需要从一幅较大的图片中截取某部分作为标注图标时，您   
            // 需要指定大图的偏移位置，此做法与css sprites技术类似。    
            //imageOffset: new BMap.Size(0, 10 * 25)   // 设置图片偏移    
        });    
    }else{
        var myIcon = new BMap.Icon("/hmdl/static/images/marker.png", new BMap.Size(32, 45), {    
            // 指定定位位置。   
            // 当标注显示在地图上时，其所指向的地理位置距离图标左上    
            // 角各偏移10像素和25像素。您可以看到在本例中该位置即是   
            // 图标中央下端的尖角位置。    
            anchor: new BMap.Size(0, 0),    
            // 设置图片偏移。   
            // 当您需要从一幅较大的图片中截取某部分作为标注图标时，您   
            // 需要指定大图的偏移位置，此做法与css sprites技术类似。    
            //imageOffset: new BMap.Size(0, 10 * 25)   // 设置图片偏移    
        }); 
    }     
    // 创建标注对象并添加到地图   
    var marker = new BMap.Marker(point, {icon: myIcon});    
    MAP.addOverlay(marker);
    marker.addEventListener("click",
	    function() {
			if(infoBox){
			    infoBox.close();
			}
	        var content = '<div class="show_map_info_title">'+substationName+
	        '</div><div onclick=getAlarmCounts("'+substationSn+'") class="btn btn-success show_map_info_menu "><i class="fa fa-warning" style="vertical-align:0!important;"></i></div>'+
	        '<div onclick=openPreview("'+substationSn+'") class="btn btn-default show_map_info_menu"><i class="fa fa-photo" style="vertical-align:0!important;"></i></div>'+
	        '<div class="btn btn-warning show_map_info_menu"><i class="fa fa-area-chart" style="vertical-align:0!important;"></i></div>'+
	        '<div class="btn btn-info show_map_info_menu"><i class="fa fa-align-justify" style="vertical-align:0!important;"></i></div>'+
	        '<div class="btn btn-primary show_map_info_menu"><i class="fa fa-gear" style="vertical-align:0!important;"></i></div>';
	        var opts = {  
	                boxClass:"show_map_info",
	                offset: new BMap.Size(-10, -10),
	                closeIconUrl:"/hmdl/static/images/close.png"
	            }    
	        infoBox = new BMapLib.InfoBox(MAP,content, opts);
	        infoBox.open(marker);
	    }
   );
    
}
//选择变电所数据
function openPreview(substation_sn){
    // 转向网页的地址;
    var url='selectStationPicture';
    $.ligerDialog.open({
        //右上角的X隐藏，否则会有bug
        allowClose: false,
        height: 500,
        url: url,
        width: 800,
        name:'choseKey',
        title:'变电站选择',
        isResize:true,
        data: {
            substationSn: substation_sn
        },
        buttons: [
                    { text: '关闭', onclick: function (item, dialog){dialog.close();}}
                ]
    }); 
}
$("#themeId").change(function(){
    var themesVal;
    if($("#themeId").val()==1){
        themesVal="dark";
    }else{
        themesVal="indexShow";
    }
	location.replace("show?parentId=901&themes="+themesVal);
});
function doChart(){
	// 基于准备好的dom，初始化echarts图表
	var myChart = echarts.init(document.getElementById('chart'), chartStyle);
	option = {
	    tooltip: {
	        trigger: 'axis',
	        axisPointer: {
	            type: 'cross',
	            crossStyle: {
	                color: '#999'
	            }
	        }
	    },
	    legend: {
	        data:['水量']
	    },
	    xAxis: [
	        {
	            type: 'category',
	            data: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
	            axisPointer: {
	                type: 'shadow'
	            }
	        }
	    ],
	    yAxis: [
	        {
	            type: 'value',
	            name: '电量',
	            min: 0,
	            max: 250,
	            interval: 50,
	            axisLabel: {
	                formatter: '{value} kwh'
	            }
	        }
	    ],
	    series: [
	        {
	            name:'电量',
	            type:'bar',
	            data:[2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]
	        },
	        {
	            name:'电量',
	            type:'line',
	            yAxisIndex: 0,
	            data:[2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]
	        }
	    ]
	};
	// 为echarts对象加载数据 
	myChart.setOption(option);

}

function doChart1(){
	// 基于准备好的dom，初始化echarts图表
	var myChart1 = echarts.init(document.getElementById('chart1'), chartStyle);
	var weatherIcons = {
	        'Sunny': './data/asset/img/weather/sunny_128.png',
	        'Cloudy': './data/asset/img/weather/cloudy_128.png',
	        'Showers': './data/asset/img/weather/showers_128.png'
	    };

	    option = {
	        title: {
	            text: '总电量统计',
	            subtext: '虚构数据',
	            left: 'center'
	        },
	        tooltip : {
	            trigger: 'item',
	            formatter: "{a} <br/>{b} : {c} ({d}%)"
	        },
	        legend: {
	            // orient: 'vertical',
	            // top: 'middle',
	            bottom: 0,
	            left: 'center',
	            data: ['变电所1', '变电所2','变电所3','变电所4','变电所5']
	        },
	        series : [
	            {
	                type: 'pie',
	                radius : '65%',
	                center: ['50%', '50%'],
	                selectedMode: 'single',
	                data:[
	                    {
	                        value:1548,
	                        name: '变电所1'
	                    },
	                    {value:535, name: '变电所2'},
	                    {value:510, name: '变电所3'},
	                    {value:634, name: '变电所4'},
	                    {value:735, name: '变电所5'}
	                ],
	                itemStyle: {
	                    emphasis: {
	                        shadowBlur: 10,
	                        shadowOffsetX: 0,
	                        shadowColor: 'rgba(0, 0, 0, 0.5)'
	                    }
	                }
	            }
	        ]
	    };

	// 为echarts对象加载数据 
	myChart1.setOption(option);

}
function doChart2(noConfirm1,confirmed1,completed1,noConfirm2,confirmed2,completed2,noConfirm3,confirmed3,completed3){
	// 基于准备好的dom，初始化echarts图表
	var myChart2 = echarts.init(document.getElementById('chart2'), chartStyle);
	option = {
	    tooltip : {
	        trigger: 'axis',
	        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
	            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
	        }
	    },
	    legend: {
	        data: ['已处理', '未处理','已确认']
	    },
	    grid: {
	        left: '3%',
	        right: '4%',
	        bottom: '3%',
	        containLabel: true
	    },
	    xAxis:  {
	        type: 'value'
	    },
	    yAxis: {
	        type: 'category',
	        data: ['故障','报警','警告']
	    },
	    series: [
	        {
	            name: '已处理',
	            type: 'bar',
	            stack: '总量',
	            label: {
	                normal: {
	                    show: true,
	                    position: 'insideRight'
	                }
	            },
	            //noConfirm1,confirmed1,completed1,noConfirm2,confirmed2,completed2,noConfirm3,confirmed3,completed3
	            data: [completed1, completed2, completed3]
	        },
	        {
	            name: '未处理',
	            type: 'bar',
	            stack: '总量',
	            label: {
	                normal: {
	                    show: true,
	                    position: 'insideRight'
	                }
	            },
	            data: [noConfirm1, noConfirm2, noConfirm3]
	        },
	        {
	            name: '已确认',
	            type: 'bar',
	            stack: '总量',
	            label: {
	                normal: {
	                    show: true,
	                    position: 'insideRight'
	                }
	            },
	            data: [confirmed1, confirmed2, confirmed3]
	        }
	    ]
	};
	myChart2.setOption(option);

}
//报警滚动
function scroll(){
    var $this = $("#alarm_log_list");
    var scrollTimer;
    $this.hover(function() {
        clearInterval(scrollTimer);
    }, function() {
        scrollTimer = setInterval(function() {
            scrollNews($this);
        }, 1000);
    }).trigger("mouseleave");
}
function scrollNews(obj) {
    var $self = obj.find("ul");
    var lineHeight = $self.find("li:first").height(); 
    $self.animate({
        "marginTop": -lineHeight + "px"
    }, 600, function() {
        $self.css({
            marginTop: 0
        }).find("li:first").appendTo($self);
    })
}
function doCount(){
    $.ajax({
        type: 'post',
        url: "doCount",
        cache: true,
        dataType: 'json',
        success: function (result) {
            $("#count_substation").html(result.count_substation);
            $("#count_task_reform").html(result.count_task_reform);
            $("#count_task_patrol").html(result.count_task_patrol);
            $("#count_task_duty").html(result.count_task_duty);
            $("#count_task_other").html(result.count_task_other);
        },
        error : function() {
            $("#count_substation").html("");
		}
    })
}
function OnlineUser(){
    $.ajax({
        type: 'get',
        url: "../onlineUser/getActiveSessions",
        cache: true,
        dataType: 'json',
        success: function (result) {
            $("#count_people").html(result);
        },
        error : function() {
            $.ligerDialog.error("异常！","异常");
		}
    })
}

/**
 * 获取报警信息
 */
function getAlarmCounts(substationSn){
    $.ajax({
        type: 'get',
        url: "../indexShow/getAlarmCounts",
        data:{substationSn:substationSn},
        cache:false,
        async:false,
        dataType: 'json',
        success: function (data) {
            /**
            * 拼接
            */
            var list = data.retList;
            var li_info = "";
            for(var i = 0;i<list.length;i++){
                var callTime = list[i].callTime.replace(" ","_");
                var fontColor = "red";
                if(list[i].alarmLevel=="1"){
                    fontColor = "#FF715E";
                }else if(list[i].alarmLevel=="2"){
                    fontColor = "#FFEE51";
                }else if(list[i].alarmLevel=="3"){
                    fontColor = "#FFAF51";
                }
                var alarmLevel = "";
                if(list[i].alarmLevel=="1"){
                    alarmLevel = "1-故障";
                }else if(list[i].alarmLevel=="2"){
                    alarmLevel = "2-报警";
                }else if(list[i].alarmLevel=="3"){
                    alarmLevel = "3-警告";
                }
                li_info += '<li>'+
					    	  '<div class="alarm_log_list_li">'+
					       		'<a href="javascript:void(0)" onclick=openAlarmDialog("' + list[i].id + '","' + list[i].substationSn + '","' + list[i].substationName+ '","' + list[i].alarmType+ '","'+ list[i].local +'","' + list[i].equName +'","'+ callTime +'") style="width:140px;">'+list[i].substationName+'</a>'+
					       		'<a href="javascript:void(0)" onclick=openAlarmDialog("' + list[i].id + '","' + list[i].substationSn + '","' + list[i].substationName+ '","' + list[i].alarmType+ '","'+ list[i].local +'","' + list[i].equName +'","'+ callTime +'") style="width:180px;">'+list[i].local+'</a>'+
					       		'<a href="javascript:void(0)" onclick=openAlarmDialog("' + list[i].id + '","' + list[i].substationSn + '","' + list[i].substationName+ '","' + list[i].alarmType+ '","'+ list[i].local +'","' + list[i].equName +'","'+ callTime +'") style="width:90px;">'+alarmLevel+'</a>'+
					       		'<a href="javascript:void(0)" onclick=openAlarmDialog("' + list[i].id + '","' + list[i].substationSn + '","' + list[i].substationName+ '","' + list[i].alarmType+ '","'+ list[i].local +'","' + list[i].equName +'","'+ callTime +'") style="width:130px;">'+list[i].callTime+'</a>'+
					       	  '</div>'+
					       '</li>';
            }
            $("#alarm_log_list_ul").html(li_info);
            /**
             * 显示统计数量
             */
            var obj = data.retObj;
          	//级别
            var gz = obj[0];//故障alarmLevel=1
            var bj = obj[1];//报警alarmLevel=2
            var jg = obj[2];//警告alarmLevel=3
            //故障中状态
            var noConfirm1 = obj[3];//未确认state=1
            var confirmed1 = obj[4];//已确认state=2
            var completed1 = obj[5];//已处理state=3
            //报警中状态
            var noConfirm2 = obj[6];//未确认
            var confirmed2 = obj[7];//已确认
            var completed2 = obj[8];//已处理
            //警告中状态
            var noConfirm3 = obj[9];//未确认
            var confirmed3 = obj[10];//已确认
            var completed3 = obj[11];//已处理
            //加载条形表格
            doChart2(noConfirm1,confirmed1,completed1,noConfirm2,confirmed2,completed2,noConfirm3,confirmed3,completed3);
        },
        error : function() {
            $.ligerDialog.error("异常！","异常");
		}
    });
}
//弹出确认对话框
function openAlarmDialog(id,substationSn,substationName,alarmType,local,equName,callTime){
    if(id!=null && id!=""){
        $("#open_alarm_local").val(local);
        $("#open_alarm_time").val(callTime);
        if(alarmType=="1"){
            $("#open_alarm_type").val("HH-超高报警");
        }else if(alarmType=="2"){
            $("#open_alarm_type").val("H-高报警");
        }else if(alarmType=="3"){
            $("#open_alarm_type").val("L-低报警");
        }else if(alarmType=="4"){
            $("#open_alarm_type").val("LL-超低报警");
        }else if(alarmType=="5"){
            $("#open_alarm_type").val("0->1变位");
        }else if(alarmType=="6"){
            $("#open_alarm_type").val("1->0变位");
        }
        $("#open_alarm_equName").val(equName);
        //弹出新窗口
        $.ligerDialog.open({
            target: $("#alarm_dialog"),
            height: 250,
            width: 450,
            title: "<i style='color:#23AABD;' class='fa fa-warning'></i>"+"<font style='color:#23AABD;'>"+substationName+"</font>",
            showMax: false,
            showToggle: false,
            allowClose: false,
            showMin: false,
            isResize: false,
            slide: false,
            buttons: [{
                text: '确认',cls:'l-dialog-btn-confirm',
                onclick: function(item, dialog) {
                    var data = serializeObject($("#alarmForm"));
                    if(data.methods!=null){
                        $.ajax({
                            type: 'post',
                            url: '../alarm/methodsconfirm',
                            data: {
                                id: id,
                                methods: data.methods
                            },
                            dataType: "json",
                            success: function(data) {
                                if(data && data.retCode==100){
                                  	
                                    dialog.hide();
                                    var success = $.ligerDialog.success('操作成功！', "提示");
                                    setTimeout(function(){success.close();},500);
                                  	//刷新
                                    getAlarmCounts(substationSn);
                                }
                            },
                            error: function(data) {
                                $.ligerDialog.error("操作错误！", "提示");
                            }
                        });
                    }else{
                        $.ligerDialog.warn("请先选择处理方式！", "提示");
                    }
                }
            },
            {
                text: '取消',cls:'l-dialog-btn-back',
                onclick: function(item, dialog) {
                    dialog.hide();
                }
            }]
        });
    }
}
/*
 * 
 */
function selectSapn(type){
    if(type==1){
        $("#open_process_mode_one").prop("checked",true);
    }else if(type==2){
        $("#open_process_mode_two").prop("checked",true);
    }
}
</script>
</body>
</html>
