<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/echarts/echarts.js" type="text/javascript"></script> 
    <script src="/hmdl/static/js/My97DatePicker/WdatePicker.js" type="text/javascript"></script> 
    <!-- 引入 vintage 主题 -->
	<script src="/hmdl/static/js/echarts/themes/dark.js"></script>
	<script src="/hmdl/static/js/echarts/themes/chalk.js"></script>
	<script src="/hmdl/static/js/echarts/themes/halloween.js"></script>
	<script src="/hmdl/static/js/echarts/themes/wonderland.js"></script>
	<script src="/hmdl/static/js/echarts/themes/essos.js"></script>
</head>
<!-- body部分 -->
<body style="padding-top:2px;">

<!-- 标题部分 -->
<h3 class="hmdl_title_h3">分析管理
    <span class="hmdl_title_span">
        &nbsp;/&nbsp;分析一览
    </span>
<a id='l-layout-pic-title'></a>&emsp;
<input  type='text' readonly='readonly' value='' id='selectStationName' onclick='selectStation(1)' 
style='width:300px;border:none;font-size:16px;background-color:transparent;color:#428bca;cursor:pointer;text-decoration:underline' />
</a>   
</h3>
<hr class="hmdl_title_hr"/>
<!-- 查询条件部分 -->
<div id="grid_search" style="margin-left:8px;">
    <form name="searchForm" id="searchForm">
        <ul>
            <li class="label">
                &emsp;方案：<input type="text" id="search_plan_sn" name="planSn" onclick='selectPlan()' placeholder="选择方案" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                &emsp;主题：
                <select id="themeId">
					<option value="">默认</option>
					<option value="dark">黑色</option>
					<option value="chalk">黑色曲线</option>
					<option value="walden">蓝色</option>
					<option value="wonderland">绿色</option>
					<option value="halloween">橙色</option>
					<option value="essos">红色曲线</option>
				</select>
                &emsp;显示方式：
                <select  name="showWay" id="search_showWay">
					<option value="0" selected = "selected">多图显示</option>
					<option value="1">单图显示</option>
				</select>
                &emsp;曲线变量：<input type="text" id="search_var_name" name="varName"  onclick='selectVar()' placeholder="选择曲线变量" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                &emsp;查看实时数据：
                <select  name="realTime" id="search_realTime" onchange="searchRealTime()">
					<option value="">关闭</option>
					 <option value="1">开启</option>
				</select>
                &emsp;时间范围：<input type="text" id="search_timeRange" name="timeRange" onclick='selectDate()' placeholder="选择时间" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                &emsp;<button id="btnSearch" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
            </li>
        </ul>
    </form>
</div>

<!-- 选择时间 -->
<div id='selectDate' style="display:none">
	<form id="am-form" enctype="multipart/form-data" method="post" action="report"  target="upframe">
		<table class="default">
			<tr>
				<td align="right">时间单位</td>
				<td align="left">
				    <select  name="search_unit" id="search_unit">
						<option value="">请选择</option>
						<option value="h">小时</option>
						<option value="d">天</option>			
						<option value="m">月</option>					
						<option value="y">年</option>
					</select>
				</td>
			</tr>
			<tr>
				<td align="right">起始日期</td>
				<td align="left"><input type="text" autocomplete="off" class="Wdate" style="width:150px" name="search_begin_date" id="search_begin_date"  onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
				</td>
			</tr>
			<tr>
				<td align="right">结束日期</td>
				<td align="left"><input type="text" autocomplete="off" class="Wdate" style="width:150px" name="search_end_date" id="search_end_date"  onclick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"/>
				</td>
			</tr>
		</table>
	</form>
</div>
<div id="layout1">
<!-- 统计图表 -->
<div id="main"  position="" style="display:none;width:100%;height:1000px"></div>
<div id="indexMain"  position="" style="width:100%;">
            <div style="font-size:36px;margin-left:40%;margin-top:200px;font-family:微软雅黑;font-weight:bold;">
                    <div style="text-align:left;height:80px;">历史曲线首页</div>
            </div>
</div>
</div>
<!-- 变量选择-->
<div id='selectVar' style="display:none">
	<!-- 查询条件部分 -->
	<div id="var_grid_search" style="margin-left:8px;margin-top:20px;">
	    <form name="var_searchForm" id="var_searchForm">
	        <ul>
	            <li class="label">
	                &emsp;变量名称：<input type="text" id="var_search_varName" name="varName" placeholder="变量名称" style="text-indent:5px;height:23px;border-radius: .25em;"/>
	                &emsp;变量描述：<input type="text" id="var_search_remark" name="remark" placeholder="变量描述" style="text-indent:5px;height:23px;border-radius: .25em;"/>
	                &emsp;<button id="var_btnSearch" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
	            </li>
	        </ul>
	    </form>
	</div>
	<br/>
	
	<!-- 列表部分 -->
	<div id="var_grid"></div>
	<div style="overflow:auto;height:170px;">
	<form  name="varForm" id="varForm">
		<table id= "table_edit" class="default">
		</table>
	</form>
	</div>
</div>
<script>
var SUBSTATIONSN ="";
var ID ="";
var SHORTNAME ="";
var VARLIST = "";
var var_grid;
var myChart;
var SHOW_STATUS;//实时模式0关闭 1开启
$(function () {
    if(SHORTNAME==""){
        selectStation();
    }
	$("#layout1").ligerLayout({
	});
    // 绑定查询按钮
	$("#btnSearch").click(function () {
		var parms = serializeObject($("#searchForm"));
		parms.varList=JSON.stringify(VARLIST);
		if(parms.varList=="{}" || parms.varList=='""'){
          	$.ligerDialog.error("请先选择变量", "提示");
		    return;
		}
		if($("#search_realTime").val()==""){
			if($("#search_begin_date").val()=="" || $("#search_end_date").val()==""){
		          	$.ligerDialog.error("请选择起始结束时间", "提示");
				    return;
			}
		    SHOW_STATUS=0;
			parms.timeBegin=$("#search_begin_date").val();
			parms.timeEnd=$("#search_end_date").val();
			parms.unit=$("#search_unit").val();
		    $("#main").show();
		    $("#indexMain").hide();
			if(myChart!=null){
				myChart.dispose();
			}
			if($("#search_showWay").val()=="0"){
				doChart(parms);
			}else{
				doChartSingle(parms);
			}
		}else{
		    SHOW_STATUS=1;
		    $("#main").show();
		    $("#indexMain").hide();
			if(myChart!=null){
				myChart.dispose();
			}
			if($("#search_showWay").val()=="0"){
				doChartReal(parms);
				socketData(parms);
			}else{
				doChartRealSingle(parms);
				socketDataSingle(parms);
			}
		}
   	});

    // 绑定查询按钮
	$("#var_btnSearch").click(function () {
		var parms = serializeObject($("#var_searchForm"));
   	    for(var p in parms){
   	     var_grid.setParm(p,parms[p].trim());
   	    }
   		 var_grid.loadData();
   	});

});
var dbclickChecked = false;    
//定时
function  checkDbclick(){
	if (dbclickChecked) {
		return true;
	}
	dbclickChecked = true;
	setTimeout(function() {
		    dbclickChecked = false;
		}, $("#search_realTime").val()*1000);
	return false;
}
function socketDataSingle(){
    myChart.showLoading();
	var data_x=new Array();
	var data_y=new Array();
	var data_numberName=VARLIST.varNumberName.split(","); //字符分割 
	data_numberName = data_numberName.slice(1);	    	
	var data_varName = VARLIST.varName.split(","); //字符分割 
	data_varName = data_varName.slice(1);
	var data_TypeName=VARLIST.varTypeName.split(","); //字符分割 
	data_TypeName = data_TypeName.slice(1);
	var data_varRemark=VARLIST.varRemark.split(","); //字符分割 
	data_varRemark = data_varRemark.slice(1);
	//获取后台数据
	var gridLength = data_numberName.length; 	
	for(var i=0;i<gridLength;i++){
	    data_x[i] = new Array();
	    data_y[i] = new Array();
	}
	ws.onmessage = function (e) { 
		if(checkDbclick()) {
			return;
		}else{
	    if(SHOW_STATUS==1){
	    var d = JSON.parse(e.data);
		var option_xAxis=new Array();
		var option_series=new Array();
	   	// 填入数据
	    for(var i=0;i<gridLength;i++){
            if (d.func == "03" && typeof(d[data_numberName[i]]) != "undefined") {
               data_x[i].push(formatDate(d.dtime));
               var temp_data = d[data_numberName[i]];
               data_y[i].push(temp_data);
               //data_y[i].push(d[data_numberName[i]].substring(0,d[data_numberName[i]].indexOf(".") + 3));
				if(data_x[i].length>100){
				    data_x[i] = data_x[i].slice(1);
				    data_y[i] = data_y[i].slice(1);
				}
            }
			option_xAxis=data_x[i];
			option_series.push({ //系列名称，如果启用legend，该值将被legend.data索引相关  
				"name" : data_varRemark[i],
				//图表类型，必要参数！如为空或不支持类型，则该系列数据不被显示。  
				"type" : "line",
				"itemStyle" : { normal: {label : {show: false}}},
				//系列中的数据内容数组，折线图以及柱状图时数组长度等于所使用类目轴文本标签数组axis.data的长度，并且他们间是一一对应的。数组项通常为数值  
				"data" : data_y[i]
			});	
		}
	    myChart.hideLoading();
		myChart.setOption({
			xAxis : {
				type : 'category',
			    boundaryGap : false,
		   		axisLine: {onZero: true},
				name : '日期',
				splitLine : {show : true},
				//类目型坐标轴文本标签数组，指定label内容。 数组项通常为文本，'\n'指定换行 
				data : option_xAxis
			},
			//sereis的数据: 用于设置图表数据之用。series是一个对象嵌套的结构；对象内包含对象  
			series : option_series
		});
	};
	}
	}
    
}
function socketData() {
    myChart.showLoading();
	var data_x=new Array();
	var data_y=new Array();
   	var data_numberName=VARLIST.varNumberName.split(","); //字符分割 
   	data_numberName = data_numberName.slice(1);	    	
	var data_varName = VARLIST.varName.split(","); //字符分割 
	data_varName = data_varName.slice(1);
	var data_TypeName=VARLIST.varTypeName.split(","); //字符分割 
	data_TypeName = data_TypeName.slice(1);
	var data_varRemark=VARLIST.varRemark.split(","); //字符分割 
	data_varRemark = data_varRemark.slice(1);
   	//获取后台数据
   	var gridLength = data_numberName.length; 	
    for(var i=0;i<gridLength;i++){
        data_x[i] = new Array();
        data_y[i] = new Array();
    }
	ws.onmessage = function (e) {  
		if(checkDbclick()) {
			return;
		}else{
	    if(SHOW_STATUS==1){
	    var d = JSON.parse(e.data);
		var option_xAxis=new Array();
		var option_yAxis=new Array();
		var option_series=new Array();
	   	// 填入数据
	    for(var i=0;i<gridLength;i++){
            if (d.func == "03" && typeof(d[data_numberName[i]]) != "undefined") {
               data_x[i].push(formatDate(d.dtime));
               var temp_data = d[data_numberName[i]];
               data_y[i].push(temp_data);
               //data_y[i].push(d[data_numberName[i]].substring(0,d[data_numberName[i]].indexOf(".") + 3));
				if(data_x[i].length>100){
				    data_x[i] = data_x[i].slice(1);
				    data_y[i] = data_y[i].slice(1);
				}
            }
	        option_xAxis.push({ //坐标轴类型，横轴默认为类目型'category'
				gridIndex: i,
				type : 'category',
			    boundaryGap : false,
		   		axisLine: {onZero: true},
				name : '日期',
				splitLine : {show : true},
				//类目型坐标轴文本标签数组，指定label内容。 数组项通常为文本，'\n'指定换行 
				data : data_x[i]
			});
			option_series.push({ //系列名称，如果启用legend，该值将被legend.data索引相关  
				"name" : data_varRemark[i],
				//图表类型，必要参数！如为空或不支持类型，则该系列数据不被显示。  
				"type" : "line",
				"itemStyle" : { normal: {label : {show: false}}},
				"xAxisIndex": i,
			    "yAxisIndex": i,
				//系列中的数据内容数组，折线图以及柱状图时数组长度等于所使用类目轴文本标签数组axis.data的长度，并且他们间是一一对应的。数组项通常为数值  
				"data" : data_y[i]
			});	
		}
	    myChart.hideLoading();
		myChart.setOption({
			xAxis : option_xAxis,
			series : option_series
		});
	};
	}
	}

ws.onopen = function (event) {
    console.info("连接开启");
    //var ta = document.getElementById('responseText');
    //ta.value = "连接开启!";
};
ws.onclose = function (event) {
    console.info("连接被关闭");
    //var ta = document.getElementById('responseText');
    //ta.value = ta.value + "连接被关闭";
};
}
//实时刷新单图
function doChartRealSingle(){
    var data_varName = VARLIST.varName.split(","); //字符分割 
	data_varName = data_varName.slice(1);
	var data_numberName=VARLIST.varNumberName.split(","); //字符分割 
	data_numberName = data_numberName.slice(1);
	var data_TypeName=VARLIST.varTypeName.split(","); //字符分割 
	data_TypeName = data_TypeName.slice(1);
	var data_varMin=VARLIST.varMin.split(","); //字符分割 
	data_varMin = data_varMin.slice(1);
	var data_varMax=VARLIST.varMax.split(","); //字符分割 
	data_varMax = data_varMax.slice(1);
	var data_varRemark=VARLIST.varRemark.split(","); //字符分割 
	data_varRemark = data_varRemark.slice(1);
	if(data_varMin[0]==""){
	    data_varMin[0]=0;
	}else{
	    data_varMin[0]=parseFloat(data_varMin[0]);
	}
	if(data_varMax[0]==""){
	    data_varMax[0]=100;
	}else{
	    data_varMax[0]=parseFloat(data_varMax[0]);  
	}
	//获取后台数据
	var gridLength = data_numberName.length;
	document.getElementById('main').style.height="500px";
	// 基于准备好的dom，初始化echarts图表
	myChart = echarts.init(document.getElementById('main'), $("#themeId").val());
	var option_grid=new Array();
	var option_legend=new Array();
	var option_dataZoom=new Array();
	var option_xAxis=new Array();
	var option_yAxis=new Array();
	var option_series=new Array();
	
	for(var i=0;i<gridLength;i++){
		option_legend.push(data_varRemark[i]);
		option_dataZoom.push(i);

		option_series.push({ //系列名称，如果启用legend，该值将被legend.data索引相关  
			"name" : data_varRemark[i],
			//图表类型，必要参数！如为空或不支持类型，则该系列数据不被显示。  
			"type" : "line",
			"itemStyle" : { normal: {label : {show: true}}},
			//系列中的数据内容数组，折线图以及柱状图时数组长度等于所使用类目轴文本标签数组axis.data的长度，并且他们间是一一对应的。数组项通常为数值  
			"data" : []
		});		
	}
	var title_text = '历史曲线';
	// 为echarts对象加载数据 
	myChart.setOption({
		title : {
			//主标题文本，'\n'指定换行  
			text : title_text,
			subtext: '数据来自华明电力',
			//水平安放位置，默认为左侧，可选为：'center' | 'left' | 'right' | {number}（x坐标，单位px）  
			x : 'center',
			//垂直安放位置，默认为全图顶端，可选为：'top' | 'bottom' | 'center' | {number}（y坐标，单位px）  
			y : 'top'
		},
		//提示框，鼠标悬浮交互时的信息提示  
		tooltip : {
			show : true,
			//触发类型，默认（'item'）数据触发，可选为：'item' | 'axis'  
			trigger : 'axis'
		},
		//图例，每个图表最多仅有一个图例  
		legend : {
			//legend的data: 用于设置图例，data内的字符串数组需要与sereis数组内每一个series的name值对应  
			data : option_legend,
			x : 'center',
			top:'50px'
		},
	    grid: {
	        top: '130px',
	        height: '300px'
	    },
		//工具箱，每个图表最多仅有一个工具箱  
		toolbox : {
			//显示策略，可选为：true（显示） | false（隐藏），默认值为false  
			show: true,
    		feature: {
	        dataZoom: {
	            yAxisIndex: 'none'
	        },
	        dataView: {readOnly: false},
	        magicType: {type: ['line', 'bar']},
	        restore: {},
	        saveAsImage: {}
   			}
		},
	    dataZoom: [
	               {
	                   show: true,
	                   realtime: true,
	                   start: 0,
	                   end: 100
	               },
	               {
	                   type: 'slider',
	                   yAxisIndex: 0,
	                   startValue:data_varMin[0],
	                   endValue:data_varMax[0],
	                   filterMode: 'empty'
	               },
	    ],
		//直角坐标系中横轴数组，数组中每一项代表一条横轴坐标轴，仅有一条时可省略数值  
		//横轴通常为类目型，但条形图时则横轴为数值型，散点图时则横纵均为数值型  
		xAxis : {
			type : 'category',
		    boundaryGap : false,
	   		axisLine: {onZero: true},
			name : '日期',
			splitLine : {show : true},
			//类目型坐标轴文本标签数组，指定label内容。 数组项通常为文本，'\n'指定换行 
			data : []
		},
		//直角坐标系中纵轴数组，数组中每一项代表一条纵轴坐标轴，仅有一条时可省略数值  
		//纵轴通常为数值型，但条形图时则纵轴为类目型  
		yAxis : {
		    type : 'value',
			name : data_TypeName[0]
		},
		//sereis的数据: 用于设置图表数据之用。series是一个对象嵌套的结构；对象内包含对象  
		series : option_series
	});
    
}
//实时刷新图标
function doChartReal(){
	var data_varName = VARLIST.varName.split(","); //字符分割 
	data_varName = data_varName.slice(1);
	var data_numberName=VARLIST.varNumberName.split(","); //字符分割 
	data_numberName = data_numberName.slice(1);
	var data_TypeName=VARLIST.varTypeName.split(","); //字符分割 
	data_TypeName = data_TypeName.slice(1);
	var data_varMin=VARLIST.varMin.split(","); //字符分割 
	data_varMin = data_varMin.slice(1);
	var data_varMax=VARLIST.varMax.split(","); //字符分割 
	data_varMax = data_varMax.slice(1);
	var data_varRemark=VARLIST.varRemark.split(","); //字符分割 
	data_varRemark = data_varRemark.slice(1);
	//获取后台数据
	var gridLength = data_numberName.length;
	document.getElementById('main').style.height=100+300*gridLength+"px";
	// 基于准备好的dom，初始化echarts图表
	myChart = echarts.init(document.getElementById('main'), $("#themeId").val());
	var option_grid=new Array();
	var option_legend=new Array();
	var option_dataZoom=new Array();
	var option_xAxis=new Array();
	var option_yAxis=new Array();
	var option_series=new Array();
	var option_dataZoomy=new Array();
	for(var i=0;i<gridLength;i++){
	    if(i==0){
	        option_grid.push({top:Number(130)+Number(300)*i+"px",height: '200px'});
	    }else{
	        option_grid.push({top:Number(100)+Number(300)*i+"px",height: '200px'});
	    }
		option_legend.push(data_varRemark[i]);
		option_dataZoom.push(i);
		option_xAxis.push({ //坐标轴类型，横轴默认为类目型'category'
			gridIndex: i,
			type : 'category',
		    boundaryGap : false,
	   		axisLine: {onZero: true},
			name : '日期',
			splitLine : {show : true},
			//类目型坐标轴文本标签数组，指定label内容。 数组项通常为文本，'\n'指定换行 
			data : []
		});	
		if(data_varMin[i]==""){
		    data_varMin[i]=0;
		}else{
		    data_varMin[i]=parseFloat(data_varMin[i]);
		}
		if(data_varMax[i]==""){
		    data_varMax[i]=100;
		}else{
		    data_varMax[i]=parseFloat(data_varMax[i]);
		}
		option_dataZoomy.push({
	                   type: 'slider',
	                   yAxisIndex: i,
	                   startValue:data_varMin[i],
	                   endValue:data_varMax[i],
	                   filterMode: 'empty'
	     });
		option_yAxis.push({ //坐标轴类型，纵轴默认为数值型'value'  
			    gridIndex: i,
			    min:data_varMin[i],
			    max:data_varMax[i],
			    type : 'value',
				name : data_TypeName[i]
		});
		option_series.push({ //系列名称，如果启用legend，该值将被legend.data索引相关  
			"name" : data_varRemark[i],
			//图表类型，必要参数！如为空或不支持类型，则该系列数据不被显示。  
			"type" : "line",
			"itemStyle" : { normal: {label : {show: true}}},
			"xAxisIndex": i,
		    "yAxisIndex": i,
			//系列中的数据内容数组，折线图以及柱状图时数组长度等于所使用类目轴文本标签数组axis.data的长度，并且他们间是一一对应的。数组项通常为数值  
			"data" : []
		});		
	}
	option_dataZoomy.push({
            show: true,
            realtime: true,
            start: 0,
            end: 100,
            xAxisIndex: option_dataZoom
        },
        {
            type: 'inside',
            realtime: true,
            start: 0,
            end: 100,
            xAxisIndex: option_dataZoom
        });
	var title_text = '历史曲线';
	// 为echarts对象加载数据 
	myChart.setOption({
		title : {
			//主标题文本，'\n'指定换行  
			text : title_text,
			subtext: '数据来自华明电力',
			//水平安放位置，默认为左侧，可选为：'center' | 'left' | 'right' | {number}（x坐标，单位px）  
			x : 'center',
			//垂直安放位置，默认为全图顶端，可选为：'top' | 'bottom' | 'center' | {number}（y坐标，单位px）  
			y : 'top'
		},
		//提示框，鼠标悬浮交互时的信息提示  
		tooltip : {
			show : true,
			//触发类型，默认（'item'）数据触发，可选为：'item' | 'axis'  
			trigger : 'axis'
		},
		//图例，每个图表最多仅有一个图例  
		legend : {
			//legend的data: 用于设置图例，data内的字符串数组需要与sereis数组内每一个series的name值对应  
			data : option_legend,
			x : 'center',
			top:'50px'
		},
		//工具箱，每个图表最多仅有一个工具箱  
		toolbox : {
			//显示策略，可选为：true（显示） | false（隐藏），默认值为false  
			show: true,
    		feature: {
	        dataZoom: {
	            yAxisIndex: 'none'
	        },
	        dataView: {readOnly: false},
	        magicType: {type: ['line', 'bar']},
	        restore: {},
	        saveAsImage: {}
   			}
		},
	    dataZoom: option_dataZoomy,
	    grid: option_grid,
		//直角坐标系中横轴数组，数组中每一项代表一条横轴坐标轴，仅有一条时可省略数值  
		//横轴通常为类目型，但条形图时则横轴为数值型，散点图时则横纵均为数值型  
		xAxis : option_xAxis,
		//直角坐标系中纵轴数组，数组中每一项代表一条纵轴坐标轴，仅有一条时可省略数值  
		//纵轴通常为数值型，但条形图时则纵轴为类目型  
		yAxis : option_yAxis,
		//sereis的数据: 用于设置图表数据之用。series是一个对象嵌套的结构；对象内包含对象  
		series : option_series
	});
}

//一张图显示
function doChartSingle(parms){
var manager = $.ligerDialog.waitting('正在加载中,数据庞大请稍候...');
$.ajax({
	type:'post',
	async:true,
	url:'../analysisChart/doChart',
	data:parms,
	dataType:"json",
	success:function(result){
	    manager.close();
		var data = result.retObj.retObj;
		//获取后台数据
		var gridLength = data.length;
		document.getElementById('main').style.height="500px";
		// 基于准备好的dom，初始化echarts图表
		myChart = echarts.init(document.getElementById('main'), $("#themeId").val());
		//处理最大最小值
		var varList = JSON.parse(parms.varList);
		var data_min= new Array(); //定义一数组 
		data_min=varList['varMin'].split(","); //字符分割 
		data_min = data_min.slice(1);
		var data_max= new Array(); //定义一数组 
		data_max=varList['varMax'].split(","); //字符分割 
		data_max = data_max.slice(1);
		var data_varRemark=VARLIST.varRemark.split(","); //字符分割 
		data_varRemark = data_varRemark.slice(1);
		if(data_min[0]==""){
		    data_min[0]=0;
		}else{
		    data_min[0]=parseFloat(data_min[0]);
		}
		if(data_max[0]==""){
		    data_max[0]=100;
		}else{
		    data_max[0]=parseFloat(data_max[0]);
		}
		var option_grid=new Array();
		var option_legend=new Array();
		var option_dataZoom=new Array();
		var option_xAxis=new Array();
		var option_yAxis=new Array();
		var option_series=[];
		for(var i=0;i<gridLength;i++){
			option_legend.push(data_varRemark[i]);
			option_dataZoom.push(i);
			var data_x = new Array();
			for(var j=0;j<data[i].length;j++){
			    if(data[i][j]['x']!=""){
			    var obj = JSON.parse(data[i][j]['x']);
			     if($("#search_unit").val()=="y"){
					data_x.push(obj['year']);
			     }else if($("#search_unit").val()=="m"){
					data_x.push(obj['year']+"-"+obj['month']);
			     }else if($("#search_unit").val()=="d"){
					data_x.push(obj['year']+"-"+obj['month']+"-"+obj['day']);
			     }else if($("#search_unit").val()=="h"){
					data_x.push(obj['year']+"-"+obj['month']+"-"+obj['day']+":"+obj['hour']);
			     }else{
					data_x.push(obj['year']+"-"+obj['month']+"-"+obj['day']);
			     }
			    }
			}
			var data_y = new Array();
			for(var j=0;j<data[i].length;j++){
			    if(data[i][j]['y']!=""){
				data_y.push(data[i][j]['y'].substring(0,data[i][j]['y'].indexOf(".") + 3));
			    }
			}		
			option_series.push({ //系列名称，如果启用legend，该值将被legend.data索引相关  
				"name" : data_varRemark[i],
				//图表类型，必要参数！如为空或不支持类型，则该系列数据不被显示。  
				"type" : "line",
				"itemStyle" : { normal: {label : {show: true}}},
				//系列中的数据内容数组，折线图以及柱状图时数组长度等于所使用类目轴文本标签数组axis.data的长度，并且他们间是一一对应的。数组项通常为数值  
				"data" : data_y
			});
		}
		var title_text = '历史曲线';
		//定义图表option  
		var option = {
			title : {
				//主标题文本，'\n'指定换行  
				text : title_text,
				subtext: '数据来自华明电力',
				//水平安放位置，默认为左侧，可选为：'center' | 'left' | 'right' | {number}（x坐标，单位px）  
				x : 'center',
				//垂直安放位置，默认为全图顶端，可选为：'top' | 'bottom' | 'center' | {number}（y坐标，单位px）  
				y : 'top'
			},
			//提示框，鼠标悬浮交互时的信息提示  
			tooltip : {
				show : true,
				//触发类型，默认（'item'）数据触发，可选为：'item' | 'axis'  
				trigger : 'axis'
			},
			//图例，每个图表最多仅有一个图例  
			legend : {
				//legend的data: 用于设置图例，data内的字符串数组需要与sereis数组内每一个series的name值对应  
				data : option_legend,
				x : 'center',
				top:'50px'
			},
		    grid: {
		        top: '130px',
		        height: '300px'
		    },
			//工具箱，每个图表最多仅有一个工具箱  
			toolbox : {
				//显示策略，可选为：true（显示） | false（隐藏），默认值为false  
				show: true,
	        	feature: {
	            dataZoom: {
	                yAxisIndex: 'none'
	            },
	            dataView: {readOnly: false},
	            magicType: {type: ['line', 'bar']},
	            restore: {},
	            saveAsImage: {}
	       		}
			},
		    dataZoom: [
		               {
		                   show: true,
		                   realtime: true,
		                   start: 0,
		                   end: 100
		               },
		               {
		                   type: 'slider',
		                   yAxisIndex: 0,
		                   startValue:data_min[0],
		                   endValue:data_max[0],
		                   filterMode: 'empty'
		               }
		    ],
			//直角坐标系中横轴数组，数组中每一项代表一条横轴坐标轴，仅有一条时可省略数值  
			//横轴通常为类目型，但条形图时则横轴为数值型，散点图时则横纵均为数值型  
			xAxis : {
			    	type : 'category',
				    boundaryGap : false,
			        axisLine: {onZero: true},
					name : '日期',
					splitLine : {show : true},
					//类目型坐标轴文本标签数组，指定label内容。 数组项通常为文本，'\n'指定换行 
					data : data_x
			},
			//直角坐标系中纵轴数组，数组中每一项代表一条纵轴坐标轴，仅有一条时可省略数值  
			//纵轴通常为数值型，但条形图时则纵轴为类目型  
			yAxis : {
			    type : 'value',
			    min :data_min[0],
			    max :data_max[0],
				name : data[0][0]['varType']
			},
			//sereis的数据: 用于设置图表数据之用。series是一个对象嵌套的结构；对象内包含对象  
			series : option_series
		};
		// 为echarts对象加载数据 
		myChart.setOption(option);
	},
	error : function() {
     	$.ligerDialog.warn("查询失败！","错误");
	}
})
}
//多张图表
function doChart(parms){
var manager = $.ligerDialog.waitting('正在加载中,数据庞大请稍候...');
	$.ajax({
		type:'post',
		async:true,
		url:'../analysisChart/doChart',
		data:parms,
		dataType:"json",
		success:function(result){
		    manager.close();
			var data = result.retObj.retObj;
			//获取后台数据
			var gridLength = data.length;
			document.getElementById('main').style.height=100+300*gridLength+"px";
			// 基于准备好的dom，初始化echarts图表
			myChart = echarts.init(document.getElementById('main'), $("#themeId").val());
			var option_grid=new Array();
			var option_legend=new Array();
			var option_dataZoom=new Array();
			var option_xAxis=new Array();
			var option_yAxis=new Array();
			var option_series=new Array();
			var option_dataZoomy = new Array();
			var data_varRemark=VARLIST.varRemark.split(","); //字符分割 
			data_varRemark = data_varRemark.slice(1);
			for(var i=0;i<gridLength;i++){
			    if(i==0){
			        option_grid.push({top:Number(130)+Number(300)*i+"px",height: '200px'});
			    }else{
			        option_grid.push({top:Number(100)+Number(300)*i+"px",height: '200px'});
			    }
				option_legend.push(data_varRemark[i]);
				option_dataZoom.push(i);
				var data_x = new Array();
				for(var j=0;j<data[i].length;j++){
				    if(data[i][j]['x']!=""){
				    var obj = JSON.parse(data[i][j]['x']);
				     if($("#search_unit").val()=="y"){
						data_x.push(obj['year']);
				     }else if($("#search_unit").val()=="m"){
						data_x.push(obj['year']+"-"+obj['month']);
				     }else if($("#search_unit").val()=="d"){
						data_x.push(obj['year']+"-"+obj['month']+"-"+obj['day']);
				     }else if($("#search_unit").val()=="h"){
						data_x.push(obj['year']+"-"+obj['month']+"-"+obj['day']+":"+obj['hour']);
				     }else{
						data_x.push(obj['year']+"-"+obj['month']+"-"+obj['day']);
				     }
				    }
				}
				var data_y = new Array();
				for(var j=0;j<data[i].length;j++){
				    if(data[i][j]['y']!=""){
					data_y.push(data[i][j]['y'].substring(0,data[i][j]['y'].indexOf(".") + 3));
				    }
				}
				//处理最大最小值
				var varList = JSON.parse(parms.varList);
				var data_min= new Array(); //定义一数组 
				data_min=varList['varMin'].split(","); //字符分割 
				data_min = data_min.slice(1);
				var data_max= new Array(); //定义一数组 
				data_max=varList['varMax'].split(","); //字符分割 
				data_max = data_max.slice(1);
				
				if(data_min[i]!=''){
					var data_min = parseFloat(data_min[i]);
				}else{
					var data_min = Math.min.apply(Math, data_y);
				}
				if(data_max[i]!=''){
					var data_max = parseFloat(data_max[i]);
				}else{
					var data_max = Math.max.apply(Math, data_y);
				}
				option_dataZoomy.push({
			                   type: 'slider',
			                   yAxisIndex: i,
			                   startValue:data_min,
			                   endValue:data_max,
			                   filterMode: 'empty'
			     });
				option_xAxis.push({ //坐标轴类型，横轴默认为类目型'category'
			   		gridIndex: i,
			    	type : 'category',
				    boundaryGap : false,
			        axisLine: {onZero: true},
					name : '日期',
					splitLine : {show : true},
					//类目型坐标轴文本标签数组，指定label内容。 数组项通常为文本，'\n'指定换行 
					data : data_x
				});
				
				option_yAxis.push({ //坐标轴类型，纵轴默认为数值型'value'  
					    gridIndex: i,
					    min:data_min,
					    max:data_max,
					    type : 'value',
						name : data[i][0]['varType']
				});
				
				option_series.push({ //系列名称，如果启用legend，该值将被legend.data索引相关  
					"name" : data_varRemark[i],
					//图表类型，必要参数！如为空或不支持类型，则该系列数据不被显示。  
					"type" : "line",
					"itemStyle" : { normal: {label : {show: true}}},
					"xAxisIndex": i,
				    "yAxisIndex": i,
					//系列中的数据内容数组，折线图以及柱状图时数组长度等于所使用类目轴文本标签数组axis.data的长度，并且他们间是一一对应的。数组项通常为数值  
					"data" : data_y
				});
			}
			option_dataZoomy.push({
		            show: true,
		            realtime: true,
		            start: 0,
		            end: 100,
		            xAxisIndex: option_dataZoom
		        },
		        {
		            type: 'inside',
		            realtime: true,
		            start: 0,
		            end: 100,
		            xAxisIndex: option_dataZoom
		        });
			var title_text = '历史曲线';
			//定义图表option  
			var option = {
				title : {
					//主标题文本，'\n'指定换行  
					text : title_text,
					subtext: '数据来自华明电力',
					//水平安放位置，默认为左侧，可选为：'center' | 'left' | 'right' | {number}（x坐标，单位px）  
					x : 'center',
					//垂直安放位置，默认为全图顶端，可选为：'top' | 'bottom' | 'center' | {number}（y坐标，单位px）  
					y : 'top'
				},
				//提示框，鼠标悬浮交互时的信息提示  
				tooltip : {
					show : true,
					//触发类型，默认（'item'）数据触发，可选为：'item' | 'axis'  
					trigger : 'axis'
				},
				//图例，每个图表最多仅有一个图例  
				legend : {
					//legend的data: 用于设置图例，data内的字符串数组需要与sereis数组内每一个series的name值对应  
					data : option_legend,
					x : 'center',
					top:'50px'
				},
				//工具箱，每个图表最多仅有一个工具箱  
				toolbox : {
					//显示策略，可选为：true（显示） | false（隐藏），默认值为false  
					show: true,
		        	feature: {
		            dataZoom: {
		                yAxisIndex: 'none'
		            },
		            dataView: {readOnly: false},
		            magicType: {type: ['line', 'bar']},
		            restore: {},
		            saveAsImage: {}
		       		}
				},
			    dataZoom: option_dataZoomy,
			    grid: option_grid,
				//直角坐标系中横轴数组，数组中每一项代表一条横轴坐标轴，仅有一条时可省略数值  
				//横轴通常为类目型，但条形图时则横轴为数值型，散点图时则横纵均为数值型  
				xAxis : option_xAxis,
				//直角坐标系中纵轴数组，数组中每一项代表一条纵轴坐标轴，仅有一条时可省略数值  
				//纵轴通常为数值型，但条形图时则纵轴为类目型  
				yAxis : option_yAxis,
				//sereis的数据: 用于设置图表数据之用。series是一个对象嵌套的结构；对象内包含对象  
				series : option_series
			};
			// 为echarts对象加载数据 
			myChart.setOption(option);
		},
		error : function() {
	         	$.ligerDialog.warn("查询失败！","错误");
		}
	})
}
//选择方案
function selectPlan(){
    var theme_id = $("#themeId").val();
    var show_way=$("#search_showWay").val();
    var time_begin=$("#search_begin_date").val();
    var time_end=$("#search_end_date").val();
    var unit_1=$("#search_unit").val();
	var var_list=JSON.stringify(VARLIST);
    var real_time=$("#search_realTime").val();
 // 转向网页的地址;
	var url='../analysisChart/selectPlan';              
	$.ligerDialog.open({
		//右上角的X隐藏，否则会有bug
	  allowClose: false,
	  height:500,  
	  url: url,  
	  width: 800,  
	  name:'choseKey',  
	  title:'方案选择',  
      data: {
          varList: var_list,
          themeId: theme_id,
          substationSn: SUBSTATIONSN,
          shortName: SHORTNAME,
          beginTime: time_begin,
          endTime: time_end,
          unit: unit_1,
          showWay: show_way,
          isRealtime: real_time
      },
	  isResize:true,  
	  buttons: [  {  text: '立即应用', onclick: function (item, dialog){
	  					var arrM = new Array();
	  					//获取子页面
	  					arrM=document.getElementById('choseKey').contentWindow;
	  					var contentgrid = arrM.$("#grid").ligerGrid();
	  					var contentrows = contentgrid.getSelectedRows();
					    if (contentrows == null || contentrows.length!=1) {
					        $.ligerDialog.warn("请选择一条记录！","提示");
					        return;
					    };
					    $("#search_plan_sn").val(contentrows[0].planName);
					    var varListJson = JSON.parse(contentrows[0].varList);

						var varId = varListJson['varId'].split(","); //字符分割 
						varId = varId.slice(1);
						
						var varRemark = varListJson['varRemark'].split(","); //字符分割 
						varRemark = varRemark.slice(1);
						
						var varTypeName = varListJson['varTypeName'].split(","); //字符分割 
						varTypeName = varTypeName.slice(1);
						
						var varLoop = varListJson['varLoop'].split(","); //字符分割 
						varLoop = varLoop.slice(1);
						
						var varShortname = varListJson['varShortname'].split(","); //字符分割 
						varShortname = varShortname.slice(1);
						
						var varNumberName = varListJson['varNumberName'].split(","); //字符分割 
						varNumberName = varNumberName.slice(1);
						
						var varName = varListJson['varName'].split(","); //字符分割 
						varName = varName.slice(1);
						
						var varMin = varListJson['varMin'].split(","); //字符分割 
						varMin = varMin.slice(1);
						
						var varMax = varListJson['varMax'].split(","); //字符分割 
						varMax = varMax.slice(1);
						document.getElementById("table_edit");
						$("#table_edit").html("");
						$("#search_begin_date").val(contentrows[0].beginTime);
						$("#search_end_date").val(contentrows[0].endTime);
						$("#search_unit").val(contentrows[0].unit);
						$("#search_showWay").val(contentrows[0].showWay);
						$("#themeId").val(contentrows[0].themeId);
						$("#search_realTime").val(contentrows[0].isRealtime);
					    for(var i=0;i<varId.length;i++){
					        var tableVar = {};
						    tableVar.varId=varId[i];
						    tableVar.varRemark=varRemark[i];
						    tableVar.varTypeName=varTypeName[i];
						    tableVar.varLoop=varLoop[i];
						    tableVar.varShortname=varShortname[i];
						    tableVar.varNumberName=varNumberName[i];
						    tableVar.varName=varName[i];
						    tableVar.varMin=varMin[i];
						    tableVar.varMax=varMax[i];
						    addCparm(tableVar);
					    }
					    VARLIST = serializeObjectNew($("#varForm")); 
					    $("#btnSearch").click();
						dialog.close();
	  			}
	  		},  
	              { text: '关闭', onclick: function (item, dialog){dialog.close();}}
	          ]                                     
		}); 
}

//选择变量
function selectVar(){
	// 转向网页的地址;
	$.ligerDialog.open({
	//右上角的X隐藏，否则会有bug
	allowClose: false,
	height: 500,  
	target:$("#selectVar"),
	width: 800, 
	name:'choseKey',  
	title:'变量选择',  
	isResize:true,  
	buttons: [  {  text: '确认', onclick: function (item, dialog){
				VARLIST = serializeObjectNew($("#varForm")); 		
				dialog.hide();
				}
			},  
	            { text: '关闭', onclick: function (item, dialog){dialog.hide();}}
	        ]                                     
	}); 
	var_initGrid();
}

function var_initGrid(){
    var_grid=$("#var_grid").ligerGrid({
        title:'&nbsp;&nbsp;<span style="color:#386792">变量一览</span>',
        headerImg:'/hmdl/static/images/list.png',
        columns: [
					{ display: '站点名称', name: 'substationName',width:120},
					{ display: '变量名称', name: 'varName',width:120},
					{ display: '描述', name: 'remark',width:120},
	                { display: '数据类型', name: 'dataTypeName',width:120},
	                { display: '功能码', name: 'funcCode',width:120},
	                { display: 'ip地址', name: 'ipAddr',width:120},
	                { display: '端口号', name: 'port',width:120},
	                { display: "名称", name: 'numberName',width:120},
	                { display: '回路', name: 'loop',width:120},
					{ display: '创建时间', name: 'createDate',width:150},
				],
		  toolbar: {
	                items: [
						{ line: true },{ line: true },
						{ text: '添加',click: function(){addCparm();}, icon: 'fa fa-plus',color:'success'}, 
						{ text: '刷新',click: function(){grid.reload();}, icon: 'fa fa-refresh',color:'primary'},
					  ]
			            },
        dataAction: 'server',
    	url:'../stationDataDetail/varSearch',
    	root:'data',
    	record:'count',
    	height: '280px',
        pageSize: 5,
        pageSizeOptions:[5,10, 20, 30, 50],
        parms:[{name:"substationSn",value:SUBSTATIONSN},{name:"shortName",value:SHORTNAME},{name:"funcCode",value:"03"},{name:"isVarName",value:"1"}],
    	enabledSort:true,
        sortnameParmName:'sortname',
        sortorderParmName:'sortorder',
    	checkbox: false,
    	rownumbers: true,
    });
}

table_edit = 0;
//动态添加行
function addCparm(data){
    var dataTypeName;
    var varId;
    var varLoop;
    var varShortname;
    var varNumberName;
    var varName;
    var varMin;
    var varMax;
    if(data!=null){
        dataTypeName=data['varTypeName'];
        varId=data['varId'];
        varLoop=data['varLoop'];
        varShortname=data['varShortname'];
        varNumberName = data['varNumberName'];
        varName=data['varName'];
        varMin = data['varMin'];
        varMax = data['varMax'];
        varRemark = data['varRemark'];
    }else{
        var rows = var_grid.getSelectedRows();
        if (rows == null || rows.length!=1) {
            $.ligerDialog.warn("请选择一条记录！","提示");
            return;
        };
        dataTypeName=rows[0].dataTypeName;
        varId=rows[0].id;
        varLoop = rows[0].loop;
        varShortname=rows[0].shortname;
        varNumberName=rows[0].numberName;
        varName=rows[0].varName;
        varRemark=rows[0].remark;
        varMin = "";
        varMax = "";
    }

	var data_check = serializeObjectNew($("#varForm"));
	data_check2=JSON.stringify(data_check);
	if(data_check2!="{}"){
		if(data_check.varNumberName.indexOf(varNumberName) != -1){
	        $.ligerDialog.warn("不能选择重复变量！","提示");
	        return;
	    }
	}
    table_edit = table_edit+1;
    var tr = $("<tr>"); 
	$("<td align='right'colspan='1'>").html("&emsp;变量:&emsp;<span style='color:red'>*</span>").appendTo(tr);
	$("<td align='left'>").html('<input name="varRemark" type="hidden" value = "'+varRemark+'"/><input name="varTypeName" type="hidden" value = "'+dataTypeName+'"/><input name="varId" type="hidden" value = "'+varId+'"/><input name="varLoop" type="hidden" value = "'+varLoop+'"/><input name="varShortname" type="hidden" value = "'+varShortname+'"/><input name="varNumberName" type="hidden" value = "'+varNumberName+'"/><input type="text" style="width:100px" id="table_eidt_'+table_edit+'" name="varName" value = "'+varName+'"/>').appendTo(tr);
	$("<td align='right'>").html('&emsp;最小值:&emsp;').appendTo(tr);
	$("<td align='left'>").html('<input class="varInput" value="'+varMin+'" name="varMin" style="width:100px" placeholder="不填自动适应" type="text"/>').appendTo(tr);
	$("<td align='right'>").html('&emsp;最大值:&emsp;').appendTo(tr);
	$("<td align='left'>").html('<input class="varInput" value="'+varMax+'" name="varMax" style="width:100px" placeholder="不填自动适应" type="text"/>').appendTo(tr);
	$("<td align='left'>").html( '&nbsp;'
	+'&nbsp;【<a href ="#" style="text-decoration:none;color:blue" onclick= "deleteRowTable('+table_edit+',this,'+'\'table_edit\''+')">删除</a>】'
	).appendTo(tr);
	$("#table_edit").append(tr);
	$(".varInput").bind("input propertychange",function(event){
	    
	});
}
//动态删除行
function deleteRowTable(id,obj,tablename){
	var index=obj.parentNode.parentNode.rowIndex;
	var table= document.getElementById(tablename);
	table.deleteRow(index);
}

function serializeObjectNew(form){
	var o={};
	$.each(form.serializeArray(),function(index){
		o[this['name']]=o[this['name']]+","+this['value'];
	});
	return o;
}
//选择时间范围
function selectDate(){
    var import_date;
	if(!import_date){
		import_date=$.ligerDialog.open({
		target:$("#selectDate"),
    	height: 300,
		width:500,
	    title: "选择时间范围",
	    isHidden: true,
	    isResize:true ,
	    allowClose:false,
	    buttons: [
	              { text: '确定', onclick: function (item, dialog) {
	                  if($("#search_unit").val()==""){
	                      $.ligerDialog.error("请选择时间单位！", "提示");
		                  return ;
	                  }
	                  if($("#search_begin_date").val()==""){
	                      $.ligerDialog.error("请选择起始时间！", "提示");
		                  return ;
	                  }
	                  if($("#search_end_date").val()==""){
	                      $.ligerDialog.error("请选择结束时间！", "提示");
		                  return ;
	                  }
	                  var end = new Date($("#search_end_date").val());
	                  var begin = new Date($("#search_begin_date").val());
	                  var temp =(end-begin)/1000;
	                  if(temp<0){
	                      $.ligerDialog.error("结束日期必须大于起始日期！", "提示");
		                  return ;
	                  }
		              if($("#search_unit").val()=="h" && temp>86500){
		                      $.ligerDialog.error("选择时日期范围不能超过1天！", "提示");
			                  return ;
		              }
		              if($("#search_unit").val()=="d" && temp>604800){
		                      $.ligerDialog.error("选择天日期范围不能超过一星期！", "提示");
			                  return ;
		              }
		              if($("#search_unit").val()=="m" && temp>31622400){
		                      $.ligerDialog.error("选择月日期范围不能超过一年！", "提示");
			                  return ;
		              }
		              if($("#search_unit").val()=="y" && temp>94867200){
		                      $.ligerDialog.error("选择年日期范围不能超过三年！", "提示");
			                  return ;
		              }
	                  dialog.hide();}},
	              { text: '取消', onclick: function (item, dialog) { dialog.hide(); } }
	          ]
		});
	}
}

//查看实时数据
function searchRealTime(){
    if($("#search_realTime").val()!=""){
        $("#search_timeRange").attr("disabled","disabled");
    }else{
        $("#search_timeRange").removeAttr("disabled");
    }
}

//时间戳转日期
function formatDate(timestamp) { 
    var date = new Date(timestamp); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
    Y = date.getFullYear() + '-';
    M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    D = change(date.getDate()) + ' ';
    h = change(date.getHours()) + ':';
    m = change(date.getMinutes()) + ':';
    s = change(date.getSeconds());
    return Y + M + D + h + m + s;
} 

function change(t) {
    if (t < 10) {
        return "0" + t;
    } else {
        return t;
    }
}

//选择变电所
function selectStation(type){
    // 转向网页的地址;
    var logerButton = new Array();
    logerButton.push({  text: '确认', onclick: function (item, dialog){
        var arrM = new Array();
        //获取子页面
        arrM=document.getElementById('choseKey').contentWindow;
        var contentgrid = arrM.$("#grid").ligerGrid();
        var contentrows = contentgrid.getSelectedRows();
        if (contentrows == null || contentrows.length!=1) {
            $.ligerDialog.warn("请选择一条记录！","提示");
            return;
        };
        SUBSTATIONSN =contentrows[0].substationSn;
        SHORTNAME =contentrows[0].shortname;
        $("#selectStationName").val(contentrows[0].substationName+": "+SHORTNAME);
        if (!window.WebSocket) {
            window.WebSocket = window.MozWebSocket;
        }
        if (window.WebSocket) {
            ws = new WebSocket("${url}");
            ws.onopen = function (e) {
            	ws.send('{action:"connect",bds:"'+SHORTNAME+'"}');
            }
        }else {
            $.ligerDialog.error("你的浏览器不支持 WebSocket！", "提示");
        };
        dialog.close();
        }
    });
    if(type==1){
        logerButton.push({ text: '取消', onclick: function (item, dialog) { dialog.hide(); }});
    }
    var url='selectStationData';
    $.ligerDialog.open({
        //右上角的X隐藏，否则会有bug
        allowClose: false,
        height: 500,
        url: url,
        width: 1000,
        name:'choseKey',
        title:'变电站选择',
        isResize:true,
        buttons:logerButton
    }); 
}
</script>
</body>
</html>
