<%@ page language="java" import="java.util.*"
    contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title></title>
	<meta http-equiv="X-UA-Compatible" content="IE=8" />
	<link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script> 
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">

<!-- 查询条件部分 -->
<div id="grid_search" style="margin-left:8px;margin-top:20px;">
    <form name="searchForm" id="searchForm">
        <ul>
            <li class="label">
                &emsp;方案名称：<input type="text" id="search_planName" name="planName" placeholder="搜索名称" style="text-indent:5px;height:23px;border-radius: .25em;"/>
                &emsp;<button id="btnSearch" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
            </li>
        </ul>
    </form>
</div>
<br/>

<!-- 列表部分 -->
<div id="grid"></div>
<div id="saveDialog">
	<form id="saveForm">
		<table class="default">
			<tr>
				<td align="right">名称：</td>
				<td align="left">
				   <input id="planName" name="planName"  style="text-indent:5px;width:180px" placeholder="请输入名称"/>
				</td>
			</tr>
			<tr>
				<td align="right">描述：</td>
				<td align="left">
				<input id="remark" name="remark"  style="text-indent:5px;width:180px" placeholder="请输入描述"/>
				</td>
			</tr>
		</table>
	</form>
</div>
<script type="text/javascript">
var dialog = frameElement.dialog;
var dialogData = dialog.get('data');
var grid;
if(dialogData.shortName==""||dialogData.shortName==null){
    dialogData.shortName="emptyError";
}
$(function(){
    initGrid();

    // 绑定查询按钮
	$("#btnSearch").click(function () {
		var parms = serializeObject($("#searchForm"));
   	    for(var p in parms){
   	        grid.setParm(p,parms[p].trim());
   	    }
   	    grid.loadData();
   	});
    
    function initGrid(){
        grid=$("#grid").ligerGrid({
            title:'&nbsp;&nbsp;<span style="color:#386792">方案一览</span>',
            headerImg:'/hmdl/static/images/list.png',
            columns: [
					{ display: '方案名称', name: 'planName',id:'treeNode',width:200,align:'left'},
					{ display: '描述', name: 'remark',width:100},
					{ display: '创建时间', name: 'createDate',width:150},
				],
            toolbar: {
                items: [
					{ text: '保存当前页面方案',click: function(){operate();}, icon: 'fa fa-plus',color:'success' },
					{ text: '删除',click: function(){deletePlan();}, icon: 'fa fa-trash',color:'danger' },
					{ text: '刷新',click: function(){grid.reload();}, icon: 'fa fa-refresh',color:'primary'},
                ]
            },
            dataAction: 'server',
            url:'searchPlan',
            root:'data',
        	parms:[{name:"shortName",value:dialogData.shortName}],
            record:'count',
            height: '97%',
            pageSize: 10,
            checkbox: true,
            rownumbers: true
        });
    }
});

//保存
function operate(){
	$.ligerDialog.open({
	//右上角的X隐藏，否则会有bug
	allowClose: false,
	height: 200,  
	target:$("#saveDialog"),
	width: 500, 
	name:'choseKey',  
	title:'确认保存',  
	isResize:true,  
	buttons: [  {  text: '确认', onclick: function (item, dialog){ 	
	    		save();
				dialog.hide();
				}
			},  
	            { text: '关闭', onclick: function (item, dialog){dialog.hide();}}
	        ]                                     
	}); 
}

//保存处理
function save() {

	if(dialogData.varList=="{}" || dialogData.varList=='""'){
		$.ligerDialog.error("请先选择变量", "提示");
	    return;
	}
	if(dialogData.isRealtime=="" && dialogData.beginTime=="" && dialogData.endTime==""){
		$.ligerDialog.error("请先选择时间", "提示");
		return;
	}
	if($("#planName").val()==""){
		$.ligerDialog.error("请输入方案名称", "提示");
		return;
	}
	dialogData.planName=$("#planName").val();
	dialogData.remark=$("#remark").val();
    $.ajax({
        type: 'post',
        url: "savePlan",
        data: dialogData,
        cache: false,
        dataType: 'json',
        success: function (data) {
            if (data && data.retCode == 100) {
                $.ligerDialog.success('保存成功！', "提示", function (opt) {
                    grid.reload();
                });
            } else {
                var text = "";
                if (data.errors && data.errors.length) {
                    for (var i = 0; i < data.errors.length; i++) {
                        text = text + "<br/>" + data.errors[i];
                    }
                }
                $.ligerDialog.error(text, "提示");
            }
        }
    });
}

//删除
function deletePlan(){
   	var rows = grid.getSelectedRows();
   	var ids="";
   	for(var i=0 ; i < rows.length ; i++){
       	if(i>0){
				ids += "|";
       	}
       	ids += rows[i].id + "," + rows[i].createDate;
   	}
	$.ligerDialog.confirm("确定要删除选中记录？","提示", function(confirm){
    	if(confirm){
     		$.post("doDelete",{ids:ids},function(data){
            		if (data.retCode==100) {
            			$.ligerDialog.success('删除成功！',"提示",function(opt){
                    		grid.reload();
                 	});}
            		else if(data.retCode==-700){
                      		$.ligerDialog.error("删除失败！存在", "提示");
                   }else{
                      		$.ligerDialog.error("删除失败！", "提示");
                   }
           },"json")
  		}
	});
}
function resetAll(){
 // FORM清空
 $("#saveForm")[0].reset();
 // 隐藏项目清空
 $("#id").val("");
 $("#edit_type").val("");
}
</script>
</body>
</html>
