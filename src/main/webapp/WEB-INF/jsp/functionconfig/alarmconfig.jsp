<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="/hmdl/static/js/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/hmdl/static/js/ligerUI/skins/Aqua/css/ligerui-all.css"  rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/ligerui-icons.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/ligerUI/skins/Gray2014/css/all.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/table.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/hmdl/static/js/validator/jquery.validator.css" rel="stylesheet" type="text/css" />
    <script src="/hmdl/static/js/jquery/jquery-1.9.0.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/jquery.validate.min.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/ligerui.all.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/base.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery.validator.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/jquery-validate.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/validator/local/zh_CN.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/My97DatePicker/WdatePicker.js"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerDrag.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="/hmdl/static/js/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"></script>
    <style type="text/css">
        .l-case-title
        {
            font-weight: bold;
            margin-top: 20px;
            margin-bottom: 20px;
        }
        .l-dialog-tc {
		    font-weight: bold;
		    height: 29px;
		    line-height: 22px;
		    background: -webkit-gradient(linear, 0 0, 0 100%, from(#4A8BC2), to(#4A8BC2));
		}
		.l-dialog-win .l-dialog-content{
			background: #fcfcfc;
		}
		
    </style>
</head>
<!-- body部分 -->
<body style="overflow-x:hidden;overflow-y:hidden;padding-top:2px;background-color:#F9F9F9;">
<div id="layout1">
	<!-- 左侧树 -->
	<div position="left" title="变电所一览" style="height:calc(100% - 40px);overflow: auto; ">
		<!-- 变电所查询条件部分 -->
		<div id="org_search" style="margin-left:8px;margin-top:0px;">
			<ul>
				<li class="label">
					<input type="text" placeholder="输入名称进行搜索" style="text-indent:5px;height:23px;border-radius: .25em;width:60%;margin-left:0;margin-top: 5px" name="search_substationName" id="search_substationName"  />					
					<button type="button" id="org_btnSearch" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x"><i class="fa fa-search"></i></button>
					<button type="button" id="org_expend" onclick="expend()" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x"><i class="fa fa-plus"></i></button>
					<button type="button" id="org_unexpend" onclick="unexpend()" class="btn btn-default  btn-small btn-mini" style="width:26px;height:26px;margin-top: -5px;margin-right: 0x;display:none"><i class="fa fa-minus"></i></button>
				</li>
			</ul>
		</div><div id="orgInfoTree"></div><br/><br/>
		<input type="hidden" id="org_sn" name="org_sn" value=""/>
		<input type="hidden" id="org_name" name="org_name" value=""/>
	</div>
	<div position="center"  title="变电所" >
		<!-- 标题部分 -->
		<h3 class="hmdl_title_h3">功能配置
		    <span class="hmdl_title_span">
		        &nbsp;/&nbsp;报警配置
		    </span>
		</h3>
		<hr class="hmdl_title_hr"/>
		
		<!-- 查询条件部分 -->
		<div id="grid_search" style="margin-left:8px;margin-top:20px;">
		    <form name="searchForm" id="searchForm">
<!-- 		        &emsp;变电所：<input id="search_substationName" name="substationName"  type="text" style="text-indent:5px;width:150px;height:25px;border-radius: .25em;" placeholder="请选择变电所"/> -->
		        <!-- 隐藏域传值 -->
		        <input type="hidden" id="substationSn" name="substationSn"/>
		        &emsp;变量名称：<input id="var_name" name="varName"  type="text" placeholder="搜索变量名称" style="text-indent:5px;width:150px;height:25px;border-radius: .25em;"/>
		        &emsp;是否报警：
		        <select id="warningIsornot" onchange="checkFast()" name="warningIsornot" style="text-indent:5px;width:150px;height:28px;border-radius: .25em;">
		        	<option selected="selected" value="">选择是否报警</option>
		        	<option value="1">是</option>
		        	<option value="0">否</option>
		        </select>
		        &emsp;功能码：
		            <select id="search_funcCode" onchange="checkFast()" name="funcCode" style="text-indent:5px;height:23px;border-radius: .25em;">
					  <option value=""> == 请选择  == </option>
					  <option value="01"> == 读线圈01  == </option>
					  <option value="02"> == 读离散量输入02  == </option>
					  <option value="03"> == 寄存器03  == </option>
					  <option value="04"> == 读寄存器04  == </option>
				    </select>
		        &emsp;<button id="btnSearch" type="button" class="btn btn-primary  btn-small btn-mini"><i class="fa fa-search"></i>&nbsp;&nbsp;查询&nbsp;&nbsp;</button>
		    </form>
		</div>
		<br/>
		<!-- 列表部分 -->
		<div id="grid"></div>
		<!-- 全局报警参数 -->
		<form id="globalVar" style="text-align: left;display: none">
			<input id="startAlarm" name="startAlarm" type="checkbox"/>系统启动，根据变量初始值强制报警；<br /><br />
			<input id="closeWind" name="closeWind" type="checkbox"/>报警恢复，自动关闭对话框；<br /><br />
			模拟量报警最小变化间隔[秒]：<input id="analogTime" name="analogTime" type="number" style="width: 70px"/>
		</form>
		<!-- 导出用Form -->
		<form action="../report/show" method="post" target="_blank" id="reportForm">
			<input type="hidden" name="fparam" id="fparam"></input>
			<input type="hidden" name="fcolumn" id="fcolumn"></input>
			<input type="hidden" name="fname" id="fname"></input>
			<input type="hidden" name="fclassname" id="fclassname"></input>
			<input type="hidden" name="fmenu" id="fmenu"></input>
			<input type="hidden" name="ftitle" id="ftitle"></input>
			<input type="hidden" name="fexport" id="fexport"></input>
			<input type="hidden" name="furl" id="furl"></input>
		</form>
	</div>
</div>
<div id="var_config_win" style="display: none">
	<!---------开始---------------------------------------------------------------------------- 变量配置form部分 --------------------------------------------------------------------------开始----------->
	<div style="width: 99%;margin-left: 9px;text-align: center;background-color: white">
	<form id="submitVarForm">
		<!-- 隐藏值传递 -->
	<input type="hidden" id="stationDataDetail_id" name="id"/><!-- 当前变量的ID -->
	<!-- 隐藏值传递 -->
	<table align="center" style="height: 400px;width: 100%;border-color: white" border="1">
	 <tr bgcolor="3399CC" style="border: 0" style="height: 50px;">
	   <td colspan="5"  align="center" style="color: white;font-size: 26px;">
		<span id="dataTypeForForm"></span>
	   	&nbsp;【<span id="varNameForForm"></span>】&nbsp;报警配置
	   </td>
	 </tr>
	 <tr bgcolor="#87CEEB" align="center" style="border-top: 0;height: 40px;font-size: 18px;font-weight: bold">
	   <td colspan="3" align="center" style="color: #4169E1;border-right: 0;width: 60%">
	   	报警类型
	   </td>
	   <td colspan="2" align="center" style="color: #4169E1;border-left: 0;width: 40%">报警参数</td>
	 </tr>
	 <tr style="height:120px;font-size:16px;">
	 	<td bgcolor="#DCDCDC" style="width:10%;">
	 		报警类型:
	 	</td>
	 	<td id="td1" style="width:25%;">
				<table style="height: 100%;width: 100%">
					<tr>
						<td align="right" style="width: 35%">
							<input value="1" onclick="switchWarningNot()" type="radio" id="warningType1" name="warningType" />
						</td>
						<td align="left" style="width: 65%">
							HH-超高报警
						</td>
					</tr>
					<tr>
						<td align="right">
							<input value="2" onclick="switchWarningNot()" type="radio" id="warningType2" name="warningType" />
						</td>
						<td align="left">
							H-高报警
						</td>
					</tr>
					<tr>
						<td align="right">
							<input value="3" onclick="switchWarningNot()" type="radio" id="warningType3" name="warningType" />
						</td>
						<td align="left">
							L-低报警
						</td>
					</tr>
					<tr>
						<td align="right">
							<input value="4" onclick="switchWarningNot()" type="radio" id="warningType4" name="warningType" />
						</td>
						<td align="left">
							LL-超低报警
						</td>
					</tr>
				</table>
	 	</td>
	 	<td id="td2" style="width:25%;">
				<table style="height: 100%;width: 100%">
					<tr>
						<td align="right" style="width: 40%">
							<input value="5" onclick="switchWarning()" type="radio" id="warningType5" name="warningType" />
						</td>
						<td align="left" style="width: 60%">
							0->1 正跳变
						</td>
					</tr>
					<tr>
						<td align="right">
							<input value="6" onclick="switchWarning()" type="radio" id="warningType6" name="warningType" />
						</td>
						<td align="left">
							1->0 负跳变
						</td>
					</tr>
				</table>
	 	</td>
	 	<td bgcolor="#DCDCDC" style="width:10%;">
	 		报警参数:
	 	</td>
	 	<td id="td3" style="width:30%;">
	 		<table style="height: 100%;width: 100%">
	 			<tr>
	 				<td align="right" style="width: 35%">
	 					超高报警值：
	 				</td>
	 				<td align="center" style="width: 30%">
	 					<input onchange="switchWarningNot()" id="waringSetValueHH" name="waringSetValueHH" type="number" style="width: 100px"/>
	 				</td>
	 				<td align="left" style="width: 35%">
	 					<span class="unit"></span>
	 				</td>
	 			</tr>
	 			<tr>
	 				<td align="right">
	 					高 报 警 值：
	 				</td>
	 				<td align="center">
	 					<input onchange="switchWarningNot()" id="waringSetValueH" name="waringSetValueH" type="number" style="width: 100px" />
	 				</td>
	 				<td align="left">
	 					<span class="unit"></span>
	 				</td>
	 			</tr>
	 			<tr>
	 				<td align="right">
	 					低 报 警 值：
	 				</td>
	 				<td align="center">
	 					<input onchange="switchWarningNot()" id="waringSetValueL" name="waringSetValueL" type="number" style="width: 100px" />
	 				</td>
	 				<td align="left">
	 					<span class="unit"></span>
	 				</td>
	 			</tr>
	 			<tr>
	 				<td align="right">
	 					超低报警值：
	 				</td>
	 				<td align="center">
	 					<input onchange="switchWarningNot()" id="waringSetValueLL" name="waringSetValueLL" type="number" style="width: 100px" />
	 				</td>
	 				<td align="left">
	 					<span class="unit"></span>
	 				</td>
	 			</tr>
	 		</table>
	 	</td>
	 </tr>
	 <tr bgcolor="#B0E0E6"  align="center" style="height: 40px;font-size: 18px;font-weight: bold" >
	   <td colspan="3" align="center" style="color: #4169E1;border-right: 0">
	   	报警定义
	   </td>
	   <td colspan="2" align="center" style="color: #4169E1;border-left: 0">
	   	报警方式
	   </td>
	 </tr>
	 <tr style="height:120px;font-size: 16px">
	 	<td bgcolor="#DCDCDC" width="100px">
	 		报警定义:
	 	</td>
	 	<td colspan="2">
	 		<table style="height: 100%;width: 100%">
	 			<tr>
	 				<td align="right" style="width: 20%">
	 					级别：
	 				</td>
	 				<td align="left" style="width: 25%">
	 					<select style="text-indent:5px;width:120px;height:28px;border-radius: .25em;" id="warningStep" name="warningStep">
		  				<option value="1">1-故障</option>
		  				<option value="2">2-报警</option>
		  				<option value="3">3-警告</option>
		  			</select>
	 				</td>
	 				<td align="right" style="width: 15%">
	 					名称：
	 				</td>
	 				<td align="left" style="width: 40%">
	 					<input readonly="readonly" id="remark" name="remark"  style="text-indent:5px;width:78%;height:24px;border-radius: .25em;" type="text"/>
	 				</td>
	 			</tr>
	 			<tr>
	 				<td align="right">
	 					位置：
	 				</td>
	 				<td align="left" colspan="3">
	 					<input id="substationName" name="substationName" readonly="readonly" style="text-indent:5px;width:89%;height:28px;border-radius: .25em;">
	 				</td>
	 			</tr>
	 			<tr>
	 				<td align="right">
	 					信息：
	 				</td>
	 				<td align="left">
	 					<input id="warningMessage" name="warningMessage" type="text" style="text-indent:5px;width:100%;height:24px;border-radius: .25em;"/>
	 				</td>
	 				<td align="right">
	 					备注：
	 				</td>
	 				<td align="left">
	 					<input id="warningBrief" name="warningBrief" type="text" style="text-indent:5px;width:78%;height:24px;border-radius: .25em;"/>
	 				</td>
	 			</tr>
	 		</table>
	 	</td>
	 	<td bgcolor="#DCDCDC" width="100px">
	 		报警方式:
	 	</td>
	 	<td align="center">
	 		<table style="height: 100%;width: 100%">
	 			<tr>
	 				<td align="right" style="width: 35%">
	 					<input value="1" id="warningIsMessage" onclick="isMessageYesOrNo()" name="warningIsMessage" type="checkbox" style="text-indent:5px;width:16px;height:16px;border-radius: .25em;"/>
	 				</td>
	 				<td align="left" style="width: 65%">
	 					推送信息
						</td>
	 			</tr>
	 			<tr>
	 				<td align="right">
	 					<input value="1" onclick="isVoiceYesOrNo()" id="warningIsVoice" name="warningIsVoice" type="checkbox" style="text-indent:5px;width:16px;height:16px;border-radius: .25em;"/>
	 				</td>
	 				<td align="left">
	  				播放声音
	  				<select id="warningVoiceType" name="warningVoiceType" style="text-indent:5px;width:150px;height:26px;border-radius: .25em;">
			  			<option value="" >--选择声音文件--</option>
			  			<c:forEach items="${voiceList }" var="voice">
		  					<option value="${voice.voiceFile }">${voice.voiceName }</option>
		  				</c:forEach>
			  		</select>
			  		<button type="button" onclick="playVid()" style="margin-bottom:0px;text-indent:5px;width:auto;height:26px;border-radius: .25em;"><i class="fa fa-youtube-play"></i>试听</button>
	 				</td>
	 			</tr>
	 			<tr align="center">
	 				<td align="right">
	 					<input value="1" id="warningIsLoop" onclick="isLoopYesOrNo()" name="warningIsLoop" type="checkbox" style="text-indent:5px;width:16px;height:16px;border-radius: .25em;"/>
	 				</td>
	 				<td align="left">
	 					循环播放
	 					<select id="warningTimes" name="warningTimes" style="text-indent:5px;width:150px;height:26px;border-radius: .25em;">
				  			<option value="" >--选择播放次数--</option>
				  			<option value="1" >仅播放1次</option>
				  			<option value="3" >连续播放3次</option>
				  		</select>
	  				</td>
	  			</tr>
	  		</table>
	  	</td>
	  </tr>
	</table>
	<br/>
	<!--------开始-------------------------------------------------------------------- 发送短信内容 --弹出窗口-------------------------------------------------------------------------开始----------->
	<div id="send_msg_win" style="display: none;width: 400;height: 350;margin: 0;padding:10px;">
		<table style="height: 100px">
		<tr>
			<td>推送报警信息类型：</td>
			<td>
				<span style="background-color: white;">
					<input id="alarmStart" name="warningStart" value="1" type="checkbox" style="text-indent:5px;width:16px;height:16px;border-radius: .25em;"/>报警&nbsp;
					<input id="alarmSure" name="warningSure" value="1" type="checkbox" style="text-indent:5px;width:16px;height:16px;border-radius: .25em;"/>确认&nbsp;
					<input id="alarmRec" name="warningRec" value="1" type="checkbox" style="text-indent:5px;width:16px;height:16px;border-radius: .25em;"/>恢复
				</span>
			</td>
		</tr>
		<tr>
			<td>推送报警信息内容：</td>
			<td>
				<span style="background-color: white;">
					<input id="messageNum" name="warningMessageNum" value="1" type="checkbox" style="text-indent:5px;width:16px;height:16px;border-radius: .25em;"/>编号&nbsp;
					<input id="messageTime" name="warningMessageTime" value="1" type="checkbox" style="text-indent:5px;width:16px;height:16px;border-radius: .25em;"/>时间&nbsp;
					<input id="messageState" name="warningMessageState" value="1" type="checkbox" style="text-indent:5px;width:16px;height:16px;border-radius: .25em;"/>状态&nbsp;
					<input id="messageLocal" name="warningMessageLocal" value="1" type="checkbox" style="text-indent:5px;width:16px;height:16px;border-radius: .25em;"/>位置
				</span>
			</td>
		</tr>
		<tr>
			<td></td>
			<td>
				<span style="background-color: white;">
					<input id="messageName" name="warningMessageName" value="1" type="checkbox" style="text-indent:5px;width:16px;height:16px;border-radius: .25em;"/>名称&nbsp;
					<input id="messageInf" name="warningMessageInf" value="1" type="checkbox" style="text-indent:5px;width:16px;height:16px;border-radius: .25em;"/>信息&nbsp;
				<input id="messageValue" name="warningMessageValue" value="1" type="checkbox" style="text-indent:5px;width:16px;height:16px;border-radius: .25em;"/>数值&nbsp;
				<input id="messageUser" name="warningMessageUser" value="1" type="checkbox" style="text-indent:5px;width:16px;height:16px;border-radius: .25em;"/>用户
				</span>
			</td>
		</tr>
		</table>
	</div>
	<!--------结束-------------------------------------------------------------------- 发送短信内容 --弹出窗口-------------------------------------------------------------------------结束----------->			
		
	</form>
	<button id="btnSubmit" type="button" onclick="submitVarForm()" class="btn btn-lg btn-primary" style="height: 29px;width: 60px;padding:0;">
	<i class="fa fa-check"></i>&nbsp;<font style="font-size: 12px">提交</font>
	</button>
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	<button class="btn btn-warning" onclick="backProPage()" style="height: 29px;width: 60px;padding:0;">
	<i class="fa fa-remove"></i>&nbsp;<font style="font-size: 12px">取消</font>
		</button>
	</div>
	<!--------结束---------------------------------------------------------------------- 变量配置form部分 --------------------------------------------------------------------------结束----------->

</div>
<script type="text/javascript">
var grid;
var authWin;
var authTree;
var globalForm;
var globalDialog;
var sendMsgDialog;
//页面加载时执行
$(function(){
    //布局设置
    $("#layout1").ligerLayout({
    	leftWidth: 200,
    });
    //变电所树检索
    substationSearch();
    //初始化表格
    initGrid();
    // 绑定查询按钮
	$("#btnSearch").click(function () {
	    $("#substationSn").val($("#org_sn").val());//左侧树传值SN
	    var parms = serializeObject($("#searchForm"));
   	    for(var p in parms){
   	        grid.setParm(p,parms[p].trim());
   	    }
   	    grid.loadData();
   	});
});
//绑定变电所查询按钮
$("#org_btnSearch").click(function () {
	substationSearch();
});

//绑定变电所收缩
$("#org_btn_collapse").click(function () {
	orgTree.collapseAll();
});

//绑定变电所收缩
$("#endAddr,#beginAddr").blur(function () {
    $("#lenth").val($("#endAddr").val()-$("#beginAddr").val());
});
//确认选中行信息
function confirm(){
 	var rows = grid.getSelectedRows();
 	var ids="";
 	if (rows == "" || rows.length!=1) {
 		$.ligerDialog.warn("请选择一条记录！","警告");
 		return;
 	}
}

//初始化表格
function initGrid(){
	grid=$("#grid").ligerGrid({
	    title:'&nbsp;&nbsp;<span style="color:#386792">报警配置一览</span>',
	    headerImg:'/hmdl/static/images/list.png',
	    columns: [
				{ display: '报警开启/关闭', width:100,isAllowHide: false,render: function (row){
				    var img = row.warningIsornot;
				    if(img==1){
				        return '<a href="javascript:void(0);" onclick="turnOff('+row.id+')"><i style="font-size:30px;color:green" class="fa fa-toggle-on"></i></a>';
				    }
				    if(img==0){
				        return '<a href="javascript:void(0);" onclick="turnOn('+row.id+')"><i style="font-size:30px;color:gray" class="fa fa-toggle-off"></i></a>';
					}
				}},
				{ display: '变电所名称', name: 'substationName',width:160},
				{ display: '变量名称', name: 'varName',width:130},
				{ display: '节点', name: 'numberName',width:80},
				{ display: '数据类型', name: 'dataType',width:130,render:function(row){
				    var html = row.dataType;
				    switch (html) {
				    			case "01":
	                                return "开关";
	                                break;
	                            case "02":
	                                return "电流-A";
	                                break;
	                            case "03":
	                                return "电压-V";
	                                break;
	                            case "04":
	                                return "电压-kV";
	                                break;
	                            case "05":
	                                return "有功功率-kW";
	                                break;
	                            case "06":
	                                return "有功功率-MW";
	                                break;
	                            case "07":
	                                return "无功功率-kvar";
	                                break;
	                            case "08":
	                                return "无功功率-Mvar";
	                                break;
	                            case "09":
	                                return "功率因数";
	                                break;
	                            case "10":
	                                return "频率-Hz";
	                                break;
	                            case "11":
	                                return "电能-kWh";
	                                break;
	                            case "12":
	                                return "视在功率-kVA";
	                                break;
	                            case "13":
	                                return "视在功率-MVA";
	                                break;
	                            case "14":
	                                return "温度-°C";
	                                break;
	                            case "15":
	                                return "湿度-%";
	                                break;
	                            case "16":
	                                return "液位";
	                                break;
	                            case "17":
	                                return "档位";
	                                break;
	                            default:
	                                return "";
	                                break;
	                          }
				}},
				{ display: '报警类型', name: 'warningType',width:120,hide:true,render: function(row) {
				        var html = row.warningType;
					    switch (html) {
		    				case "1":
	                            return "HH超高报警";
	                            break;
	                        case "2":
	                            return "H高报警";
	                            break;
	                        case "3":
	                            return "L低报警";
	                            break;
	                        case "4":
	                            return "LL超低报警";
	                            break;
	                        case "5":
	                            return "0->正跳变";
	                            break;
	                        case "6":
	                            return "1->负跳变";
	                            break;
	                        case "7":
	                            return "0--1变位";
	                            break;
	                        default:
	                            return "";
	                            break;
	                     }
	            }},
				{ display: '信息', name: 'warningMessage',width:150},
				{ display: '备注', name: 'warningBrief',width:150},
				{ display: '更新时间', name: 'updateDate',width:180},
				{ display: '功能码', name: 'funcCode',hide:true,width:120},
				{ display: '变电所地址', name: 'substationAddress',hide:true,width:160}
	    ],
	    toolbar: {
	        items: [
				{ line: true },{ line: true },<%=request.getAttribute("btn")%>,
	        ]
	    },
	    dataAction: 'server',
	    url:'search',
	    root:'data',
	    record:'count',
	    height: '97%',
	    pageSize: 20,
	    checkbox: false,
	    rownumbers: true
	});
}
//全局报警参数设置
function allVar() {
    //首先获取已经设置的全局参数
    $.ajax({
		type : 'post',
		url : 'getGlobalVar',
		dataType : "json",
		success : function(data) {
			if(data.retObj && data.retCode == 100){
			    //初始化模拟量报警最小变化间隔[秒]
			    $("#analogTime").attr("value",data.retObj[0].analogTime);
			    //初始化系统启动，根据变量初始值强制报警；
			    if(data.retObj[0].startAlarm==1){
			        $("#startAlarm").attr("checked",true);
			    }else{
			        $("#startAlarm").attr("checked",false);
			    }
			    //初始化报警恢复，自动关闭对话框；
				if(data.retObj[0].closeWind==1){
				    $("#closeWind").attr("checked",true);
				}else{
				    $("#closeWind").attr("checked",false);
				}
				//打开弹窗，更新全局参数
				globalDialog = $.ligerDialog.open({
				    target:$("#globalVar"),
				    height: 170,
				    width:300,
				    allowClose:false,
				    title:'全局报警参数',
				    buttons: [
				        { text: '确定',onclick:function(item, dialog){
				          	//获取数据
						    var ant = $("#analogTime").prop("value");//模拟量报警最小变化间隔[秒]
						    var sta = 0;//系统启动，根据变量初始值强制报警；
						    var clw = 0;//报警恢复，自动关闭对话框；
						    if($("#startAlarm").prop("checked")==true){
						    	sta = 1;
						    }
						    if($("#closeWind").prop("checked")==true){
						        clw = 1;
						    }
				            //发送更新请求
				            $.ajax({
								type : 'post',
								url : 'updateGlobalVar',
								data : {"id":data.retObj[0].id,"ant":ant,"sta":sta,"clw":clw},//三个参数更新
								dataType : "json",
								success : function(data) {
								    if(data&&data.retCode==100){
								        $.ligerDialog.hide();
								        var success = $.ligerDialog.success("设置成功！","提示");
								        setTimeout(function(){success.close();},500);
								    }else{
								        $.ligerDialog.error("设置失败！","提示",function(){
									        $.ligerDialog.hide();
								        });
								    }
								},
								error:function(){
								    $.ligerDialog.error("操作错误！", "错误");
								}
				            });
				        }},
				        { text: '取消',onclick:function(item, dialog){
				            $.ligerDialog.hide();
				     	}}
				    ]
				});
			}else{
				//设置新的全局参数
				globalDialog = $.ligerDialog.open({
				    target:$("#globalVar"),
				    height: 170,
				    width:300,
				    allowClose:false,
				    title:'全局报警参数',
				    buttons: [
				        { text: '确定',onclick:function(item, dialog){
				          	//获取数据
						    var ant = $("#analogTime").prop("value");//模拟量报警最小变化间隔[秒]
						    var sta = 0;//系统启动，根据变量初始值强制报警；
						    var clw = 0;//报警恢复，自动关闭对话框；
						    if($("#startAlarm").prop("checked")==true){
						    	sta = 1;
						    }
						    if($("#closeWind").prop("checked")==true){
						        clw = 1;
						    }
				            //发送更新请求
				            $.ajax({
								type : 'post',
								url : 'addGlobalVar',
								data : {"ant":ant,"sta":sta,"clw":clw},//三个参数更新
								dataType : "json",
								success : function(data) {
								    if(data&&data.retCode==100){
								        $.ligerDialog.hide();
								        var success = $.ligerDialog.success("设置成功！","提示");
								        setTimeout(function(){success.close();},500);
								    }else{
								        $.ligerDialog.error("设置失败！","提示",function(){
									        $.ligerDialog.hide();
								        });
								    }
								},
								error:function(){
								    $.ligerDialog.error("操作错误！", "错误");
								}
				            });
				        }},
				        { text: '取消',onclick:function(item, dialog){
				            $.ligerDialog.hide();
				     	}}
				    ]
				});
			}
		}
	});
}

//清空全局参数
function resetGlobalSet(){
    $("#analogTime").removeAttr("value");
    $("#startAlarm").attr("checked",false);
    $("#closeWind").attr("checked",false);
}
//开启变量报警
function turnOn(id) {
	if(id==null){
	    $.ligerDialog.warn("请选择一条记录！","警告");
		return;
	}
    //判断是否已确认，确认就变成已处理，已处理的进行页面刷新
    $.ajax({
        type : 'post',
        url : 'turnOn?id='+id,
        dataType : "json",
    });
    grid.loadData();
}
//关闭变量报警
function turnOff(id) {
    if(id==null){
	    $.ligerDialog.warn("请选择一条记录！","警告");
		return;
	}
    //判断是否已确认，确认就变成已处理，已处理的进行页面刷新
    $.ajax({
        type : 'post',
        url : 'turnOff?id='+id,
        dataType : "json",
    });
  	//重载表格数据
    grid.loadData();
}

//回车键搜索
$("input,select").keydown(function (e) {//当按下按键时
    if (e.which == 13) {//.which属性判断按下的是哪个键，回车键的键位序号为13
        var parms = serializeObject($("#searchForm"));
		for(var p in parms){
	        grid.setParm(p,parms[p].trim());
	    }
	    grid.loadData();
    }
});


//清空变量配置form表单
function resetForm(){
    document.getElementById("submitVarForm").reset();
}

//初始化变量配置页面
function initConfigInfo() {
    var rows = grid.getSelectedRows();
	if (rows == "" || rows.length!=1) {
		$.ligerDialog.warn("请选择一条记录！","警告");
		return;
	}
    //重置表单
	resetForm();
	//DIV显示/隐藏
	$("#layout1").hide();
	$("#var_config_win").show();
	//配置页面显示选择行详细信息
	$("#stationDataDetail_id").val(rows[0].id);
	$("#dataTypeForForm").html(showDataType(rows[0].dataType));//数据类型显示（表单题目）
	$("#varNameForForm").html(rows[0].varName);//显示varName
	showWarningType(rows[0].warningType);//显示报警类型
	$("#waringSetValueHH").val(rows[0].waringSetValueHH);
	$("#waringSetValueH").val(rows[0].waringSetValueH);
	$("#waringSetValueL").val(rows[0].waringSetValueL);
	$("#waringSetValueLL").val(rows[0].waringSetValueLL);
	showWarningStep(rows[0].warningStep);//回显报警级别
	$("#remark").val(rows[0].remark);
	$("#substationName").val(rows[0].substationName);
	$("#warningMessage").val(rows[0].warningMessage);
	$("#warningBrief").val(rows[0].warningBrief);
	showWarningIsMessage(rows[0].warningIsMessage);
	showWarningIsVoice(rows[0].warningIsVoice);
	showWarningVoiceType(rows[0].warningVoiceType);
	showWarningIsLoop(rows[0].warningIsLoop);
	$("#warningTimes").val(rows[0].warningTimes);
	//推送消息窗口回显
	showSendMessage(rows[0]);
	initTypeAndValue(rows[0]);//初始化报警类型、报警参数
}
//初始化报警类型、报警参数
function initTypeAndValue(rows){
    var dataType = rows.dataType;
    if (dataType == "01") {
        $("#td1").css("background","#DCDCDC");//背景灰色
        $("#td2").css("background","white");
        $("#td3").css("background","#DCDCDC");//背景灰色
        //设置disabled
        makeDisabledOne();
        //判断报警类别的属性checked是否为true，为false则禁止提交
        var wt5 = $("#warningType5").prop("checked");
        var wt6 = $("#warningType6").prop("checked");
        if (wt5 == false && wt6 == false) {
            $("#btnSubmit").attr("disabled", true); //提交按钮不可用
        } else {
            $("#btnSubmit").attr("disabled", false); //提交按钮可用
        }
    } else {
        //2、单位
        if (dataType == "01") {
            $(".unit").html("");
        } else if (dataType == "02") {
            $(".unit").html("A");
        } else if (dataType == "03") {
            $(".unit").html("V");
        } else if (dataType == "04") {
            $(".unit").html("kV");
        } else if (dataType == "05") {
            $(".unit").html("kW");
        } else if (dataType == "06"){
            $(".unit").html("MW");
        }else if (dataType == "07") {
            $(".unit").html("kvar");
        } else if (dataType == "08") {
            $(".unit").html("Mvar");
        } else if (dataType == "09") {
            $(".unit").html("");
        } else if (dataType == "10") {
            $(".unit").html("Hz");
        } else if (dataType == "11"){
            $(".unit").html("kWh");
        }else if (dataType == "12") {
            $(".unit").html("kVA");
        } else if (dataType == "13") {
            $(".unit").html("MVA");
        } else if (dataType == "14") {
            $(".unit").html("°C");
        } else if (dataType == "15") {
            $(".unit").html("m");
        } else if (dataType == "16"){
            $(".unit").html("%");
        }else{
            $(".unit").html("");
        }
        //设定背景色
        $("#td1").css("background","white");
        $("#td2").css("background","#DCDCDC");//背景灰色
        $("#td3").css("background","white");
        //设置disabled
        makeDisabledTwo();
        checkTypeAndValue();//根据当前选择的报警类别，开放对应的报警输入值框disabled
        //判断报警类别的属性checked是否为true，为false则禁止提交
        var val1 = $("#waringSetValueHH").val();
        var val2 = $("#waringSetValueH").val();
        var val3 = $("#waringSetValueL").val();
        var val4 = $("#waringSetValueLL").val();
        if (val1 == false && val2 == false && val3 == false && val4 == false) {
            $("#btnSubmit").attr("disabled", true); //提交按钮不可用
        } else {
            $("#btnSubmit").attr("disabled", false); //提交按钮可用
        }
    }
}
//设置非开关类型的input属性disabled为true,请求后台不被传输
function makeDisabledOne() {
    //非开关
    $("#warningType1").attr("disabled", true);
    $("#warningType2").attr("disabled", true);
    $("#warningType3").attr("disabled", true);
    $("#warningType4").attr("disabled", true);
    $("#waringSetValueHH").attr("disabled", true);
    $("#waringSetValueH").attr("disabled", true);
    $("#waringSetValueL").attr("disabled", true);
    $("#waringSetValueLL").attr("disabled", true);
    //开关
    $("#warningType5").attr("disabled", false);
    $("#warningType6").attr("disabled", false);
}
//设置开关类型的input属性disabled为true,请求后台不被传输
function makeDisabledTwo() {
    //非开关
    $("#warningType1").attr("disabled", false);
    $("#warningType2").attr("disabled", false);
    $("#warningType3").attr("disabled", false);
    $("#warningType4").attr("disabled", false);
    $("#waringSetValueHH").attr("disabled", false);
    $("#waringSetValueH").attr("disabled", false);
    $("#waringSetValueL").attr("disabled", false);
    $("#waringSetValueLL").attr("disabled", false);
    //开关
    $("#warningType5").attr("disabled", true);
    $("#warningType6").attr("disabled", true);
}
//报警类别及报警值
function checkTypeAndValue() {
  	
    //判断报警类别是否为空
    var wt1 = $("#warningType1").prop("checked");
    var wt2 = $("#warningType2").prop("checked");
    var wt3 = $("#warningType3").prop("checked");
    var wt4 = $("#warningType4").prop("checked");
    //如果都为空，则禁止所有报警值输入
    if (wt1 == false && wt2 == false && wt3 == false && wt4 == false) {
        $("#waringSetValueHH").attr("disabled", true);
        $("#waringSetValueH").attr("disabled", true);
        $("#waringSetValueL").attr("disabled", true);
        $("#waringSetValueLL").attr("disabled", true);
    }else{
        if(wt1 == true && wt2 == false && wt3 == false && wt4 == false){
            $("#waringSetValueHH").attr("disabled", false);
            $("#waringSetValueH").attr("disabled", true);
            $("#waringSetValueL").attr("disabled", true);
            $("#waringSetValueLL").attr("disabled", true);
        }
        if(wt1 == false && wt2 == true && wt3 == false && wt4 == false){
            $("#waringSetValueHH").attr("disabled", true);
            $("#waringSetValueH").attr("disabled", false);
            $("#waringSetValueL").attr("disabled", true);
            $("#waringSetValueLL").attr("disabled", true);
        }
        if(wt1 == false && wt2 == false && wt3 == true && wt4 == false){
            $("#waringSetValueHH").attr("disabled", true);
            $("#waringSetValueH").attr("disabled", true);
            $("#waringSetValueL").attr("disabled", false);
            $("#waringSetValueLL").attr("disabled", true);
        }
        if(wt1 == false && wt2 == false && wt3 == false && wt4 == true){
            $("#waringSetValueHH").attr("disabled", true);
            $("#waringSetValueH").attr("disabled", true);
            $("#waringSetValueL").attr("disabled", true);
            $("#waringSetValueLL").attr("disabled", false);
        }
    }
}
//非开关报警类别变化时，更改input的disabled属性
function switchWarningNot() {
    //获取每个报警类别勾选框
    var wt1 = $("#warningType1");
    var wt2 = $("#warningType2");
    var wt3 = $("#warningType3");
    var wt4 = $("#warningType4");
    //获取每个报警类别值输入框
    var val1 = $("#waringSetValueHH").val();
    var val2 = $("#waringSetValueH").val();
    var val3 = $("#waringSetValueL").val();
    var val4 = $("#waringSetValueLL").val();
    //如果所有值都为0的话，提交按钮不可用
    if(val1.trim()=="" && val2.trim()=="" && val3.trim()=="" && val4.trim()==""){
        $("#btnSubmit").attr("disabled", true); //提交按钮不可用
    }else{
        $("#btnSubmit").attr("disabled", false); //提交按钮可用
    }
    if (wt1.prop("checked") == true) {
        $("#waringSetValueHH").attr("disabled", false);
        $("#waringSetValueH").attr("disabled", true);
        $("#waringSetValueL").attr("disabled", true);
        $("#waringSetValueLL").attr("disabled", true);
    } else if (wt2.prop("checked") == true) {
        $("#waringSetValueHH").attr("disabled", true);
        $("#waringSetValueH").attr("disabled", false);
        $("#waringSetValueL").attr("disabled", true);
        $("#waringSetValueLL").attr("disabled", true);
    } else if (wt3.prop("checked") == true) {
        $("#waringSetValueHH").attr("disabled", true);
        $("#waringSetValueH").attr("disabled", true);
        $("#waringSetValueL").attr("disabled", false);
        $("#waringSetValueLL").attr("disabled", true);
    } else if (wt4.prop("checked") == true) {
        $("#waringSetValueHH").attr("disabled", true);
        $("#waringSetValueH").attr("disabled", true);
        $("#waringSetValueL").attr("disabled", true);
        $("#waringSetValueLL").attr("disabled", false);
    }
}
//开关报警类别变化时，更改提交按钮的disabled属性
function switchWarning() {
    var wt5 = $("#warningType5");
    var wt6 = $("#warningType6");
    if (wt5.prop("checked") == true) {
        $("#btnSubmit").attr("disabled", false); //提交按钮可用
    } else if (wt6.prop("checked") == true) {
        $("#btnSubmit").attr("disabled", false); //提交按钮可用
    }
}
//判断发送短信是否被勾选
function isMessageYesOrNo() {
    var boo = $("#warningIsMessage").prop("checked");
    if (boo) {//被勾选
      	//如果被勾选，则弹出推送信息窗口
        popUp();
    }else{
        //清空Form表单
        resetMsg();
    }
}
//清空弹出框勾选内容
function resetMsg(){
    $("#alarmStart").attr("checked", false);
    $("#alarmSure").attr("checked", false);
    $("#alarmRec").attr("checked", false);
    $("#messageNum").attr("checked", false);
    $("#messageTime").attr("checked", false);
    $("#messageState").attr("checked", false);
    $("#messageLocal").attr("checked", false);
    $("#messageName").attr("checked", false);
    $("#messageInf").attr("checked", false);
    $("#messageValue").attr("checked", false);
    $("#messageUser").attr("checked", false);
}
//判断推送消息窗口是否有勾选内容（返回true为有勾选，false为没有）
function checkedAll(){
    var boo1 = $("#alarmStart").prop("checked");
    var boo2 = $("#alarmSure").prop("checked");
    var boo3 = $("#alarmRec").prop("checked");
    var boo4 = $("#messageNum").prop("checked");
    var boo5 = $("#messageTime").prop("checked");
    var boo6 = $("#messageState").prop("checked");
    var boo7 = $("#messageLocal").prop("checked");
    var boo8 = $("#messageName").prop("checked");
    var boo9 = $("#messageInf").prop("checked");
    var boo10 = $("#messageValue").prop("checked");
    var boo11 = $("#messageUser").prop("checked");
    if(!boo1&&!boo2&&!boo3&&!boo4&&!boo5&&!boo6&&!boo7&&!boo8&&!boo9&&!boo10&&!boo11){
        return false;
    }
    return true;
}
//推送信息弹窗
function popUp(){
    if ($("#warningIsMessage").prop("checked")) {
        sendMsgDialog = $.ligerDialog.open({
            target: $("#send_msg_win"),
            height: 200,
            width: 360,
            allowClose: false,
            title: '推送报警信息',
            buttons: [{
                text: '确定',
                onclick: function(item, dialog) {
                    if(!checkedAll()){
                        $.ligerDialog.warn("请选择推送消息要包含的内容！","提示");
                        return;
                    }
                    //隐藏弹窗
                    $.ligerDialog.hide();
                }
            },
            {
                text: '取消',
                onclick: function(item, dialog) {
                    resetMsg();//重置
                    $("#warningIsMessage").prop("checked", false);//取消推送内容勾选
                    $.ligerDialog.hide();//隐藏弹窗
                }
            }]
        });
    }
}
//判断声音播放是否被勾选
function isVoiceYesOrNo(){
    if ($("#warningIsVoice").prop("checked")) {
        //声音文件可选择，可传值
        $("#warningVoiceType").attr("disabled", false);
    } else {
      	//声音文件不可选择，不可传值
        $("#warningVoiceType").attr("disabled", true);
        //恢复声音文件第一个被选中
        $("#warningVoiceType").children().first().prop("selected",true);
      	//恢复循环播放未选中状态
        $("#warningIsLoop").prop("checked",false);
      	//恢复循环播放第一个被选中
        $("#warningTimes").children().first().prop("selected",true);
      	//恢复循环播放不可选择，不可传值
        $("#warningTimes").prop("disabled",true);
    }
}
//判断循环播放是否被勾选
function isLoopYesOrNo(){
    if ($("#warningIsVoice").prop("checked")) {
        if($("#warningIsLoop").prop("checked")){
	        //允许选择次数
	        $("#warningTimes").prop("disabled",false);
        }else{
          	//禁止选择次数
	        $("#warningTimes").prop("disabled",true);
        }
    } else {
        $.ligerDialog.warn("请先选择播放声音文件！","提示");
        $("#warningIsLoop").prop("checked",false);
    }
}
//回显声音类型
function showWarningVoiceType(warningVoiceType){
    //遍历域中的数据voiceList
    <c:forEach items="${voiceList}" var="voice">
    	var id = '${voice.id }';
    	var voiceName = '${voice.voiceName }';
    	var voiceFile = '${voice.voiceFile }';
    	
    	if(id==warningVoiceType){
    	    $("#warningVoiceType").val(voiceFile);
    	}
    </c:forEach>
}
//回显是否推送信息
function showWarningIsMessage(warningIsMessage){
    if(warningIsMessage==0){
        $("#warningIsMessage").prop("checked",false);
    }else if(warningIsMessage==1){
        $("#warningIsMessage").prop("checked",true);
    }
}
//回显是否播放声音
function showWarningIsVoice(warningIsVoice){
    if(warningIsVoice==0){
        $("#warningIsVoice").prop("checked",false);
        //如果不播放声音，则不可选择声音文件
        $("#warningVoiceType").prop("disabled",true);
    }else if(warningIsVoice==1){
        $("#warningIsVoice").prop("checked",true);
      	//如果播放声音，则可选择声音文件
        $("#warningVoiceType").prop("disabled",false);
    }
}
//回显是否循环播放
function showWarningIsLoop(warningIsLoop){
    if(warningIsLoop==0){
        $("#warningIsLoop").prop("checked",false);
      	//如果不循环播放，则不可选择播放次数
        $("#warningTimes").prop("disabled",true);
    }else if(warningIsLoop==1){
        $("#warningIsLoop").prop("checked",true);
      	//如果循环播放，则可选择播放次数
        $("#warningTimes").prop("disabled",false);
    }
}

//回显推送消息配置
function showSendMessage(row){
    if(row.warningStart==0){
        $("#alarmStart").prop("checked",false);
    }else if(row.warningStart==1){
        $("#alarmStart").prop("checked",true);
    }
    
    if(row.warningSure==0){
        $("#alarmSure").prop("checked",false);
    }else if(row.warningSure==1){
        $("#alarmSure").prop("checked",true);
    }
    
    if(row.warningRec==0){
        $("#alarmRec").prop("checked",false);
    }else if(row.warningRec==1){
        $("#alarmRec").prop("checked",true);
    }
    
    if(row.warningMessageNum==0){
        $("#messageNum").prop("checked",false);
    }else if(row.warningMessageNum==1){
        $("#messageNum").prop("checked",true);
    }
    
    if(row.warningMessageTime==0){
        $("#messageTime").prop("checked",false);
    }else if(row.warningMessageTime==1){
        $("#messageTime").prop("checked",true);
    }
    
    if(row.warningMessageState==0){
        $("#messageState").prop("checked",false);
    }else if(row.warningMessageState==1){
        $("#messageState").prop("checked",true);
    }
    
    if(row.warningMessageLocal==0){
        $("#messageLocal").prop("checked",false);
    }else if(row.warningMessageLocal==1){
        $("#messageLocal").prop("checked",true);
    }
    
    if(row.warningMessageName==0){
        $("#messageName").prop("checked",false);
    }else if(row.warningMessageName==1){
        $("#messageName").prop("checked",true);
    }
    
    if(row.warningMessageInf==0){
        $("#messageInf").prop("checked",false);
    }else if(row.warningMessageInf==1){
        $("#messageInf").prop("checked",true);
    }
    
    if(row.warningMessageValue==0){
        $("#messageValue").prop("checked",false);
    }else if(row.warningMessageValue==1){
        $("#messageValue").prop("checked",true);
    }
    
    if(row.warningMessageUser==0){
        $("#messageUser").prop("checked",false);
    }else if(row.warningMessageUser==1){
        $("#messageUser").prop("checked",true);
    }
}
//回显报警级别
function showWarningStep(warningStep){
    if(warningStep==1){
        $("#warningStep").val("1");
    }else if(warningStep==2){
        $("#warningStep").val("2");
    }else if(warningStep==3){
        $("#warningStep").val("3");
    }
}
//回显warningType
function showWarningType(warningType){
    if(warningType==null){
        return;
    }
    switch (warningType) {
		case "1":
		    $("#warningType1").prop("checked",true);
		break;
		case "2":
		    $("#warningType2").prop("checked",true);
		break;
		case "3":
		    $("#warningType3").prop("checked",true);
		break;
		case "4":
		    $("#warningType4").prop("checked",true);
		break;
		case "5":
		    $("#warningType5").prop("checked",true);
		break;
		case "6":
		    $("#warningType6").prop("checked",true);
		break;
		case "7":
		    $("#warningType7").prop("checked",true);
		break;
		default:
		    return "";
		break;
	}
}
//数据类型显示（表单题目）
function showDataType(dataType){
    switch (dataType){
		case "01":
            return "开关";
            break;
        case "02":
            return "电流-A";
            break;
        case "03":
            return "电压-V";
            break;
        case "04":
            return "电压-kV";
            break;
        case "05":
            return "有功功率-kW";
            break;
        case "06":
            return "有功功率-MW";
            break;
        case "07":
            return "无功功率-kvar";
            break;
        case "08":
            return "无功功率-Mvar";
            break;
        case "09":
            return "功率因数";
            break;
        case "10":
            return "频率-Hz";
            break;
        case "11":
            return "电能-kWh";
            break;
        case "12":
            return "视在功率-kVA";
            break;
        case "13":
            return "视在功率-MVA";
            break;
        case "14":
            return "温度-°C";
            break;
        case "15":
            return "湿度-%";
            break;
        case "16":
            return "液位";
            break;
        case "17":
            return "档位";
            break;
        default:
            return "";
            break;
	}
}
//页面返回
function backProPage() {
  	//DIV显示/隐藏
	$("#layout1").show();
	$("#var_config_win").hide();
}
//报警声音试听
function playVid(){
    var voiceFile = $("#warningVoiceType").val();
    if(voiceFile!=""){
	    var audio = new Audio(voiceFile);
	    audio.play();
    }
}
//提交表单
function submitVarForm() {
    //提交前，进行判空
    if(!checkBeforeSubmit()){
        return;
    }
    //提交配置信息
    $.ligerDialog.confirm('确认配置信息？', "提示",
        function(confirm) {
            if(confirm){
                //获取表单数据
             	var data = getFormData();  
                //请求更新操作
                $.ajax({
                    type: 'post',
                    url: "updateVar",
                    data: data,
                    cache: false,
                    dataType: 'json',
                    success: function(data) {
                        if (data && data.retCode == 100) {
                            var success = $.ligerDialog.success('配置成功!', "提示");
                            var dialog = frameElement.dialog; //调用页面的dialog对象(ligerui对象)
                            setTimeout(function(){success.close();},600);
                          	//重置表单
	                    	resetForm();
                          	//刷新表格
                          	grid.reload();
	                    	//DIV显示/隐藏
	                    	$("#layout1").show();
	                    	$("#var_config_win").hide();
                        } else {
                            $.ligerDialog.warn("配置失败！请重新再试！", "提示");
                        }
                    },
                	error:function(){
                		$.ligerDialog.error("操作错误！尝试刷新页面重新操作或者联系系统管理员！", "提示");
                	}
                });
            }else{
                return;
            }
     });    
}
//提交前判空
function checkBeforeSubmit(){
    if($("#warningIsVoice").prop("checked") && $("#warningVoiceType").val()==""){
        $.ligerDialog.warn("没有选择声音文件！","提示");
        return false;
    }
    if($("#warningIsLoop").prop("checked") && $("#warningTimes").val()==""){
        $.ligerDialog.warn("没有选择播放次数！","提示");
        return false;
    }
    return true;
}
//获取数据(返回值：表单的数据data)
function getFormData(){
    //将所有报警值输入框解除disabled
    $("#waringSetValueHH").attr("disabled", false);
    $("#waringSetValueH").attr("disabled", false);
    $("#waringSetValueL").attr("disabled", false);
    $("#waringSetValueLL").attr("disabled", false);
  	//序列化表单对象
    var data = serializeObject($("#submitVarForm"));
  	//获取推送消息弹窗的配置信息
    if($("#alarmStart").prop("checked")){
        data.warningStart=1;
    }else{
        data.warningStart=0;
    }
    if($("#alarmSure").prop("checked")){
        data.warningSure=1;
    }else{
        data.warningSure=0;
    }
    if($("#alarmRec").prop("checked")){
        data.warningRec=1;
    }else{
        data.warningRec=0;
    }
    if($("#messageNum").prop("checked")){
        data.warningMessageNum=1;
    }else{
        data.warningMessageNum=0;
    }
    if($("#messageTime").prop("checked")){
        data.warningMessageTime=1;
    }else{
        data.warningMessageTime=0;
    }
    if($("#messageState").prop("checked")){
        data.warningMessageState=1;
    }else{
        data.warningMessageState=0;
    }
    if($("#messageLocal").prop("checked")){
        data.warningMessageLocal=1;
    }else{
        data.warningMessageLocal=0;
    }
    if($("#messageName").prop("checked")){
        data.warningMessageName=1;
    }else{
        data.warningMessageName=0;
    }
    if($("#messageInf").prop("checked")){
        data.warningMessageInf=1;
    }else{
        data.warningMessageInf=0;
    }
    if($("#messageValue").prop("checked")){
        data.warningMessageValue=1;
    }else{
        data.warningMessageValue=0;
    }
    if($("#messageUser").prop("checked")){
        data.warningMessageUser=1;
    }else{
        data.warningMessageUser=0;
    }
    return data;
}
//快速检索
function checkFast(){
    var parms = serializeObject($("#searchForm"));
    for(var p in parms){
        grid.setParm(p,parms[p].trim());
    }
 	grid.loadData();
}
</script>
</body>  
</html>

