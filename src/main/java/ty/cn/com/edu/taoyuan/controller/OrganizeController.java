package ty.cn.com.edu.taoyuan.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ty.cn.com.edu.taoyuan.bean.res.DepartmentResVo;
import ty.cn.com.edu.taoyuan.common.bean.ActionRetBean;
import ty.cn.com.edu.taoyuan.common.bean.PageRetBean;
import ty.cn.com.edu.taoyuan.common.consts.FrameConsts;
import ty.cn.com.edu.taoyuan.common.consts.MsgConsts;
import ty.cn.com.edu.taoyuan.common.pojo.Department;
import ty.cn.com.edu.taoyuan.common.pojo.Key;
import ty.cn.com.edu.taoyuan.common.pojo.ResourceInfo;
import ty.cn.com.edu.taoyuan.common.service.ICommonService;
import ty.cn.com.edu.taoyuan.common.util.CommUtil;
import ty.cn.com.edu.taoyuan.common.util.MsgUtil;
import ty.cn.com.edu.taoyuan.system.bean.DepartmentBean;
import ty.cn.com.edu.taoyuan.system.bean.UserBean;
import ty.cn.com.edu.taoyuan.system.service.IDepartmentService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * 组织
 * 
 * @author zhangrui
 */
@Controller
@RequestMapping("/organize")
public class OrganizeController {

	/**
	 * 日誌
	 */
	private static Logger logger = Logger.getLogger(OrganizeController.class);

	/**
	 * 部门Server
	 */
	@Resource
	private IDepartmentService departmentService;

	/**
	 * 共通Service
	 */
	@Resource
	private ICommonService commonService;

	/**
	 * 页面启动  系统管理-部门管理
     * @return 页面地址
	 */
	@RequestMapping("/show")
	public String show(HttpServletRequest request, Model model, @RequestParam("parentId") Long parentId) {
		logger.info("页面启动  系统管理-部门管理");
		UserBean bean = (UserBean) request.getSession().getAttribute(FrameConsts.SESSION_KEY);
		List<ResourceInfo> list = commonService.getControlerBtn(bean.getRoleSn(), parentId);
		CommUtil.doSetBtns(list, model);
		return "organize/organize";
	}

	/**
	 * 部门列表检索
     * @return 数据集
	 */
	@RequestMapping("/search")
	@ResponseBody
	public List<DepartmentResVo> search(@ModelAttribute("department") DepartmentBean department) {
		logger.info("部门列表检索 开始");
		PageRetBean<DepartmentBean> ret = departmentService.search(department);
		//转换
        List<DepartmentResVo> resVos = new ArrayList<>();
        for(DepartmentBean departmentBean : ret.getData()){
            DepartmentResVo vo = new DepartmentResVo();
            vo.setId(departmentBean.getDepartmentSn());
            vo.setPid(departmentBean.getParentSn());
            vo.setDepartmentName(departmentBean.getDepartmentName());
            vo.setContactUser(departmentBean.getContactUser());
            vo.setContactTel(departmentBean.getContactTel());
            vo.setAddress(departmentBean.getAddress());
            vo.setType(departmentBean.getType());
            vo.setRemark(departmentBean.getRemark());
            resVos.add(vo);
        }
		logger.info("部门列表检索 结束");
		return resVos;
	}
	
    /**
     * 获取部门
     * @return 数据集
     */
    @RequestMapping("/getDepartment")
    @ResponseBody
    public ActionRetBean<?> getDepartment() {
        @SuppressWarnings("rawtypes")
        ActionRetBean<?> ret = new ActionRetBean();        
        List<Key> result = new ArrayList<Key>();
        result = departmentService.getDepartment();
        ret.setRetObj(result);
        ret.setRetCode(FrameConsts.NORMAL);        
        return ret;
    }
    /**
     * 部门明细检索
     * @param id 根据id获取部门明细
     * @return 数据集
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping("/getDetail")
    @ResponseBody
    public ActionRetBean<?> getDetail(@RequestParam("id") Long id) {
        logger.info("部门明细检索 开始");

        ActionRetBean<?> ret = new ActionRetBean();
        Department dict = departmentService.selectDepartmentById(id);
        if (dict != null) {
            ret.setRetObj(dict);
            ret.setRetCode(FrameConsts.NORMAL);
        } else {
            ret.setRetCode(FrameConsts.NOTFIND_DATA);
            ret.getErrors().add(MsgUtil.getCommMsg(MsgConsts.MSG_E_001));
        }
        logger.info("部门明细检索 结束");
        return ret;
    }
    /**
     * 部门保存（更新、插入）
     * @return 数据集
     */
    @SuppressWarnings("rawtypes")
    @RequestMapping("/save")
    @ResponseBody
    public ActionRetBean save(ModelMap model, HttpServletRequest request, @Validated @ModelAttribute("department") Department department, BindingResult bindingResult) {
        logger.info("部门保存 开始");
        ActionRetBean<?> ret = new ActionRetBean();
        // 获取校验错误信息
        if (bindingResult.hasErrors()) {
            // 输出错误信息
            List<ObjectError> allErrors = bindingResult.getAllErrors();
            for (ObjectError objectError : allErrors) {
                ret.getErrors().add(objectError.getDefaultMessage());
            }
            ret.setRetCode(FrameConsts.CHECK_ERROR);
            logger.info("部门保存 异常");
            return ret;
        }
        department.setCreater(commonService.getUserSn(request));
        department.setUpdater(commonService.getUserSn(request));
        int record = departmentService.saveDepartment(department);
        if (record > 0) {
            ret.setRetCode(FrameConsts.NORMAL);
        } else {
            ret.setRetCode(FrameConsts.EXCLUSIVE_ERROR);
        }
        logger.info("账号保存 结束");
        return ret;
    }
    /**
     * 账号删除
     * @param ids 拼接的多个id加逗号加时间字符串再以|拼接 
     * @return 数据集
     */
    @RequestMapping("/doDelete")
    @ResponseBody
    public ActionRetBean<Department> doDelete(@RequestParam("ids") String ids) {
        logger.info("部门删除 开始");
        ActionRetBean<Department> ret = new ActionRetBean<Department>();
        ret.setRetCode(FrameConsts.EXCLUSIVE_ERROR);
        int record = departmentService.deleteDepartment(ids);
        if(record>0){
            ret.setRetCode(FrameConsts.NORMAL);
        }else{
            ret.setRetCode(FrameConsts.EXCLUSIVE_ERROR);
            ret.getErrors().add(MsgUtil.getCommMsg(MsgConsts.MSG_E_001));
        }
        logger.info("部门删除 结束");
        return ret;
    }
}
