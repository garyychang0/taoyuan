/*
 * $Id: FrameConsts.java 42 2014-07-26 03:04:20Z guyejun $
 */

package ty.commons.consts;

/**
 * 共通画面定义常量
 * @author guyejun
 */
public interface FrameConstsTwo {

    
    /**
     * 设置
     */
    String SET_PATH = "set.properties";
    
    /**
     * 设置
     */
    String ET_PATH = "et.properties";
    
    
    /**
     * 设置
     */
    String ER_PATH = "error.properties";
    
    
    /**
     * 设置
     */
    String TEL_PATH = "tel.properties";
    
    /**
     * 最大字节数
     */
    int MAX_LENGHT = 1024;
    
    /**
     * 0000
     */
    public String ZERO4 = "0000";
    
}
